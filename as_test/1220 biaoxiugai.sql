-- ALTER TABLE `1220_1元特价正济堂` 
-- 	CHANGE COLUMN `正济堂` `huohao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `商品ID`;
-- ALTER TABLE `1220_1元特价康佰馨` 
-- 	CHANGE COLUMN `康佰馨` `huohao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `商品ID`;
-- 
-- 
-- ALTER TABLE `1220_吃喝应酬康佰馨` 
-- 	CHANGE COLUMN `康佰馨货号` `货号` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `药快到ID`;
-- ALTER TABLE `1220_吃喝应酬正济堂` 
-- 	CHANGE COLUMN `正济堂货号` `货号` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `药快到ID`;
-- 
-- 
-- ALTER TABLE `1220_1元特价正济堂` 
-- 	-- MODIFY COLUMN `序号` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL FIRST,
-- 	MODIFY COLUMN `商品ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `序号`,
-- 	ADD PRIMARY KEY(`商品ID`);
-- 
-- ALTER TABLE `1220_1元特价康佰馨` 
-- 	MODIFY COLUMN `商品ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `序号`,
-- 	ADD PRIMARY KEY(`商品ID`);


ALTER TABLE `1220_吃喝应酬康佰馨` 
	CHANGE COLUMN `药快到ID` `prod_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `子标签`,
	CHANGE COLUMN `货号` `huohao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `prod_id`;
