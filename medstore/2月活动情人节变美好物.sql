/*
北京天一同步执行，活动时间：2月11日-2月28日

情人节商品配置：
列表页打标：商品主图角标，满48减5/满88减10
商品详情页打标： 商品主图底标，满减-满48减5元/
满减-满88减10元
时间：2月12日-2月15日0dian


变美好物商品配置：
列表页打标：商品主图角标，满100减50
商品详情页打标： 商品主图底标，满减-满100减50元
时间：2月15日-2月28日

1002756822 and 1002756835 -- 情人节
1002756837 and 1002756850 -- 变美好物
*/

-- SELECT * from pm_prod_sku WHERE pharmacy_huohao = ''


SELECT * from as_test.`2bj_情人节`;
SELECT * from as_test.`2bj_好物`;

SELECT * from as_test.`2ty_情人节-中西药品`;

SELECT * from as_test.`2ty_情人节-性福生活`;

SELECT * from sm_image_link ORDER BY link_id DESC limit 500;

SELECT * from sm_image_link WHERE drugstore_id = 200 and link_status =1  ORDER BY link_id DESC limit 500;

SELECT * from am_coupon_info ORDER BY coupon_id DESC LIMIT 500;
 
SELECT * from am_code_act_info


SELECT * from pm_dir_info WHERE dir_code like '%act48%'; 
1001001819   20
1001001820   30 
-- 1002756814	tyact48-20	用券100减20
-- 1002756815	tyact48-30	用券100减30
SELECT * from pm_sku_dir WHERE dir_id = 1002756826;

SELECT * from pm_sku_dir a ,pm_sku_dir b WHERE a.sku_id = b.sku_id and a.dir_id = 1001001819 and b.dir_id =1002756814;


SELECT * from pm_sku_dir a ,pm_sku_dir b WHERE a.sku_id = b.sku_id and a.dir_id = 1001001820 and b.dir_id =1002756815;

SELECT * from pm_sku_dir a WHERE dir_id = 1001001819 and not EXISTS(SELECT * from pm_sku_dir b WHERE a.sku_id = b.sku_id and dir_id = 1002756814 );


SELECT * from pm_sku_dir a WHERE dir_id = 1001001820 and not EXISTS(SELECT * from pm_sku_dir b WHERE a.sku_id = b.sku_id and dir_id = 1002756815 );

SELECT * from pm_sku_dir WHERE dir_id = 1002756814 ;

SELECT * from pm_sku_dir WHERE dir_id = 1001001820 ;


SELECT * from pm_sku_dir WHERE dir_id = 1002756815 ;

SELECT * from pm_prod_sku ORDER BY sku_id DESC;

SELECT * from am_stat_info   GROUP BY item_id ;


SELECT * from am_item_range WHERE item_id in (1351,1401);
SELECT * from am_act_range WHERE range_id in (1587,1588,1672,1673);
SELECT * from pm_dir_info WHERE dir_id in (1001001715,1001001716,1002756778,1002756779);


-- 修改 item中满100减50的标签文字 *****
-- UPDATE am_act_item set item_name = '满100减50',item_desc = '用券满100减50元'
-- SELECT * from am_act_item
 WHERE item_name = '用券100减50';

-- UPDATE am_stat_info set  item_name = '满100减50',item_remark = '用券满100减50元'
-- SELECT * from am_stat_info 
WHERE item_name = '用券100减50' ;


-- 修改 item中满100减30的标签文字
-- UPDATE am_act_item set item_name = '满100减30',item_desc = '用券满100减30元'
-- SELECT * from am_act_item
 WHERE item_name = '用券100减30';

-- UPDATE am_stat_info set  item_name = '满100减30',item_remark = '用券满100减30元'
-- SELECT * from am_stat_info 
WHERE item_name = '用券100减30' ;

SELECT * from am_act_item  ORDER BY item_id DESC;

-- 首单免费 下单抽奖上的详情页图标 从年货节改为情人节 *******  又改了，  去掉年货节图标， 给情人节的加情人节标，好物加好物标
-- UPDATE am_stat_info set other_str1 = '' -- '{"itemImageR":"470","itemImage":"http://image.ykd365.cn/act/1902/88-10detaillogo.png"}'
-- SELECT * from am_stat_info 
WHERE item_name = '下单抽奖' ;-- GROUP BY item_id ;

-- UPDATE am_act_item set item_remark = '{"itemImageR":"470"}'  ,item_img = 'http://image.ykd365.cn/act/1902/88-10detaillogo.png'
-- SELECT * from am_act_item 
WHERE item_name = '下单抽奖' and item_status =1;


-- UPDATE am_stat_info set other_str1 =  '{"itemImageR":"470","itemImage":"http://image.ykd365.cn/act/1902/88-10detaillogo.png"}'
-- SELECT * from am_stat_info 
WHERE item_name = '满48减5' ;

-- UPDATE am_act_item set item_remark = '{"itemImageR":"470"}'  ,item_img = 'http://image.ykd365.cn/act/1902/88-10detaillogo.png'
-- SELECT * from am_act_item 
WHERE item_name = '满48减5' and item_status =1;

------------------------------------============================
-- UPDATE am_stat_info set other_str1 =   '{"itemImageR":"765","itemImage":"http://image.ykd365.cn/act/1902/100-50detaillogo.png"}'
-- SELECT * from am_stat_info 
WHERE item_name = '满100减50' ;

-- UPDATE am_act_item set item_remark = '{"itemImageR":"765"}'  ,item_img = 'http://image.ykd365.cn/act/1902/88-10detaillogo.png'
-- SELECT * from am_act_item 
WHERE item_name = '满100减50' and item_status =1;

-- 首单免费 下单抽奖上的列表页图标 从年货节改为情人节 *******   又改了，  去掉年货节图标， 给情人节的加情人节标，好物加好物标
-- UPDATE pm_label_image set  label_status=1, label_name = '情人节' ,label_url = 'http://image.ykd365.cn/act/1902/88-10listlogo.png',label_url_double = 'http://image.ykd365.cn/act/1902/88-10listlogo.png',label_update_time = NOW()
-- SELECT * from pm_label_image 
-- WHERE label_name = '年货节' ORDER BY label_id DESC;


-- 去掉之前的标  失效掉
-- UPDATE pm_label_image set  label_status=0 -- , label_name = '情人节' ,label_url = 'http://image.ykd365.cn/act/1902/88-10listlogo.png',label_url_double = 'http://image.ykd365.cn/act/1902/88-10listlogo.png',label_update_time = NOW()
-- SELECT * from pm_label_image 
WHERE label_name = '情人节' ORDER BY label_id DESC;

-- UPDATE pm_label_image set  label_status=1, label_name = '情人节' ,label_url = 'http://image.ykd365.cn/act/1902/qrj_list.png',label_url_double = 'http://image.ykd365.cn/act/1902/qrj_list.png',label_update_time = NOW()
-- SELECT * from pm_label_image 
-- WHERE label_name = '情人节' ORDER BY label_id DESC;
-- 

-- 情人节的角标
-- UPDATE pm_label_image a,pm_sku_dir b set  label_status=1,label_url = 'http://image.ykd365.cn/act/1902/qrj_list.png',label_url_double = 'http://image.ykd365.cn/act/1902/qrj_list.png',label_update_time = NOW()
-- SELECT * from pm_label_image a,pm_sku_dir b
WHERE a.sku_id = b.sku_id and b.dir_id BETWEEN  
 1002756822 and 1002756835 -- 情人节
-- 1002756837 and 1002756850 -- 变美好物
and label_name = '情人节'
 ORDER BY label_id DESC;

-- 变美好物的角标
-- UPDATE pm_label_image a,pm_sku_dir b set  label_status=1,label_url = 'http://image.ykd365.cn/act/1902/2019_bmhw_list.png',label_url_double = 'http://image.ykd365.cn/act/1902/2019_bmhw_list.png',label_update_time = NOW()
-- SELECT * from pm_label_image a,pm_sku_dir b
WHERE a.sku_id = b.sku_id and b.dir_id BETWEEN  
-- 1002756822 and 1002756835 -- 情人节
 1002756837 and 1002756850 -- 变美好物
and label_name = '情人节'
 ORDER BY label_id DESC;


-- 186  北京1月活动要延期     天一延期秒杀和多买少算  ******
-- UPDATE am_act_info set act_end_time  = '2019-02-28 23:59:59' WHERE act_id = 186;
 UPDATE am_act_item set item_status =0   WHERE item_id BETWEEN 1365 and 1373;

-- 2月满100减20 30 商品 各差5个，手动删掉就行 *****
SELECT * from pm_sku_dir a WHERE dir_id = 1001001819 and not EXISTS(SELECT * from pm_sku_dir b WHERE a.sku_id = b.sku_id and dir_id = 1002756814 );
SELECT * from pm_sku_dir a WHERE dir_id = 1001001820 and not EXISTS(SELECT * from pm_sku_dir b WHERE a.sku_id = b.sku_id and dir_id = 1002756815 );



-- 修改sm_image_link 四宫格中1元 99 的商品目录 ***** 往下有一个全体的
-- UPDATE sm_image_link set link_url = REPLACE(link_url,link_param,'1002756810') 
-- SELECT * from sm_image_link 
WHERE drugstore_id = 200 and link_name = '1元必抢' and link_status =1 ; 

-- UPDATE sm_image_link set link_url = REPLACE(link_url,link_param,'1002756811') 
-- SELECT * from sm_image_link 
WHERE drugstore_id = 200 and link_name = '9.9超值' and link_status =1 ; 





-- UPDATE pm_label_image set label_status = 0
-- SELECT * from pm_label_image 
WHERE label_status = 1;

SELECT * from sm_image_link ORDER BY link_id DESC;


-- 轮播和通栏 情人节和变美好物
--   tl
-- INSERT INTO `medstore`.`sm_image_link` (`drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`)
--  VALUES ('5835', '1620', '1', 'http://image.ykd365.cn/icon/act/ashacker/181201/12_tl.gif', 'http://store.ykd365.com/medstore/actUserpage/OneYuan_module_1811?dirId=1001001305', 'web2', '入冬滋补月', '1', '1', '2018-12-08 00:01:00', '2018-12-06 18:30:54', '{\"aspect_ratio\":260,\"image_seat\":\"button\",\"having_line\":0,\"image_seat\":1}', '2018-12-08 00:00:00', '2018-12-31 23:59:59', 'activity', '1');
SELECT a.pharmacy_id
,1 `seq_num`
,'http://image.ykd365.cn/act/1902/88-10tl.jpg' `image_url`
,null `link_url`
,'dirSku' `link_type`
,'情人节' `link_name`
,a.dir_id `link_param`
,1 `link_status`
,NOW() `link_update_time`
,NOW() `link_create_time`
,'{"aspect_ratio":226,"image_seat":"button","having_line":0,"image_seat":1}' `link_remark`
,'2018-02-12 00:00:00' `link_start_time`
,'2019-02-14 23:59:59' `link_end_time`
,'activity' `link_view`,'1' `link_version`
  from  pm_dir_info a 
WHERE a.dir_id BETWEEN  1002756822 and 1002756835 ; -- 情人节
-- dir_id BETWEEN  1002756837 and 1002756850 -- 变美好物
;

--  通栏 
-- INSERT INTO `medstore`.`sm_image_link` (`drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`)
--  VALUES ('5835', '1620', '1', 'http://image.ykd365.cn/icon/act/ashacker/181201/12_tl.gif', 'http://store.ykd365.com/medstore/actUserpage/OneYuan_module_1811?dirId=1001001305', 'web2', '入冬滋补月', '1', '1', '2018-12-08 00:01:00', '2018-12-06 18:30:54', '{\"aspect_ratio\":260,\"image_seat\":\"button\",\"having_line\":0,\"image_seat\":1}', '2018-12-08 00:00:00', '2018-12-31 23:59:59', 'activity', '1');
SELECT a.pharmacy_id,1 `seq_num`
,'http://image.ykd365.cn/act/1902/100-50tl.jpg' `image_url`
,null `link_url`
,'dirSku' `link_type`
,'变美好物' `link_name`
,a.dir_id `link_param`
,1 `link_status`
,NOW() `link_update_time`
,NOW() `link_create_time`
,'{"aspect_ratio":226,"image_seat":"button","having_line":0,"image_seat":1}' `link_remark`
,'2018-02-15 00:00:00' `link_start_time`
,'2019-02-28 23:59:59' `link_end_time` 
,'activity' `link_view`,'1' `link_version`
  from  pm_dir_info a 
WHERE -- a.dir_id BETWEEN  1002756822 and 1002756835 ; -- 情人节
  dir_id BETWEEN  1002756837 and 1002756850 -- 变美好物
;

 -- lunbo
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`,link_version) 
SELECT -- a.*,
a.pharmacy_id
,2 as seq_num
 ,'http://image.ykd365.cn/act/1902/88-10banner.jpg' as image_url
,'' as link_url
,'category' as link_type
,'情人节' as link_name
,a.dir_id as link_param,'1' as link_status
,NOW() as link_update_time,NOW() as link_create_time
,'' as link_remark
,'2018-02-12 00:00:00' `link_start_time`
,'2019-02-14 23:59:59' `link_end_time`
,'' as  `link_view`
,1 as link_version
 from  pm_dir_info a 
WHERE a.dir_id BETWEEN  1002756822 and 1002756835 ; -- 情人节
-- dir_id BETWEEN  1002756837 and 1002756850 -- 变美好物
;


 
--  lunbo
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`,link_version) 
SELECT -- a.*,
a.pharmacy_id
,1 as seq_num
 ,'http://image.ykd365.cn/act/1902/100-50banner.jpg' as image_url
,'' as link_url
,'category' as link_type
,'变美好物' as link_name
,a.dir_id as link_param,'1' as link_status
,NOW() as link_update_time,NOW() as link_create_time
,'' as link_remark
,'2018-02-15 00:00:00' `link_start_time`
,'2019-02-28 23:59:59' `link_end_time`
,'' as  `link_view`
,1 as link_version
 from  pm_dir_info a 
WHERE   dir_id BETWEEN  1002756837 and 1002756850 -- 变美好物


--   2月的 满100减 活动那个 北京
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`)
--  VALUES ('5886', '1611', '1', 'http://image.ykd365.cn/newFirstPage/lb/bj_100-30lb.jpg', NULL, 'category', '满100减30专区', '1001001959', '1', '2019-01-08 18:33:33', '2019-01-08 18:33:33', '', '2019-01-08 00:00:00', '2019-02-11 23:59:59', NULL, '1');

SELECT  `drugstore_id`
,4 `seq_num`
,'http://image.ykd365.cn/act/1902/bj_100-30lb2.png' `image_url`, `link_url`, `link_type`, `link_name`, `link_param`
,0 `link_status`
,NOW() `link_update_time`
,NOW() `link_create_time`
,'1902' `link_remark`
,'2018-02-12 00:00:00' `link_start_time`
,'2019-02-28 23:59:59' `link_end_time`, `link_view`, `link_version`
 from sm_image_link WHERE link_name ='满100减30专区' and image_url = 'http://image.ykd365.cn/newFirstPage/lb/bj_100-30lb.jpg' and link_status =1;


-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`)
--  VALUES ('5886', '1611', '1', 'http://image.ykd365.cn/newFirstPage/lb/bj_100-30lb.jpg', NULL, 'category', '满100减30专区', '1001001959', '1', '2019-01-08 18:33:33', '2019-01-08 18:33:33', '', '2019-01-08 00:00:00', '2019-02-11 23:59:59', NULL, '1');

SELECT  `drugstore_id`
,4 `seq_num`
,'http://image.ykd365.cn/act/1902/bj_100-50lb2.png' `image_url`, `link_url`, `link_type`, `link_name`, `link_param`
,0 `link_status`
,NOW() `link_update_time`
,NOW() `link_create_time`
,'1902' `link_remark`
,'2018-02-12 00:00:00' `link_start_time`
,'2019-02-28 23:59:59' `link_end_time`, `link_view`, `link_version`
 from sm_image_link WHERE link_name ='满100减50专区' and image_url = 'http://image.ykd365.cn/newFirstPage/lb/bj_100-50lb.jpg' and link_status =1;



-- UPDATE sm_image_link set seq_num = 4
-- SELECT * from sm_image_link
 WHERE link_name ='满100减50专区';

-- UPDATE sm_image_link set seq_num = 4
-- SELECT * from sm_image_link
 WHERE link_name ='满100减30专区';

-- UPDATE sm_image_link set seq_num = 2
-- SELECT * from sm_image_link
 WHERE link_name ='情人节'
;

-- UPDATE sm_image_link set link_status= 1,link_end_time = '2018-02-14 23:59:59'
-- SELECT * from sm_image_link
 WHERE link_name ='情人节'
and link_view = '';

-- UPDATE sm_image_link set seq_num = 3
-- SELECT * from sm_image_link
 WHERE link_name ='变美好物';


-- UPDATE sm_image_link set seq_num = 1
-- SELECT * from sm_image_link
 WHERE link_name ='开工券包';




-- 将北京的情人节商品导入数据表 1
--   (  `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `unit`, `huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- INSERT INTO `as_test`.`2_act_temp` ( `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `fee`, `price`, `start_time`, `end_time` )
SELECT a.xh `xh`
,'情人节' `act_name`
,'满48减5 满88减10' `act_remark`
, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, sku_json  , `pharmacy_huohao`
,a.`市场价`*100 `fee`
,a.`销售价`*100 `price`
,'2018-02-12 00:00:00' `start_time`
,'2019-02-14 23:59:59' `end_time`  
 from as_test.`2bj_情人节` a, pm_prod_sku b
WHERE 
-- a.康佰馨 = b.pharmacy_huohao and   b.drugstore_id  = 1620
 ((a.康佰馨 = b.pharmacy_huohao and   b.drugstore_id  in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) and a.康佰馨!='')
or  (a.正济堂 = b.pharmacy_huohao  and b.drugstore_id    in  (1610,1611,1612) and a.正济堂!='' ))
ORDER BY a.xh
; 




-- 将北京的变美好物节商品导入数据表 2
--   (  `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `unit`, `huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- INSERT INTO `as_test`.`2_act_temp` ( `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `fee`, `price`, `start_time`, `end_time` )
SELECT a.xh `xh`
,'变美好物' `act_name`
,'满100减50' `act_remark`
, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, sku_json  , `pharmacy_huohao`
,a.`市场价`*100 `fee`
,'' `price`
,'2018-02-15 00:00:00' `start_time`
,'2019-02-28 23:59:59' `end_time`   from as_test.`2bj_好物` a, pm_prod_sku b
WHERE 
--   a.康佰馨 = b.pharmacy_huohao and   b.drugstore_id  = 1620
 ((a.康佰馨 = b.pharmacy_huohao and   b.drugstore_id  in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) and a.康佰馨!='')
or  (a.正济堂 = b.pharmacy_huohao  and b.drugstore_id    in  (1610,1611,1612) and a.正济堂!='' ))
ORDER BY a.xh
; 


-- 将天一的情人节商品导入数据表 3
--   (  `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `unit`, `huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- INSERT INTO `as_test`.`2_act_temp` ( `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `fee`, `price`, `start_time`, `end_time` )
SELECT a.序号 `xh`
,'情人节' `act_name`
,'满48减5 满88减10' `act_remark`
 , a.`sku_id`
 , `prod_id`, `drugstore_id`, `prod_name`, sku_json  , `pharmacy_huohao`
,a.零售价*100 `fee`
,''  `price`
,'2018-02-12 00:00:00' `start_time`
,'2019-02-14 23:59:59' `end_time`   
from as_test.`2ty_情人节-中西药品` a, pm_prod_sku b
WHERE 
  a.货号 = b.pharmacy_huohao and   b.drugstore_id  = 200
--  and a.`sku_id` != b.`sku_id`
-- ORDER BY a.序号
; 

-- 将天一的情人节商品导入数据表 4
--   (  `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `unit`, `huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- INSERT INTO `as_test`.`2_act_temp` ( `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `fee`, `price`, `start_time`, `end_time` )
SELECT a.序号 `xh`
,'情人节' `act_name`
,'满48减5 满88减10' `act_remark`
 , a.`sku_id`
 , `prod_id`, `drugstore_id`, `prod_name`, sku_json  , `pharmacy_huohao`
,a.零售价*100 `fee`
,''  `price`
,'2018-02-12 00:00:00' `start_time`
,'2019-02-14 23:59:59' `end_time`   
from as_test.`2ty_情人节-性福生活` a, pm_prod_sku b
WHERE 
  a.货号 = b.pharmacy_huohao and   b.drugstore_id  = 200
ORDER BY a.序号
;


-- 将天一的变美好物节商品导入数据表 5
--   (  `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `unit`, `huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- INSERT INTO `as_test`.`2_act_temp` ( `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `fee`, `price`, `start_time`, `end_time` )
SELECT a.序号 `xh`
,'变美好物' `act_name`
,'满100减50' `act_remark`
, a.`sku_id`, `prod_id`, `drugstore_id`, `prod_name`, sku_json  , `pharmacy_huohao`
,a.零售价*100 `fee`
,'' `price`
,'2018-02-15 00:00:00' `start_time`
,'2019-02-28 23:59:59' `end_time`   from as_test.`2ty_变美好物（营养保健）` a, pm_prod_sku b
WHERE 
  a.货号 = b.pharmacy_huohao and   b.drugstore_id  = 200
ORDER BY a.序号
; 




-- 将天一的1元商品导入数据表 6
--   (  `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `unit`, `huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- INSERT INTO `as_test`.`2_act_temp` ( `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `fee`, `price`, `start_time`, `end_time` ,old_huohao,`limit_stock`, `limit_stock_day`)
SELECT a.序号 `xh`
,'1元必抢' `act_name`
,'1元' `act_remark`
, a.`sku_id`, `prod_id`, `drugstore_id`, `prod_name`, sku_json  
,CONCAT('1902YYY',pharmacy_huohao) `pharmacy_huohao`
,'' `fee`
,'100' `price`
,'2018-02-11 00:00:00' `start_time`
,'2019-02-28 23:59:59' `end_time` 
,pharmacy_huohao  as old_huohao
,a.`每天限量` `limit_stock`,a.`月投入量` `limit_stock_day`
  from as_test.`2ty_1元专区` a, pm_prod_sku b
WHERE 
  a.货号 = b.pharmacy_huohao and   b.drugstore_id  = 200
-- ORDER BY a.序号
; 



-- 将天一的99元商品导入数据表 7
--   (  `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `unit`, `huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- INSERT INTO `as_test`.`2_act_temp` ( `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `fee`, `price`, `start_time`, `end_time`,old_huohao )
SELECT a.序号 `xh`
,'9.9元超值' `act_name`
,'9.9元' `act_remark`
, a.`sku_id`, `prod_id`, `drugstore_id`, `prod_name`, sku_json  
,CONCAT('1902YJY',pharmacy_huohao) `pharmacy_huohao`
,'' `fee`
,'990' `price`
,'2018-02-11 00:00:00' `start_time`
,'2019-02-28 23:59:59' `end_time` 
,pharmacy_huohao  as old_huohao
  from as_test.`2ty_9.9专区` a, pm_prod_sku b
WHERE 
  a.货号 = b.pharmacy_huohao and   b.drugstore_id  = 200
-- ORDER BY a.序号
; 

-- 天一1元99的sku_id更新成新的
-- UPDATE as_test.2_act_temp a ,pm_prod_sku b set a.sku_id = b.sku_id 
-- SELECT * from as_test.2_act_temp a ,pm_prod_sku b 
WHERE 
a.pharmacy_huohao = b.pharmacy_huohao
and act_remark in ('1元','9.9元')
;




-- 将天一的满100减20商品导入数据表 8
--   (  `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `unit`, `huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- INSERT INTO `as_test`.`2_act_temp` ( `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `fee`, `price`, `start_time`, `end_time`,old_huohao )
SELECT a.序号 `xh`
,'满100减20' `act_name`
,'满100减20' `act_remark`
, a.`sku_id`, `prod_id`, `drugstore_id`, `prod_name`, sku_json  
,pharmacy_huohao  `pharmacy_huohao`
,'' `fee`
,'' `price`
,'2018-02-12 00:00:00' `start_time`
,'2019-02-28 23:59:59' `end_time` 
,'' old_huohao
  from as_test.`2ty_满100减20` a, pm_prod_sku b
WHERE 
  a.货号 = b.pharmacy_huohao and   b.drugstore_id  = 200
-- ORDER BY a.序号
; 


-- 将天一的满100减30商品导入数据表 9
--   (  `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `unit`, `huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- INSERT INTO `as_test`.`2_act_temp` ( `xh`, `act_name`, `act_remark`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `fee`, `price`, `start_time`, `end_time`,old_huohao )
SELECT a.序号 `xh`
,'满100减30' `act_name`
,'满100减30' `act_remark`
, a.`sku_id`, `prod_id`, `drugstore_id`, `prod_name`, sku_json  
,pharmacy_huohao  `pharmacy_huohao`
,'' `fee`
,'' `price`
,'2018-02-12 00:00:00' `start_time`
,'2019-02-28 23:59:59' `end_time` 
,'' old_huohao
  from as_test.`2ty_满100减30` a, pm_prod_sku b
WHERE 
  a.货号 = b.pharmacy_huohao and   b.drugstore_id  = 200
-- ORDER BY a.序号
; 


-- 天一1元99的sku_id更新成新的
-- UPDATE as_test.2_act_temp a ,pm_prod_sku b set a.sku_id = b.sku_id 
-- SELECT * from as_test.2_act_temp a ,pm_prod_sku b 
WHERE 
a.pharmacy_huohao = b.pharmacy_huohao
and act_remark in ('1元','9.9元')
;









-- 创建1元商品和99元商品秒殺 天一
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
 SELECT      i.prod_id
,'1' as sku_status
-- ,a.`特价`*100 as sku_price
,  sku_price
,  sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,CASE 
WHEN a.act_remark = '1元' THEN '2月 1Y ty' 
WHEN a.act_remark = '9.9元' THEN '2月 9Y ty'
WHEN a.act_remark = '秒杀' THEN a.act_remark
ELSE '12y' END as    sku_remark,i.sku_logistics,i.prod_name
,CASE 
WHEN a.act_remark = '1元' THEN CONCAT('1902YYY',i.pharmacy_huohao) 
WHEN a.act_remark = '9.9元' THEN CONCAT('1902YJY',i.pharmacy_huohao)
WHEN a.act_remark = '秒杀' THEN CONCAT('1902YMS',i.pharmacy_huohao)
ELSE i.pharmacy_huohao END as pharmacy_huohao,'1',i.sku_json,null as sku_attr
,CASE 
WHEN a.act_remark = '1元' THEN CONCAT('1902YYY','') 
WHEN a.act_remark = '9.9元' THEN CONCAT('1902YJY','')
WHEN a.act_remark = '秒杀' THEN CONCAT('1902YMS','')
ELSE i.pharmacy_huohao END as sku_img,'' as sku_sum
--   ,a.*,i.* -- ,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
-- SELECT *
from
as_test.2_act_temp a 
LEFT JOIN pm_prod_sku i on a.old_huohao = i.pharmacy_huohao  and i.drugstore_id =200 -- and a.act_remark in ('1元','9.9元')
-- LEFT JOIN pm_sku_sale c on i.sku_id = c.sku_id and c.sale_status =1
-- ORDER BY a.`全部序号`;
WHERE a.act_remark in ('1元','9.9元')
;

SELECT `sku_id`, `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sort`, `sku_rank`, `sku_type` FROM pm_prod_sku ORDER BY sku_id DESC;





--  pm_sku_sale build
-- 1元和99元商品的价格配置
-- INSERT INTO `medstore`.`pm_sku_sale` (  `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
--  VALUES ('364577', '25506122', '950', '100', '2018-11-06 20:00:00', '2018-11-06 21:59:59', '0', '2018-09-15 23:40:26', '2018-09-15 23:40:26', '9月 秒杀 ty');
SELECT `sku_id`
,sku_fee `sale_fee`
,100 `sale_price`

,'2018-02-12 00:00:00' `sale_start_time`
,'2019-02-28 23:59:59'  `sale_end_time`
,1 `sale_status`
,NOW() `sale_create_time`
,NOW() `sale_update_time`
,sku_remark `sale_remark` 
from pm_prod_sku
WHERE sku_remark like '2月 1Y%'
UNION all
SELECT `sku_id`
,sku_fee `sale_fee`
,990 `sale_price`

,'2018-02-12 00:00:00' `sale_start_time`
,'2019-02-28 23:59:59'  `sale_end_time`
,1 `sale_status`
,NOW() `sale_create_time`
,NOW() `sale_update_time`
,sku_remark `sale_remark` 
from pm_prod_sku
WHERE sku_remark like '2月 9Y%';



SELECT * from am_stages_sale -- WHERE sg_id = 25274 
ORDER BY  sg_id DESC;
 
SELECT * from am_stages_sale_detail -- WHERE sg_id = 25274  
ORDER BY sg_detail_id DESC;



-- 配置限购，搞出来一票range
-- INSERT INTO `medstore`.`am_act_range` (  `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) 
-- VALUES ('1388', 'category', '1001000651', '1', '分类', '95折', '2018-10-23 16:23:38', '2018-10-23 16:21:28');
SELECT 'category',dir_id, '1', '分类', dir_name,NOW(),now() -- ,a.*
 from pm_dir_info a
WHERE dir_id BETWEEN 1002756812 and  1002756812  -- 1元
or dir_id BETWEEN 1002756813 and 1002756813  -- 9.9元
or dir_id BETWEEN 1002756822 and 1002756835 -- 情人节
or dir_id BETWEEN 1002756837 and 1002756850 -- 变美好物
;



-- -- 关联item和目录
 -- INSERT INTO `medstore`.`am_item_range` ( `range_id`, `item_id`)
 -- VALUES ('761', '729', '688');
SELECT  -- *,
 b.range_id
 , c.item_id
  ,b.range_name,b.range_remark,a.pharmacy_id,c.act_id,c.item_name,c.item_desc,c.item_remark,a.dir_code,a.dir_name,a.dir_remark ,  b.*,c.*,a.*
from     
 pm_dir_info a  
  LEFT JOIN  am_act_range b on b.range_value = a.dir_id
  LEFT JOIN am_act_item c on  b.range_remark = c.item_remark and  IF(a.pharmacy_id=200,200,162) = c.pharmacy_id-- and c.pharmacy_id = 200 and a.pharmacy_id=200-- IF(a.pharmacy_id=200,200,162) = c.pharmacy_id
WHERE  (range_value BETWEEN 1002756812 and  1002756812  -- 1元
or range_value BETWEEN  1002756813 and 1002756813  -- 9.9元
or range_value BETWEEN  1002756822 and 1002756835 -- 情人节
or range_value BETWEEN 1002756837 and 1002756850 -- 变美好物
)   
and c.act_id BETWEEN 188 and 190
;

SELECT * FROM am_act_info ORDER BY `act_id` DESC ;
SELECT * FROM am_act_item --  WHERE act_id in (185)
 ORDER BY `item_id` DESC; -- WHERE act_id = 160;
SELECT * FROM am_act_range ORDER BY range_id desc;
 
SELECT * FROM am_item_range 
 
ORDER BY `link_id` DESC ;  -- ---------------------
 
-- SELECT * from pm_dir_info WHERE dir_id =  1000035993

SELECT * FROM am_item_details ORDER BY details_id desc;
-- INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('916', '1', '1413', 'cut', 'number', '5000', '2019-01-28 18:33:21', '2019-01-30 16:06:35', '满100减50', '满100减50');
-- INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('915', '1', '1412', 'cut', 'number', '5000', '2019-01-28 18:33:21', '2019-01-30 16:06:39', '满100减50', '满100减50');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('874', '916', '11');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('875', '915', '11');

 
  SELECT * from am_act_rule ORDER BY rule_id desc;
 
  SELECT * from am_act_rule WHERE rule_value in ('>=4800','>=8800','>=10000') ORDER BY rule_id desc;
 
SELECT * from sm_config;

-- 
-- SELECT * from pm_dir_info WHERE dir_name= '首单免费'; pharmacy_id =200;

SELECT * FROM am_stock_limit ORDER BY as_id DESC ;
-- 1yuan  9.9 库存
 -- INSERT INTO `medstore`.`am_stock_limit` (`item_id`, `quota_id`, `sku_id`, `sku_total`, `as_remark`,max_total) 
SELECT     
  d.item_id,d.quota_id,a.sku_id,10, CONCAT(e.drugstore_id,'-',e.prod_name,'-2月活动')  
,10
-- ,e.*
-- ,e.*
-- ,h.* 
-- ,h.* ,g.*,f.* ,e.*,c.*,d.*,b.*
from pm_sku_dir a 
LEFT JOIN am_act_range b on a.dir_id = b.range_value  -- and a.dir_id BETWEEN  1001001518    and 1001001561
 LEFT JOIN am_item_range c on b.range_id  = c.range_id  
			  
LEFT JOIN am_quota_info d on c.item_id = d.item_id -- and d.quota_id BETWEEN 564 and 627
LEFT JOIN pm_prod_sku e on e.sku_id = a.sku_id

LEFT JOIN am_act_item f on f.item_id = c.item_id
LEFT JOIN am_act_range g on g.range_id = c.range_id
 LEFT JOIN pm_dir_info h on h.dir_id = g.range_value

WHERE  
   ( a.dir_id BETWEEN 1002756812 and  1002756812  -- 1元
or  a.dir_id BETWEEN 1002756813 and 1002756813  -- 9.9元
 )
-- (a.dir_id BETWEEN  1001001518  and 1001001531 or a.`dir_id` BETWEEN 1001001548 and 1001001561) -- (a.dir_id BETWEEN  1001000509  and 1001000522 or a.`dir_id` BETWEEN 1001000569 AND  1001000582)
-- a.dir_id BETWEEN  1001001578 and 1001001641
and c.item_id BETWEEN 1408 and  1409  ;


UPDATE am_stock_limit cc join (
SELECT a.*
-- ,IF(SUM(st.`sku_amount`),SUM(st.`sku_amount`),0) ,a.mtkc+IF(SUM(st.`sku_amount`),SUM(st.`sku_amount`),0) as sum_sku_amount
  ,c.`as_id` ,c.sku_total,c.max_total
-- ,a.*
-- ,SUM(st.`sku_amount`)
-- , st.`sku_id` -- , a.*,b.drugstore_id,b.prod_name,b.prod_id,b.sku_id,c.*,st.* 
-- SELECT *
    from as_test.2_act_temp  a  
LEFT JOIN pm_prod_sku b on a.pharmacy_huohao   = b.pharmacy_huohao and b.drugstore_id =200 
-- and b.drugstore_id  =1620 --  in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) 
LEFT JOIN am_stock_limit c on b.sku_id = c.sku_id
WHERE a.act_remark  in ('1元')
--     LEFT JOIN `am_sku_stat` st on b.`sku_id`  = st.`sku_id` 
--    WHERE c.`sku_id` = 25510186
-- GROUP BY  st.`sku_id`
) ccc on cc.`as_id`  = ccc.as_id   set cc.sku_total = ccc.limit_stock   ,cc.max_total = ccc.limit_stock_day 
 ;

--- ************************
-- 
-- UPDATE as_test.2_act_temp set  end_time = '2019-02-14 23:59:59' WHERE end_time = '2019-02-15 23:59:59'
-- 
SELECT * from as_test.2_act_temp WHERE end_time = '2019-02-15 23:59:59'

SELECT * from pm_dir_info WHERE dir_code like '%act48%';
-- and pharmacy_id = 200;

SELECT * from pm_dir_info WHERE dir_code like '%act46%'
and pharmacy_id = 200;


-- pm_dir_info build
-- 创建活动目录 zsc  dir_code dir_name img  dir_all_name
-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'48') AS dir_code 
, CONCAT('1902月活动情人节和变美好物--',pharmacy_id) AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum
, 46 AS dir_num,'2' AS dir_level
,  '', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'' dir_remark,CONCAT(dir_all_name	,'--2019-2月活动情人节和变美好物') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 1000034627 AND 1000034639  or dir_id = 1000034601;


-- 分会场 	 情人节
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(pharmacy_id,'act4804') AS dir_code 
, '情人节' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 2 AS dir_num,'3' AS dir_level
, ''
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,'情人节' AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1002756795 and 1002756808; -- ACT48 活动目录

-- 分会场 	 变美好物
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(pharmacy_id,'act4805') AS dir_code 
, '变美好物' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 2 AS dir_num,'3' AS dir_level
, ''
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,'变美好物' AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1002756795 and 1002756808; -- ACT48 活动目录


-- INSERT INTO `medstore`.`pm_dir_info` ( `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`)
-- VALUES ('1001001717', 'ykd20001001act46', '1901月活动--200', 'dir', '1', NULL, '100', '46', '2', '', '1000034601', '2018-12-21 14:18:47', '2018-12-21 14:18:47', '', '--2019-1月活动', NULL, '200', '', NULL);
-- SELECT REPLACE(dir_code,'act46','act48') `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`

SELECT * from pm_sku_dir ORDER BY link_id desc limit 1000;

SELECT * from as_test.2_act_temp a ; -- 2971


-- 和活动关联
-- INSERT INTO   pm_sku_dir ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
--  VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');
SELECT b.dir_id,a.sku_id ,b.dir_code,a.xh,NOW() update_time
-- , a.* 
from as_test.2_act_temp a,pm_dir_info b
WHERE a.act_remark = b.dir_all_name  -- 和活动关联
and b.dir_code like '%act48%'
and ((a.drugstore_id = b.pharmacy_id    ) or (a.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633,1610,1611,1612) and b.pharmacy_id =162))  

and not EXISTS(SELECT * from pm_sku_dir c WHERE a.sku_id =  c.sku_id and  b.dir_id = c.dir_id)
;







-- INSERT INTO `medstore`.`pm_label_image` ( `label_name`, `label_url`, `label_url_double`, `label_type`, `label_status`, `label_create_time`, `label_update_time`, `pharmacy_id`, `label_flag`, `sku_id`) 
SELECT '年货节' `label_name`
,'http://image.ykd365.cn/act/190101/lb_icon.png' `label_url`
,'http://image.ykd365.cn/act/190101/lb_icon.png' `label_url_double`
,1 `label_type`
,1 `label_status`
,NOW() `label_create_time`
,NOW() `label_update_time`
,s.drugstore_id `pharmacy_id`
,0 `label_flag`
, sd.`sku_id`
 from pm_sku_dir sd,pm_prod_sku s
 WHERE
s.sku_id = sd.sku_id and 
sd.dir_id BETWEEN 1001001988 and 1001002257;





== ***************************************  延期北京的四宫格和天一的秒杀  天一的1元和9.9的目录已经在上面修改过了，这里不用改
-- UPDATE `sm_image_link` set  `link_end_time`  = '2019-02-28 23:59:59',link_status=1
-- SELECT * FROM `sm_image_link` 
WHERE  ((link_view = 'essence'  and link_name in ('1元必抢','9.9超值','多买少算','限时抢购')) or (link_view = 'all_essence' and link_name in ('活动必抢')))
and link_version=1 and drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633,1610,1611,1612,200);
 
 


-- UPDATE pm_sku_sale set sale_end_time =   '2019-02-28 23:59:59'
-- SELECT * from pm_sku_sale  -- ORDER BY sale_id desc
WHERE   sale_end_time = '2019-02-11 23:59:59'   
and sale_remark like '1月%'
 and `sale_remark` not like '%ty%'  and `sale_remark` not like '%fj%';



-- update `medstore`.`lm_tem_instance` set `invalid_time`='2028-01-01 23:59:59' where  tem_id in (4,6) -- `tem_ins_id`=37;
SELECT * from lm_tem_instance WHERE tem_id  in (4,6);




CREATE TEMPORARY TABLE tmp_mstime (     
stime VARCHAR(50) NOT NULL
) ;   
 
-- DELETE from  tmp_mstime
INSERT INTO tmp_mstime (stime)
VALUES  
('02-12'),
('02-13'),
('02-14'),
('02-15'),
('02-16'),
('02-17'),
('02-18'),
('02-19'),
('02-20'),
('02-21'),
('02-22'),
('02-23'),
('02-24'),
('02-25'),
('02-26'),
('02-27'),
('02-28')


SELECT * from tmp_mstime;

SELECT * from am_stages_sale ORDER BY sg_id DESC;
 

-- INSERT INTO `medstore`.`am_stages_sale` ( `pharmacy_id`, `sg_title`, `sg_status`, `sg_start_time`, `sg_end_time`, `quota_id`, `act_id`, `item_id`, `sg_create_time`, `sg_update_time`, `sg_json`, `sg_remark`) 
SELECT  `pharmacy_id`, `sg_title`, `sg_status`
,REPLACE(REPLACE(sg_start_time,'11-30',t.stime),'2018','2019')  `sg_start_time`
,REPLACE(REPLACE(sg_end_time,'11-30',t.stime),'2018','2019')  `sg_end_time`
,835  `quota_id`
,186 `act_id`
,1374 `item_id`
,now() `sg_create_time`
,now() `sg_update_time`, `sg_json`, `sg_remark`

from am_stages_sale a ,tmp_mstime t  
WHERE `sg_start_time` BETWEEN  '2018-11-30 00:00:00' and '2018-11-30 23:59:58' and `pharmacy_id`    in (200,1610,1611,1612,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
ORDER BY  sg_id DESC;



-- 新版秒杀需要配置的数据 新表
-- INSERT INTO `medstore`.`am_stages_sale_detail` (  `sg_id`, `act_id`, `item_id`, `quota_id`, `pharmacy_id`, `dir_id`, `sg_detail_create_time`, `sg_detail_update_time`, `sg_detail_remark`, `sg_detail_flag`, `sg_detail_type`)
--  VALUES ('23', '12', '163', '691', '236', '1620', '1000036249', '2018-08-13 10:20:33', '2018-08-13 10:20:33', '1', '0', '0');
SELECT  a.`sg_id`, a.`act_id`, a.`item_id`, a.`quota_id`, a.`pharmacy_id`
,d.`dir_id`  `dir_id`
,NOW() `sg_detail_create_time`
,NOW() `sg_detail_update_time`
,'1902-秒杀-' `sg_detail_remark`
,0 `sg_detail_flag`
, 0`sg_detail_type`
--  ,a.*,d.*
from am_stages_sale a LEFT JOIN  `pm_dir_info` d on a.sg_title =  SUBSTR(d.`dir_name`, 1,5) -- and d.dir_code like '%act44%' 
and a.`pharmacy_id` = d.`pharmacy_id` -- am_item_range b on a.item_id = b.item_id
and d.dir_id BETWEEN  1001001829 and 1001001892

-- (d.dir_id BETWEEN 1001000966 and 1001000969 or d.dir_id BETWEEN 1001001578 and 1001001637)
-- LEFT JOIN am_act_range c on b.range_id = c.range_id
WHERE act_id =186
 
AND  a.sg_start_time BETWEEN '2019-02-12' and  '2019-03-01'  -- a.`sg_id` >= 29615;





SELECT * from pm_sku_sale ORDER BY sale_id DESC;
 
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
--  VALUES ('397737', '590098', '3580', '2890', '2018-11-30 20:00:00', '2018-11-30 21:59:59', '0', '2018-10-31 17:17:58', '2018-10-31 17:17:58', '11月 秒杀 bj ');
select `sku_id`, `sale_fee`, `sale_price`
,REPLACE(sale_start_time,'02-11',t.stime)  `sale_start_time`
,REPLACE(sale_end_time,'02-11',t.stime)  `sale_end_time`, `sale_status`
, `sale_create_time`
, `sale_update_time`
,'11月 秒杀 bj jia' `sale_remark`
-- ,SS.*
 from pm_sku_sale ss    ,tmp_mstime t  
WHERE `sale_start_time` BETWEEN  '2019-02-11 00:00:00' and '2019-02-11 23:59:58' -- and `pharmacy_id`  in (200,1610,1611,1612,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
and sale_remark not like '%fj%'

ORDER BY sku_id,t.stime desc;

 
SELECT * from sm_image_link ORDER BY link_id DESC