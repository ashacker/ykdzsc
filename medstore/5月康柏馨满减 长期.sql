-- `as_test`.`517_kbx_58-8商品明细` (`中类`, `商品编码`, `名称`, `规格`, `厂家`, `成本均价`, `零售均价`, `毛利`, `毛利率`, `满减形式`) 
-- VALUES ('感冒咳嗽药', '930144', '蓝芩口服液', '10ML*12支', '扬子江药业集团有限公司', '31.4333333333333', '38', '6.56666666666667', '0.17280701754386', '满58-8');
-- 
-- 
--  `as_test`.`517_kbx_60-20商品明细` (`中类`, `商品编码`, `名称`, `规格`, `厂家`, `成本均价`, `零售均价`, `毛利`, `毛利率`, `满减形式`) 
-- VALUES ('外用药', '300027', '开塞露', '20ml*2支', '北京麦迪海药业有限责任公司', '1.3', '2.9', '1.6', '0.551724137931034', '满60-20');
-- 
按照提供给的那个表格里的商品配置阶梯。满58减8满116减16满232减32;满60减20满120减40满240减80

标签文字“满58减8” 描述“满58元减8元/满116元减16元”
-- 创建临时表
-- CREATE TEMPORARY TABLE tmp_yaodian14 (     
-- ydid VARCHAR(10) NOT NULL    
-- ) ;    
-- INSERT INTO tmp_yaodian14 (ydid)
-- VALUES ('1610'),('1611'),('1612'),('1634'),('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633');
-- SELECT * from tmp_yaodian14;
CREATE TEMPORARY TABLE tmp_kbx (     
ydid VARCHAR(50) NOT NULL
) ;   
-- CREATE TEMPORARY TABLE tmp_kbx(ydid VARCHAR(50) NOT NULL); 

    
INSERT INTO tmp_kbx (ydid)
VALUES ('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633');
SELECT * from tmp_kbx;
-- CREATE TEMPORARY TABLE tmp_zjt (     
-- ydid VARCHAR(10) NOT NULL    
-- );     
-- INSERT INTO tmp_zjt (ydid)
-- VALUES ('1610'),('1611'),('1612'),('1634');
-- SELECT * from tmp_zjt;
-- 

SELECT * from pm_dir_info 
 WHERE dir_code like '%act37%'
ORDER BY dir_id DESC;

-- 配置康柏馨满 58-8目录
-- INSERT INTO `medstore`.`pm_dir_info` ( `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
-- 1000037128	200act358z	第二件8折	dir	1		37	2	1			2018-05-16 21:44:22	2018-05-09 17:57:08				200		
-- VALUES ('1000037248', '200act35secondxz', '第二件*折', 'dir', '1', NULL, '21', '16', '3', '', '1000037132', '2018-05-16 21:44:22', '2018-05-14 17:52:34', '', '55月活动--200>>第二件*折', NULL, '200', NULL, NULL);
SELECT CONCAT(ydid,'act3758-8') as `dir_code`
,'满58-8' as `dir_name`
,'sdir' `dir_type`
,'1' `dir_status`
,null `dir_revalue`
,1501 `prod_sum`
,1 `dir_num`
,4 `dir_level`
,null `dir_img`
,null `parent_dir_id`
,NOW() `dir_update_time`
,NOW()  `dir_create_time`
,CONCAT(ydid,'康柏馨10 满58-8 长期') `dir_remark`
,null `dir_all_name`
,null `category_id`
,ydid as  `pharmacy_id`
,null `dir_gotocata`
,null `dir_main_show`
from tmp_kbx;



-- 配置康柏馨满 60-20目录
-- INSERT INTO `medstore`.`pm_dir_info` ( `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
-- 1000037128	200act358z	第二件8折	dir	1		37	2	1			2018-05-16 21:44:22	2018-05-09 17:57:08				200		
-- VALUES ('1000037248', '200act35secondxz', '第二件*折', 'dir', '1', NULL, '21', '16', '3', '', '1000037132', '2018-05-16 21:44:22', '2018-05-14 17:52:34', '', '55月活动--200>>第二件*折', NULL, '200', NULL, NULL);
SELECT CONCAT(ydid,'act3760-20') as `dir_code`
,'满60-20' as `dir_name`
,'sdir' `dir_type`
,'1' `dir_status`
,null `dir_revalue`
,1501 `prod_sum`
,1 `dir_num`
,4 `dir_level`
,null `dir_img`
,null `parent_dir_id`
,NOW() `dir_update_time`
,NOW()  `dir_create_time`
,CONCAT(ydid,'康柏馨10 满60-20 长期') `dir_remark`
,null `dir_all_name`
,null `category_id`
,ydid as  `pharmacy_id`
,null `dir_gotocata`
,null `dir_main_show`
from tmp_kbx;


SELECT * from pm_dir_info 
--  WHERE dir_code like '%act37%'
ORDER BY dir_id DESC;

1000037790	1633act3760-20	满60-20
1000037789	1632act3760-20	满60-20
1000037788	1631act3760-20	满60-20
1000037787	1630act3760-20	满60-20
1000037786	1629act3760-20	满60-20
1000037785	1627act3760-20	满60-20
1000037784	1623act3760-20	满60-20
1000037783	1622act3760-20	满60-20
1000037782	1621act3760-20	满60-20
1000037781	1620act3760-20	满60-20
1000037775	1633act3758-8	满58-8
1000037774	1632act3758-8	满58-8
1000037773	1631act3758-8	满58-8
1000037772	1630act3758-8	满58-8
1000037771	1629act3758-8	满58-8
1000037770	1627act3758-8	满58-8
1000037769	1623act3758-8	满58-8
1000037768	1622act3758-8	满58-8
1000037767	1621act3758-8	满58-8
1000037766	1620act3758-8	满58-8
1000037766 1000037790

SELECT * from pm_sku_dir a ;


-- 58-8 配置关联
-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');
SELECT c.dir_id,b.sku_id,c.dir_code,1 as sku_order,NOW()
-- ,b.*,c.*,a.*
 from as_test.`517_kbx_58-8商品明细` a LEFT JOIN pm_prod_sku b on a.`商品编码` = b.pharmacy_huohao  and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
LEFT JOIN pm_dir_info c on b.drugstore_id = c.pharmacy_id and c.dir_id BETWEEN 1000037766 and 1000037775
WHERE b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) and c.dir_id BETWEEN 1000037766 and 1000037775
;

SELECT * FROM `pm_sku_dir` WHERE `dir_id` IN 

(
	SELECT dir_id FROM `pm_dir_info` WHERE `dir_code` LIKE '%act3758_8%'

);


-- 60-20 配置关联
-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');
SELECT c.dir_id,b.sku_id,c.dir_code,1 as sku_order,NOW()
--  ,b.*,c.*,a.*
 from as_test.`517_kbx_60-20商品明细` a LEFT JOIN pm_prod_sku b on a.`商品编码` = b.pharmacy_huohao  and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
LEFT JOIN pm_dir_info c on b.drugstore_id = c.pharmacy_id and c.dir_id BETWEEN  1000037781 and 1000037790
WHERE b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) and c.dir_id BETWEEN 1000037781 and 1000037790
;


SELECT * from am_act_info ORDER BY act_id DESC;

-- 172 活动 康柏馨免检
-- INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`)
 VALUES ('172', '康柏馨满减活动长期', 'date', '1', '打折促销', '2018-05-17 15:58:05', '2018-05-17 15:56:04', '2017-05-15 00:00:00', '2019-06-03 23:59:59', '1', '1', '', '', '1623');

SELECT * from am_act_item ORDER BY item_id DESC;


-- 58-8 配置关联
-- INSERT INTO `medstore`.`am_act_item` ( `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) 
-- VALUES ('850', 'single', '特价', '170', '1', '90', 'nothing', '本品享受特价优惠', '2018-05-11 14:35:57', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/tj.jpg\"}', '', '162', '', '', '', '');
SELECT 'multi', '满58-8', '172', '1', '90', 'cut', '本品享受满减优惠', '2018-05-17 14:35:57', '2018-05-17 17:46:31', '{\"itemLogoR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/mj.png\"}', '', ydid, '', '', '', ''
 from tmp_kbx ;



-- 60-20 配置关联
-- INSERT INTO `medstore`.`am_act_item` ( `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) 
-- VALUES ('850', 'single', '特价', '170', '1', '90', 'nothing', '本品享受特价优惠', '2018-05-11 14:35:57', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/tj.jpg\"}', '', '162', '', '', '', '');
SELECT 'multi', '满60-20', '172', '1', '90', 'cut', '本品享受满减优惠', '2018-05-17 14:35:57', '2018-05-17 17:46:31', '{\"itemLogoR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/mj.png\"}', '', ydid, '', '', '', ''
 from tmp_kbx ;

SELECT * from am_act_item ORDER BY item_id DESC;

953	single	满60-20
952	single	满60-20
951	single	满60-20
950	single	满60-20
949	single	满60-20
948	single	满60-20
947	single	满60-20
946	single	满60-20
945	single	满60-20
944	single	满60-20
938	single	满58-8
937	single	满58-8
936	single	满58-8
935	single	满58-8
934	single	满58-8
933	single	满58-8
932	single	满58-8
931	single	满58-8
930	single	满58-8
929	single	满58-8


SELECT * from am_act_range ORDER BY range_id desc;


-- 58-8 配置关联
-- INSERT INTO `medstore`.`am_act_range` ( `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) 
-- VALUES ('977', 'category', '1000037216', '1', '分类', '特价', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
SELECT 'category', dir_id, '1', '分类', '满58-8', NOW() range_update_time, NOW() range_create_time
from pm_dir_info WHERE dir_id BETWEEN 1000037766 and 1000037775;


-- 60-20 配置关联
-- INSERT INTO `medstore`.`am_act_range` ( `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) 
-- VALUES ('977', 'category', '1000037216', '1', '分类', '特价', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
SELECT 'category', dir_id, '1', '分类', '满60-20', NOW() range_update_time, NOW() range_create_time
from pm_dir_info WHERE  dir_id BETWEEN 1000037781 and 1000037790;


SELECT * from am_item_range ORDER BY link_id DESC;


-- 5.6.2 秒杀 item 和 range 关联
-- INSERT INTO `medstore`.`am_item_range` ( `range_id`, `item_id`)
-- VALUES ('761', '729', '688');
SELECT  -- *,
 b.range_id,c.item_id
--  ,a.dir_code,c.*
from pm_dir_info a 
 LEFT JOIN am_act_range b on a.dir_id = b.range_value
 LEFT JOIN am_act_item c on a.pharmacy_id = c.pharmacy_id  and a.dir_name = c.item_name
WHERE 
 a.dir_id BETWEEN 1000037766 and 1000037790 

and a.dir_code like '%act37%'
and c.act_id =172;


SELECT * from am_act_rule ORDER BY rule_id desc;

--  满 多少
INSERT INTO `medstore`.`am_act_rule` (`rule_id`, `rule_type`, `rule_name`, `rule_value_type`, `rule_value`, `rule_remark`, `rule_update_time`, `rule_create_time`) VALUES ('39', 'threshold', 'sum_fee', 'string', '>=5800', 'sum_fee', '2018-05-17 17:23:19', '2018-05-17 17:23:19');
INSERT INTO `medstore`.`am_act_rule` (`rule_id`, `rule_type`, `rule_name`, `rule_value_type`, `rule_value`, `rule_remark`, `rule_update_time`, `rule_create_time`) VALUES ('40', 'threshold', 'sum_fee', 'string', '>=6000', 'sum_fee', '2018-05-17 17:23:19', '2018-05-17 17:23:19');


--

SELECT * from am_item_details ORDER BY details_id DESC;


-- 满58元减8元
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
-- VALUES ('191', '1', '476', 'cut', 'number', '3000', '2017-06-07 10:01:15', '2017-03-31 00:00:00', '已满58元减8元', '满58元减8元');
SELECT 
 '1', item_id, 'cut', 'number', '800',NOW(), NOW(), '已满58元减8元', '满58元减8元'
 from am_act_item WHERE act_id = 172 and item_name = '满58减8'  -- '满58-8';


-- 满116减16 
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
-- VALUES ('191', '1', '476', 'cut', 'number', '3000', '2017-06-07 10:01:15', '2017-03-31 00:00:00', '已满58元减8元', '满58元减8元');
SELECT 
 '2', item_id, 'cut', 'number', '1600',NOW(), NOW(), '已满116元减16元', '满116元减16元'
 from am_act_item WHERE act_id = 172 and item_name = '满58减8'


--  满232减32
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
-- VALUES ('191', '1', '476', 'cut', 'number', '3000', '2017-06-07 10:01:15', '2017-03-31 00:00:00', '已满58元减8元', '满58元减8元');
SELECT 
 '3', item_id, 'cut', 'number', '3200',NOW(), NOW(), '已满232元减32元', '满232元减32元'
 from am_act_item WHERE act_id = 172 and item_name = '满58减8'



--   满240减80；

INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
SELECT 
 '3', item_id, 'cut', 'number', '8000',NOW(), NOW(), '已满240元减80元', '满240元减80元'
 from am_act_item WHERE act_id = 172 and item_name = '满60减20';


-- 满120减40  
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
SELECT 
 '2', item_id, 'cut', 'number', '4000',NOW(), NOW(), '已满120元减40元', '满120元减40元'
 from am_act_item WHERE act_id = 172 and item_name = '满60减20';

-- 满60元减20元
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
SELECT 
 '1', item_id, 'cut', 'number', '2000',NOW(), NOW(), '已满60元减20元', '满60元减20元'
 from am_act_item WHERE act_id = 172 and item_name = '满60减20';


SELECT * from am_details_rule ORDER BY rule_id DESC;

44	>=24000
43	>=12000
42	>=23200
41	>=11600
40	>=6000
39	>=5800

58减8
满116减16
满232减32；

满60减20
满120减40
满240减80；
-- 58-8
-- INSERT INTO `medstore`.`am_details_rule` ( `details_id`, `rule_id`)
-- VALUES ('11', '15', '10');
SELECT details_id,CASE WHEN details_value = 3200 then 42
WHEN details_value = 1600 then 41
WHEN details_value = 800 then 39

WHEN details_value = 8000 then 44
WHEN details_value = 4000 then 43
WHEN details_value = 2000 then 40
else null
end 
-- ,a.*
from  am_item_details  a 
WHERE details_id  >318  -- BETWEEN 288 and 297





-- 58-8 行行行XXXX
-- INSERT INTO `medstore`.`am_details_rule` ( `details_id`, `rule_id`)
-- VALUES ('11', '15', '10');
SELECT details_id,39
from  am_item_details 
WHERE details_id BETWEEN 288 and 297


-- 60-20 XXXX
-- INSERT INTO `medstore`.`am_details_rule` ( `details_id`, `rule_id`)
-- VALUES ('11', '15', '10');
SELECT details_id,40
from  am_item_details 
WHERE details_id BETWEEN 303 and 312;


SELECT * from sm_config ;

SELECT  * from sm_image_link -- WHERE link_type ='dirSku';
ORDER BY link_id desc;


SELECT  * from sm_image_link WHERE 
drugstore_id = 1620 
-- and link_status = 1 
and link_version = 0 and (link_view = '' or link_view is null) ORDER BY link_id desc;

-- 轮播图 分心就
-- INSERT INTO `medstore`.`sm_image_link` (  `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`)
--  VALUES (  '1633', '2', 'http://imgstore.camore.cn/icon/act/ashacker/180515/n_act35_g_02.jpg',
--  'http://deve.ykd365.com/medstore/actpage/stripGoods_module?dirId=1000037245', 'dirSku',
--  '第二件*折', '1000037245', '1', '2018-05-14 18:41:36', '2018-05-14 18:21:55', '第二件*折', '2017-05-15 00:00:00', '2018-06-03 23:59:59', 'essence', '1');

SELECT c.pharmacy_id as `drugstore_id`
,'2' as `seq_num`
,'http://imgstore.camore.cn/icon/act/ashacker/180515/act3760-20_lb.jpg' as `image_url`
,'' as  `link_url`
,'dirSku' as  `link_type`
,'满60减20' `link_name`
,c.dir_id as `link_param`
,1 as `link_status`
,NOW() as `link_update_time`
,NOW() as `link_create_time`
,'满60减20' as `link_remark`
,'2018-05-27 00:00:00' as  `link_start_time`
,'2019-06-03 23:59:59' as  `link_end_time`
,'essence' as  `link_view`
,'0' as `link_version`
 from pm_dir_info c 
WHERE c.pharmacy_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) and c.dir_id BETWEEN 1000037781 and 1000037790
;

-- INSERT INTO `medstore`.`sm_image_link` (  `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`)

SELECT c.pharmacy_id as `drugstore_id`
,'2' as `seq_num`
,'http://imgstore.camore.cn/icon/act/ashacker/180515/n_act3760-20_lb.jpg' as `image_url`
,'' as  `link_url`
,'dirSku' as  `link_type`
,'满60减20' `link_name`
,c.dir_id as `link_param`
,1 as `link_status`
,NOW() as `link_update_time`
,NOW() as `link_create_time`
,'满60减20' as `link_remark`
,'2018-05-27 00:00:00' as  `link_start_time`
,'2019-06-03 23:59:59' as  `link_end_time`
,'essence' as  `link_view`
,'1' as `link_version`
 from pm_dir_info c 
WHERE c.pharmacy_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) and c.dir_id BETWEEN 1000037781 and 1000037790
;
-------- 上生产

SELECT * FROM am_item_details ORDER BY details_id desc;

-- SELECT * FROM am_details_rule;
SELECT * from am_act_rule  ORDER BY rule_id desc;
SELECT * FROM am_details_rule ORDER BY `details_id` DESC ;

SELECT * FROM am_details_rule WHERE details_id in (316,317,318) ORDER BY `details_id` DESC ;
-- 58-8
-- INSERT INTO `medstore`.`am_details_rule` ( `details_id`, `rule_id`)
-- VALUES ('11', '15', '10');
SELECT details_id,CASE WHEN details_value = 3200 then 42
WHEN details_value = 1600 then 41
WHEN details_value = 800 then 39

WHEN details_value = 8000 then 44
WHEN details_value = 4000 then 43
WHEN details_value = 2000 then 40
else null
end as rule_id
-- ,a.*
from  am_item_details  a 
WHERE details_id  >318



INSERT INTO `medstore`.`am_act_rule` (`rule_id`, `rule_type`, `rule_name`, `rule_value_type`, `rule_value`, `rule_remark`, `rule_update_time`, `rule_create_time`) VALUES ('44', 'threshold', 'sum_fee', 'string', '>=24000', 'sum_fee', '2018-05-25 16:13:24', '2018-05-17 17:23:19');
INSERT INTO `medstore`.`am_act_rule` (`rule_id`, `rule_type`, `rule_name`, `rule_value_type`, `rule_value`, `rule_remark`, `rule_update_time`, `rule_create_time`) VALUES ('43', 'threshold', 'sum_fee', 'string', '>=12000', 'sum_fee', '2018-05-25 16:12:57', '2018-05-17 17:23:19');
INSERT INTO `medstore`.`am_act_rule` (`rule_id`, `rule_type`, `rule_name`, `rule_value_type`, `rule_value`, `rule_remark`, `rule_update_time`, `rule_create_time`) VALUES ('42', 'threshold', 'sum_fee', 'string', '>=23200', 'sum_fee', '2018-05-18 10:15:03', '2018-05-17 17:23:19');
INSERT INTO `medstore`.`am_act_rule` (`rule_id`, `rule_type`, `rule_name`, `rule_value_type`, `rule_value`, `rule_remark`, `rule_update_time`, `rule_create_time`) VALUES ('41', 'threshold', 'sum_fee', 'string', '>=11600', 'sum_fee', '2018-05-18 10:14:59', '2018-05-17 17:23:19');
INSERT INTO `medstore`.`am_act_rule` (`rule_id`, `rule_type`, `rule_name`, `rule_value_type`, `rule_value`, `rule_remark`, `rule_update_time`, `rule_create_time`) VALUES ('40', 'threshold', 'sum_fee', 'string', '>=6000', 'sum_fee', '2018-05-17 15:51:03', '2018-05-17 17:23:19');
INSERT INTO `medstore`.`am_act_rule` (`rule_id`, `rule_type`, `rule_name`, `rule_value_type`, `rule_value`, `rule_remark`, `rule_update_time`, `rule_create_time`) VALUES ('39', 'threshold', 'sum_fee', 'string', '>=5800', 'sum_fee', '2018-05-17 17:23:19', '2018-05-17 17:23:19');





INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('421', '1', '953', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('420', '1', '952', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('419', '1', '951', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('418', '1', '950', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('417', '1', '949', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('416', '1', '948', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('415', '1', '947', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('414', '1', '946', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('413', '1', '945', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('412', '1', '944', 'cut', 'number', '2000', '2018-05-25 16:38:54', '2018-05-25 16:38:54', '已满60元减20元', '满60元减20元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('406', '2', '953', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('405', '2', '952', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('404', '2', '951', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('403', '2', '950', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('402', '2', '949', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('401', '2', '948', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('400', '2', '947', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('399', '2', '946', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('398', '2', '945', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('397', '2', '944', 'cut', 'number', '4000', '2018-05-25 16:38:47', '2018-05-25 16:38:47', '已满120元减40元', '满120元减40元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('391', '3', '953', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('390', '3', '952', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('389', '3', '951', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('388', '3', '950', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('387', '3', '949', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('386', '3', '948', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('385', '3', '947', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('384', '3', '946', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('383', '3', '945', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('382', '3', '944', 'cut', 'number', '8000', '2018-05-25 16:38:37', '2018-05-25 16:38:37', '已满240元减80元', '满240元减80元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('376', '1', '938', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('375', '1', '937', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('374', '1', '936', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('373', '1', '935', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('372', '1', '934', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('371', '1', '933', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('370', '1', '932', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('369', '1', '931', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('368', '1', '930', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('367', '1', '929', 'cut', 'number', '800', '2018-05-25 16:36:59', '2018-05-25 16:36:59', '已满58元减8元', '满58元减8元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('361', '2', '938', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('360', '2', '937', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('359', '2', '936', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('358', '2', '935', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('357', '2', '934', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('356', '2', '933', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('355', '2', '932', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('354', '2', '931', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('353', '2', '930', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('352', '2', '929', 'cut', 'number', '1600', '2018-05-25 16:36:56', '2018-05-25 16:36:56', '已满116元减16元', '满116元减16元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('346', '3', '938', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('345', '3', '937', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('344', '3', '936', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('343', '3', '935', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('342', '3', '934', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('341', '3', '933', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('340', '3', '932', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('339', '3', '931', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('338', '3', '930', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('337', '3', '929', 'cut', 'number', '3200', '2018-05-25 16:36:44', '2018-05-25 16:36:44', '已满232元减32元', '满232元减32元');

---

INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1330', '1062', '953');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1329', '1061', '952');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1328', '1060', '951');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1327', '1059', '950');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1326', '1058', '949');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1325', '1057', '948');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1324', '1056', '947');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1323', '1055', '946');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1322', '1054', '945');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1321', '1053', '944');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1320', '1047', '938');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1319', '1046', '937');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1318', '1045', '936');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1317', '1044', '935');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1316', '1043', '934');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1315', '1042', '933');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1314', '1041', '932');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1313', '1040', '931');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1312', '1039', '930');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1311', '1038', '929');

\




INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1062', 'category', '1000037790', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1061', 'category', '1000037789', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1060', 'category', '1000037788', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1059', 'category', '1000037787', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1058', 'category', '1000037786', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1057', 'category', '1000037785', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1056', 'category', '1000037784', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1055', 'category', '1000037783', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1054', 'category', '1000037782', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1053', 'category', '1000037781', '1', '分类', '满60-20', '2018-05-17 15:43:19', '2018-05-17 15:43:19');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1047', 'category', '1000037775', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1046', 'category', '1000037774', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1045', 'category', '1000037773', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1044', 'category', '1000037772', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1043', 'category', '1000037771', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1042', 'category', '1000037770', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1041', 'category', '1000037769', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1040', 'category', '1000037768', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1039', 'category', '1000037767', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1038', 'category', '1000037766', '1', '分类', '满58-8', '2018-05-17 15:43:16', '2018-05-17 15:43:16');


SELECT * FROM am_act_range ORDER BY range_id desc;
-- r 13 i 11

SELECT * FROM am_item_range 
-- WHERE item_id >=681
ORDER BY `link_id` DESC ; 

INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('953', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:22', '2018-05-17 17:46:31', '', '', '1633', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('952', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:22', '2018-05-17 17:46:31', '', '', '1632', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('951', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:22', '2018-05-17 17:46:31', '', '', '1631', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('950', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:22', '2018-05-17 17:46:31', '', '', '1630', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('949', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:22', '2018-05-17 17:46:31', '', '', '1629', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('948', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:22', '2018-05-17 17:46:31', '', '', '1627', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('947', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:22', '2018-05-17 17:46:31', '', '', '1623', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('946', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:22', '2018-05-17 17:46:31', '', '', '1622', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('945', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:22', '2018-05-17 17:46:31', '', '', '1621', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('944', 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-18 16:45:12', '2018-05-17 17:46:31', '', '', '1620', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('938', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1633', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('937', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1632', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('936', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1631', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('935', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1630', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('934', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1629', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('933', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1627', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('932', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1623', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('931', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1622', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('930', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1621', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('929', 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-18 16:43:52', '2018-05-17 17:46:31', '', '', '1620', '', '', '', '');


SELECT * FROM am_act_item ORDER BY `item_id` DESC ;

SELECT * from am_act_info ORDER BY act_id DESC;


INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`) VALUES ('172', '康柏馨满减活动长期', 'date', '1', '打折促销', '2018-05-17 15:58:05', '2018-05-17 15:56:04', '2017-05-15 00:00:00', '2019-06-03 23:59:59', '1', '1', '', '', '1623');


-- 60-20 配置关联
-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');
SELECT c.dir_id,b.sku_id,c.dir_code,1 as sku_order,NOW()
--  ,b.*,c.*,a.*
 from as_test.`517_kbx_60-20商品明细` a LEFT JOIN pm_prod_sku b on a.`商品编码` = b.pharmacy_huohao  and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
LEFT JOIN pm_dir_info c on b.drugstore_id = c.pharmacy_id and c.dir_id BETWEEN  1000037781 and 1000037790
WHERE b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) and c.dir_id BETWEEN 1000037781 and 1000037790
;



-- 58-8 配置关联
-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');
SELECT c.dir_id,b.sku_id,c.dir_code,1 as sku_order,NOW()
-- ,b.*,c.*,a.*
 from as_test.`517_kbx_58-8商品明细` a LEFT JOIN pm_prod_sku b on a.`商品编码` = b.pharmacy_huohao  and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
LEFT JOIN pm_dir_info c on b.drugstore_id = c.pharmacy_id and c.dir_id BETWEEN 1000037766 and 1000037775
WHERE b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) and c.dir_id BETWEEN 1000037766 and 1000037775
;





INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037790', '1633act3760-20', '满60减20', 'sdir', '1', NULL, '19336', '1', '1', NULL, NULL, '2018-05-22 02:00:08', '2018-05-17 15:11:23', '', NULL, NULL, '1633', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037789', '1632act3760-20', '满60减20', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:11:23', '', NULL, NULL, '1632', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037788', '1631act3760-20', '满60减20', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:11:23', '', NULL, NULL, '1631', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037787', '1630act3760-20', '满60减20', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:11:23', '', NULL, NULL, '1630', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037786', '1629act3760-20', '满60减20', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:11:23', '', NULL, NULL, '1629', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037785', '1627act3760-20', '满60减20', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:11:23', '', NULL, NULL, '1627', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037784', '1623act3760-20', '满60减20', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:11:23', '', NULL, NULL, '1623', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037783', '1622act3760-20', '满60减20', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:11:23', '', NULL, NULL, '1622', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037782', '1621act3760-20', '满60减20', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:11:23', '', NULL, NULL, '1621', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037781', '1620act3760-20', '满60减20', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:11:23', '', NULL, NULL, '1620', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037775', '1633act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1633', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037774', '1632act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1632', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037773', '1631act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1631', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037772', '1630act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1630', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037771', '1629act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1629', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037770', '1627act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1627', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037769', '1623act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1623', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037768', '1622act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1622', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037767', '1621act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1621', NULL, NULL);
INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1000037766', '1620act3758-8', '满58减8', 'sdir', '1', NULL, '1501', '1', '1', NULL, NULL, '2018-05-17 17:22:13', '2018-05-17 15:09:25', '', NULL, NULL, '1620', NULL, NULL);


