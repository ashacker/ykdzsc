

SELECT * from as_test.3_act_temp;


-- 新建199m商品
-- INSERT INTO `medstore`.`pm_prod_sku` (  `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sort`, `sku_rank`, `sku_type`) 
SELECT  s.`prod_id`, `sku_status`
,a.fee `sku_price`
,a.fee `sku_fee`, s.`drugstore_id`, `brand_id`
,NOW() `sku_update_time`
,NOW() `sku_create_time`
,CONCAT('3月女神节-',a.act_name,'-',a.act_type)  `sku_remark`, `sku_logistics`, s.`prod_name`
,CASE
 WHEN a.act_type= '1元' then CONCAT('1903YYY',a.old_huohao)
 WHEN a.act_type = '9.9元' then CONCAT('1903YJY',a.old_huohao)
 WHEN a.act_type = '秒杀' then CONCAT('1903YMS',a.old_huohao)
 else '*****'  end   `pharmacy_huohao`, `source_id`, s.`sku_json`, `sku_attr`, `sku_img`, `sku_sum`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sort`, `sku_rank`, `sku_type`
from as_test.3_act_temp a,pm_prod_sku s
WHERE a.old_huohao = s.pharmacy_huohao
and a.drugstore_id = s.drugstore_id
 
;

-- 新建的商品有了 skuid 重新补充到_act_temp表中
-- UPDATE  as_test.3_act_temp a,pm_prod_sku s  set a.sku_id = s.sku_id  
-- SELECT * from as_test.3_act_temp a,pm_prod_sku s 
WHERE a.pharmacy_huohao = s.pharmacy_huohao
and a.drugstore_id = s.drugstore_id
;
