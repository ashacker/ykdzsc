
-- 66912  16712
-- INSERT INTO `medstore`.`pm_label_image` ( `label_name`, `label_url`, `label_url_double`, `label_type`, `label_status`, `label_create_time`, `label_update_time`, `pharmacy_id`, `label_flag`, `sku_id`)
--  VALUES ('103485', '年货节', 'http://image.ykd365.cn/act/190101/lb_icon.png', 'http://image.ykd365.cn/act/190101/lb_icon.png', '1', '1', '2019-01-16 15:16:19', '2019-01-16 15:16:19', '200', '0', '11424030');

SELECT   '年货节' `label_name`
,'http://image.ykd365.cn/act/190101/lb_icon.png' `label_url`
,'http://image.ykd365.cn/act/190101/lb_icon.png' `label_url_double`
,1 `label_type`
,1 `label_status`
,NOW() `label_create_time`
,NOW() `label_update_time`
,aa.drugstore_id `pharmacy_id`
,0 `label_flag`
, aa.`sku_id`  FROM `pm_prod_sku` aa WHERE aa.sku_id not in (
SELECT a.sku_id  FROM `pm_prod_sku` a,pm_label_image b WHERE
a.sku_id = b.sku_id
AND a.`drugstore_id`  in (200,1610,1611,1612,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
and  b.label_name = '年货节')
and aa.`drugstore_id`  in (200,1610,1611,1612,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
;


-- INSERT INTO `medstore`.`am_stat_info` ( `batch_num`, `sku_id`, `act_id`, `item_id`, `item_remark`, `item_name`, `item_attr`, `stat_update_time`, `stat_create_time`, `item_effect_time`, `item_expire_time`, `item_type`, `other_str1`, `quota_id`, `item_flag`, `item_priority`) 
-- VALUES ('28012246', '20190120145800', '100070', '186', '1373', '直降到底', '95折', 'single', '2019-01-22 16:17:44', '2019-01-20 15:20:20', '2019-01-08 00:00:00', '2019-02-11 23:59:59', 'nothing', '{\"itemImageR\":\"370\",\"itemImage\":\"http://image.ykd365.cn/act/190101/detail_icon.png\"}', NULL, '', '100');
SELECT 20190120145800  `batch_num`, `sku_id`
,185 `act_id`
,1351 `item_id`
,'下单后随机抽奖，100%中奖' `item_remark`
,'下单抽奖' `item_name`,'single' `item_attr`,NOW() `stat_update_time`,NOW() `stat_create_time`
,'2019-01-08 00:00:00' `item_effect_time`
,'2019-02-11 23:59:59' `item_expire_time`
,'drugtag' `item_type`,
'{"itemImageR":"370","itemImage":"http://image.ykd365.cn/act/190101/detail_icon.png"}' `other_str1`,NULL `quota_id`,NULL `item_flag`
,90 `item_priority`
 FROM `pm_prod_sku` aa WHERE aa.sku_id not in (
SELECT a.sku_id  FROM `pm_prod_sku` a,am_stat_info b WHERE
a.sku_id = b.sku_id
AND a.`drugstore_id`  in (200,1610,1611,1612,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
and  b.item_name = '下单抽奖')
and aa.`drugstore_id`  in (200,1610,1611,1612,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
;

