-- 
--  SELECT * from  pm_prod_info ORDER BY PROD_ID DESC LIMIT 300;
-- 
-- SELECT * from  pm_prod_info as p,as_test.1905_bj_act as a
-- WHERE p.prod_id = A.商品ID
-- 

/*
活动主题：
药快到 药品福利管理计划
慢病药品最高享8折优惠
活动时间：
2019.5.1-2019.5.31
活动配置：
*药品福利主会场页面
*发券页面配置

1、备注里标注”特价“的在活动期间按”活动期间多盒单价“和”活动期间多盒销售价“替换改sku的”疗程购单价“和”多盒sku销售价“，活动后恢复现有价格；
--- 配个特价
2、备注里标注”需新增“的按表中的”多盒件数“、”疗程购单盒价“、”多盒套餐原组合价“、”多盒套餐组合特价“、”前缀“、“康佰馨多盒货号”、“正济堂多盒货号”等信息新建疗程购、多买少算sku（别忘了疗程购sku与单品的对应关系）；活动结束后不下架，正常销售；
--- 新增疗程购
3、“高血压”、“高血糖”、类目中排名前5位的商品在该类目下1排1陈列，剩余的商品按1排2陈列；“高血脂”排名前3位的商品在该类目下1排1陈列，剩余的商品按1排2陈列；

4、其他类目的商品全部按1排2陈列；
*/

-- SELECT * from as_test.1905_bj_act;  -- 190
-- 
-- SELECT * from as_test.1905_act_temp;

SELECT * from pm_prod_info ORDER BY prod_id DESC ;

-- 增加新的多盒prod_info bj   11 
-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`, `prod_synonyms`, `prod_split_word`) 
SELECT `prod_name`
,CONCAT('',a.多盒,'件装 ',prod_brand)  `prod_brand`, `prod_status`, `prod_code`, `prod_sn`
,a.多盒套装原组合价*100 `prod_price`, `prod_level`, `prod_firm`, `prod_logist`
,NOW() `prod_update_time`
,NOW() `prod_create_time`, `prod_type`
,CONCAT(prod_spec,'*',a.多盒,prod_unit) `prod_spec`, `prod_unit`, `prod_gen_name`
,a.商品ID `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`, `prod_synonyms`, `prod_split_word`
-- SELECT *
 from as_test.1905_bj_act as a ,pm_prod_info as s 
WHERE a.商品ID = s.prod_id
and  a.备注 = '需新建'
;


-- 增加新的多盒prod_info bj       新增了4个
-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`, `prod_synonyms`, `prod_split_word`) 
SELECT `prod_name`
,CONCAT('',a.多盒,'件装 ',prod_brand)  `prod_brand`, `prod_status`, `prod_code`, `prod_sn`
,a.多盒套装原组合价*100 `prod_price`, `prod_level`, `prod_firm`, `prod_logist`
,NOW() `prod_update_time`
,NOW() `prod_create_time`, `prod_type`
,CONCAT(prod_spec,'*',a.多盒,prod_unit) `prod_spec`, `prod_unit`, `prod_gen_name`
,a.商品ID `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`, `prod_synonyms`, `prod_split_word`
-- SELECT *
 from as_test.1905_bj_act as a ,pm_prod_info as s 
WHERE a.商品ID = s.prod_id
and  a.备注 = '需新建'
and a.类目 = '高血脂' and a.序号 BETWEEN 40 and 43
;

SELECT * from pm_prod_info ORDER BY prod_id DESC;

-- 增加新的多盒prod_info  ty 13
-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`, `prod_synonyms`, `prod_split_word`) 
SELECT `prod_name`
,CONCAT('',a.多盒,'件装 ',prod_brand)  `prod_brand`, `prod_status`, `prod_code`, `prod_sn`
,a.多盒套装原组合价*100 `prod_price`, `prod_level`, `prod_firm`, `prod_logist`
,NOW() `prod_update_time`
,NOW() `prod_create_time`, `prod_type`
,CONCAT(prod_spec,'*',a.多盒,prod_unit) `prod_spec`, `prod_unit`, `prod_gen_name`
,a.商品ID `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`, `prod_synonyms`, `prod_split_word`
-- SELECT *
 from as_test.1905_ty_act as a ,pm_prod_info as s 
WHERE a.商品ID = s.prod_id
and  a.备注 = '需新建'
;



-- 创建多盒商品的sku bj
-- INSERT INTO `medstore`.`pm_prod_sku` (  `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`) 
SELECT p.`prod_id`,1 `sku_status`
,a.多盒套装组合特价*100 `sku_price`
,a.多盒套装原组合价*100 `sku_fee`
,s.`drugstore_id`, s.`brand_id`
,NOW() `sku_update_time`,NOW() `sku_create_time`
,'1905月活动' `sku_remark`, s.`sku_logistics`
,CONCAT("[",a.前缀,"] ",s.prod_name) `prod_name`
,a.康佰馨多盒货号 `pharmacy_huohao`, s.`source_id`
,concat('{\"prod_spec\":\"',p.prod_spec,'\"}') `sku_json`, s.`sku_attr`,a.前缀 `sku_img`, `sku_activate`, `sales_info`
,0 `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`
-- SELECT  a.*,s.*,p.*
 from as_test.1905_bj_act as a ,pm_prod_sku as s ,pm_prod_info p
WHERE -- (
(a.康佰馨单品货号 = s.pharmacy_huohao and s.drugstore_id  in   (1621,1622,1623,1627,1629,1630,1631,1632,1633)) -- 1620,
-- or (a.正济堂单品货号 = s.pharmacy_huohao and s.drugstore_id in (1610,1611,1612) ))
and a.商品ID = p.prod_remark and p.prod_create_time BETWEEN '2019-04-19' and '2019-04-20'
and  a.备注 = '需新建'
ORDER BY a.序号
;

-- 创建多盒商品的sku bj
-- INSERT INTO `medstore`.`pm_prod_sku` (  `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`) 
SELECT p.`prod_id`,1 `sku_status`
,a.多盒套装组合特价*100 `sku_price`
,a.多盒套装原组合价*100 `sku_fee`
,s.`drugstore_id`, s.`brand_id`
,NOW() `sku_update_time`,NOW() `sku_create_time`
,'1905月活动' `sku_remark`, s.`sku_logistics`
,CONCAT("[",a.前缀,"] ",s.prod_name) `prod_name`
,a.正济堂多盒货号 `pharmacy_huohao`, s.`source_id`
,concat('{\"prod_spec\":\"',p.prod_spec,'\"}') `sku_json`, s.`sku_attr`,a.前缀 `sku_img`, `sku_activate`, `sales_info`
,0 `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`
-- SELECT  a.*,s.*,p.*
 from as_test.1905_bj_act as a ,pm_prod_sku as s ,pm_prod_info p
WHERE  
-- (a.康佰馨单品货号 = s.pharmacy_huohao and s.drugstore_id  in   (1621,1622,1623,1627,1629,1630,1631,1632,1633)) -- 1620,
 (a.正济堂单品货号 = s.pharmacy_huohao and s.drugstore_id in (1610,1611,1612) ) 
and a.商品ID = p.prod_remark and p.prod_create_time BETWEEN '2019-04-19' and '2019-04-27'
and  a.备注 = '需新建'
ORDER BY a.序号
;





-- 创建多盒商品的sku bj 4
-- INSERT INTO `medstore`.`pm_prod_sku` (  `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`) 
SELECT p.`prod_id`,1 `sku_status`
,a.多盒套装组合特价*100 `sku_price`
,a.多盒套装原组合价*100 `sku_fee`
,s.`drugstore_id`, s.`brand_id`
,NOW() `sku_update_time`,NOW() `sku_create_time`
,'1905月活动' `sku_remark`, s.`sku_logistics`
,CONCAT("[",a.前缀,"] ",s.prod_name) `prod_name`
,a.康佰馨多盒货号 `pharmacy_huohao`, s.`source_id`
,concat('{\"prod_spec\":\"',p.prod_spec,'\"}') `sku_json`, s.`sku_attr`,a.前缀 `sku_img`, `sku_activate`, `sales_info`
,0 `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`
-- SELECT  a.*,s.*,p.*
 from as_test.1905_bj_act as a ,pm_prod_sku as s ,pm_prod_info p
WHERE -- (
 a.康佰馨单品货号 = s.pharmacy_huohao and s.drugstore_id     in   (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) -- 1620,
-- or (a.正济堂单品货号 = s.pharmacy_huohao and s.drugstore_id in (1610,1611,1612) ))
and a.商品ID = p.prod_remark and p.prod_create_time BETWEEN '2019-04-30' and '2019-05-01'
and  a.备注 = '需新建'  and a.类目 = '高血脂' and a.序号 BETWEEN 40 and 43
ORDER BY a.序号
;

-- 创建多盒商品的sku bj 4
-- INSERT INTO `medstore`.`pm_prod_sku` (  `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`) 
SELECT p.`prod_id`,1 `sku_status`
,a.多盒套装组合特价*100 `sku_price`
,a.多盒套装原组合价*100 `sku_fee`
,s.`drugstore_id`, s.`brand_id`
,NOW() `sku_update_time`,NOW() `sku_create_time`
,'1905月活动' `sku_remark`, s.`sku_logistics`
,CONCAT("[",a.前缀,"] ",s.prod_name) `prod_name`
,a.正济堂多盒货号 `pharmacy_huohao`, s.`source_id`
,concat('{\"prod_spec\":\"',p.prod_spec,'\"}') `sku_json`, s.`sku_attr`,a.前缀 `sku_img`, `sku_activate`, `sales_info`
,0 `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`
-- SELECT  a.*,s.*,p.*
 from as_test.1905_bj_act as a ,pm_prod_sku as s ,pm_prod_info p
WHERE  
-- (a.康佰馨单品货号 = s.pharmacy_huohao and s.drugstore_id  in   (1621,1622,1623,1627,1629,1630,1631,1632,1633)) -- 1620,
 (a.正济堂单品货号 = s.pharmacy_huohao and s.drugstore_id in (1610,1611,1612) ) 
and a.商品ID = p.prod_remark and p.prod_create_time  BETWEEN '2019-04-30' and '2019-05-01'
and  a.备注 = '需新建'  and a.类目 = '高血脂' and a.序号 BETWEEN 40 and 43
ORDER BY a.序号
;




SELECT * from pm_prod_sku ORDER BY sku_id DESC;

-- UPDATE as_test.1905_ty_act set 多盒货号 = CONCAT(前缀,单盒货号) WHERE 备注 = '需新建';

SELECT * from as_test.1905_ty_act a WHERE 备注 = '需新建';

-- 创建多盒商品的sku  ty 13
-- INSERT INTO `medstore`.`pm_prod_sku` (  `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`) 
SELECT p.`prod_id`,1 `sku_status`
,a.多盒套装组合特价*100 `sku_price`
,a.多盒套装原组合价*100 `sku_fee`
,s.`drugstore_id`, s.`brand_id`
,NOW() `sku_update_time`,NOW() `sku_create_time`
,'1905月活动-ty' `sku_remark`, s.`sku_logistics`
,CONCAT("[",a.前缀,"] ",s.prod_name) `prod_name`
,a.多盒货号 `pharmacy_huohao`, s.`source_id`
,concat('{\"prod_spec\":\"',p.prod_spec,'\"}') `sku_json`, s.`sku_attr`,a.前缀 `sku_img`, `sku_activate`, `sales_info`
,0 `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`
-- SELECT  a.*,s.*,p.*
 from as_test.1905_ty_act as a ,pm_prod_sku as s ,pm_prod_info p
WHERE a.单盒货号 = s.pharmacy_huohao
and a.商品ID = p.prod_remark and p.prod_create_time BETWEEN '2019-04-26' and '2019-04-27'
and s.drugstore_id  = 200
and  a.备注 = '需新建'
ORDER BY a.序号
;

-- SELECT * from pm_prod_sku WHERE pharmacy_huohao like '%件装%';

-- 增加新建组合商品prod和图片的关联关系  bj
-- INSERT INTO `medstore`.`pm_prod_img` ( `prod_id`, `attr_id`, `attr_value_id`, `img_id`, `img_kind`, `img_name`)
SELECT  p.`prod_id`  , `attr_id`, `attr_value_id`, `img_id`, `img_kind`, `img_name` 
-- ,a.oldprodid , pi.prod_id
-- ,a.*,pi.*,p.*

from  as_test.1905_bj_act a ,pm_prod_info p  ,pm_prod_img  pi -- pm_image_info ii
WHERE a.商品ID = p.prod_remark  
 and a.商品ID = pi.prod_id
and  a.备注 = '需新建'




-- 增加新建组合商品prod和图片的关联关系  bj
-- INSERT INTO `medstore`.`pm_prod_img` ( `prod_id`, `attr_id`, `attr_value_id`, `img_id`, `img_kind`, `img_name`)
SELECT  p.`prod_id`  , `attr_id`, `attr_value_id`, `img_id`, `img_kind`, `img_name` 
-- ,a.oldprodid , pi.prod_id
-- ,a.*,pi.*,p.*

from  as_test.1905_bj_act a ,pm_prod_info p  ,pm_prod_img  pi -- pm_image_info ii
WHERE a.商品ID = p.prod_remark  
 and a.商品ID = pi.prod_id
and  a.备注 = '需新建'    and a.类目 = '高血脂' and a.序号 BETWEEN 40 and 43
;


-- 增加新建组合商品prod和图片的关联关系  ty 57
-- INSERT INTO `medstore`.`pm_prod_img` ( `prod_id`, `attr_id`, `attr_value_id`, `img_id`, `img_kind`, `img_name`)
SELECT  p.`prod_id`  , `attr_id`, `attr_value_id`, `img_id`, `img_kind`, `img_name` 
-- ,a.oldprodid , pi.prod_id
-- ,a.*,pi.*,p.*

from  as_test.1905_ty_act a ,pm_prod_info p  ,pm_prod_img  pi -- pm_image_info ii
WHERE a.商品ID = p.prod_remark  
 and a.商品ID = pi.prod_id
and  a.备注 = '需新建'
;

-- bj
-- INSERT INTO `medstore`.`pm_packet_info` (  `packet_name`, `packet_status`, `packet_type`, `packet_price`, `packet_fee`,  `packet_content`, `packet_stock`,  `drugstore_id`, `packet_update_time`, `packet_create_time`, `packet_remark`, `sku_id`, `pre_prod_name` ) 
SELECT s.prod_name `packet_name`
,1 `packet_status`
,'course' `packet_type`
,s.sku_price `packet_price`
,s.sku_fee `packet_fee`
,s.pharmacy_huohao  `packet_content`
,1 `packet_stock`
,s.drugstore_id  `drugstore_id`
,NOW() `packet_update_time`
,NOW() `packet_create_time`
,'1905月活动' `packet_remark`
,s.sku_id
,s.sku_img `pre_prod_name`  
FROM pm_prod_sku s
WHERE s.sku_remark = '1905月活动'
;


SELECT * from pm_packet_info ORDER BY packet_id DESC;
-- bj
-- INSERT INTO `medstore`.`pm_packet_info` (  `packet_name`, `packet_status`, `packet_type`, `packet_price`, `packet_fee`,  `packet_content`, `packet_stock`,  `drugstore_id`, `packet_update_time`, `packet_create_time`, `packet_remark`, `sku_id`, `pre_prod_name`,packet_coures ) 
SELECT s.prod_name `packet_name`
,1 `packet_status`
,'course' `packet_type`
,s.sku_price `packet_price`
,s.sku_fee `packet_fee`
,s.pharmacy_huohao  `packet_content`
,1 `packet_stock`
,s.drugstore_id  `drugstore_id`
,NOW() `packet_update_time`
,NOW() `packet_create_time`
,'1905月活动' `packet_remark`
,s.sku_id
,s.sku_img `pre_prod_name`  
,'single' packet_coures
-- ,s.*
FROM pm_prod_sku s
WHERE s.sku_remark = '1905月活动' and s.sku_create_time BETWEEN '2019-04-30 14:37:20' and '2019-05-01'
;




-- ty 13
-- INSERT INTO `medstore`.`pm_packet_info` (  `packet_name`, `packet_status`, `packet_type`, `packet_price`, `packet_fee`,  `packet_content`, `packet_stock`,  `drugstore_id`, `packet_update_time`, `packet_create_time`, `packet_remark`, `sku_id`, `pre_prod_name` ) 
SELECT s.prod_name `packet_name`
,1 `packet_status`
,'course' `packet_type`
,s.sku_price `packet_price`
,s.sku_fee `packet_fee`
,s.pharmacy_huohao  `packet_content`
,1 `packet_stock`
,s.drugstore_id  `drugstore_id`
,NOW() `packet_update_time`
,NOW() `packet_create_time`
,'1905月活动-ty' `packet_remark`
,s.sku_id
,s.sku_img `pre_prod_name`  
-- ,s.*
FROM pm_prod_sku s
WHERE s.sku_remark = '1905月活动-ty'
;



-- SELECT * from am_act_range a LEFT JOIN pm_dir_info d on a.range_value = d.dir_id WHERE range_value BETWEEN 1002757035 and 1002757048;




-- 新建疗程购 bj 11
--  INSERT INTO `medstore`.`pm_packet_sku` (  `packet_id`, `sku_id`, `prod_name`, `sku_huohao`,    `sku_amount`,   `link_remark`)
-- SELECT  `packet_id`, s.`sku_id`, s.`prod_name`,s.pharmacy_huohao `sku_huohao`,a.多盒 `sku_amount`,'1905月活动'   `link_remark`
-- -- SELECT *
-- from  as_test.1905_bj_act a ,pm_prod_sku s,pm_prod_sku sn ,pm_packet_info p
-- WHERE a.康佰馨单品货号 = s.pharmacy_huohao
-- and a.康佰馨多盒货号 = sn.pharmacy_huohao
-- 
-- and  a.备注 = '需新建'
-- and sn.sku_id = p.sku_id
-- 
-- and s.drugstore_id = 1620
-- and sn.drugstore_id = 1620
-- ;
-- 




-- 新建疗程购 bj 105
--  INSERT INTO `medstore`.`pm_packet_sku` (  `packet_id`, `sku_id`, `prod_name`, `sku_huohao`,    `sku_amount`,   `link_remark`)
SELECT  `packet_id`, s.`sku_id`, s.`prod_name`,s.pharmacy_huohao `sku_huohao`,a.多盒 `sku_amount`,'1905月活动'   `link_remark`
-- SELECT *
from  as_test.1905_bj_act a ,pm_prod_sku s,pm_prod_sku sn ,pm_packet_info p
WHERE a.正济堂单品货号 = s.pharmacy_huohao
and a.正济堂多盒货号 = sn.pharmacy_huohao

and  a.备注 = '需新建'   and a.类目 = '高血脂' and a.序号 BETWEEN 40 and 43
and sn.sku_id = p.sku_id

and s.drugstore_id in   (1610,1611,1612)
and sn.drugstore_id = s.drugstore_id
;




-- 新建疗程购 bj 105
--  INSERT INTO `medstore`.`pm_packet_sku` (  `packet_id`, `sku_id`, `prod_name`, `sku_huohao`,    `sku_amount`,   `link_remark`)
SELECT  `packet_id`, s.`sku_id`, s.`prod_name`,s.pharmacy_huohao `sku_huohao`,a.多盒 `sku_amount`,'1905月活动'   `link_remark`
-- SELECT *
from  as_test.1905_bj_act a ,pm_prod_sku s,pm_prod_sku sn ,pm_packet_info p
WHERE a.康佰馨单品货号 = s.pharmacy_huohao
and a.康佰馨多盒货号 = sn.pharmacy_huohao
 
and  a.备注 = '需新建'   and a.类目 = '高血脂' and a.序号 BETWEEN 40 and 43
and sn.sku_id = p.sku_id

and s.drugstore_id =1620 -- in   ( 1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
and sn.drugstore_id = s.drugstore_id
;



-- 新建疗程购 ty 13
--  INSERT INTO `medstore`.`pm_packet_sku` (  `packet_id`, `sku_id`, `prod_name`, `sku_huohao`,    `sku_amount`,   `link_remark`)
SELECT  `packet_id`, s.`sku_id`, s.`prod_name`,s.pharmacy_huohao `sku_huohao`,a.多盒 `sku_amount`,'1905月活动-ty'   `link_remark`
-- SELECT *
from  as_test.1905_ty_act a ,pm_prod_sku s,pm_prod_sku sn ,pm_packet_info p
WHERE a.单盒货号 = s.pharmacy_huohao
and a.多盒货号 = sn.pharmacy_huohao

and  a.备注 = '需新建'
and sn.sku_id = p.sku_id

and s.drugstore_id = 200
and sn.drugstore_id = 200

ORDER BY a.序号
;
 


-- SELECT * from pm_packet_info ORDER BY packet_id DESC;
-- 
-- SELECT * from pm_packet_sku ORDER BY link_id DESC;
-- SELECT * 
-- FROM pm_prod_sku
-- WHERE sku_id = 25550420
-- SELECT * from pm_prod_sku WHERE pharmacy_huohao = '1627753';
-- 
-- SELECT * from pm_sku_sale ORDER BY sale_id desc limit 1000;
-- SELECT * from am_stat_info
-- WHERE item_id = 1438
-- GROUP BY item_id 
-- SELECT * from am_act_item


-- 5月活动特价的商品配置价格 活动期价的
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
SELECT s.sku_id,a.多盒套装原组合价*100 sale_fee,a.活动期间多盒销售价*100 sale_price
,'2018-05-01 00:00:00' sale_start_time
,'2019-05-31 23:59:59' sale_end_time
,1 `sale_status`,NOW() `sale_create_time`,NOW()  `sale_update_time`,'1905月活动 多盒特价' `sale_remark`
-- SELECT  *
 from as_test.1905_bj_act as a ,pm_prod_sku as s 
WHERE a.康佰馨多盒货号 = s.pharmacy_huohao

and s.drugstore_id  = 1620
and  a.备注 = '特价'
ORDER BY a.序号
;




-- 5月活动特价的商品配置价格 活动期价的 ty
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
SELECT s.sku_id,a.多盒套装原组合价*100 sale_fee,a.活动期间多盒销售价*100 sale_price
,'2018-05-01 00:00:00' sale_start_time
,'2019-05-31 23:59:59' sale_end_time
,1 `sale_status`,NOW() `sale_create_time`,NOW()  `sale_update_time`,'1905月活动 多盒特价-ty' `sale_remark`
-- SELECT  *
 from as_test.1905_ty_act as a ,pm_prod_sku as s 
WHERE a.多盒货号 = s.pharmacy_huohao

and s.drugstore_id  = 200
and  a.备注 = '特价'
ORDER BY a.序号
;

SELECT * FROM sm_image_link WHERE drugstore_id = 1620;

SELECT * from am_act_info ORDER BY act_id DESC;


-- pm_dir_info build
-- 创建活动目录 zsc  dir_code dir_name img  dir_all_name
-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'194') AS dir_code 
, CONCAT('1905月活动北京--',pharmacy_id) AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum
, 100 AS dir_num,'2' AS dir_level
,  'http://image.ykd365.cn/act/1905/bj_02.jpg' dir_img, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'#ffb0c5' dir_remark,CONCAT(dir_all_name	,'--2019-5月活动') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 1000034627 AND 1000034639  or dir_id = 1000034601;



-- SELECT * from pm_dir_info ORDER BY dir_id DESC limit 300;
-- 1002757623	ykd21061013act194	1905月活动北京--1633
-- 1002757622	ykd21061012act194	1905月活动北京--1632
-- 1002757621	ykd21061011act194	1905月活动北京--1631
-- 1002757620	ykd21061010act194	1905月活动北京--1630
-- 1002757619	ykd21061009act194	1905月活动北京--1629
-- 1002757618	ykd21061007act194	1905月活动北京--1627
-- 1002757617	ykd21061004act194	1905月活动北京--1623
-- 1002757616	ykd21061003act194	1905月活动北京--1622
-- 1002757615	ykd21061002act194	1905月活动北京--1621
-- 1002757614	ykd21061001act194	1905月活动北京--1620
-- 1002757613	ykd21051003act194	1905月活动北京--1612
-- 1002757612	ykd21051002act194	1905月活动北京--1611
-- 1002757611	ykd21051001act194	1905月活动北京--1610
-- 1002757610	ykd20001001act194	1905月活动北京--200


-- INSERT INTO `medstore`.`pm_dir_info` (  `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`)
--  VALUES ('1002757598', '16402019dmss19', '非药品', 'dir', '1', NULL, '100', '19', '1', NULL, NULL, '2019-01-11 21:18:06', '2019-01-11 21:18:06', '2019多买少算 bj', NULL, NULL, '1640', NULL, NULL);
SELECT CONCAT(d.dir_code,LPAD(xh,2,'0')) `dir_code`,a.类目 `dir_name`,'sdir' `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`
,xh `dir_num`
,3 `dir_level`
,CONCAT('http://image.ykd365.cn/act/1905/bj_',LPAD(xh,2,'0'),'.jpg') `dir_img`,d.dir_id `parent_dir_id`, `dir_update_time`, `dir_create_time`
, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`
-- SELECT a.*,@i:=@i+1
from ( SELECT a.*,@i:=@i+1 as xh from 
(SELECT a.*   from as_test.1905_bj_act a
GROUP BY a.类目
ORDER BY a.序号
) a  ,(SELECT @i:=3) b) a,pm_dir_info d
WHERE d.dir_code like '%act194'
;

SELECT * FROM `pm_packet_info` WHERE `packet_id` = 6511;
SELECT  @i:=@i+1 from  sm_config  a,( SELECT   @i:=0) b 

-- 优惠的图 
-- INSERT INTO `medstore`.`pm_dir_info` (  `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`)
--  VALUES ('1002757598', '16402019dmss19', '非药品', 'dir', '1', NULL, '100', '19', '1', NULL, NULL, '2019-01-11 21:18:06', '2019-01-11 21:18:06', '2019多买少算 bj', NULL, NULL, '1640', NULL, NULL);
SELECT CONCAT(d.dir_code,'03') `dir_code`,'' `dir_name`,'sdir' `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`
,3 `dir_num`
,3 `dir_level`
,CONCAT('http://image.ykd365.cn/act/1905/bj_03.jpg') `dir_img`,d.dir_id `parent_dir_id`, `dir_update_time`, `dir_create_time`
, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`
-- SELECT *
from 
pm_dir_info d  
WHERE d.dir_code like '%act194' and dir_level =2
;





-- 目录和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT d.dir_id,s.sku_id,d.dir_code,a.序号,NOW() 
-- SELECT *
  from  as_test.1905_bj_act a,  pm_prod_sku s,pm_dir_info d
WHERE   a.康佰馨单品货号 = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)  
and d.dir_name = a.类目 and  d.dir_code like '%act194%'
and s.drugstore_id = d.pharmacy_id
 
;


-- 目录和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT d.dir_id,s.sku_id,d.dir_code,a.序号,NOW() 
-- SELECT *
  from  as_test.1905_bj_act a,  pm_prod_sku s,pm_dir_info d
WHERE   a.正济堂单品货号 = s.pharmacy_huohao  and s.drugstore_id   in   (1610,1611,1612)
and d.dir_name = a.类目 and  d.dir_code like '%act194%'
and s.drugstore_id = d.pharmacy_id
 
;

-- 目录和商品的关联 ty 60
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT d.dir_id,s.sku_id,d.dir_code,a.序号,NOW() 
-- ,d.*
-- SELECT *
  from  as_test.1905_ty_act a,  pm_prod_sku s,pm_dir_info d
WHERE   a.单盒货号 = s.pharmacy_huohao  and s.drugstore_id =200 --  in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)  
and d.dir_name = a.类目 and  d.dir_code like '%act194%'
and s.drugstore_id = d.pharmacy_id
 
ORDER BY a.xh
;

-- 过敏性鼻炎

SELECT s.* from pm_sku_dir sd,pm_prod_sku s WHERE 
sd.sku_id = s.sku_id and
dir_code like 'ykd20001001act194%'  ;

SELECT * from pm_packet_info WHERE packet_id = 56;

SELECT * from pm_packet_sku WHERE sku_id = 101812;

SELECT * from pm_prod_sku WHERE pharmacy_huohao = '4件装7000245';

-- DELETE from pm_sku_dir WHERE dir_code like 'ykd20001001act194%' 
SELECT * from pm_sku_dir WHERE dir_code like 'ykd20001001act194%'  ;
SELECT * from pm_dir_info WHERE dir_code like 'ykd20001001act194%'  ;
SELECT * from pm_dir_info WHERE dir_code like '%act194%'  ;

-- 目录和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT d.dir_id,s.sku_id,d.dir_code,a.序号,NOW() 
-- SELECT *
  from  as_test.1905_bj_act a,  pm_prod_sku s,pm_dir_info d
WHERE   a.康佰馨单品货号 = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)  
and d.dir_name = a.类目 and  d.dir_code like '%act194%'
and s.drugstore_id = d.pharmacy_id  and a.类目 = '高血脂' and a.序号 BETWEEN 40 and 43
 
;


SELECT * FROM pm_sku_dir sd ,pm_prod_sku s
WHERE sd.sku_id = s.sku_id 
and s.drugstore_id in  (1610,1611,1612) ORDER BY link_id desc;


-- 目录和商品的关联 bj 533
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT d.dir_id,s.sku_id,d.dir_code,a.序号,NOW() 
-- SELECT *
  from  as_test.1905_bj_act a,  pm_prod_sku s,pm_dir_info d
WHERE --  a.康佰馨单品货号 = s.pharmacy_huohao  and s.drugstore_id  in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)  
a.正济堂单品货号 = s.pharmacy_huohao and s.drugstore_id    in  (1610,1611,1612)
and d.dir_name = a.类目 and  d.dir_code like '%act194%'
and s.drugstore_id = d.pharmacy_id  and a.类目 = '高血脂' and a.序号 BETWEEN 40 and 43
 
;

SELECT * from sm_config;

SELECT * from pm_dir_info  ORDER BY dir_id DESC;

SELECT * from pm_dir_info WHERE dir_code like '%act194%' and pharmacy_id = 1620;

SELECT * from am_stat_info GROUP BY item_id ;

SELECT * from am_act_info ORDER BY act_id DESC;

SELECT * from am_act_item 
--     GROUP BY item_type
ORDER BY item_id DESC;

SELECT * from am_act_range ORDER BY range_id DESC;

SELECT * from am_item_range ORDER BY link_id desc;

--  INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`) VALUES ('194', '北京19年5月药品福利管理计划', 'date', '1', '活动', '2019-04-22 16:55:28', '2019-04-22 16:56:22', '2019-05-01 00:00:00', '2019-05-01 23:59:59', '1', NULL, NULL, NULL, '1620');

-- 配置限购，搞出来一票range
-- INSERT INTO `medstore`.`am_act_range` (  `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) 
-- VALUES ('1388', 'category', '1001000651', '1', '分类', '95折', '2018-10-23 16:23:38', '2018-10-23 16:21:28');
SELECT 'category',dir_id, '1',CONCAT(a.pharmacy_id,'-',a.dir_name) dir_name, dir_code,NOW(),now() -- ,a.*
-- SELECT *
 from pm_dir_info a
WHERE dir_code like '%act194%' 
 
and dir_type = 'sdir'

-- and dir_name = 'top50'
;



-- 主会场对应
-- INSERT INTO `medstore`.`am_item_range` (  `range_id`, `item_id`) 
SELECT b.range_id,a.item_id
--  ,a.*,b.* 
FROM am_act_item a,am_act_range b
WHERE -- a.act_id = 193
a.item_id =1438
and b.range_id BETWEEN 2227 and  2422   
;

-- *****************************88


-- INSERT INTO `medstore`.`pm_label_image` ( `label_name`, `label_url`, `label_url_double`, `label_type`, `label_status`, `label_create_time`, `label_update_time`, `pharmacy_id`, `label_flag`, `sku_id`) 
-- VALUES ('173326', '年货节', 'http://image.ykd365.cn/act/190101/lb_icon.png', 'http://image.ykd365.cn/act/190101/lb_icon.png', '1', '0', '2019-02-11 22:35:05', '2019-02-11 22:35:05', '1633', '0', '25513371');
SELECT  '药品福利管理计划' `label_name`
,'http://image.ykd365.cn/act/1905/1905_list.png' `label_url`
,'http://image.ykd365.cn/act/1905/1905_list.png' `label_url_double`
,1 `label_type`
,1 `label_status`
,NOW() `label_create_time`
,NOW() `label_update_time`
,drugstore_id `pharmacy_id`
,0 `label_flag`
, `sku_id`
-- SELECT *
FROM as_test.1905_bj_act a ,pm_prod_sku as s 
WHERE a.康佰馨单品货号 = s.pharmacy_huohao
and s.drugstore_id  = 1620

-- ORDER BY a.序号
;

-- INSERT INTO `medstore`.`pm_label_image` ( `label_name`, `label_url`, `label_url_double`, `label_type`, `label_status`, `label_create_time`, `label_update_time`, `pharmacy_id`, `label_flag`, `sku_id`) 
-- VALUES ('173326', '年货节', 'http://image.ykd365.cn/act/190101/lb_icon.png', 'http://image.ykd365.cn/act/190101/lb_icon.png', '1', '0', '2019-02-11 22:35:05', '2019-02-11 22:35:05', '1633', '0', '25513371');
SELECT  '药品福利管理计划' `label_name`
,'http://image.ykd365.cn/act/1905/1905_list.png' `label_url`
,'http://image.ykd365.cn/act/1905/1905_list.png' `label_url_double`
,1 `label_type`
,1 `label_status`
,NOW() `label_create_time`
,NOW() `label_update_time`
,drugstore_id `pharmacy_id`
,0 `label_flag`
, `sku_id`
-- SELECT *
FROM as_test.1905_bj_act a ,pm_prod_sku as s 
WHERE a.康佰馨多盒货号 = s.pharmacy_huohao
and s.drugstore_id  = 1620

-- ORDER BY a.序号
;

-- INSERT INTO `medstore`.`pm_label_image` ( `label_name`, `label_url`, `label_url_double`, `label_type`, `label_status`, `label_create_time`, `label_update_time`, `pharmacy_id`, `label_flag`, `sku_id`) 
-- VALUES ('173326', '年货节', 'http://image.ykd365.cn/act/190101/lb_icon.png', 'http://image.ykd365.cn/act/190101/lb_icon.png', '1', '0', '2019-02-11 22:35:05', '2019-02-11 22:35:05', '1633', '0', '25513371');
SELECT  '药品福利管理计划' `label_name`
,'http://image.ykd365.cn/act/1905/1905_list.png' `label_url`
,'http://image.ykd365.cn/act/1905/1905_list.png' `label_url_double`
,1 `label_type`
,1 `label_status`
,NOW() `label_create_time`
,NOW() `label_update_time`
,drugstore_id `pharmacy_id`
,0 `label_flag`
, `sku_id`
-- SELECT *
FROM as_test.1905_ty_act a ,pm_prod_sku as s 
WHERE a.单盒货号 = s.pharmacy_huohao
and s.drugstore_id  = 200

-- ORDER BY a.序号
;


-- INSERT INTO `medstore`.`pm_label_image` ( `label_name`, `label_url`, `label_url_double`, `label_type`, `label_status`, `label_create_time`, `label_update_time`, `pharmacy_id`, `label_flag`, `sku_id`) 
-- VALUES ('173326', '年货节', 'http://image.ykd365.cn/act/190101/lb_icon.png', 'http://image.ykd365.cn/act/190101/lb_icon.png', '1', '0', '2019-02-11 22:35:05', '2019-02-11 22:35:05', '1633', '0', '25513371');
SELECT  '药品福利管理计划' `label_name`
,'http://image.ykd365.cn/act/1905/1905_list.png' `label_url`
,'http://image.ykd365.cn/act/1905/1905_list.png' `label_url_double`
,1 `label_type`
,1 `label_status`
,NOW() `label_create_time`
,NOW() `label_update_time`
,drugstore_id `pharmacy_id`
,0 `label_flag`
, `sku_id`
-- SELECT *
FROM as_test.1905_ty_act a ,pm_prod_sku as s 
WHERE a.多盒货号 = s.pharmacy_huohao
and s.drugstore_id  = 200

-- ORDER BY a.序号
;


 

SELECT * from pm_label_image WHERE label_name = '药品福利管理计划';

SELECT * from pm_dir_info ORDER BY dir_id DESC;
SELECT *
  FROM `sm_image_link`
 ORDER BY link_id DESC ;
SELECT * from sm_image_link WHERE drugstore_id = 1620 ORDER BY link_id DESC;

-- 轮播和通栏  
--   tl
-- INSERT INTO `medstore`.`sm_image_link` (`drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`)
--  VALUES ('5835', '1620', '1', 'http://image.ykd365.cn/icon/act/ashacker/181201/12_tl.gif', 'http://store.ykd365.com/medstore/actUserpage/OneYuan_module_1811?dirId=1001001305', 'web2', '入冬滋补月', '1', '1', '2018-12-08 00:01:00', '2018-12-06 18:30:54', '{\"aspect_ratio\":260,\"image_seat\":\"button\",\"having_line\":0,\"image_seat\":1}', '2018-12-08 00:00:00', '2018-12-31 23:59:59', 'activity', '1');
SELECT a.pharmacy_id
,1 `seq_num`
,'http://image.ykd365.cn/act/1905/1905_tl.jpg' `image_url`
,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1904?pageSize=1000&dirId=',a.dir_id) `link_url`
,'web2' `link_type`
,'药品福利管理计划' `link_name`
,a.dir_id `link_param`
,1 `link_status`
,NOW() `link_update_time`
,NOW() `link_create_time`
,'{"aspect_ratio":403,"image_seat":"button","having_line":0,"image_seat":1}' `link_remark`
,'2018-05-01 00:00:00' `link_start_time`
,'2019-05-31 23:59:59' `link_end_time`
,'activity' `link_view`,'1' `link_version`
  from  pm_dir_info a 
WHERE a.dir_id BETWEEN  1002757610 and 1002757623 ;  

 


-- INSERT INTO `medstore`.`sm_image_link_window` ( `drugstore_id`, `seq_num`, `image_url`, `window_url`, `window_type`, `window_name`, `window_param`, `window_view`, `window_status`, `window_update_time`, `window_create_time`, `window_remark`, `window_start_time`, `window_end_time`, `window_go_type`) 
-- VALUES ('554', '1635', '1', 'http://imgstore.camore.cn/icon/act/ashacker/180301/act32_tc.png', 'http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1803?&showName=北京&pageSize=54&dirId=1000035782', 'furl', '暖春惊喜 神秘红包', '738', '', '1', '2018-03-13 18:08:27', '2017-12-29 10:51:52', '3月招财猫', '2018-03-02 00:00:00', '2018-04-01 00:00:14', '0');
SELECT  -- a.*,
a.pharmacy_id
,1 as seq_num
,'http://image.ykd365.cn/act/1905/1905_tc.png' as image_url
,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1904?pageSize=1000&dirId=',a.dir_id) as window_url
,'everyday' as window_type
,'药品福利管理计划' as window_name
,NULL as window_param
,'' as window_view
,'1' as window_status
,NOW() as window_update_time
,NOW() as window_create_time
,'1905月 药品福利管理计划' as window_remark
,'2018-04-01 00:00:00' as `window_start_time`
,'2019-04-30 23:59:59'  as `window_end_time`
,'0' as  `window_go_type`
 from  pm_dir_info a 
WHERE a.dir_id BETWEEN   1002757610 and 1002757623 ;  

SELECT * from sm_image_link_window ORDER BY window_id desc;



-- **

SELECT * FROM pm_prod_sku WHERE prod_name like '%激情热感情趣%' and drugstore_id = 1620 and sku_status =1;


SELECT * from am_stat_info WHERE sku_id =  11621344;

SELECT * from am_stat_info WHERE other_str1 is not NULL;
-- ***********************

--  `xh`, `act_name`, `act_type`, `act_desc`, `act_remark`, `old_sku_id`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `old_huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_code`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`
-- INSERT INTO `as_test`.`1905_act_temp` (  `xh`, `act_name`, `act_type`, `act_desc`, `act_remark`, `old_sku_id`, `sku_id`, `prod_id`, `drugstore_id`, `prod_name`, `sku_json`, `pharmacy_huohao`, `old_huohao`, `fee`, `price`, `start_time`, `end_time`, `act_id`, `item_id`, `quota_id`, `dir_code`, `dir_id`, `limit_count`, `limit_stock`, `limit_stock_day`)
-- SELECT * from as_test.1905_bj_act as a,pm_prod_sku as s
-- WHERE a.康佰馨单品货号 = s.pharmacy_huohao and s.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
-- 
-- ORDER BY a.序号
--  ;




-- INSERT INTO pm_prod_sku (  `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`, `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`)
--  SELECT  *  from as_test.1905_bj_act as a,pm_prod_sku as s
-- WHERE a.康佰馨单品货号 = s.pharmacy_huohao and s.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
-- and a.备注 = '需新建'
-- ORDER BY a.序号
--  ;



-- (1610,1611,1612)4件装101051

SELECT * from pm_prod_sku WHERE  pharmacy_huohao = '4件装101051';

SELECT * from pm_prod_sku WHERE  pharmacy_huohao = '101051';


SELECT * from pm_prod_sku WHERE  sku_id = 10118175;

SELECT * from pm_sku_dir WHERE sku_id = 10118175;
SELECT * from pm_packet_sku  WHERE sku_id = 10118175;

SELECT * from pm_prod_sku WHERE drugstore_id = 1620 order by sku_id DESC;

SELECT * from pm_packet_sku ORDER BY link_id DESC;


SELECT * from pm_packet_info ORDER BY packet_id desc;

SELECT * from pm_dir_info WHERE dir_code like '%act194%';

SELECT * from pm_sku_dir WHERE sku_id = 20114622;-- dir_code = 'ykd21061013act19408'; -- 1002758083

SELECT * from pm_packet_sku WHERE sku_id = 20114622;


SELECT * from pm_packet_info WHERE  packet_id in (16708,16718);

SELECT * from pm_packet_info GROUP BY sku_id HAVING COUNT(sku_id)>1

SELECT * from pm_packet_info  WHERE packet_name like '%碳酸钙D3%';

SELECT * from pm_packet_sku WHERE prod_name like '%碳酸钙D3%';

SELECT * from pm_packet_sku ORDER BY link_id DESC;

SELECT * from sm_image_link WHERE drugstore_id = 1641;
-- GROUP BY sku_id

SELECT * from pm_label_image
GROUP BY label_url
 ORDER BY label_id desc limit 1000
;




-- 天一对接erp 需加几个字段
-- 	private int isSet; //是否为套装 1,是， 0 不是
-- 	private int setNum; //套装包含单品的数量
-- 	private int disBeforePrice;// 单品折前价格  单位均为分
-- 	private int disAfterPrice; //单品折后价格  单位均为分
-- 	private int discountPrice;//单品优惠的价格  单位均为分

SELECT * from as_test.190501_ty_5acterp;

SELECT * from as_test.190501_ty_dherp;
-- INSERT INTO `as_test`.`190501_ty_5acterp` (`类目`, `序号`, `单品ID`, `单盒货号`, `品牌名`, `通用名`, `规格`, `生产厂家`, `单品
-- 进价`, `单品
-- 市场价`, `单品
-- 销售价`, `多盒ID`, `全名`, `多盒货号`, `前缀`, `盒数`, `疗程购
-- 单盒价`, `单盒
-- 折扣额`, `是否多盒`, `折前单价`, `折后单价`, `折价`, `套装
-- 折扣率`, `备注`, `活动期间多盒单价`, `折后单价(附加）`, `AA`, `xh`) VALUES ('高血压', '1', '101812', '7000245', '司乐平', '拉西地平片', '4mg*30片', '哈药集团三精明水药业有限公司', '28', '30', '30', '25523112', '【4件装】拉西地平片', '4件装7000245', '', '4', '29.2', '0.800000000000001', '是', '120', '116.8', '3.2', '0.973333333333333', '', '', '', '', '1');
-- INSERT INTO `as_test`.`190501_ty_dherp` (`序号`, `单盒ID`, `单盒货号`, `天一中类`, `品牌名`, `通用名`, `规格`, `生产厂家`, `进价`, `市场价`, `销售价`, `多盒ID`, `全名`, `多盒货号`, `盒数`, `疗程购单盒价`, `单盒    折扣额`, `是否多盒`, `折前单价`, `折后单价`, `折价`, `说明`, `xh`) VALUES ('25', '100299', '02099425', '心血管类', '奥吉娜', '阿司匹林肠溶片', '100mg*36片', '沈阳奥吉娜药业有限公司', '6', '15', '15', '25523540', '【2件装】奥吉娜 阿司匹林肠溶片', '2件装02099425', '2', '14', '1', '是', '30', '28', '2', '带商品图片展示', '1');

SELECT
 * from pm_prod_sku ORDER BY sku_id desc limit 1000;

--  `sku_id`, `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sum`, `sku_sort`, `sku_rank`, `sku_type`
-- , `is_set`, `set_num`, `dis_before_price`, `dis_after_price`, `discount_price`, `pre_prod_name`) VALUES ('25550725', '327484', '1', '121000', '127600', '200', '1', '2019-04-30 14:54:44', '2019-04-30 14:54:44', '1904 ty多盒常用搭配商品-新增', NULL, '22件装康健纳 心脑清软胶囊', '22件装02100550', '1', '{\"prod_spec\":\"0.415g*180粒*226902727229754\"}', NULL, '', '0', NULL, '0', '0', '0', '0', '0', 'comb', NULL, NULL, NULL, NULL, NULL, NULL);
-- UPDATE pm_prod_sku set 


-- 是否多盒 折前单价 折后单价 折价 盒数

UPDATE pm_prod_sku set is_set = IF(expr1,expr2,expr3)
-- SELECT  a.*,s.*
 from as_test.190501_ty_5acterp as a ,pm_prod_sku as s 
WHERE  a.多盒货号 = s.pharmacy_huohao and s.drugstore_id  =200
 
ORDER BY a.xh
;
 


SELECT * from pm_dir_info WHERE pharmacy_id = 200 ORDER BY dir_id DESC;

SELECT * from pm_sku_dir WHERE dir_code = 'ykd20001001act19407';


SELECT * FROM pm_label_image WHERE pharmacy_id = 200;

SELECT * FROM pm_label_image
WHERE `label_url` = 'http://image.ykd365.cn/act/1905/1905_list.png'
;

SELECT * from sm_image_link WHERE link_view = 'essence' and link_end_time = '2019-04-30 23:59:59';

SELECT * from sm_image_link  WHERE drugstore_id = 1620;

SELECT * from sm_image_link_window  WHERE drugstore_id = 1620
;

SELECT * from pm_sku_dir WHERE sku_id in (25507508,
25507504,
25504450,
25507673,
740062,
25473992
)

ORDER BY dir_id ,sku_order

;

SELECT sd.* from pm_sku_dir sd,pm_prod_sku s
WHERE sd.sku_id = s.sku_id
and s.drugstore_id = 200
and sd.dir_code like '%act194%'
and s.prod_name like '%朗洁 萘敏维%';



SELECT * from pm_sku_dir sd,pm_prod_sku s
WHERE sd.sku_id = s.sku_id
and s.drugstore_id = 200
and sd.dir_code like '%act194%'
and s.prod_name like '%肾复康%';


SELECT * from pm_sku_sale WHERE sku_id = 102366;


SELECT * FROM `pm_prod_sku` WHERE `pharmacy_huohao` = '2件装9910754'

SELECT * from pm_prod_sku WHERE sku_id = 102366;




