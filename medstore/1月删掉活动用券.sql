
-- 删掉活动限券 181
-- DELETE c  
-- SELECT * 
from as_test.`123_组合可用券_bj` a ,pm_prod_sku b ,am_stat_info c
WHERE (
    (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633))
    or
    (a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612))
    )
and b.sku_id = c.sku_id
and c.item_name = '限券'
;


-- 删掉活动限券 1304
-- DELETE c  
-- SELECT * 
from as_test.`123_多盒可用券_bj` a ,pm_prod_sku b ,am_stat_info c
WHERE (
    (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633))
    or
    (a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612))
    )
and b.sku_id = c.sku_id
and c.item_name = '限券'
;


-- 删掉目录限券  181
-- DELETE c  
-- SELECT c.* 
from as_test.`123_组合可用券_bj` a ,pm_prod_sku b ,pm_sku_dir c,pm_dir_info d
WHERE (
    (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633))
    or
    (a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612))
    )
and b.sku_id = c.sku_id
and (c.dir_id BETWEEN 1001000001 and 1001000013 or c.dir_id =2321029)
and c.dir_id = d.dir_id
;

-- 删掉目录限券 1317
-- DELETE c  
-- SELECT c.* 
from as_test.`123_多盒可用券_bj` a ,pm_prod_sku b ,pm_sku_dir c,pm_dir_info d
WHERE (
    (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633))
    or
    (a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612))
    )
and b.sku_id = c.sku_id
and (c.dir_id BETWEEN 1001000001 and 1001000013 or c.dir_id =2321029)
and c.dir_id = d.dir_id
;