
-- as_test.`1220_1元特价康佰馨`
-- as_test.`1220_1元特价正济堂`
-- as_test.`1220_吃喝应酬康佰馨`
-- as_test.`1220_吃喝应酬正济堂`
-- as_test.`1220_孝敬父母`
-- as_test.`1220_常备药`
-- as_test.`1220_更多推荐`

-- — 1610,1611,1612,1634,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633,1635
-- — 1610,1611,1612,1634 — 北京正济堂药品连锁超市(亚运村店)
-- — 1620,1621,1622,1623,1627,1629,1630,1631,1632,1633 —  北京市康佰馨大药房昆泰店
-- — 1635  北京万民阳光大药房有限公司朝阳路店
CREATE TEMPORARY TABLE tmp_yaodian14 (     
ydid VARCHAR(10) NOT NULL    
) ;    
INSERT INTO tmp_yaodian14 (ydid)
VALUES ('1610'),('1611'),('1612'),('1634'),('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633');
SELECT * from tmp_yaodian14;

CREATE TEMPORARY TABLE tmp_kbx (     
ydid VARCHAR(10) NOT NULL    
);     
INSERT INTO tmp_kbx (ydid)
VALUES ('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633');
SELECT * from tmp_kbx;

CREATE TEMPORARY TABLE tmp_zjt (     
ydid VARCHAR(10) NOT NULL    
);     
INSERT INTO tmp_zjt (ydid)
VALUES ('1610'),('1611'),('1612'),('1634');
SELECT * from tmp_zjt;



-- 
-- SELECT * from  as_test.`1220_吃喝应酬康佰馨` a LEFT JOIN pm_prod_info b on a.`药快到ID` = b.prod_id 
-- WHERE prod_id is  null


-- 增加1元商品
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
from 
-- `as_test`.`1220_1元特价正济堂` 
`as_test`.`1220_1元特价康佰馨`
a LEFT JOIN pm_prod_info i on  a.`prod_id` =  i.prod_id  LEFT JOIN tmp_kbx c  on 1=1
WHERE a.`序号` != '' and a.`序号` != 0
and not EXISTS(SELECT * from pm_prod_sku s WHERE drugstore_id = c.ydid and s.prod_id = a.`prod_id` and s.pharmacy_huohao = CONCAT('1YYY',a.`huohao`))
;

-- 增加1元商品
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
-- SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
SELECT  i.prod_id,'0' as sku_status,'100' as sku_price,a.`原价`*100 as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
,a.*
from 
 `as_test`.`1220_1元特价正济堂` 
-- `as_test`.`1220_1元特价康佰馨`
a LEFT JOIN pm_prod_info i on  a.`prod_id` =  i.prod_id  LEFT JOIN tmp_zjt c  on 1=1
WHERE a.`序号` != '' and a.`序号` != 0
and not EXISTS(SELECT * from pm_prod_sku s WHERE drugstore_id = c.ydid and s.prod_id = a.`prod_id` -- and s.pharmacy_huohao = CONCAT('1YYY',a.`huohao`)
)
;




-----------
-- -- 增加吃喝应酬康佰馨
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');

-- SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 吃喝应酬' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
-- from 
--  `as_test`.`1220_吃喝应酬康佰馨` 
-- a LEFT JOIN pm_prod_info i on  a.prod_id =  i.prod_id  LEFT JOIN tmp_kbx c  on 1=1
-- WHERE a.`序号` != ''
-- and not EXISTS(SELECT * from pm_prod_sku s WHERE drugstore_id = c.ydid and s.prod_id = a.prod_id and s.pharmacy_huohao = CONCAT('1YYY',a.`huohao`))
-- ;

-- -- 增加吃喝应酬正济堂
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
-- SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 吃喝应酬' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
-- from 
--  `as_test`.`1220_吃喝应酬正济堂` 
-- a LEFT JOIN pm_prod_info i on  a.prod_id =  i.prod_id  LEFT JOIN tmp_zjt c  on 1=1
-- WHERE a.`序号` != ''
-- and not EXISTS(SELECT * from pm_prod_sku s WHERE drugstore_id = c.ydid and s.prod_id = a.prod_id and s.pharmacy_huohao = CONCAT('1YYY',a.`huohao`))
-- ;

---------------
-- 新建吃喝应酬 Prod info 


-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`)
-- VALUES ('100000', '神威 复方三维亚油酸胶丸Ⅰ', '神威', '1', '6938007000652', '国药准字H13023694', '550', '5', '神威药业集团有限公司', NULL, '2017-09-11 13:28:26', '2017-09-11 13:28:26', '2', '100粒', '瓶', '复方三维亚油酸胶丸Ⅰ', '1', '15303', '用于动脉粥样硬化的辅助治疗和预防。', '1', NULL);
SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`,'2017-12-20 00:00:00' `prod_create_time`, `prod_type`,CASE when locate('特价',zz.zh)<=0 then CONCAT(zz.prod_spec,'*',SUBSTR(zz.zh,1,2 )) else zz.prod_spec END as  `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
from (
SELECT a.cxjg,a.zh,b.*  
from as_test.`1220_吃喝应酬康佰馨` as a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
WHERE locate('组合',a.zh)>0
) zz 

-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`)
-- VALUES ('100000', '神威 复方三维亚油酸胶丸Ⅰ', '神威', '1', '6938007000652', '国药准字H13023694', '550', '5', '神威药业集团有限公司', NULL, '2017-09-11 13:28:26', '2017-09-11 13:28:26', '2', '100粒', '瓶', '复方三维亚油酸胶丸Ⅰ', '1', '15303', '用于动脉粥样硬化的辅助治疗和预防。', '1', NULL);
SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`,CASE when locate('特价',zz.zh)<=0 then CONCAT(zz.prod_spec,'*',SUBSTR(zz.zh,1,2 )) else zz.prod_spec END as  `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
from (
SELECT a.cxjg,a.zh,b.* 
from as_test.`1220_吃喝应酬正济堂` as a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
) zz

-- LIMIT 1


--------------

---------------
-- 新建孝敬父母 Prod info 


-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`)
-- VALUES ('100000', '神威 复方三维亚油酸胶丸Ⅰ', '神威', '1', '6938007000652', '国药准字H13023694', '550', '5', '神威药业集团有限公司', NULL, '2017-09-11 13:28:26', '2017-09-11 13:28:26', '2', '100粒', '瓶', '复方三维亚油酸胶丸Ⅰ', '1', '15303', '用于动脉粥样硬化的辅助治疗和预防。', '1', NULL);
SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`,CASE when locate('特价',zz.zh)<=0 then CONCAT(zz.prod_spec,'*',SUBSTR(zz.zh,1,2 )) else zz.prod_spec END as  `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
from (
SELECT a.cxjg,a.zh,b.*  
from as_test.`1220_孝敬父母` as a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
) zz 


-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`)
-- VALUES ('100000', '神威 复方三维亚油酸胶丸Ⅰ', '神威', '1', '6938007000652', '国药准字H13023694', '550', '5', '神威药业集团有限公司', NULL, '2017-09-11 13:28:26', '2017-09-11 13:28:26', '2', '100粒', '瓶', '复方三维亚油酸胶丸Ⅰ', '1', '15303', '用于动脉粥样硬化的辅助治疗和预防。', '1', NULL);
SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`,CASE when locate('特价',zz.zh)<=0 then CONCAT(zz.prod_spec,'*',SUBSTR(zz.zh,1,2 )) else zz.prod_spec END as  `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
from (
SELECT a.cxjg,a.zh,b.* 
from as_test.`1220_更多推荐` as a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
) zz

-- LIMIT 1
-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`)
-- VALUES ('100000', '神威 复方三维亚油酸胶丸Ⅰ', '神威', '1', '6938007000652', '国药准字H13023694', '550', '5', '神威药业集团有限公司', NULL, '2017-09-11 13:28:26', '2017-09-11 13:28:26', '2', '100粒', '瓶', '复方三维亚油酸胶丸Ⅰ', '1', '15303', '用于动脉粥样硬化的辅助治疗和预防。', '1', NULL);
SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`,CASE when locate('特价',zz.zh)<=0 then CONCAT(zz.prod_spec,'*',SUBSTR(zz.zh,1,2 )) else zz.prod_spec END as  `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
from (
SELECT a.cxjg,a.zh,b.* 
from as_test.`1220_常备药` as a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
) zz
--------------

-----------------------------



SELECT * from as_test.`1220_1元特价康佰馨` b  LEFT JOIN pm_prod_info a
on  a.prod_id = b.`商品ID` 

SELECT * from pm_prod_sku

-- and a.prod_name LIKE '%创口贴%医用消毒棉球%'

SELECT * from pm_prod_info ORDER BY prod_id DESC
SELECT * from pm_prod_sku 
WHERE prod_id in ( 322939,322940)

SELECT * from  pm_prod_info a
 where  a.prod_name LIKE '%创口贴%医用消毒棉球%'
-- where  a.prod_name LIKE '%纸棒棉签%医用消毒棉球%'
-- 322940	纸棒棉签 + 海氏海诺 (酒精)医用消毒棉球 
-- 	康顾多	1			880	5	莆田市大明轻工制品有限公司		2017-09-29 15:28:23	2017-09-29 15:27:30	2	50支 + 25枚	盒	纸棒棉签 + (酒精)医用消毒棉球				1	
-- 322939	创口贴（PE组合装 )+ 医用消毒棉球	康顾多	1	6934897702539	浙温食药监械（准）字2013第1640007号	1490	5	浙江康力迪医疗用品有限公司		2017-10-12 13:21:40	2017-08-15 09:12:02	2	25片	盒	创口贴（PE透明防水组合) + (碘伏)医用消毒棉球				1	
SELECT * from as_test.`1220_1元特价康佰馨` b 


-- INSERT INTO `as_test`.`1220_1元特价正济堂` (`序号`, `商品ID`, `正济堂`, `品牌名`, `通用名称`, `规格`, `生产企业`, `成本价`, `原价`, `活动价`, `单次可购买数量`, `备注`)
 VALUES ('21', '322940', '', '康顾多', '纸棒棉签 + 海氏海诺 (酒精)医用消毒棉球', '50支 + 25枚', '莆田市大明轻工制品有限公司', '0', '0', '1元', '组合1组', '');

-- 322940	纸棒棉签 + 海氏海诺 (酒精)医用消毒棉球 
-- 	康顾多	1			880	5	莆田市大明轻工制品有限公司		2017-09-29 15:28:23	2017-09-29 15:27:30	2	50支 + 25枚	盒	纸棒棉签 + (酒精)医用消毒棉球				1	

delimiter $$
drop procedure if exists lopp;
create procedure lopp()
begin 
declare i int ;
set i = 1;

lp1 : LOOP　　　　　　　　　　　　　
	INSERT INTO `as_test`.`1220_1元特价康佰馨` (`序号`, `商品ID`, `正济堂`, `品牌名`, `通用名称`, `规格`, `生产企业`, `成本价`, `原价`, `活动价`, `单次可购买数量`, `备注`)
 VALUES ('25', '322940', '', '康顾多', '纸棒棉签 + 海氏海诺 (酒精)医用消毒棉球', '50支 + 25枚', '莆田市大明轻工制品有限公司', '0', '0', '1元', '组合1组', '');

    set i = i+1;
    
    if i > 30 then
leave lp1;　　　　　　　　　　　　
    end if;
end LOOP;　　　　　　　　　　　　　
end $$


— 1610,1611,1612,1634,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633,1635
— 1610,1611,1612,1634 — 北京正济堂药品连锁超市(亚运村店)
— 1620,1621,1622,1623,1627,1629,1630,1631,1632,1633 —  北京市康佰馨大药房昆泰店

delimiter $$
drop procedure if exists wk;
create procedure wk()
begin
declare i int;
set i = 1;
set @arr = '1610,1611';
while i < 11 do
	INSERT INTO `as_test`.`1220_1元特价正济堂` (`序号`, `商品ID`, `正济堂`, `品牌名`, `通用名称`, `规格`, `生产企业`, `成本价`, `原价`, `活动价`, `单次可购买数量`, `备注`)
 VALUES ('25', arr[i];
, '', '康顾多', '纸棒棉签 + 海氏海诺 (酒精)医用消毒棉球', '50支 + 25枚', '莆田市大明轻工制品有限公司', '0', '0', '1元', '组合1组', '');

set i = i +1;
end while;
end $$

delimiter ;
call wk();



CREATE TEMPORARY TABLE tmp_yaodian (     
ydid VARCHAR(10) NOT NULL    
);     

INSERT INTO tmp_yaodian (ydid)
VALUES ('1610'),('1611'),('1612'),('1634'),('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633');

SELECT * from tmp_yaodian;

-- 增加1元商品
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,'1620' as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,a.`正济堂` as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
,c.ydid
 from `as_test`.`1220_1元特价正济堂` a LEFT JOIN pm_prod_info i on  a.`商品ID` =  i.prod_id  LEFT JOIN tmp_yaodian c  on 1=1
WHERE a.`序号` != ''


SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,'1620' as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,a.`正济堂` as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
,c.ydid
 from `as_test`.`1220_1元特价正济堂` a LEFT JOIN pm_prod_info i on  a.`商品ID` =  i.prod_id  , tmp_yaodian c -- on 1=1
WHERE a.`序号` != ''
-- insert into `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
 
SELECT  a.*,i.*-- i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,'1620' as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,a.`正济堂` as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null,'','' 
from `as_test`.`1220_1元特价正济堂` a LEFT JOIN pm_prod_sku i on  a.`商品ID` =  i.prod_id 
WHERE a.`序号` in (21,22) -- != ''


SELECT * from pm_prod_sku WHERE pharmacy_huohao like '%10YYY%' and  drugstore_id = 1620
-- INSERT INTO ``.`` (`prod_id`, `sku_status`, `prod_price`, `prod_price`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `prod_logist`, `prod_name`, `正济堂`, `1`, `concat('{\"prod_spec\":\"',i.prod_spec,'\"}')`, `NULL`, ``, ``) VALUES ('322940', '0', '880', '880', '1620', '1', '2017-12-21 11:32:34', '2017-12-21 11:32:34', '1220 1元商品', NULL, '纸棒棉签 + 海氏海诺 (酒精)医用消毒棉球 \r\n', '', '1', '{\"prod_spec\":\"50支 + 25枚\"}', NULL, '', '');
-- INSERT INTO ``.`` (`prod_id`, `sku_status`, `prod_price`, `prod_price`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `prod_logist`, `prod_name`, `正济堂`, `1`, `concat('{\"prod_spec\":\"',i.prod_spec,'\"}')`, `NULL`, ``, ``) VALUES ('322939', '0', '1490', '1490', '1620', '1', '2017-12-21 11:32:34', '2017-12-21 11:32:34', '1220 1元商品', NULL, '创口贴（PE组合装 )+ 医用消毒棉球', NULL, '1', '{\"prod_spec\":\"25片\"}', NULL, '', '');
-- 
-- INSERT INTO `as_test`.`1220_1元特价康佰馨` (`序号`, `商品ID`, `康佰馨`, `品牌名`, `通用名称`, `规格`, `生产企业`, `成本价`, `原价`, `活动价`, `单次可购买数量`, `备注`) VALUES ('22', '322940', '', '康顾多', '纸棒棉签 + 海氏海诺 (酒精)医用消毒棉球', '50支 + 25枚', '莆田市大明轻工制品有限公司', '0', '14.9', '1元', '组合1组', '');
-- INSERT INTO `as_test`.`1220_1元特价康佰馨` (`序号`, `商品ID`, `康佰馨`, `品牌名`, `通用名称`, `规格`, `生产企业`, `成本价`, `原价`, `活动价`, `单次可购买数量`, `备注`) VALUES ('21', '322939', NULL, '康顾多', '创口贴（PE组合装 )+ 医用消毒棉球', NULL, NULL, NULL, '8.8', NULL, NULL, NULL);
-- 

WHERE not EXISTS(SELECT * from pm_prod_sku s WHERE drugstore_id = 1620 and s.prod_id = a1620.`商品ID` and s.pharmacy_huohao = a1620.`商品编码` 
)

SELECT * from as_test.`1220_1元特价康佰馨` b


SELECT a.*,i.* 
FROM as_test.`1220_常备药` a LEFT JOIN pm_prod_sku i on  a.`药快到ID` =  i.prod_id 
WHERE i.prod_id is  null 
or i.prod_id =''


SELECT * from pm_prod_sku s WHERE prod_id = 101926

---------

CREATE TEMPORARY TABLE tmp_yaodian14 (     
ydid VARCHAR(10) NOT NULL    
) ;    
INSERT INTO tmp_yaodian14 (ydid)
VALUES ('1610'),('1611'),('1612'),('1634'),('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633');
SELECT * from tmp_yaodian14;

CREATE TEMPORARY TABLE tmp_kbx (     
ydid VARCHAR(10) NOT NULL    
);     
INSERT INTO tmp_kbx (ydid)
VALUES ('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633');
SELECT * from tmp_kbx;

CREATE TEMPORARY TABLE tmp_zjt (     
ydid VARCHAR(10) NOT NULL    
);     
INSERT INTO tmp_zjt (ydid)
VALUES ('1610'),('1611'),('1612'),('1634');
SELECT * from tmp_zjt;


-- 
-- SELECT * from  as_test.`1220_吃喝应酬康佰馨` a LEFT JOIN pm_prod_info b on a.`药快到ID` = b.prod_id 
-- WHERE prod_id is  null


-- 增加1元商品
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
-- SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
SELECT  i.prod_id,'0' as sku_status,'100' as sku_price,a.`原价`*100 as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
from 
-- `as_test`.`1220_1元特价正济堂` 
`as_test`.`1220_1元特价康佰馨`
a LEFT JOIN pm_prod_info i on  a.`prod_id` =  i.prod_id  LEFT JOIN tmp_kbx c  on 1=1
WHERE a.`序号` != '' and a.`序号` != 0
and not EXISTS(SELECT * from pm_prod_sku s WHERE drugstore_id = c.ydid and s.prod_id = a.`prod_id` and s.pharmacy_huohao = CONCAT('1YYY',a.`huohao`))
;

-- 增加1元商品
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
-- SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
 SELECT  i.prod_id,'0' as sku_status,'100' as sku_price,a.`原价`*100 as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
-- ,a.*
from 
 `as_test`.`1220_1元特价正济堂` 
-- `as_test`.`1220_1元特价康佰馨`
a LEFT JOIN pm_prod_info i on  a.`prod_id` =  i.prod_id  LEFT JOIN tmp_zjt c  on 1=1
WHERE a.`序号` != '' and a.`序号` != 0
and not EXISTS(SELECT * from pm_prod_sku s WHERE drugstore_id = c.ydid and s.prod_id = a.`prod_id`  and s.pharmacy_huohao = CONCAT('1YYY',a.`huohao`)
)
;





-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`)
SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`,CASE when locate('特价',zz.zh)<=0 then CONCAT(zz.prod_spec,'*',SUBSTR(zz.zh,1,2 )) else zz.prod_spec END as  `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
from (
SELECT a.cxjg,a.zh,b.* 
from as_test.`1220_吃喝应酬正济堂` as a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
WHERE locate('组合',a.zh)>0
) zz


UNION ALL
SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`,CASE when locate('特价',zz.zh)<=0 then CONCAT(zz.prod_spec,'*',SUBSTR(zz.zh,1,2 )) else zz.prod_spec END as  `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
from (
SELECT a.cxjg,a.zh,b.*  
from as_test.`1220_孝敬父母` as a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
WHERE locate('组合',a.zh)>0
) zz 


UNION ALL
SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`,CASE when locate('特价',zz.zh)<=0 then CONCAT(zz.prod_spec,'*',SUBSTR(zz.zh,1,2 )) else zz.prod_spec END as  `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
from (
SELECT a.cxjg,a.zh,b.* 
from as_test.`1220_更多推荐` as a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
WHERE locate('组合',a.zh)>0
) zz


UNION ALL
SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`,CASE when locate('特价',zz.zh)<=0 then CONCAT(zz.prod_spec,'*',SUBSTR(zz.zh,1,2 )) else zz.prod_spec END as  `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
from (
SELECT a.cxjg,a.zh,b.* 
from as_test.`1220_常备药` as a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
WHERE locate('组合',a.zh)>0
) zz


-- ------------
-- 这个是最终写入的sku

-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
 



SELECT i.prod_id,
       '0' as sku_status,
       i.prod_price as sku_price,
       i.prod_price as sku_fee,
       c.ydid as drugstore_id,
       '1' as brand_id,
       now() as sku_update_time,
       now() as sku_create_time,
       '1220 多盒商品' as sku_remark,
       i.prod_logist,
       i.prod_name,

 CONCAT('1YZH', i.`huohao_kbx`) as pharmacy_huohao,
-- CONCAT('1YZH', i.`huohao_zjt`) as pharmacy_huohao,
       '1',
       concat('{\"prod_spec\":\"', i.prod_spec, '\"}'),
       null as sku_attr,
       '' as sku_img,
       '' as sku_sum -- SELECT  i.prod_id,'0' as sku_status,'100' as sku_price,a.`原价`*100 as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
        -- ,a.*
from(
SELECT  (@i:=@i+1) as prod_id,zzz.* FROM (
  /*
 SELECT zz.huohao_zjt, zz.huohao_kbx, `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, CASE when locate('特价', zz.zh)<= 0 then CONCAT(zz.prod_spec, '*', SUBSTR(zz.zh, 1, 2)) else zz.prod_spec END as `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`
  from(
SELECT a.cxjg, a.zh, b.*, a.huohao as huohao_zjt,'' as  huohao_kbx
  from as_test.`1220_吃喝应酬正济堂` as a
  LEFT JOIN pm_prod_info b on a.prod_id= b.prod_id
 WHERE locate('组合', a.zh)> 0) zz
 UNION ALL
    */
    
    SELECT zz.huohao_zjt, zz.huohao_kbx, `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, CASE when locate('特价', zz.zh)<= 0 then CONCAT(zz.prod_spec, '*', SUBSTR(zz.zh, 1, 2)) else zz.prod_spec END as `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`
  from(
SELECT a.cxjg, a.zh, b.*, '' as huohao_zjt,a.huohao as  huohao_kbx
  from as_test.`1220_吃喝应酬康佰馨` as a
  LEFT JOIN pm_prod_info b on a.prod_id= b.prod_id
 WHERE locate('组合', a.zh)> 0) zz
 UNION ALL
SELECT zz.huohao_zjt, zz.huohao_kbx, `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, CASE when locate('特价', zz.zh)<= 0 then CONCAT(zz.prod_spec, '*', SUBSTR(zz.zh, 1, 2)) else zz.prod_spec END as `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`
  from(
SELECT a.cxjg, a.zh, b.*, a.huohao_zjt, a.huohao_kbx
  from as_test.`1220_孝敬父母` as a
  LEFT JOIN pm_prod_info b on a.prod_id= b.prod_id
 WHERE locate('组合', a.zh)> 0) zz
 UNION ALL
SELECT zz.huohao_zjt, zz.huohao_kbx, `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, CASE when locate('特价', zz.zh)<= 0 then CONCAT(zz.prod_spec, '*', SUBSTR(zz.zh, 1, 2)) else zz.prod_spec END as `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`
  from(
SELECT a.cxjg, a.zh, b.*, a.huohao_zjt, a.huohao_kbx
  from as_test.`1220_更多推荐` as a
  LEFT JOIN pm_prod_info b on a.prod_id= b.prod_id
 WHERE locate('组合', a.zh)> 0) zz
 UNION ALL
SELECT zz.huohao_zjt, zz.huohao_kbx, `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, round(zz.cxjg*100.0) as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, CASE when locate('特价', zz.zh)<= 0 then CONCAT(zz.prod_spec, '*', SUBSTR(zz.zh, 1, 2)) else zz.prod_spec END as `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`
  from(
SELECT a.cxjg, a.zh, b.*, a.huohao_zjt, a.huohao_kbx
  from as_test.`1220_常备药` as a
  LEFT JOIN pm_prod_info b on a.prod_id= b.prod_id
 WHERE locate('组合', a.zh)> 0) zz
) zzz,(
select @i:= 323072) as xxx) i
  LEFT JOIN
-- tmp_zjt c
 tmp_kbx c
on 1= 1 


-- 康佰馨由于差了一个商品，导致prod_id对应出现错位 下面update +1 对应上
SELECT  a.`prod_id` ,a.`prod_name`,b.`prod_name`  ,b.`prod_id`,a.*,b.*  FROM  `pm_prod_sku` a LEFT JOIN  `pm_prod_info` b on a.`prod_id`  = b.`prod_id` 
 WHERE  a.`sku_remark`  = '1220 多盒商品'
and a.`drugstore_id`  in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
ORDER BY  `sku_id` DESC
;

-- UPDATE pm_prod_sku set `prod_id` = `prod_id`+1  WHERE  `sku_remark`  = '1220 多盒商品'
and `drugstore_id`  in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)


------------
SELECT  COUNT(*) FROM pm_prod_sku
where `sku_id`  BETWEEN   25474265 and  25475432
ORDER BY `sku_id`  DESC  

-- yuan 25474009

25474010 null
--  1220_1元特价康佰馨  25474265  25474484 - 25474265


--  1220_1元特价zjt 25474485  25474607



--  1220 多盒 zjt  25474608  25474863

--  1220 多盒 kbx  25474864   25475432


-- DELETE FROM   pm_prod_sku WHERE `sku_id`  BETWEEN   25474010 and  25474229


SELECT  * FROM `pm_prod_info` 
 WHERE `prod_id`  BETWEEN 323072 and   323125 
ORDER BY `prod_id`  DESC  

-- last 323071
-- 323072   323125 
--  --------
-- 天一 增加商品
/*
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`, a.`kgsl`
 FROM as_test.1220_天一1元特价 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`
 FROM as_test.1220_天一吃喝应酬 as a
UNION all 
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`
 FROM as_test.1220_天一更多推荐 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`
 FROM as_test.1220_天一年前备药年节无忧 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`
 FROM as_test.1220_天一孝敬父母 as a
*/



SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一吃喝应酬 as a
UNION all 
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一更多推荐 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一年前备药年节无忧 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一孝敬父母 as a
-- 一元商品
-- SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
-- from 
-- -- `as_test`.`1220_1元特价正济堂` 
-- `as_test`.`1220_1元特价康佰馨`
-- a LEFT JOIN pm_prod_info i on  a.`prod_id` =  i.prod_id  LEFT JOIN tmp_kbx c  on 1=1
-- WHERE a.`序号` != '' and a.`序号` != 0
-- and not EXISTS(SELECT * from pm_prod_sku s WHERE drugstore_id = c.ydid and s.prod_id = a.`prod_id` and s.pharmacy_huohao = CONCAT('1YYY',a.`huohao`))
-- ;
-- 
-- 
-- SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`, a.`kgsl`
-- ,b.*
--  FROM as_test.1220_天一1元特价 as a LEFT JOIN pm_prod_sku as b on a.sku_id = b.sku_id
-- 




