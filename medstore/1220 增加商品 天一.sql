
-- as_test.`1220_1元特价康佰馨`
-- as_test.`1220_1元特价正济堂`
-- as_test.`1220_吃喝应酬康佰馨`
-- as_test.`1220_吃喝应酬正济堂`
-- as_test.`1220_孝敬父母`
-- as_test.`1220_常备药`
-- as_test.`1220_更多推荐`

-- — 1610,1611,1612,1634,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633,1635
-- — 1610,1611,1612,1634 — 北京正济堂药品连锁超市(亚运村店)
-- — 1620,1621,1622,1623,1627,1629,1630,1631,1632,1633 —  北京市康佰馨大药房昆泰店
-- — 1635  北京万民阳光大药房有限公司朝阳路店
-- 1000035648	ykd20001001act3018	家中必备	dir	1		100	18	3	http://imgstore.camore.cn/icon/act/ashacker/180101/act3018.jpg	1000035639	2018-01-02 17:21:53	2018-01-02 16:39:42	#7B21F5	天一18年1月活动>>关爱父母		200		
-- SELECT * from pm_dir_info  WHERE dir_code like 'ykd20001001act30%'


CREATE TEMPORARY TABLE tmp_yaodian14 (     
ydid VARCHAR(10) NOT NULL    
) ;    
INSERT INTO tmp_yaodian14 (ydid)
VALUES ('1610'),('1611'),('1612'),('1634'),('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633');
SELECT * from tmp_yaodian14;

CREATE TEMPORARY TABLE tmp_kbx (     
ydid VARCHAR(10) NOT NULL    
);     
INSERT INTO tmp_kbx (ydid)
VALUES ('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633');
SELECT * from tmp_kbx;

CREATE TEMPORARY TABLE tmp_zjt (     
ydid VARCHAR(10) NOT NULL    
);     
INSERT INTO tmp_zjt (ydid)
VALUES ('1610'),('1611'),('1612'),('1634');
SELECT * from tmp_zjt;

-- 天一 增加商品
/*
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`, a.`kgsl`
 FROM as_test.1220_天一1元特价 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`
 FROM as_test.1220_天一吃喝应酬 as a
UNION all 
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`
 FROM as_test.1220_天一更多推荐 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`
 FROM as_test.1220_天一年前备药年节无忧 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`
 FROM as_test.1220_天一孝敬父母 as a


*/
-- *******先把准生产的as_test 天一的标  导入到生产库
-- 添加 组合商品  生产已经加上了，不需要重复添加了
-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`)
 VALUES ( '康顾多创口贴+医用碘伏消毒棉球', '康顾多+海氏海诺', '1', '1', '', '1490', '5', '浙江康力迪医疗用品有限公司+青岛海诺生物工程有限公司', NULL, '2018-01-04 13:21:40', '2018-01-04 13:21:40', '2', '25片+ 25枚', '盒', '康顾多创口贴+医用碘伏消毒棉球', '1220 组合 天一', NULL, NULL, '1', NULL);
-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`)
 VALUES ('康顾多纸棒棉签+海氏海诺(酒精)医用消毒棉球', '康顾多+海氏海诺', '1', '', '', '880', '5', '浙江康力迪医疗用品有限公司+青岛海诺生物工程有限公司', NULL, '2018-01-04 13:21:40','2018-01-04 13:21:40', '2', '25片+ 25枚', '盒', '康顾多纸棒棉签+海氏海诺(酒精)医用消毒棉球', '1220 组合 天一', NULL, NULL, '1', NULL);


-- SELECT * from pm_prod_info WHERE prod_name in ('康顾多创口贴+医用碘伏消毒棉球','康顾多纸棒棉签+海氏海诺(酒精)医用消毒棉球')
-- *************************************************


-- SELECT * from pm_prod_info WHERE prod_name like '%消毒棉球%'
-- SELECT * from pm_prod_sku WHERE prod_name like '%消毒棉球%' and  drugstore_id = 200 -- pharmacy_huohao = 1620

-- SELECT * from pm_prod_sku ORDER BY sku_id desc  -- 25475435 +22

-- 一元商品 22 
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
SELECT  i.prod_id,'1' as sku_status,a.sku_fee as sku_price,a.sku_fee as sku_fee,'200' as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品 天一' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`pharmacy_huohao`)  as pharmacy_huohao,'1',
CASE WHEN a.kgsl = '2盒' then concat('{\"prod_spec\":\"',i.prod_spec,'*2盒\"}')
ELSE concat('{\"prod_spec\":\"',i.prod_spec,'\"}') 
end prod_spec
,null as sku_attr,'' as sku_img,'' as sku_sum
 FROM as_test.1220_天一1元特价 as a 
	LEFT JOIN pm_prod_sku as b on a.sku_id = b.sku_id 
	LEFT JOIN pm_prod_info as i on b.prod_id = i.prod_id or a.K = i.prod_id;
-- ORDER BY b.prod_id;





-- 活动商品
-- 增加组合商品  94
-- INSERT INTO `medstore`.`pm_prod_info` ( `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`)
 SELECT  `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`,zz.cxjg as `prod_price`, `prod_level`, `prod_firm`, `prod_logist`,NOW() as  `prod_update_time`,NOW() as  `prod_create_time`, `prod_type`, CONCAT(zz.prod_spec,'*',SUBSTR(zz.cxfs,1,2 )) as  `prod_spec`, `prod_unit`, `prod_gen_name`,CONCAT('1220 多盒 天一 ',prod_id)  `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order` 
 from (
SELECT a.sku_price as cxjg,a.cxfs,i.* -- ,a.*-- a.cxjg,a.zh,b.* 
from (

	SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一吃喝应酬 as a
UNION all 
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一更多推荐 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一年前备药年节无忧 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一孝敬父母 as a
) as a 
LEFT JOIN pm_prod_sku as b on a.sku_id = b.sku_id 
	LEFT JOIN pm_prod_info as i on b.prod_id = i.prod_id -- or a.K = i.prod_id;
-- LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
WHERE locate('组合',a.cxfs)>0
and i.prod_id is not null
 ) zz

-- ORDER BY 
;


-- SELECT * from pm_prod_info 
--  WHERE prod_remark like '1220 多盒 天一 %' -- '1220 活动 天一' 
-- ORDER BY prod_id desc
-- 
-- LIMIT 400
-- **************************************************
-- ------------*************************
-- 这个是最终写入的sku  多盒  94  老的也不用新建sku了

-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`) 
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');

SELECT DISTINCT ii.prod_id,
       '1' as sku_status,
       ii.prod_price as sku_price,
       ii.prod_price as sku_fee,
       '200' as drugstore_id,
       '1' as brand_id,
       now() as sku_update_time,
       now() as sku_create_time,
       '1220 多盒商品 天一' as sku_remark,
       ii.prod_logist,
       ii.prod_name,

 CONCAT('1YZH', aa.pharmacy_huohao) as pharmacy_huohao,
-- CONCAT('1YZH', i.`huohao_zjt`) as pharmacy_huohao,
       '1',
       concat('{\"prod_spec\":\"', ii.prod_spec, '\"}'),
       null as sku_attr,
       '' as sku_img,
       '' as sku_sum -- SELECT  i.prod_id,'0' as sku_status,'100' as sku_price,a.`原价`*100 as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
        -- ,a.*
 -- SELECT aa.* , ii.* 
from 
(
SELECT a.pharmacy_huohao,  i.prod_id-- i.* -- ,ii.*-- a.pharmacy_huohao-- a.sku_price as cxjg,a.cxfs,i.*,a.* -- ,a.*-- a.cxjg,a.zh,b.* 
from (

	SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一吃喝应酬 as a
UNION all 
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一更多推荐 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一年前备药年节无忧 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一孝敬父母 as a
) as a 
LEFT JOIN pm_prod_sku as b on a.sku_id = b.sku_id 
	LEFT JOIN pm_prod_info as i on b.prod_id = i.prod_id -- or a.K = i.prod_id;
-- LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
WHERE locate('组合',a.cxfs)>0
and i.prod_id is not null
) aa  LEFT JOIN pm_prod_info ii on  LOCATE('1220 多盒 天一 ',ii.prod_remark)>0 and LOCATE(aa.prod_id,ii.prod_remark)>0 -- aa.prod_id = REPLACE(ii.prod_remark,'1220 多盒 天一 ','')

-- pm_prod_info i 
-- WHERE i.prod_remark = '1220 活动 天一' 



-- *************************************************************************************************************************8
-- *************************************************************************************************************************8
-- *************************************************************************************************************************8

-- *************************************************************************************************************************8
-- *************************************************************************************************************************8-- *************************************************************************************************************************8
-- *************************************************************************************************************************8-- *************************************************************************************************************************8
-- *************************************************************************************************************************8
-- *************************************************************************************************************************8
-- *************************************************************************************************************************8

-- 关联关系

-- 一元商品的价格配置 22
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT b.sku_id,b.sku_fee ,'100' as sale_price,'2018-01-01 00:00:00' as sale_start_time, '2018-02-11 00:00:00' as sale_end_time ,
 '1' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'1220 一元商品 天一'
 -- c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.`序号` as sku_order,NOW() as update_time -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao,a.*,b.*,c.*
-- SELECT * 
from 
as_test.1220_天一1元特价 as a , pm_prod_sku as b
where 
CONCAT('1YYY',a.pharmacy_huohao)= b.pharmacy_huohao


-- SELECT * from pm_sku_sale WHERE   sku_id = 25475450
-- 
--  一元狂想 和商品的关联
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao,a.*,b.*,c.*
 from 
as_test.1220_天一1元特价 as a , pm_dir_info as c  , pm_prod_sku as b
where 
CONCAT('1YYY',a.pharmacy_huohao)= b.pharmacy_huohao
AND  b.drugstore_id = c.pharmacy_id
and c.dir_code LIKE '%act3015';

-- ykd20001001act3015  25475435
-- SELECT * from pm_sku_dir a LEFT JOIN pm_prod_sku b on a.sku_id = b.sku_id  WHERE dir_code = 'ykd20001001act3015'
-- 
-- SELECT * 
-- 1000035646	ykd20001001act3015		dir	1		100	15	3	http://imgstore.camore.cn/icon/act/ashacker/180101/act3015.jpg	1000035639	2018-01-03 17:04:48	2018-01-02 16:39:27	#7B21F5	天一18年1月活动>>一元狂想		200		
-- SELECT * from pm_dir_info WHERE dir_code like '%act3015' and pharmacy_id = 200



-- ***********************************************
-- 活动商品关联  227  241
除了更多推荐
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT DISTINCT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time 
 -- ,a.*,c.* 
-- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao,a.*,b.*,c.*
 from 
(

	SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一吃喝应酬 as a
UNION all 
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一更多推荐 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一年前备药年节无忧 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一孝敬父母 as a
) as a  LEFT JOIN pm_dir_info c on a.zbq = c.dir_name and  c.pharmacy_id = 200 and c.dir_code LIKE 'ykd20001001act30%' and a.zbq !='' and a.zbq is not null

 LEFT JOIN pm_prod_sku as b on   
				  (CONCAT('1YZH',a.pharmacy_huohao)= b.pharmacy_huohao and locate('组合', a.cxfs)> 0) or 
				(a.pharmacy_huohao= b.pharmacy_huohao and a.sku_id = b.sku_id  and locate('组合', a.cxfs)<= 0)


-- LEFT JOIN pm_prod_info as i on b.prod_id = i.prod_id -- or a.K = i.prod_id;
-- LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id 
WHERE -- locate('组合',a.cxfs)>0
 a.zbq !='' and a.zbq is not null
AND A.SKU_ID IS NOT NULL

ORDER BY  c.dir_code,a.sku_id;


-- *******
更多推荐
-- ykd20001001act30
1000035648	ykd20001001act3018	家中必备	dir	1		100	18	3	http://imgstore.camore.cn/icon/act/ashacker/180101/act3018.jpg	1000035639	2018-01-02 17:21:53	2018-01-02 16:39:42	#7B21F5	天一18年1月活动>>关爱父母		200		
SELECT * from pm_dir_info  WHERE dir_code like 'ykd20001001act30%'

-- 更多推荐
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT DISTINCT  '1000035648' as dir_id,a.sku_id as sku_id,'ykd20001001act3018' as dir_code,a.xh as sku_order,NOW() as update_time 
-- SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一更多推荐 as a



--- 关爱父母的前三个改成 主推  先查一下看看 update_time 然后把dir_id gaile
 SELECT * from pm_sku_dir WHERE  sku_id in (20000061,140637,140499) and update_time > '2018-01-06 17:50:03';
-- UPDATE pm_sku_dir set dir_id = 1000035656 ,dir_code = 'ykd20001001act300803' WHERE sku_id in (20000061,140637,140499) and update_time > '2018-01-07 17:50:03'
 

-- 手动删掉三个 重复的，这两个商品特殊
SELECT a.* from pm_sku_dir a,pm_prod_sku b 
WHERE a.sku_id =b.sku_id 
and a.dir_code in ('ykd20001001act300806','ykd20001001act300812')
-- and b.prod_name in ('大活络丸','阿卡波糖片')
and b.pharmacy_huohao in ('1YZH0102113','1YZH9906275')




-- SELECT * from pm_sku_dir   WHERE  dir_id = '1000035651'
-- SELECT * from pm_sku_dir a LEFT JOIN pm_prod_sku b on a.sku_id = b.sku_id WHERE  dir_id = '1000035651'-- sku_id = 102132 and dir_id = '1000035651'
-- -- dir_id = 1000035656 --  sku_id in (20000061,140637,140499) 
-- -- and dir_id = 1000035656
-- and update_time > '2018-01-06 17:50:03';

SELECT DISTINCT a.dir_id,a.sku_id,sku_order  -- COUNT(dir_id,sku_id),dir_id,sku_id -- ,dir_code,sku_order,update_time
from pm_sku_dir a
WHERE  a.dir_code LIKE 'ykd20001001act30%'
 and EXISTS (SELECT * from pm_sku_dir b WHERE a.dir_id = b.dir_id and a.sku_id = b.sku_id)

ORDER BY a.dir_id ,a.sku_id


-- and ((CONCAT('1YZH',a.pharmacy_huohao)= b.pharmacy_huohao and locate('组合', a.cxfs)> 0) or 
--  (a.pharmacy_huohao= b.pharmacy_huohao  and locate('组合', a.cxfs)<= 0))
-- DELETE from  pm_sku_dir WHERE update_time = '2018-01-06 20:36:47'
--  SELECT * from pm_sku_dir WHERE update_time > '2018-01-06 19:50:03' -- ORDER BY  --  dir_id = 1000035657 and sku_id = 20000061 and dir_code = 'ykd20001001act300804'
--  SELECT * from pm_sku_dir WHERE sku_id in (20000061,140637,140499) and update_time > '2018-01-06 17:50:03'
-- 
-- 1000035656 ykd20001001act300803
-- -- UPDATE pm_sku_dir set dir_id = 1000035656 ,dir_code = 'ykd20001001act300803' WHERE sku_id in (20000061,140637,140499) and update_time > '2018-01-07 17:50:03'
-- SELECT * from pm_dir_info WHERE dir_id in (1000035656,1000035651)
1000035651	ykd20001001act301003	主推	dir	1		100	3	3	http://imgstore.camore.cn/icon/act/ashacker/180102/act301003.jpg	1000035643	2018-01-05 15:54:13	2018-01-02 16:57:29	#7B21F5	吃喝应酬		200		
1000035656	ykd20001001act300803	主推	dir	1		100	3	3	http://imgstore.camore.cn/icon/act/ashacker/180102/act300803.jpg	1000035642	2018-01-05 15:53:25	2018-01-02 17:01:27	#7B21F5	关爱父母		200		

-- where ((CONCAT('1YZH',a.pharmacy_huohao)= b.pharmacy_huohao and locate('组合', a.zh)> 0) or 
-- (a.pharmacy_huohao= b.pharmacy_huohao  and locate('组合', a.zh)<= 0))
--******************************************************8
-- 天一 折扣 特价

1000035678	tyact306z	天一6折	dir	0		4	2	1			2017-12-25 16:31:28	2017-12-25 16:30:26				200		
1000035681	tyact307z	天一7折	dir	0		4	2	1			2017-12-25 16:31:28	2017-12-25 16:30:26				200		
1000035677	tyact3085z	天一两件起85折	dir	0		4	2	1			2018-01-04 22:17:35	2017-12-25 16:30:26				200		
1000035680	tyact308z	天一两件起8折	dir	0		4	2	1			2018-01-04 22:17:35	2017-12-25 16:30:26				200		
1000035682	tyact3095z	天一两件起95折	dir	0		4	2	1			2018-01-04 22:17:35	2017-12-25 16:30:26				200		
1000035676	tyact309z	天一两件起9折	dir	0		4	2	1			2018-01-04 22:16:57	2017-12-25 16:30:26				200		
1000035675	tyact30sale	天一特价	dir	0		4	2	1			2017-12-25 16:31:33	2017-12-25 16:30:26				200		
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT DISTINCT
	CASE
WHEN locate('6折', aa.cxfs) > 0 THEN
	'1000035678'
WHEN locate('7折', aa.cxfs) > 0 THEN
	'1000035681'
WHEN locate('8折', aa.cxfs) > 0 THEN
	'1000035680'
WHEN locate('8.5折', aa.cxfs) > 0 THEN
	'1000035677'
WHEN locate('9折', aa.cxfs) > 0 THEN
	'1000035676'
WHEN locate('9.5折', aa.cxfs) > 0 THEN
	'1000035682'
WHEN locate('特价', aa.cxfs) > 0 THEN
	'1000035675'
-- WHEN locate('组合', aa.cxfs) > 0 THEN
-- 	'1000035675'
ELSE
	''
END dir_id,
 aa.sku_id,
 CASE
WHEN locate('6折', aa.cxfs) > 0 THEN
	'tyact306z'
WHEN locate('7折', aa.cxfs) > 0 THEN
	'tyact307z'
WHEN locate('8折', aa.cxfs) > 0 THEN
	'tyact308z'
WHEN locate('8.5折', aa.cxfs) > 0 THEN
	'tyact3085z'
WHEN locate('9折', aa.cxfs) > 0 THEN
	'tyact309z'
WHEN locate('9.5折', aa.cxfs) > 0 THEN
	'tyact3095z'
WHEN locate('特价', aa.cxfs) > 0 THEN
	'tyact30sale'
-- WHEN locate('组合', aa.cxfs) > 0 THEN
-- 	'tyact30sale'
ELSE
	''
END dir_code,
 aa.xh AS sku_order,
 NOW() AS update_time
-- ,aa.*,b.*
 -- ,a.cxjg, a.zh,a.子标签, a.huohao_zjt,a.huohao_kbx,b.*
FROM
	(

	SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一吃喝应酬 as a
UNION all 
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一更多推荐 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一年前备药年节无忧 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一孝敬父母 as a
) AS aa
LEFT JOIN pm_prod_sku b ON 
	aa.pharmacy_huohao = b.pharmacy_huohao
	AND b.drugstore_id =200
AND (
	locate('特价', aa.cxfs) > 0
	OR locate('折', aa.cxfs) > 0
)
and b.sku_id is not null
-- and b.sku_id ! = ''
WHERE locate('特价', aa.cxfs) > 0
	OR locate('折', aa.cxfs) > 0
-- ORDER BY b.sku_id DESC

-- 组合商品添加到 特价下  1000035675	tyact30sale	天一特价
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT '1000035675' dir_id, a.sku_id sku_id, 'tyact30sale' dir_code,'1' AS sku_order, NOW() AS update_time
from pm_prod_sku a 
WHERE a.sku_remark = '1220 多盒商品 天一'
and pharmacy_huohao LIKE '%1YZH%'



-- 特价配置

-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
-- SELECT b.sku_id,sku_fee ,'100' as sale_price,'2017-12-27 00:00:00' as sale_start_time, '2017-01-31 23:59:59' as sale_end_time ,
 -- '0' as sale_status,'2017-12-27 23:15:36' as sale_create_time,NOW() as  sale_update_time,'1220 一元商品'
 -- c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.`序号` as sku_order,NOW() as update_time -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao,a.*,b.*,c.*
 
SELECT
	b.sku_id,b.sku_fee,aa.sku_price as sale_price,'2018-01-01 00:00:00' as sale_start_time, '2018-02-10 23:59:59' as sale_end_time ,
  '1' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'1220 特价 天一'
 -- ,aa.*,b.*
  -- ,a.`序号` AS sku_order ,aa.cxjg, a.zh,aa.子标签, a.huohao_zjt,a.huohao_kbx,b.*
FROM
	(

	SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一吃喝应酬 as a
UNION all 
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一更多推荐 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一年前备药年节无忧 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一孝敬父母 as a 
) AS aa
LEFT JOIN pm_prod_sku b ON 
	aa.pharmacy_huohao = b.pharmacy_huohao
	AND b.drugstore_id =200
AND locate('特价', aa.cxfs) > 0
and b.sku_id is not null
-- and b.sku_id ! = ''
WHERE locate('特价', aa.cxfs) > 0

ORDER BY b.sku_id DESC;
 


-- *********************************
-- 删除重复的sku_id的价格配置
-- DELETE 
FROM pm_sku_sale WHERE sale_id in (
SELECT sale_id
from 
(
SELECT sale_id  
FROM pm_sku_sale 
WHERE SALE_REMARK LIKE '1220 特价 天一'
GROUP BY sku_id 
HAVING COUNT(sku_id)>1
) a
);
SELECT *-- COUNT(sku_id),sku_id 
FROM pm_sku_sale WHERE sku_id in (101678,101806,140499,20000061) and SALE_REMARK LIKE '1220 特价 天一';


1000035685	tyact30cjaj	天一阿胶	dir	1		100	4	3			2018-01-06 09:42:17	2017-12-29 14:59:18		1月活动>>阿胶		200		
1000035684	tyact30cjhmyj	天一鸿茅药酒	dir	1		100	4	3			2018-01-06 09:42:12	2017-12-29 14:59:18		1月活动>>鸿茅药酒		200		
1000035683	tyact30cjtc	天一汤臣	dir	1		100	4	3			2018-01-06 09:42:06	2017-12-29 14:59:18		1月活动>>汤臣倍健		200		
1000035679	tyact30excoupon	天一不可使用优惠券	dir	0		4	2	1			2018-01-04 22:20:47	2017-12-25 16:30:26				200		

汤臣倍健20元券	生产厂家为汤臣倍健的所有商品
福牌阿胶100元券	庆安天一药房货号15100373
鸿茅药酒50元券	庆安天一药房货号04000766


-- SELECT * from pm_sku_dir ORDER BY link_id desc limit 100
-- SELECT * from pm_sku_dir 
-- where dir_id = '1000035638'
-- or dir_code = 'act30cjaj'
-- ORDER BY link_id DESC  LIMIT 100

--   福牌阿胶100元券	庆安天一药房货号 15100373  1000035685	tyact30cjaj	天一阿胶
-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');
SELECT '1000035685' as `dir_id`, `sku_id`,'tyact30cjaj' as  `dir_code`,1 as  `sku_order`,NOW() as  `update_time` from (
SELECT * from pm_prod_sku a 
WHERE a.pharmacy_huohao = 15100373
and a.drugstore_id =200

) zz  

--  鸿茅药酒50元券	庆安天一药房货号 04000766  1000035684	tyact30cjhmyj	天一鸿茅药酒


-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');
SELECT '1000035684' as `dir_id`, `sku_id`,'tyact30cjhmyj' as  `dir_code`,1 as  `sku_order`,NOW() as  `update_time` from (
SELECT * from pm_prod_sku a 
WHERE a.pharmacy_huohao = '04000766'
and a.drugstore_id =200

) zz  



--  1000035683	tyact30cjtc	天一汤臣	
-- 生产厂家为汤臣倍健的全部商品
-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');

SELECT '1000035683' as `dir_id`, `sku_id`,'tyact30cjtc' as  `dir_code`,1 as  `sku_order`,NOW() as  `update_time` from pm_prod_sku a ,pm_prod_info b 
WHERE a.prod_id = b.prod_id
and a.drugstore_id =200
and b.prod_firm like '%汤臣倍健%'


-- ****************************************88
-- 不可用券
1000035679	tyact30excoupon	天一不可使用优惠券	dir	0		4	2	1			2018-01-04 22:20:47	2017-12-25 16:30:26				200		

-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');
SELECT '1000035679' as `dir_id`, `sku_id`,'tyact30excoupon' as  `dir_code`,1 as  `sku_order`,NOW() as  `update_time`
FROM as_test.`6%以下毛利商品明细（不参与满减）` as a 

-- --------------------------------------
组合商品的图片 

-- INSERT INTO `medstore`.`pm_image_info` ( `img_name`, `img_url`, `img_type`, `img_status`, `img_update_time`, `img_create_time`, `img_remark`, `img_other1`, `img_other2`) 
-- VALUES ('9330', '/08/DSC_5408.jpg', 'http://imgstore.camore.cn/prod_img_ty/08/DSC_5408.jpg', 'jpg', '1', '2016-11-15 17:10:15', '2016-11-15 17:10:15', NULL, NULL, NULL);

SELECT
 b.prod_id AS img_name,
CONCAT('http://imgstore.camore.cn/icon/act/ashacker/180101/ty/',aa.prod_name,'.jpg'),
'jpg' as img_type,
'1' as img_status,
NOW() AS img_update_time, NOW() AS `img_create_time`,'1220 组合 天一' AS  `img_remark`, null as `img_other1`,null as  `img_other2`
-- ,aa.*,b.* 
-- NOW() AS update_time
 -- ,a.cxjg, a.zh,a.子标签, a.huohao_zjt,a.huohao_kbx,a.prod_id,b.*
FROM
	(

	SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一吃喝应酬 as a
UNION all 
SELECT a.`xh`,'' as `zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一更多推荐 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一年前备药年节无忧 as a
UNION all 
SELECT a.`xh`,a.`zbq`, a.`sku_id`, a.`pharmacy_huohao`, a.`prod_brand`, a.`prod_name`, a.`prod_spec`, a.`prod_firm`, a.`sku_fee`,a. `sku_price`,'' as `kgsl`,a.cxfs
 FROM as_test.1220_天一孝敬父母 as a

) AS aa
LEFT JOIN  pm_prod_sku b ON 
CONCAT('1YZH', aa.pharmacy_huohao) = b.pharmacy_huohao
AND b.drugstore_id = 200
and locate('组合', aa.cxfs) > 0
and b.sku_id is not null
-- and b.sku_id ! = ''
WHERE locate('组合', aa.cxfs) > 0
/*
and ( b.prod_name like '%阿托伐他汀钙片%' or b.prod_name like '%非洛地平缓释片%'
or b.prod_name like '%盐酸二甲双胍片%'
or b.prod_name like '%酒石酸美托洛尔片%'
or b.prod_name like '%马来酸依那普利片%'
or b.prod_name like '%牛黄清心丸%'

or b.prod_name like '%盐酸贝那普利片%'
-- or b.prod_name like '%牛黄清心丸%'
-- or b.prod_name like '%牛黄清心丸%'
-- or b.prod_name like '%牛黄清心丸%'
 )
*/
GROUP BY b.prod_id;

-- 一元组合商品 差两个
-- INSERT INTO `medstore`.`pm_image_info` ( `img_name`, `img_url`, `img_type`, `img_status`, `img_update_time`, `img_create_time`, `img_remark`, `img_other1`, `img_other2`) 
VALUES ( '323135', 'http://imgstore.camore.cn/icon/act/ashacker/180101/ty/323135.jpg', 'jpg', '1', '2018-01-07 00:08:12', '2018-01-07 00:01:43', '1220 组合 天一 一元', NULL, NULL);

-- INSERT INTO `medstore`.`pm_image_info` ( `img_name`, `img_url`, `img_type`, `img_status`, `img_update_time`, `img_create_time`, `img_remark`, `img_other1`, `img_other2`) 
VALUES ( '323136', 'http://imgstore.camore.cn/icon/act/ashacker/180101/ty/323136.jpg', 'jpg', '1', '2018-01-07 00:08:12', '2018-01-07 00:01:43', '1220 组合 天一 一元', NULL, NULL);


SELECT b.prod_id,b.prod_name,b.prod_brand,b.prod_spec,b.prod_gen_name,a.* from pm_image_info a LEFT JOIN pm_prod_info b on a.img_name = b.prod_id
WHERE  LOCATE('阿托伐他汀钙片',img_url) AND img_remark = '1220 组合 天一';
SELECT * from pm_prod_img WHERE LOCATE('ty/阿托伐他汀钙片',img_name);
UPDATE pm_image_info set img_url = REPLACE(img_url,'.jpg',CONCAT('_',img_name,'.jpg')) WHERE LOCATE('阿托伐他汀钙片',img_url) AND img_remark = '1220 组合 天一'
UPDATE pm_prod_img set img_name = REPLACE(img_name,'.jpg',CONCAT('_',prod_id,'.jpg')) WHERE LOCATE('ty/阿托伐他汀钙片',img_name)


SELECT b.prod_id,b.prod_name,b.prod_brand,b.prod_spec,b.prod_gen_name,a.* from pm_image_info a LEFT JOIN pm_prod_info b on a.img_name = b.prod_id
WHERE  LOCATE('苯磺酸氨氯地平片',img_url) AND img_remark = '1220 组合 天一';
SELECT * from pm_prod_img WHERE LOCATE('ty/苯磺酸氨氯地平片',img_name);
UPDATE pm_image_info set img_url = REPLACE(img_url,'.jpg',CONCAT('_',img_name,'.jpg')) WHERE LOCATE('苯磺酸氨氯地平片',img_url) AND img_remark = '1220 组合 天一';
UPDATE pm_prod_img set img_name = REPLACE(img_name,'.jpg',CONCAT('_',prod_id,'.jpg')) WHERE LOCATE('ty/苯磺酸氨氯地平片',img_name);

-- 酒石酸美托洛尔片
SELECT b.prod_id,b.prod_name,b.prod_brand,b.prod_spec,b.prod_gen_name,a.* from pm_image_info a LEFT JOIN pm_prod_info b on a.img_name = b.prod_id
WHERE  LOCATE('酒石酸美托洛尔片',img_url) AND img_remark = '1220 组合 天一';
SELECT * from pm_prod_img WHERE LOCATE('ty/酒石酸美托洛尔片',img_name);
UPDATE pm_image_info set img_url = REPLACE(img_url,'.jpg',CONCAT('_',img_name,'.jpg')) WHERE LOCATE('酒石酸美托洛尔片',img_url) AND img_remark = '1220 组合 天一';
UPDATE pm_prod_img set img_name = REPLACE(img_name,'.jpg',CONCAT('_',prod_id,'.jpg')) WHERE LOCATE('ty/酒石酸美托洛尔片',img_name);


-- 非洛地平缓释片
SELECT b.prod_id,b.prod_name,b.prod_brand,b.prod_spec,b.prod_gen_name,a.* from pm_image_info a LEFT JOIN pm_prod_info b on a.img_name = b.prod_id
WHERE  LOCATE('非洛地平缓释片',img_url) AND img_remark = '1220 组合 天一';
SELECT * from pm_prod_img WHERE LOCATE('ty/非洛地平缓释片',img_name);
UPDATE pm_image_info set img_url = REPLACE(img_url,'.jpg',CONCAT('_',img_name,'.jpg')) WHERE LOCATE('非洛地平缓释片',img_url) AND img_remark = '1220 组合 天一';
UPDATE pm_prod_img set img_name = REPLACE(img_name,'.jpg',CONCAT('_',prod_id,'.jpg')) WHERE LOCATE('ty/非洛地平缓释片',img_name);

-- 马来酸依那普利片
SELECT b.prod_id,b.prod_name,b.prod_brand,b.prod_spec,b.prod_gen_name,a.* from pm_image_info a LEFT JOIN pm_prod_info b on a.img_name = b.prod_id
WHERE  LOCATE('马来酸依那普利片',img_url) AND img_remark = '1220 组合 天一';
SELECT * from pm_prod_img WHERE LOCATE('ty/马来酸依那普利片',img_name);
UPDATE pm_image_info set img_url = REPLACE(img_url,'.jpg',CONCAT('_',img_name,'.jpg')) WHERE LOCATE('马来酸依那普利片',img_url) AND img_remark = '1220 组合 天一';
UPDATE pm_prod_img set img_name = REPLACE(img_name,'.jpg',CONCAT('_',prod_id,'.jpg')) WHERE LOCATE('ty/马来酸依那普利片',img_name);


-- 盐酸贝那普利片
SELECT b.prod_id,b.prod_name,b.prod_brand,b.prod_spec,b.prod_gen_name,a.* from pm_image_info a LEFT JOIN pm_prod_info b on a.img_name = b.prod_id
WHERE  LOCATE('盐酸贝那普利片',img_url) AND img_remark = '1220 组合 天一';
SELECT * from pm_prod_img WHERE LOCATE('ty/盐酸贝那普利片',img_name);
UPDATE pm_image_info set img_url = REPLACE(img_url,'.jpg',CONCAT('_',img_name,'.jpg')) WHERE LOCATE('盐酸贝那普利片',img_url) AND img_remark = '1220 组合 天一';
UPDATE pm_prod_img set img_name = REPLACE(img_name,'.jpg',CONCAT('_',prod_id,'.jpg')) WHERE LOCATE('ty/盐酸贝那普利片',img_name);

-- 盐酸二甲双胍片
SELECT b.prod_id,b.prod_name,b.prod_brand,b.prod_spec,b.prod_gen_name,a.* from pm_image_info a LEFT JOIN pm_prod_info b on a.img_name = b.prod_id
WHERE  LOCATE('盐酸二甲双胍片',img_url) AND img_remark = '1220 组合 天一';
SELECT * from pm_prod_img WHERE LOCATE('ty/盐酸二甲双胍片',img_name);
UPDATE pm_image_info set img_url = REPLACE(img_url,'.jpg',CONCAT('_',img_name,'.jpg')) WHERE LOCATE('盐酸二甲双胍片',img_url) AND img_remark = '1220 组合 天一';
UPDATE pm_prod_img set img_name = REPLACE(img_name,'.jpg',CONCAT('_',prod_id,'.jpg')) WHERE LOCATE('ty/盐酸二甲双胍片',img_name);

-- 根据上面的配置图片
-- INSERT INTO `medstore`.`pm_prod_img` ( `prod_id`, `attr_id`, `attr_value_id`, `img_id`, `img_kind`, `img_name`) 
-- VALUES ('9330', '100000', NULL, NULL, '9330', 'prodImage', 'http://imgstore.camore.cn/prod_img_ty/08/DSC_5408.jpg');
SELECT a.img_name as prod_id,
	NULL AS attr_id,
NULL AS attr_value_id,
a.img_id ,
'prodImage' as img_kind,
a.img_url as img_name
  from pm_image_info a 
WHERE img_remark = '1220 组合 天一';



-- SELECT * from pm_dir_info WHERE dir_code like '%act30%'
-- 1000035639
-- ykd20001001act30
-- 天一18年1月活动
-- 
-- SELECT * from sm_image_link 
-- WHERE drugstore_id in ( 200,1610,1611,1612,1634,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633,1635)
-- and link_remark LIKE '%1220%'
--  ORDER BY link_name,link_type DESC




-- 轮播两张   ********deve 换成 store

-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`) 
SELECT a.pharmacy_id,'1' as seq_num,'http://imgstore.camore.cn/icon/act/ashacker/180101/ty/1220_lunbo1.jpg' as image_url,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1801?&showName=庆安&pageSize=40&dirId=',a.dir_id) as link_url
,'url','健康年货','1','1',NOW() as link_update_time,NOW() as link_create_time,'1220 主场馆','2017-12-28 00:00:00','2018-02-11 00:00:00','' as link_view
-- SELECT * 
FROM pm_dir_info as a  WHERE a.dir_code LIKE 'ykd20001001act30'




-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`) 
SELECT a.pharmacy_id,'1' as seq_num,'http://imgstore.camore.cn/icon/act/ashacker/180101/ty/1220_lunbo2.jpg' as image_url,CONCAT('http://deve.ykd365.com/medhtml/common/turnplate?prize_id=1&dirId=',a.dir_id) as link_url
,'url','嗨购得车票','1','1',NOW() as link_update_time,NOW() as link_create_time,'1220 抽奖','2017-12-28 00:00:00','2018-02-11 00:00:00','' as link_view
-- SELECT * 
FROM pm_dir_info as a  WHERE a.dir_code LIKE 'ykd20001001act30'


SELECT * from pm_dir_info WHERE dir_id = 1000035639
SELECT * from sm_image_link ORDER BY link_id desc


----
下面大按钮
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`) 
SELECT a.pharmacy_id,'2' as seq_num,'http://imgstore.camore.cn/icon/act/ashacker/180101/1220_bottom.png' as image_url,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1801?&showName=庆安&pageSize=40&dirId=',a.dir_id) as link_url
,'web2','嗨购得车票','1','1',NOW() as link_update_time,NOW() as link_create_time,'1220 抽奖','2017-12-28 00:00:00','2018-02-11 00:00:00','control' as link_view
-- SELECT * 
FROM pm_dir_info as a  WHERE a.dir_code LIKE 'ykd20001001act30'

-- 弹出框
-- INSERT INTO `medstore`.`sm_image_link_window` ( `drugstore_id`, `seq_num`, `image_url`, `window_url`, `window_type`, `window_name`, `window_param`, `window_status`, `window_update_time`, `window_create_time`, `window_remark`, `window_start_time`, `window_end_time`, `window_view`) 
-- VALUES ('517', '200', '2', 'http://imgstore.camore.cn/icon/act/ashacker/171201/1712_act29_TC111.png', 'http://deve.ykd365.com/medstore//OneYuan_module_bj1712?&showName=北京&pageSize=40&dirId=1000034823', 'furl', '好礼免费送，尽享折上折', '726', NULL, '1', '2017-12-02 10:42:02', '2017-11-28 22:49:17', '20171201', '2017-11-28 22:49:17', '2018-01-01 00:00:00');
SELECT a.pharmacy_id,'1' as seq_num,'http://imgstore.camore.cn/icon/act/ashacker/180101/ty/1220_tanchu.jpg' as image_url,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1801?&showName=庆安&pageSize=40&dirId=',a.dir_id) as link_url
,'furl','嗨购得车票','734','1',NOW() as link_update_time,NOW() as link_create_time,'1220 弹出','2017-12-28 00:00:00','2018-02-10 00:00:00','' as link_view
FROM pm_dir_info as a  WHERE a.dir_code LIKE 'ykd20001001act30'
-- SELECT * from pm_prod_sku ORDER BY sku_id desc  limit 400
