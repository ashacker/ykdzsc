SELECT * from pm_dir_info ORDER BY dir_id DESC;

-- 3.1 创建活动目录 北京 zsc  dir_code dir_name img  dir_all_name
-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'43') AS dir_code ,
 CONCAT('9月 补1月情人节--',pharmacy_id) AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum, 28 AS dir_num,'2' AS dir_level
,  '', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'' dir_remark
,CONCAT(dir_all_name	,'--2018-9月补1月情人节') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 1000034627 AND 1000034641   -- or dir_id = 1000034601;
/*
1001000387	ykd21071001act43	9月 补1月情人节--1635
1001000386	ykd21061014act43	9月 补1月情人节--1634
1001000385	ykd21061013act43	9月 补1月情人节--1633
1001000384	ykd21061012act43	9月 补1月情人节--1632
1001000383	ykd21061011act43	9月 补1月情人节--1631
1001000382	ykd21061010act43	9月 补1月情人节--1630
1001000381	ykd21061009act43	9月 补1月情人节--1629
1001000380	ykd21061007act43	9月 补1月情人节--1627
1001000379	ykd21061004act43	9月 补1月情人节--1623
1001000378	ykd21061003act43	9月 补1月情人节--1622
1001000377	ykd21061002act43	9月 补1月情人节--1621
1001000376	ykd21061001act43	9月 补1月情人节--1620
1001000375	ykd21051003act43	9月 补1月情人节--1612
1001000374	ykd21051002act43	9月 补1月情人节--1611
1001000373	ykd21051001act43	9月 补1月情人节--1610
*/

SELECT * from  as_test.`917_情人节`;



-- 4.1 6月 主活动页  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
 -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.917_情人节 as a , pm_dir_info as c  , pm_prod_sku as b
where 
(
 (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
  OR
 ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634))
)

 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id BETWEEN 1001000373 and 1001000387 -- 北京商品目录 -- >=1000046192 
-- and c.dir_name = a.新楼层
-- and b.drugstore_id = 1620
ORDER BY b.drugstore_id,a.序号;


SELECT * from pm_sku_dir WHERE dir_id = 1001000377 -- BETWEEN 1001000373 and 1001000387;




SELECT * from sm_drugstore_info WHERE drugstore_id in   (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633,1610,1611,1612,200) ;


SELECT * from pm_dir_info ORDER BY dir_id DESC;


SELECT *from sm_image_link ORDER BY link_id desc;


---

SELECT *from am_act_info ORDER BY act_id DESC;


SELECT * from am_act_item WHERE act_id in
(164
,163
,157
,156);

SELECT * from am_act_item ORDER BY item_id DESC;

SELECT * from pm_prod_sku WHERE
pharmacy_huohao in (930389
,930440
,200200
)and drugstore_id in (1620);

 prod_id in (178097
,133398,184471
)  and drugstore_id in (1620);

SELECT * from pm_sku_dir a LEFT JOIN pm_dir_info b on a.dir_id = b.dir_id WHERE sku_id in (11620373
,11621276
,11621285) and b.dir_create_time   BETWEEN '2017-11-01' and '2018-03-01'
and dir_name in ('补肾壮阳','计生用品','经典伟哥','情人节','情人节活动') 
-- 11621276;

SELECT * from pm_dir_info WHERE dir_name like '%情人%' or dir_remark like '%情人%';


SELECT * from pm_dir_info WHERE dir_create_time BETWEEN '2017-11-01' and '2017-12-01' ;


SELECT * from pm_dir_info WHERE dir_create_time BETWEEN '2017-12-01' and '2018-01-01' ;

SELECT * from pm_dir_info WHERE dir_create_time BETWEEN '2018-01-01' and '2018-02-01' ;


SELECT * from pm_dir_info WHERE dir_create_time BETWEEN '2018-02-01' and '2018-03-01' ;


SELECT * from pm_dir_info WHERE dir_name in ('补肾壮阳','计生用品','经典伟哥','情人节','日程用药')  and pharmacy_id = 1620 ;


SELECT * from pm_dir_info WHERE dir_name in ( '计生用品','经典伟哥','情人节','日程用药') and pharmacy_id = 1620 ;


SELECT * from pm_sku_sale a LEFT JOIN pm_prod_sku b on a.sku_id = b.sku_id WHERE sale_price = 17900 -- 49800 -- 9900

-- and b.prod_id = 133398
 ORDER BY sale_create_time