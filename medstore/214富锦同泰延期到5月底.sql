
-- DELETE from as_test.212_bj_dmss_gai;
SELECT * from as_test.212_bj_dmss_gai;



-- update `medstore`.`sm_image_link` set `link_status`=1,`link_end_time`='2019-02-28 23:59:59' 
 SELECT * FROM `sm_image_link` 
WHERE `drugstore_id` = 1641
and `link_end_time`  = '2019-02-11 23:59:59'
and `link_view` in ('essence','all_essence')
ORDER BY `link_id` desc;

-- update pm_sku_sale a,pm_prod_sku b set a.sale_end_time = '2019-02-28 23:59:59'
 SELECT * from pm_sku_sale a,pm_prod_sku b
WHERE
a.sku_id = b.sku_id
and b.drugstore_id = 1641
AND a.sale_end_time = '2019-02-11 23:59:59'
ORDER BY sale_id DESC;

-- update am_stat_info set item_expire_time = '2019-02-28 23:59:59'
-- SELECT * from am_stat_info 
WHERE  act_id = 187 and   item_expire_time = '2019-02-11 23:59:59';

UPDATE am_act_info set act_end_time = '2019-02-28 23:59:59'
-- SELECT * from am_act_info 
WHERE act_id = 187
 ;


SELECT * from pm_sku_sale
WHERE sale_remark like '%1901月 秒杀 fj%'
and sale_end_time BETWEEN '2019-02-11' and '2019-02-12'
 ORDER BY sku_id desc
;



CREATE TEMPORARY TABLE tmp_mstime (     
stime VARCHAR(50) NOT NULL
) ;   
--
-- DELETE from  tmp_mstime

-- DELETE FROM tmp_mstime

select * from pm_prod_sku WHERE drugstore_id = 1641 and prod_name = '';

-- //注意DELIMITER后有个空格
-- //创建存储过程，名为test（可随意取名）
--  //添加19行数据
-- //给表名为table_name的行id，num添加数据
-- //存储成功
-- //用call命令调用存储

DELIMITER ;;                 
CREATE PROCEDURE test5()      
BEGIN 
DECLARE Y datetime;
set Y='2019-02-14';
WHILE Y<'2019-06-01'                
DO
INSERT INTO tmp_mstime(stime) values(DATE_FORMAT(Y,'%m-%d'));    
SET Y=DATE_ADD(Y,INTERVAL 1 DAY) ; 
END WHILE ; 
COMMIT; 
END;;                       
 
call test5;                   

desc tmp_mstime;
SELECT * from tmp_mstime


-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT  a.sku_id,a.sale_fee,a.sale_price as sale_price
, replace(sale_start_time,'02-11',t.stime)  as sale_start_time
 
,replace(sale_end_time,'02-11',t.stime)    as sale_end_time
,'0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'1901月 秒杀 fj jia'
--  ,i.*,a.*
from pm_sku_sale a,pm_prod_sku b,tmp_mstime t
WHERE
a.sku_id = b.sku_id
 and b.drugstore_id = 1641
and sale_remark   in ('1901月 秒杀 fj' )
AND a.sale_end_time BETWEEN    '2019-02-11 00:00:00' and '2019-02-11 23:59:59'
-- and t.stime = '02-15'
;

-- SELECT * from pm_sku_sale WHERE sale_remark like '1901月 秒杀 fj%' ORDER BY sale_id DESC limit 1000;





-- 增加 每日秒杀时间段
-- INSERT INTO `medstore`.`am_stages_sale` ( `pharmacy_id`, `sg_title`, `sg_status`, `sg_start_time`, `sg_end_time`, `quota_id`, `act_id`, `item_id`, `sg_create_time`, `sg_update_time`, `sg_json`, `sg_remark`) 

SELECT 1641 `pharmacy_id`, `sg_title`, `sg_status`
,REPLACE(REPLACE(sg_start_time,'01-01',t.stime),'2018','2019')  `sg_start_time`
,REPLACE(REPLACE(sg_end_time,'01-01',t.stime),'2018','2019')  `sg_end_time`
,839  `quota_id`
,187 `act_id`
,1388 `item_id`
,now() `sg_create_time`
,now() `sg_update_time`, `sg_json`, `sg_remark`

from am_stages_sale a ,tmp_mstime t  
WHERE `sg_start_time` BETWEEN  '2019-01-01 00:00:00' and '2019-01-01 23:59:58' and `pharmacy_id`  = 200  --  in (200,1610,1611,1612,1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
ORDER BY t.stime, sg_id DESC;




SELECT * FROM `am_stages_sale` ORDER BY `sg_id` DESC ;



-- 新版秒杀需要配置的数据 新表
-- INSERT INTO `medstore`.`am_stages_sale_detail` (  `sg_id`, `act_id`, `item_id`, `quota_id`, `pharmacy_id`, `dir_id`, `sg_detail_create_time`, `sg_detail_update_time`, `sg_detail_remark`, `sg_detail_flag`, `sg_detail_type`)
--  VALUES ('23', '12', '163', '691', '236', '1620', '1000036249', '2018-08-13 10:20:33', '2018-08-13 10:20:33', '1', '0', '0');
SELECT  a.`sg_id`, a.`act_id`, a.`item_id`, a.`quota_id`, a.`pharmacy_id`
,d.`dir_id`  `dir_id`
,NOW() `sg_detail_create_time`
,NOW() `sg_detail_update_time`
,'1901月 新秒杀 jia fjtt' `sg_detail_remark`
,0 `sg_detail_flag`
, 0`sg_detail_type`
-- ,b.*,c.*
from am_stages_sale a LEFT JOIN  `pm_dir_info` d on a.sg_title =  SUBSTR(d.`dir_name`, 1,5) -- and d.dir_code like '%act44%' 
and a.`pharmacy_id` = d.`pharmacy_id` -- am_item_range b on a.item_id = b.item_id
and d.dir_id BETWEEN     1002756739 and 1002756742 -- ms

-- (d.dir_id BETWEEN 1001000966 and 1001000969 or d.dir_id BETWEEN 1001001578 and 1001001637)
-- LEFT JOIN am_act_range c on b.range_id = c.range_id
WHERE act_id =187
 
 ;














SELECT * from pm_sku_sale a LEFT join pm_prod_sku b on a.sku_id = b.sku_id
WHERE 
 sale_remark   in ('1901月 秒杀 fj' )
AND a.sale_end_time BETWEEN    '2019-02-11 00:00:00' and '2019-02-11 23:59:59'
and a.sku_id = b.sku_id

SELECT * from pm_prod_sku WHERE sku_id = 25526411

as_test.`801_bj_秒杀`  as a , pm_prod_sku as i,tmp_mstime t
where  -- a.huohao = b.pharmacy_huohao and b.drugstore_id =1520;
((a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633))
  OR ( a.正济堂货号 = i.pharmacy_huohao and i.drugstore_id in (1610,1611,1612,1634)))
--  and i.drugstore_id = 1620/
ORDER BY a.序号;


 SELECT * from pm_sku_sale a,pm_prod_sku b
WHERE
a.sku_id = b.sku_id
 and b.drugstore_id = 1641
and sale_remark not in ('1901月 秒杀 fj','1901月商品调价-new-fj');



AND a.sale_end_time = '2019-02-11 23:59:59'
ORDER BY sale_id DESC;


SELECT * FROM `sm_image_link` 
WHERE `drugstore_id` = 1641
-- and `link_end_time`  = '2019-02-11 23:59:59'
and `link_view` in ('essence','all_essence');

SELECT * from pm_prod_sku WHERE sku_id = '25526266';

SELECT * from pm_prod_info  WHERE prod_id = 327974;

SELECT * from pm_prod_sku WHERE pharmacy_huohao = '51602010039';

SELECT * from pm_prod_info  WHERE prod_id = 327974;

SELECT * from om_cart_info WHERE user_id = 9921 and pharmacy_id = 1641;

insert into `medstore`.`sm_user_banner` ( `banner_name`, `banner_image`, `banner_url`, `banner_type`, `pharmacy_id`, `banner_remark`, `banner_create_time`, `banner_update_time`, `banner_flag`, `banner_ratio`, `banner_attr`, `banner_priority`, `banner_param`) values ( '邀请好友个人中心', 'http://ys1000.oss-cn-beijing.aliyuncs.com/dirBrand/personal20015.jpg', null, '0', '1641', null, '2018-08-03 11:40:11', '2018-08-03 11:40:11', '1', '337', 'personal', '1', null);

SELECT * FROM `sm_user_banner` WHERE `pharmacy_id`  = 200  ORDER BY `banner_id` DESC ;

SELECT * FROM `sm_user_banner`
WHERE `pharmacy_id`  = 1641
ORDER BY `banner_id` DESC ;


-- 延期 到  '2019-05-31 23:59:59'


-- gaiyixia 改一下199m的价格
-- UPDATE as_test.`218_fjtt_199m` a ,pm_sku_sale d set d.sale_fee = a.`原价`*100 ,d.sale_price = a.`活动价`*100
-- SELECT * from as_test.218_fjtt_199m a ,pm_sku_sale d
WHERE a.SKU_ID = d.sku_id; -- 1767 



SELECT * FROM `pm_sku_sale` WHERE `sale_remark` like '%1901月 秒杀 fj jia 218%'
 ;

-- update `medstore`.`am_stat_info` set `item_expire_time`='2019-05-31 23:59:59'  
-- SELECT * FROM `am_stat_info`
WHERE `act_id` = 187
 ;

-- update `medstore`.`sm_image_link` set `link_end_time`='2019-05-31 23:59:59'  
-- SELECT * FROM `sm_image_link`
WHERE `drugstore_id` = 1641
and `link_end_time` = '2019-02-28 23:59:59'
ORDER BY `link_id` DESC ;


SELECT * FROM `am_act_info` WHERE `act_id` = 187;
