-- 疗程购和多买少算的修改； 更改疗程购及多买少算的货号、名称前缀、件数、组合原价、组合特价、单盒特价
-- 富锦店的 促销图 配置，列表和详情图 ；
-- 富锦店，列表页的标签配置，缺少，30分必达和破首单免费的标签全店都要
-- 

-- 2 富锦店的 促销图 配置，列表和详情图 ； v
SELECT * from sm_config;
 
 
SELECT * from pm_dir_info WHERE pharmacy_id = 1641;

SELECT * from pm_dir_info WHERE pharmacy_id = 200;

SELECT * from pm_dir_info WHERE dir_code like '%sdmf%';
-- 1002756852	fjttact45sdmf	首单免费

SELECT * from pm_prod_sku b WHERE drugstore_id = 1641; -- 2113


-- 增加一个富锦同泰的目录 item 首单免费
-- INSERT INTO `medstore`.`pm_dir_info` (`dir_id`, `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) VALUES ('1002756852', 'fjttact45sdmf', '首单免费', 'dir', '1', '', '1', '1', '1', '', NULL, '2019-02-13 15:30:50', '2018-11-02 13:25:10', '首单免费 标签 富锦同泰', '', NULL, '1641', '', NULL);


-- 把富锦同泰的商品单独放到一个首单免费的目录里
-- UPDATE pm_sku_dir a ,pm_prod_sku b set a.dir_id = '1002756852',a.dir_code = 'fjttact45sdmf',a.update_time = NOW()
-- SELECT  1002756852 `dir_id`, a.`sku_id`,'fjttact45sdmf' `dir_code`, a.`sku_order`,NOW() `update_time` from pm_sku_dir a ,pm_prod_sku b
WHERE a.sku_id = b.sku_id 
and b.drugstore_id = 1641
and a.dir_code like  '%sdmf%'
;

-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
-- VALUES ('8953601', '1001003037', '25524093', 'ykd21081001FLZYylqx1104', '3', '2019-01-24 18:18:46');
SELECT  1002756852 `dir_id`
, a.`sku_id`
,'fjttact45sdmf' `dir_code`,1 `sku_order`,NOW() `update_time` from pm_prod_sku a 
WHERE a.drugstore_id = 1641
and NOT EXISTS(SELECT * from pm_sku_dir b WHERE a.sku_id = b.sku_id and b.dir_code like '%sdmf%')
;


SELECT * from pm_sku_dir WHERE sku_id = 25524093;
SELECT * from am_stat_info WHERE sku_id = 25524093;

-- 增加一个富锦同泰的活动标签配置 item 首单免费
-- INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`, `item_num`, `item_title`, `activity_img`, `activity_img_ratio`) VALUES ('1400', 'single', '首单免费', '185', '1', '90', 'discount', '新用户首单15元以内免费', '2019-02-13 15:52:51', '2018-11-30 11:15:28', '{\"itemImageR\":\"545\"}', 'http://image.ykd365.cn/act/1902/fjtt_detail.png', '1641', '', '', '', '', '90', '', '', '');

SELECT * FROM am_act_item   WHERE act_id in (185)
 ORDER BY `item_id` DESC;

-- 增加一个 首单免费标签的关联
-- INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('1704', 'category', '1002756852', '1', '分类', '首单免费-1641', '2019-01-28 17:29:55', '2019-01-28 17:29:55');

-- INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('2049', '1704', '1400');


-- 富锦详情页图标
-- UPDATE am_stat_info a ,pm_prod_sku b set other_str1 =  '{"itemImageR":"545","itemImage":"http://image.ykd365.cn/act/1902/fjtt_detail.png"}'
-- SELECT * from am_stat_info a ,pm_prod_sku b
WHERE a.sku_id = b.sku_id
 and b.drugstore_id = 1641
 and item_name = '下单抽奖';


-- 列表页的角标
-- INSERT INTO `medstore`.`pm_label_image` ( `label_name`, `label_url`, `label_url_double`, `label_type`, `label_status`, `label_create_time`, `label_update_time`, `pharmacy_id`, `label_flag`, `sku_id`) 
SELECT '首单免费' `label_name`
,'http://image.ykd365.cn/act/1902/fjtt_list.png' `label_url`
,'http://image.ykd365.cn/act/1902/fjtt_list.png' `label_url_double`
,1 `label_type`
,1 `label_status`
,NOW() `label_create_time`
,NOW() `label_update_time`
,s.drugstore_id `pharmacy_id`
,0 `label_flag`
, s.`sku_id`
 from  pm_prod_sku s
 WHERE drugstore_id = 1641;


SELECT * from am_sku_img ORDER BY link_id DESC;


-- 30分钟必达标志
-- INSERT INTO `medstore`.`am_sku_img` ( `sku_id`, `act_type`, `act_start_time`, `act_end_time`, `act_status`, `create_time`, `update_time`, `act_img_url`, `aspect_ratio`, `remark`)
--  VALUES ('151664', '100120', 'logo', '2018-09-01 00:00:00', '2028-09-01 00:00:00', '1', '2018-09-03 17:43:01', '2018-09-03 17:43:01', 'http://image.ykd365.cn/icon/home/30minbd.png', '393', '周年庆之半小时必达图片');
SELECT `sku_id`,'logo' `act_type`
,'2018-09-01 00:00:00' `act_start_time`
,'2028-09-01 00:00:00' `act_end_time`
,1 `act_status`
,NOW() `create_time`
,NOW() `update_time`
,'http://image.ykd365.cn/icon/home/30minbd.png' `act_img_url`
,393 `aspect_ratio`
,'周年庆之半小时必达图片' `remark` 
from pm_prod_sku a 
WHERE a.drugstore_id = 1641;


SELECT * from pm_label_image WHERE pharmacy_id = 1641 ORDER BY label_id DESC;
 

SELECT * from am_act_item WHERE act_id = 185;


SELECT * from am_act_item a ,am_act_range b ,am_item_range c,pm_dir_info d
WHERE a.item_id = c.item_id
and b.range_id = c.range_id
and b.range_value = d.dir_id
and act_id = 185

-- 疗程购和多买少算的修改； 更改疗程购及多买少算的货号、名称前缀、件数、组合原价、组合特价、单盒特价
SELECT * from as_test.212_fjtt_dmss_gai;


-- 更新基础表的sku和prod信息 多盒
-- UPDATE as_test.212_fjtt_dmss_gai a,pm_prod_sku b ,pm_prod_info c set a.sku_id = b.sku_id,a.prod_id = b.prod_id
-- SELECT * from as_test.212_fjtt_dmss_gai a,pm_prod_sku b ,pm_prod_info c -- ,pm_sku_sale d
WHERE a.old_huohao = b.pharmacy_huohao
and b.prod_id = c.prod_id
-- and b.sku_id = d.sku_id
and b.drugstore_id = 1641

-- and a.sku_id != b.sku_id
;

-- 更新基础表的sku和prod信息 单盒
-- UPDATE as_test.212_fjtt_dmss_gai a,pm_prod_sku b ,pm_prod_info c set a.d_sku_id = b.sku_id,a.d_prod_id = b.prod_id
 -- SELECT * from as_test.212_fjtt_dmss_gai a,pm_prod_sku b ,pm_prod_info c -- ,pm_prod_sku b2
WHERE a.huohao = b.pharmacy_huohao
and b.prod_id = c.prod_id
-- and b.sku_id = d.sku_id
and b.drugstore_id = 1641

-- and a.sku_id != b.sku_id
;


  SELECT * from as_test.212_fjtt_dmss_gai a,pm_prod_sku b ,pm_prod_info c 
WHERE a.sku_id = b.sku_id
and a.prod_id = c.prod_id
 
and b.drugstore_id = 1641
;



-- 去掉不再参加多买少算的商品
-- DELETE b from  as_test.212_fjtt_dmss_gai a,pm_sku_dir b
 -- SELECT b.* from as_test.212_fjtt_dmss_gai a,pm_sku_dir b
WHERE a.sku_id = b.sku_id
and b.dir_code like '1641%'
and a.修改 = '取消疗程购及多买少算'
;


-- 更改疗程购及多买少算的货号、名称前缀、件数、组合原价、组合特价、单盒特价
 
  SELECT b.pharmacy_huohao , a.new_huohao
,b.prod_name ,REPLACE(b.prod_name,a.old_前缀,a.new_前缀)
,b.sku_img,REPLACE(b.sku_img,a.old_前缀,a.new_前缀)
,c.prod_brand,REPLACE(c.prod_brand,a.old_前缀,a.new_前缀)

,c.prod_spec ,c.prod_unit,CONCAT(a.商品规格,'*',a.new_件数,c.prod_unit)
,b.sku_json,CONCAT('{"prod_spec":"',a.商品规格,'*',a.new_件数,c.prod_unit,'"}')

,b.sku_fee,a.新多盒套装原组合价*100
,b.sku_price,a.新多盒套装组合特价*100


,d.packet_name,REPLACE(d.packet_name,a.old_前缀,a.new_前缀)
,d.packet_fee,a.新多盒套装原组合价*100
,d.packet_price,a.新多盒套装组合特价*100
,d.packet_content,REPLACE(d.packet_content,a.old_前缀,a.new_前缀)

,e.packet_sku_price,a.new_d特价*100
,e.sku_amount,a.new_件数
,e.total_price,a.new_件数*a.new_d特价*100
 ,e.*

,d.*
 from as_test.212_fjtt_dmss_gai a,pm_prod_sku b ,pm_prod_info c ,pm_packet_info d ,pm_packet_sku e
WHERE a.sku_id = b.sku_id
and a.prod_id = c.prod_id
and b.drugstore_id = 1641
-- and new_huohao !=''
and a.修改 = '更改疗程购及多买少算的货号、名称前缀、件数、组合原价、组合特价、单盒特价'
and a.sku_id = d.sku_id 
and a.d_sku_id= e.sku_id
;





-- UPDATE as_test.212_fjtt_dmss_gai a,pm_prod_sku b ,pm_prod_info c ,pm_packet_info d ,pm_packet_sku e
 set b.pharmacy_huohao = a.new_huohao
,b.prod_name =REPLACE(b.prod_name,a.old_前缀,a.new_前缀)
,b.sku_img=REPLACE(b.sku_img,a.old_前缀,a.new_前缀)
,c.prod_brand=REPLACE(c.prod_brand,a.old_前缀,a.new_前缀)

,c.prod_spec =CONCAT(a.商品规格,'*',a.new_件数,c.prod_unit)
,b.sku_json=CONCAT('{"prod_spec":"',a.商品规格,'*',a.new_件数,c.prod_unit,'"}')

,b.sku_fee=a.新多盒套装原组合价*100
,b.sku_price=a.新多盒套装组合特价*100


,d.packet_name=REPLACE(d.packet_name,a.old_前缀,a.new_前缀)
,d.packet_fee=a.新多盒套装原组合价*100
,d.packet_price=a.新多盒套装组合特价*100
,d.packet_content=REPLACE(d.packet_content,a.old_前缀,a.new_前缀)

,e.packet_sku_price=a.new_d特价*100
,e.sku_amount=a.new_件数
,e.total_price=a.new_件数*a.new_d特价*100
 
--  from as_test.212_fjtt_dmss_gai a,pm_prod_sku b ,pm_prod_info c ,pm_packet_info d ,pm_packet_sku e
WHERE a.sku_id = b.sku_id
and a.prod_id = c.prod_id
and b.drugstore_id = 1641
-- and new_huohao !=''
and a.修改 = '更改疗程购及多买少算的货号、名称前缀、件数、组合原价、组合特价、单盒特价'
and a.sku_id = d.sku_id 
and a.d_sku_id= e.sku_id
;


-- INSERT INTO `as_test`.`212_fjtt_dmss_gai` (`大类`, `门店中类`, `class`, `xh`, `修改`, `old_huohao`, `new_huohao`, `huohao`, `d_prod_id`, `old_前缀`, `new_前缀`, `商品名称`, `商品规格`, `生产企业`, `old_件数`, `new_件数`
-- , `old_d特价`, `new_d特价`, `老多盒套装原组合价`, `新多盒套装原组合价`, `老多盒套装组合特价`, `新多盒套装组合特价`, `sku_id`, `prod_id`) VALUES ('中西成药', '心脑血管类', '心脑血管', '1', '更改疗程购及多买少算的货号、名称前缀、件数、组合原价、组合特价、单盒特价', '3件装12105010006', '5件装12105010006', '12105010006', '102226', '3件装', '5件装', '硫酸氢氯吡格雷片', '75毫克*7片', 'Sanofi Winthrop Industrie,France', '3', '5', '124.9', '116', '386.7', '645', '374.7', '580', '25526521', '328001');


SELECT  b.* from as_test.212_fjtt_dmss_gai a,pm_prod_sku b
WHERE a.sku_id = b.sku_id
;
SELECT  b.* from as_test.212_fjtt_dmss_gai a,pm_packet_info b
WHERE a.sku_id = b.sku_id
-- and b.dir_code like '1641%'
-- and a.修改 = '取消疗程购及多买少算'
;

SELECT * from pm_prod_sku WHERE pharmacy_huohao = 1641001;


SELECT * from pm_prod_sku WHERE sku_id = 1641001 OR prod_id = 1641001;

SELECT * from pm_packet_info WHERE packet_huohao = 1641001;

SELECT * FROM pm_packet_info ORDER BY packet_id DESC;


SELECT * FROM pm_packet_sku
-- WHERE packet_id = 9335
 ORDER BY link_id DESC;

SELECT * from pm_dir_info WHERE dir_id = 1001000296;

-- 北京的多买少算 疗程购的商品 改价   -- 调整组合原价、组合特价以及疗程购中的单品特价

-- 更新基础表的sku和prod信息 多盒 80
-- insert into `as_test`.`212_bj_dmss_gai` (  `class`, `修改`,  `new_huohao`, `huohao`,  `new_前缀`, `商品名称`, `new_件数`, `new_d特价`,  `新多盒套装原组合价`,  `新多盒套装组合特价`, `sku_id`, `prod_id`, `d_sku_id`, `d_prod_id`,drugstore_id) 
 -- values ( '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
  SELECT a.class `class`,a.备注 `修改`
,康佰馨多盒货号  `new_huohao`,康佰馨单品货号 `huohao`
, 货号前缀 `new_前缀`,单品通用名 `商品名称`,多盒套装件数 `new_件数`
,单盒特价 `new_d特价`
,多盒套装原组合价 `新多盒套装原组合价`
,多盒套装组合特价 `新多盒套装组合特价`
,b.sku_id `sku_id`
,b.prod_id `prod_id`
,b2.sku_id `d_sku_id`
,b2.prod_id `d_prod_id`
,b.drugstore_id drugstore_id
-- ,b.prod_name ,b2.prod_name
 from as_test.212_bj8_dmss_gai a,pm_prod_sku b ,pm_prod_info c,pm_prod_sku b2 ,pm_prod_info c2 -- ,pm_sku_sale d
WHERE a.康佰馨多盒货号 = b.pharmacy_huohao
and b.prod_id = c.prod_id

and a.康佰馨单品货号 = b2.pharmacy_huohao
and b2.prod_id = c2.prod_id
-- and b.sku_id = d.sku_id
and b.drugstore_id    in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)   
and b2.drugstore_id = b.drugstore_id
-- and a.备注 != ''
-- and a.sku_id != b.sku_id
;


 


-- 更新基础表的sku和prod信息 多盒 21
-- insert into `as_test`.`212_bj_dmss_gai` (  `class`, `修改`,  `new_huohao`, `huohao`,  `new_前缀`, `商品名称`, `new_件数`, `new_d特价`,  `新多盒套装原组合价`,  `新多盒套装组合特价`, `sku_id`, `prod_id`, `d_sku_id`, `d_prod_id`,drugstore_id) 
 -- values ( '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
  SELECT a.class `class`,a.备注 `修改`
,康佰馨多盒货号  `new_huohao`,康佰馨单品货号 `huohao`
, 货号前缀 `new_前缀`,单品通用名 `商品名称`,多盒套装件数 `new_件数`
,单盒特价 `new_d特价`
,多盒套装原组合价 `新多盒套装原组合价`
,多盒套装组合特价 `新多盒套装组合特价`
,b.sku_id `sku_id`
,b.prod_id `prod_id`
,b2.sku_id `d_sku_id`
,b2.prod_id `d_prod_id`
,b.drugstore_id drugstore_id
-- ,b.prod_name ,b2.prod_name
 from as_test.212_bj8_dmss_gai a,pm_prod_sku b ,pm_prod_info c,pm_prod_sku b2 ,pm_prod_info c2 -- ,pm_sku_sale d
WHERE a.正济堂多盒货号 = b.pharmacy_huohao
and b.prod_id = c.prod_id

and a.正济堂单品货号 = b2.pharmacy_huohao
and b2.prod_id = c2.prod_id
-- and b.sku_id = d.sku_id
and b.drugstore_id    in   (1610,1611,1612)      
and b2.drugstore_id = b.drugstore_id
;

-- SELECT * from `as_test`.`212_bj_dmss_gai` a ,as_test.212_bj8_dmss_gai b

-- DELETE FROM `as_test`.`212_bj_dmss_gai`

SELECT * from  as_test.212_bj_dmss_gai;


-- 下架的多盒商品 北京 v
-- UPDATE as_test.212_bj_dmss_gai a,pm_prod_sku b  set b.sku_status = 0
-- SELECT * from as_test.212_bj_dmss_gai a,pm_prod_sku b 
 WHERE a.sku_id = b.sku_id
and a.修改 = '下架';

-- 调整组合原价、组合特价以及疗程购中的单品特价
  SELECT b.pharmacy_huohao , a.new_huohao
,b.prod_name ,REPLACE(b.prod_name,a.old_前缀,a.new_前缀)
,b.sku_img,REPLACE(b.sku_img,a.old_前缀,a.new_前缀)
,c.prod_brand,REPLACE(c.prod_brand,a.old_前缀,a.new_前缀)

,c.prod_spec ,c.prod_unit,CONCAT(a.商品规格,'*',a.new_件数,c.prod_unit)
,b.sku_json,CONCAT('{"prod_spec":"',a.商品规格,'*',a.new_件数,c.prod_unit,'"}')

,b.sku_fee,a.新多盒套装原组合价*100
,b.sku_price,a.新多盒套装组合特价*100


,d.packet_name,REPLACE(d.packet_name,a.old_前缀,a.new_前缀)
,d.packet_fee,a.新多盒套装原组合价*100
,d.packet_price,a.新多盒套装组合特价*100
,d.packet_content,REPLACE(d.packet_content,a.old_前缀,a.new_前缀)

,e.packet_sku_price,a.new_d特价*100
,e.sku_amount,a.new_件数
,e.total_price,a.new_件数*a.new_d特价*100
 ,e.*

,d.*
,b.*
 from as_test.212_bj_dmss_gai a,pm_prod_sku b ,pm_prod_info c ,pm_packet_info d ,pm_packet_sku e
WHERE a.sku_id = b.sku_id
and a.prod_id = c.prod_id
 
-- and new_huohao !=''
and a.修改 = '调整组合原价、组合特价以及疗程购中的单品特价'
and a.sku_id = d.sku_id 
and a.d_sku_id= e.sku_id

  ORDER BY a.new_huohao
;


-- 调整组合原价、组合特价以及疗程购中的单品特价
-- UPDATE as_test.212_bj_dmss_gai a,pm_prod_sku b ,pm_prod_info c ,pm_packet_info d ,pm_packet_sku e
 set  
 b.sku_fee=a.新多盒套装原组合价*100
,b.sku_price=a.新多盒套装组合特价*100
 
,d.packet_fee=a.新多盒套装原组合价*100
,d.packet_price=a.新多盒套装组合特价*100
 

,e.packet_sku_price=a.new_d特价*100
 
,e.total_price=a.new_件数*a.new_d特价*100
 
--  from as_test.212_bj_dmss_gai a,pm_prod_sku b ,pm_prod_info c ,pm_packet_info d ,pm_packet_sku e
WHERE a.sku_id = b.sku_id
and a.prod_id = c.prod_id
 
-- and new_huohao !=''
and a.修改 = '调整组合原价、组合特价以及疗程购中的单品特价'
and a.sku_id = d.sku_id 
and a.d_sku_id= e.sku_id
;


SELECT * from pm_prod_sku WHERE drugstore_id = 1641;

SELECT * from sm_image_link WHERE drugstore_id = 1620;
SELECT * from sm_image_link WHERE drugstore_id  =1641;

SELECT * from sm_image_link WHERE  image_url like '%-%'  640-960;


SELECT * from sm_image_logo_window WHERE drugstore_id = 1641

