
-- DELETE from tmp_kbx;

-- 康柏馨
CREATE TEMPORARY TABLE tmp_kbx (     
ydid VARCHAR(50) NOT NULL
) ;   
 
INSERT INTO tmp_kbx (ydid)
VALUES ('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633') -- ; 康柏馨

SELECT * from tmp_kbx;

-- 正济堂
CREATE TEMPORARY TABLE tmp_zjt (     
ydid VARCHAR(50) NOT NULL
) ;   
 
    
INSERT INTO tmp_zjt (ydid)
VALUES ('1610'),('1611'),('1612')

SELECT * from tmp_zjt;

-- 北京13家

CREATE TEMPORARY TABLE tmp_bj (     
ydid VARCHAR(50) NOT NULL
) ;   
 
INSERT INTO tmp_bj (ydid)
VALUES ('1620'),('1621'),('1622'),('1623'),('1627'),('1629'),('1630'),('1631'),('1632'),('1633') -- ; 康柏馨
,('1610'),('1611'),('1612') -- 正济堂
SELECT * from tmp_bj;

SELECT * from pm_dir_info 
--   WHERE dir_code like '%act37%'
ORDER BY dir_id DESC;

SELECT * from pm_dir_info 
  WHERE dir_code like '%act37%'
ORDER BY dir_id DESC;

-- 删掉之家的满减活动
DELETE from pm_sku_dir WHERE dir_id BETWEEN 1000037766 and 1000037790;


SELECT * from pm_sku_dir WHERE dir_id BETWEEN 1000037766 and 1000037790;


SELECT * from pm_dir_info 
  WHERE dir_code like '%act37%' and dir_id >= 1000044957
ORDER BY dir_id DESC;
-- 配置康柏馨满 58-8目录
-- INSERT INTO `medstore`.`pm_dir_info` ( `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
-- 1000037128	200act358z	第二件8折	dir	1		37	2	1			2018-05-16 21:44:22	2018-05-09 17:57:08				200		
-- VALUES ('1000037248', '200act35secondxz', '第二件*折', 'dir', '1', NULL, '21', '16', '3', '', '1000037132', '2018-05-16 21:44:22', '2018-05-14 17:52:34', '', '55月活动--200>>第二件*折', NULL, '200', NULL, NULL);
SELECT CONCAT(ydid,'act3758_8') as `dir_code`
,'满58减8' as `dir_name`
,'sdir' `dir_type`
,'1' `dir_status`
,null `dir_revalue`
,1501 `prod_sum`
,1 `dir_num`
,1 `dir_level`
,null `dir_img`
,null `parent_dir_id`
,NOW() `dir_update_time`
,NOW()  `dir_create_time`
,'满58-8' `dir_remark`-- ,CONCAT(ydid,'正济堂3 满58-8 长期') `dir_remark`
,null `dir_all_name`
,null `category_id`
,ydid as  `pharmacy_id`
,null `dir_gotocata`
,null `dir_main_show`
from tmp_bj;



-- 配置康柏馨满 60-20目录
-- INSERT INTO `medstore`.`pm_dir_info` ( `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
-- 1000037128	200act358z	第二件8折	dir	1		37	2	1			2018-05-16 21:44:22	2018-05-09 17:57:08				200		
-- VALUES ('1000037248', '200act35secondxz', '第二件*折', 'dir', '1', NULL, '21', '16', '3', '', '1000037132', '2018-05-16 21:44:22', '2018-05-14 17:52:34', '', '55月活动--200>>第二件*折', NULL, '200', NULL, NULL);
SELECT CONCAT(ydid,'act3760_20') as `dir_code`
,'满60减20' as `dir_name`
,'sdir' `dir_type`
,'1' `dir_status`
,null `dir_revalue`
,1501 `prod_sum`
,1 `dir_num`
,1 `dir_level`
,null `dir_img`
,null `parent_dir_id`
,NOW() `dir_update_time`
,NOW()  `dir_create_time`
,'满60-20' `dir_remark`
,null `dir_all_name`
,null `category_id`
,ydid as  `pharmacy_id`
,null `dir_gotocata`
,null `dir_main_show`
from tmp_bj;



1000044984	1612act3760_20	满60减20
1000044983	1611act3760_20	满60减20
1000044982	1610act3760_20	满60减20
1000044981	1633act3760_20	满60减20
1000044980	1632act3760_20	满60减20
1000044979	1631act3760_20	满60减20
1000044978	1630act3760_20	满60减20
1000044977	1629act3760_20	满60减20
1000044976	1627act3760_20	满60减20
1000044975	1623act3760_20	满60减20
1000044974	1622act3760_20	满60减20
1000044973	1621act3760_20	满60减20
1000044972	1620act3760_20	满60减20
1000044969	1612act3758_8	满58减8
1000044968	1611act3758_8	满58减8
1000044967	1610act3758_8	满58减8
1000044966	1633act3758_8	满58减8
1000044965	1632act3758_8	满58减8
1000044964	1631act3758_8	满58减8
1000044963	1630act3758_8	满58减8
1000044962	1629act3758_8	满58减8
1000044961	1627act3758_8	满58减8
1000044960	1623act3758_8	满58减8
1000044959	1622act3758_8	满58减8
1000044958	1621act3758_8	满58减8
1000044957	1620act3758_8	满58减8

1000044957 and 1000044969 -- bj mj 58-8
1000044972 and 1000044984 -- bj mj 60-20
 1000044957 and 1000044984 -- bj mj all
-- 58-8 配置关联 kbx
-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('240', '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');
SELECT c.dir_id,b.sku_id,c.dir_code,a.xh as sku_order,NOW()
 ,a.xh , b.pharmacy_huohao,b.prod_name-- ,c.*,a.*
 from as_test.529_bj_mj5086020 a LEFT JOIN pm_prod_sku b on a.`康佰馨货号` = b.pharmacy_huohao  -- and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
LEFT JOIN pm_dir_info c on b.drugstore_id = c.pharmacy_id   and c.dir_id BETWEEN 1000044957 and 1000044984 -- bj mj all
WHERE b.drugstore_id in ( 1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) and c.dir_id BETWEEN 1000044957 and 1000044984 -- bj mj all
and a.满减范围 = c.dir_remark 
-- and  b.drugstore_id = 1620
;


-- 58-8 配置关联 zjt
-- INSERT INTO `medstore`.`pm_sku_dir` (`dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
SELECT c.dir_id,b.sku_id,c.dir_code,1 as sku_order,NOW()
 -- ,b.*,c.*,a.*
 from as_test.529_bj_mj5086020 a LEFT JOIN pm_prod_sku b on a.`正济堂货号` = b.pharmacy_huohao  and b.drugstore_id in (1610,1611,1612)
LEFT JOIN pm_dir_info c on b.drugstore_id = c.pharmacy_id and c.dir_id BETWEEN 1000044957 and 1000044984 -- bj mj all
WHERE b.drugstore_id in (1610,1611,1612) and c.dir_id BETWEEN 1000044957 and 1000044984 -- bj mj all
and a.满减范围 = c.dir_remark 
 
-- and  b.drugstore_id = 1610
;



SELECT * from am_act_info ORDER BY act_id DESC;

-- 172 活动 康柏馨免检
-- INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`)
 VALUES ('172', '康柏馨满减活动长期', 'date', '1', '打折促销', '2018-05-17 15:58:05', '2018-05-17 15:56:04', '2017-05-15 00:00:00', '2019-06-03 23:59:59', '1', '1', '', '', '1623');

SELECT * from am_act_item ORDER BY item_id DESC;


-- 58-8 配置关联
-- INSERT INTO `medstore`.`am_act_item` ( `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) 
-- VALUES ('850', 'single', '特价', '170', '1', '90', 'nothing', '本品享受特价优惠', '2018-05-11 14:35:57', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/tj.jpg\"}', '', '162', '', '', '', '');
SELECT 'multi', '满58减8', '172', '1', '80', 'cut', '满58元减8元/满116元减16元', '2018-05-17 14:35:57', '2018-05-17 17:46:31', '{\"itemLogoR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/mj.png\"}', '', ydid, '', '', '', ''
 from tmp_bj ;



-- 60-20 配置关联
-- INSERT INTO `medstore`.`am_act_item` ( `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) 
-- VALUES ('850', 'single', '特价', '170', '1', '90', 'nothing', '本品享受特价优惠', '2018-05-11 14:35:57', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/tj.jpg\"}', '', '162', '', '', '', '');
SELECT 'multi', '满60减20', '172', '1', '80', 'cut', '满60元减20元/满120元减40元', '2018-05-17 14:35:57', '2018-05-17 17:46:31', '{\"itemLogoR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/mj.png\"}', '', ydid, '', '', '', ''
 from tmp_bj ;

SELECT * from am_act_item ORDER BY item_id DESC;

SELECT * from am_act_range ORDER BY range_id desc;

-- 60-20 配置关联
-- INSERT INTO `medstore`.`am_act_range` ( `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) 
-- VALUES ('977', 'category', '1000037216', '1', '分类', '特价', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
SELECT 'category', dir_id, '1', '分类', '满60减20', NOW() range_update_time, NOW() range_create_time
from pm_dir_info WHERE  dir_id BETWEEN  1000044957 and 1000044984 -- bj mj all


SELECT * from am_item_range ORDER BY link_id DESC;



-- 5.6.2 秒杀 item 和 range 关联
-- INSERT INTO `medstore`.`am_item_range` ( `range_id`, `item_id`)
-- VALUES ('761', '729', '688');
SELECT  -- *,
 b.range_id,c.item_id
--  ,a.dir_code,c.*
from pm_dir_info a 
 LEFT JOIN am_act_range b on a.dir_id = b.range_value
 LEFT JOIN am_act_item c on a.pharmacy_id = c.pharmacy_id  and a.dir_name = c.item_name
WHERE 
 a.dir_id BETWEEN 1000044957 and 1000044984 -- bj mj all 

and a.dir_code like '%act37%'
and c.act_id =172;


SELECT * from am_act_rule ORDER BY rule_id desc;



SELECT * from am_item_details ORDER BY details_id DESC;

--  满232减32
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
-- VALUES ('191', '1', '476', 'cut', 'number', '3000', '2017-06-07 10:01:15', '2017-03-31 00:00:00', '已满58元减8元', '满58元减8元');
SELECT 
 '3', item_id, 'cut', 'number', '3200',NOW(), NOW(), '已满232元减32元', '满232元减32元'
 from am_act_item WHERE act_id = 172 and item_name = '满58减8'



-- 满116减16 
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
-- VALUES ('191', '1', '476', 'cut', 'number', '3000', '2017-06-07 10:01:15', '2017-03-31 00:00:00', '已满58元减8元', '满58元减8元');
SELECT 
 '2', item_id, 'cut', 'number', '1600',NOW(), NOW(), '已满116元减16元', '满116元减16元'
 from am_act_item WHERE act_id = 172 and item_name = '满58减8'




-- 满58元减8元
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
-- VALUES ('191', '1', '476', 'cut', 'number', '3000', '2017-06-07 10:01:15', '2017-03-31 00:00:00', '已满58元减8元', '满58元减8元');
SELECT 
 '1', item_id, 'cut', 'number', '800',NOW(), NOW(), '已满58元减8元', '满58元减8元'
 from am_act_item WHERE act_id = 172 and item_name = '满58减8'  -- '满58-8';




--   满240减80；

INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
SELECT 
 '3', item_id, 'cut', 'number', '8000',NOW(), NOW(), '已满240元减80元', '满240元减80元'
 from am_act_item WHERE act_id = 172 and item_name = '满60减20';


-- 满120减40  
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
SELECT 
 '2', item_id, 'cut', 'number', '4000',NOW(), NOW(), '已满120元减40元', '满120元减40元'
 from am_act_item WHERE act_id = 172 and item_name = '满60减20';

-- 满60元减20元
INSERT INTO `medstore`.`am_item_details` ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
SELECT 
 '1', item_id, 'cut', 'number', '2000',NOW(), NOW(), '已满60元减20元', '满60元减20元'
 from am_act_item WHERE act_id = 172 and item_name = '满60减20';


SELECT * from am_item_details ORDER BY details_id DESC;
SELECT * from am_details_rule ORDER BY rule_id DESC;


-- 58-8
-- INSERT INTO `medstore`.`am_details_rule` ( `details_id`, `rule_id`)
-- VALUES ('11', '15', '10');
SELECT details_id,CASE WHEN details_value = 3200 then 42
WHEN details_value = 1600 then 41
WHEN details_value = 800 then 39

WHEN details_value = 8000 then 44
WHEN details_value = 4000 then 43
WHEN details_value = 2000 then 40
else null
end 
-- ,a.*
from  am_item_details  a 
WHERE details_id  >=427  -- BETWEEN 288 and 297



SELECT * from sm_config ;

SELECT * from am_stat_info ORDER BY stat_id DESC;


SELECT * from am_act_item WHERE item_id BETWEEN 929 and 953;


 
SELECT * FROM am_act_item -- WHERE act_id in (168,169)
 ORDER BY `item_id` DESC; -- WHERE act_id = 160;
SELECT * FROM am_act_range WHERE range_id BETWEEN 1038 and 1062 ORDER BY range_id desc;
-- r 13 i 11

SELECT * FROM am_item_range 
WHERE item_id BETWEEN 929 and 953
ORDER BY `link_id` DESC ;  -- ---------------------



SELECT * FROM am_item_details WHERE item_id BETWEEN 929 and 953 ORDER BY details_id desc;


SELECT * FROM am_details_rule WHERE details_id in (SELECT details_id FROM am_item_details WHERE item_id BETWEEN 929 and 953) ORDER BY `details_id` DESC ; -- ----------


DELETE from am_details_rule WHERE details_id in (SELECT details_id FROM am_item_details WHERE item_id BETWEEN 929 and 953) ORDER BY `details_id` DESC ; -- ----------

DELETE from am_item_details WHERE item_id BETWEEN 929 and 953 ;

DELETE FROM am_item_range  WHERE item_id BETWEEN 929 and 953;

DELETE FROM am_act_range WHERE range_id BETWEEN 1038 and 1062 ORDER BY range_id desc;
DELETE from am_act_item WHERE item_id BETWEEN 929 and 953;