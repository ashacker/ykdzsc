

-- 时间：11月1日-11月30日
-- 主题：健康狂欢月  直降到底不墨迹 
-- 区块：1元必抢 、9.9超值、多买少算一起购、品牌特价秒
-- 创新点：下单后参与互动游戏，100%中奖
-- 、

SELECT * from as_test.`1008_bj_1元必抢（康佰馨）一稿` a1 LEFT JOIN pm_prod_sku s on a1.`康佰馨货号` = s.pharmacy_huohao and s.drugstore_id = 1620 WHERE s.sku_id is null;

SELECT * from as_test.`1008_bj_1元必抢（正济堂）一稿` a1 LEFT JOIN pm_prod_sku s on a1.`正济堂货号` = s.pharmacy_huohao and s.drugstore_id = 1610  WHERE s.sku_id is null;

SELECT * from as_test.`1008_bj_9.9超值（康佰馨）一稿` a1 LEFT JOIN pm_prod_sku s on a1.`康佰馨货号` = s.pharmacy_huohao and s.drugstore_id = 1620  WHERE s.sku_id is null;

SELECT * from as_test.`1008_bj_9.9超值（正济堂）一稿` a1 LEFT JOIN pm_prod_sku s on a1.`正济堂货号` = s.pharmacy_huohao and s.drugstore_id = 1610  WHERE s.sku_id is null;


SELECT * from as_test.`1008_bj_主活动（康佰馨）二稿` a1 LEFT JOIN pm_prod_sku s on a1.`康佰馨货号` = s.pharmacy_huohao and s.drugstore_id = 1620;

SELECT * from as_test.`1008_bj_主活动（正济堂）二稿` a1 LEFT JOIN pm_prod_sku s on a1.`正济堂货号` = s.pharmacy_huohao and s.drugstore_id = 1610;

SELECT * from as_test.`1008_ty_一元` a1 LEFT JOIN pm_prod_sku s on a1.`货号` = s.pharmacy_huohao and s.drugstore_id = 200 ORDER BY 序号;

SELECT * from as_test.`1008_ty_9.9超值` a1 LEFT JOIN pm_prod_sku s on a1.`货号` = s.pharmacy_huohao and s.drugstore_id = 200 ORDER BY 序号;

  

SELECT * from as_test.`1008_bj_所有活动商品`



SELECT * from pm_dir_info d ORDER BY dir_id DESC LIMIT 1000;

SELECT * FROM pm_sku_dir ORDER BY LINK_ID DESC LIMIT 10000;

SELECT * FROM pm_prod_sku WHERE pharmacy_huohao like '%11YYY%' ;

SELECT * FROM pm_prod_sku WHERE pharmacy_huohao like '%11YjY%' ;


-- 创建1元商品和99元商品
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
 SELECT      i.prod_id
,'1' as sku_status
-- ,a.`特价`*100 as sku_price
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 1Y bj' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YYY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_bj_1元必抢（康佰馨）一稿` a 
LEFT JOIN pm_prod_sku i on  (a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) 
-- or (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )) 
-- WHERE I.sku_id IS NOT NULL

 union all 
SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 1Y bj' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YYY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_bj_1元必抢（正济堂）一稿` a 
LEFT JOIN pm_prod_sku i on  -- (a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) 
  (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )
-- WHERE I.sku_id IS NOT NULL
 
 union all 
  SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 1Y ty' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YYY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_ty_一元` a 
LEFT JOIN pm_prod_sku i on  (a.货号 = i.pharmacy_huohao and i.drugstore_id =200 ) 
-- or (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )) 
-- WHERE I.sku_id IS NOT NULL
 union all 
 SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 9Y bj' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YJY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_bj_9.9超值（康佰馨）一稿` a 
LEFT JOIN pm_prod_sku i on  (a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) 
-- or (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )) 
-- WHERE I.sku_id IS NOT NULL

UNION all 
SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 9Y bj' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YJY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_bj_9.9超值（正济堂）一稿` a 
LEFT JOIN pm_prod_sku i on  -- (a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) 
  (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )
-- WHERE I.sku_id IS NOT NULL
 

UNION all 
 SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 9Y ty' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YJY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_ty_9.9超值` a 
LEFT JOIN pm_prod_sku i on  (a.货号 = i.pharmacy_huohao and i.drugstore_id =200 ) 
-- or (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )) 
 WHERE I.sku_id IS NOT NULL;









-- 用这个创建的生产 北京1y商品 kbx
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- SELECT   i.prod_id,'1' as sku_status,i.sku_price as sku_price,i.sku_price as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'5月 秒杀活动' as sku_remark,i.sku_logistics,i.prod_name,CONCAT('5YMX',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
SELECT      i.prod_id
,'1' as sku_status
-- ,a.`特价`*100 as sku_price
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 1Y bj' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YYY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_bj_1元必抢（康佰馨）一稿` a 
LEFT JOIN pm_prod_sku i on  (a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) 
-- or (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )) 
-- WHERE I.sku_id IS NOT NULL
ORDER BY a.`序号`;



-- 用这个创建的生产 北京1y商品 zjt
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- SELECT   i.prod_id,'1' as sku_status,i.sku_price as sku_price,i.sku_price as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'5月 秒杀活动' as sku_remark,i.sku_logistics,i.prod_name,CONCAT('5YMX',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 1Y bj' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YYY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_bj_1元必抢（正济堂）一稿` a 
LEFT JOIN pm_prod_sku i on  -- (a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) 
  (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )
WHERE I.sku_id IS NOT NULL
ORDER BY a.`序号`;


-- 用这个创建的生产 北京1y商品 ty
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- SELECT   i.prod_id,'1' as sku_status,i.sku_price as sku_price,i.sku_price as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'5月 秒杀活动' as sku_remark,i.sku_logistics,i.prod_name,CONCAT('5YMX',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 1Y ty' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YYY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_ty_一元` a 
LEFT JOIN pm_prod_sku i on  (a.货号 = i.pharmacy_huohao and i.drugstore_id =200 ) 
-- or (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )) 
WHERE I.sku_id IS NOT NULL
ORDER BY a.`序号`;




-- 用这个创建的生产 北京9y商品 kbx
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- SELECT   i.prod_id,'1' as sku_status,i.sku_price as sku_price,i.sku_price as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'5月 秒杀活动' as sku_remark,i.sku_logistics,i.prod_name,CONCAT('5YMX',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 9Y bj' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YJY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_bj_9.9超值（康佰馨）一稿` a 
LEFT JOIN pm_prod_sku i on  (a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) 
-- or (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )) 
WHERE I.sku_id IS NOT NULL
ORDER BY a.`序号`;



-- 用这个创建的生产 北京1y商品 zjt
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- SELECT   i.prod_id,'1' as sku_status,i.sku_price as sku_price,i.sku_price as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'5月 秒杀活动' as sku_remark,i.sku_logistics,i.prod_name,CONCAT('5YMX',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 9Y bj' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YJY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_bj_9.9超值（正济堂）一稿` a 
LEFT JOIN pm_prod_sku i on  -- (a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) 
  (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )
WHERE I.sku_id IS NOT NULL
ORDER BY a.`序号`;


-- 用这个创建的生产 北京1y商品 ty
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- SELECT   i.prod_id,'1' as sku_status,i.sku_price as sku_price,i.sku_price as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'5月 秒杀活动' as sku_remark,i.sku_logistics,i.prod_name,CONCAT('5YMX',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
SELECT      i.prod_id
,'1' as sku_status
,a.`市场价`*100 as sku_price
,a.`市场价`*100 as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'11月 9Y ty' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('11YJY',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.xh
from
as_test.`1008_ty_9.9超值` a 
LEFT JOIN pm_prod_sku i on  (a.货号 = i.pharmacy_huohao and i.drugstore_id =200 ) 
-- or (a.正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )) 
WHERE I.sku_id IS NOT NULL
ORDER BY a.`序号`;

SELECT * from pm_sku_sale ORDER BY sale_id DESC limit 1000;


-- 1元和99元商品的价格配置
-- INSERT INTO `medstore`.`pm_sku_sale` (  `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
--  VALUES ('364577', '25506122', '950', '100', '2018-11-06 20:00:00', '2018-11-06 21:59:59', '0', '2018-09-15 23:40:26', '2018-09-15 23:40:26', '9月 秒杀 ty');
SELECT `sku_id`
,sku_fee `sale_fee`
,100 `sale_price`
,'2017-11-01 00:00:00' `sale_start_time`
,'2018-11-30 23:59:59'  `sale_end_time`
,1 `sale_status`
,NOW() `sale_create_time`
,NOW() `sale_update_time`
,'11月 1Y ' `sale_remark` 
from pm_prod_sku
WHERE sku_remark like '11月 1Y%'
UNION all
SELECT `sku_id`
,sku_fee `sale_fee`
,990 `sale_price`
,'2017-11-01 00:00:00' `sale_start_time`
,'2018-11-30 23:59:59'  `sale_end_time`
,1 `sale_status`
,NOW() `sale_create_time`
,NOW() `sale_update_time`
,'11月 1Y ' `sale_remark` 
from pm_prod_sku
WHERE sku_remark like '11月 9Y%';

-- 用这个创建的生产 天一秒杀 

SELECT * from pm_prod_sku 
WHERE sku_remark like '%11月%'
ORDER BY sku_id  desc;

-- 新建活动 act44 11月狂欢

-- 创建活动目录 zsc  dir_code dir_name img  dir_all_name
-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'44') AS dir_code 
, CONCAT('11月活动--',pharmacy_id) AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum, 28 AS dir_num,
'2' AS dir_level,  'http://image.ykd365.cn/icon/act/ashacker/181001/bj_01.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'#8dffb7' dir_remark,CONCAT(dir_all_name	,'--2018-11月活动') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 1000034627 AND 1000034639  or dir_id = 1000034601;
/*
1001000414	ykd21061013act44	11月活动--1633
1001000413	ykd21061012act44	11月活动--1632
1001000412	ykd21061011act44	11月活动--1631
1001000411	ykd21061010act44	11月活动--1630
1001000410	ykd21061009act44	11月活动--1629
1001000409	ykd21061007act44	11月活动--1627
1001000408	ykd21061004act44	11月活动--1623
1001000407	ykd21061003act44	11月活动--1622
1001000406	ykd21061002act44	11月活动--1621
1001000405	ykd21061001act44	11月活动--1620
1001000404	ykd21051003act44	11月活动--1612
1001000403	ykd21051002act44	11月活动--1611
1001000402	ykd21051001act44	11月活动--1610
1001000401	ykd20001001act44	11月活动--200
*/



-- 主会场目录 	6z
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'06') AS dir_code 
, '8折区' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 6 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/6z.jpg'
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>8折区') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000401 and 1001000414;


-- 主会场目录 	9z
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'09') AS dir_code 
, '9折区' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 9 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/9z.jpg'
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>9折区') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000401 and 1001000414;


-- 主会场目录 	9.5z
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'10') AS dir_code 
, '9.5折区' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 10 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/95z.jpg'
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>95折区') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000401 and 1001000414;





-- 分会场 	1元
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'01') AS dir_code 
, '1元必抢' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 10 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/bj_1y.jpg'
, null AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>1元必抢') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000401 and 1001000414;



-- 分会场 	9.9元
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'02') AS dir_code 
, '9.9超值' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 10 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/bj_9y.jpg'
, null AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>9.9超值') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000401 and 1001000414;


-- 分会场 	多买少算
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'03') AS dir_code 
, '多买少算' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 4 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/bj_dmss.jpg'
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>多买少算') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000401 and 1001000414;

/*
1001000444	ykd21061013act4402	9.9超值	11月活动--1633>>9.9超值
1001000443	ykd21061012act4402	9.9超值	11月活动--1632>>9.9超值
1001000442	ykd21061011act4402	9.9超值	11月活动--1631>>9.9超值
1001000441	ykd21061010act4402	9.9超值	11月活动--1630>>9.9超值
1001000440	ykd21061009act4402	9.9超值	11月活动--1629>>9.9超值
1001000439	ykd21061007act4402	9.9超值	11月活动--1627>>9.9超值
1001000438	ykd21061004act4402	9.9超值	11月活动--1623>>9.9超值
1001000437	ykd21061003act4402	9.9超值	11月活动--1622>>9.9超值
1001000436	ykd21061002act4402	9.9超值	11月活动--1621>>9.9超值
1001000435	ykd21061001act4402	9.9超值	11月活动--1620>>9.9超值
1001000434	ykd21051003act4402	9.9超值	11月活动--1612>>9.9超值
1001000433	ykd21051002act4402	9.9超值	11月活动--1611>>9.9超值
1001000432	ykd21051001act4402	9.9超值	11月活动--1610>>9.9超值
1001000431	ykd20001001act4402	9.9超值	11月活动--200>>9.9超值

1001000429	ykd21061013act4401	1元必抢	11月活动--1633>>1元必抢
1001000428	ykd21061012act4401	1元必抢	11月活动--1632>>1元必抢
1001000427	ykd21061011act4401	1元必抢	11月活动--1631>>1元必抢
1001000426	ykd21061010act4401	1元必抢	11月活动--1630>>1元必抢
1001000425	ykd21061009act4401	1元必抢	11月活动--1629>>1元必抢
1001000424	ykd21061007act4401	1元必抢	11月活动--1627>>1元必抢
1001000423	ykd21061004act4401	1元必抢	11月活动--1623>>1元必抢
1001000422	ykd21061003act4401	1元必抢	11月活动--1622>>1元必抢
1001000421	ykd21061002act4401	1元必抢	11月活动--1621>>1元必抢
1001000420	ykd21061001act4401	1元必抢	11月活动--1620>>1元必抢
1001000419	ykd21051003act4401	1元必抢	11月活动--1612>>1元必抢
1001000418	ykd21051002act4401	1元必抢	11月活动--1611>>1元必抢
1001000417	ykd21051001act4401	1元必抢	11月活动--1610>>1元必抢
1001000416	ykd20001001act4401	1元必抢	11月活动--200>>1元必抢
*/

-- fen会场目录 	1y
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'01') AS dir_code 
, '1元区' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 1 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/1y_01.jpg'
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>1元区') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000416 and 1001000429;

-- fen会场目录 	1y
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'02') AS dir_code 
, '1元区' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 2 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/1y_02.jpg'
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>1元区') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000416 and 1001000429;


-- 主会场目录 	5z
 -- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'01') AS dir_code 
, '9.9元区' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 1 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/9y_01.jpg'
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>9.9元区') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000431 and 1001000444 -- 1001000401 and 1001000414;


-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'02') AS dir_code 
, '9.9元区底部' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 2 AS dir_num,'3' AS dir_level
, 'http://image.ykd365.cn/icon/act/ashacker/181001/9y_02.jpg'
, dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time, dir_remark
,CONCAT(dir_name	,'>>9.9元区') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1001000431 and 1001000444 -- 1001000401 and 1001000414;
/*
1001000567	ykd21061013act4402	9.9元区
1001000566	ykd21061012act4402	9.9元区
1001000565	ykd21061011act4402	9.9元区
1001000564	ykd21061010act4402	9.9元区
1001000563	ykd21061009act4402	9.9元区
1001000562	ykd21061007act4402	9.9元区
1001000561	ykd21061004act4402	9.9元区
1001000560	ykd21061003act4402	9.9元区
1001000559	ykd21061002act4402	9.9元区
1001000558	ykd21061001act4402	9.9元区
1001000557	ykd21051003act4402	9.9元区
1001000556	ykd21051002act4402	9.9元区
1001000555	ykd21051001act4402	9.9元区
1001000554	ykd20001001act4402	9.9元区
1001000552	ykd21061013act4401	9.9元区
1001000551	ykd21061012act4401	9.9元区
1001000550	ykd21061011act4401	9.9元区
1001000549	ykd21061010act4401	9.9元区
1001000548	ykd21061009act4401	9.9元区
1001000547	ykd21061007act4401	9.9元区
1001000546	ykd21061004act4401	9.9元区
1001000545	ykd21061003act4401	9.9元区
1001000544	ykd21061002act4401	9.9元区
1001000543	ykd21061001act4401	9.9元区
1001000542	ykd21051003act4401	9.9元区
1001000541	ykd21051002act4401	9.9元区
1001000540	ykd21051001act4401	9.9元区
1001000539 1001000552	ykd20001001act4401	9.9元区
1001000537	ykd21061013act440102	1元区
1001000536	ykd21061012act440102	1元区
1001000535	ykd21061011act440102	1元区
1001000534	ykd21061010act440102	1元区
1001000533	ykd21061009act440102	1元区
1001000532	ykd21061007act440102	1元区
1001000531	ykd21061004act440102	1元区
1001000530	ykd21061003act440102	1元区
1001000529	ykd21061002act440102	1元区
1001000528	ykd21061001act440102	1元区
1001000527	ykd21051003act440102	1元区
1001000526	ykd21051002act440102	1元区
1001000525	ykd21051001act440102	1元区
1001000524	ykd20001001act440102	1元区
1001000522	ykd21061013act440101	1元区
1001000521	ykd21061012act440101	1元区
1001000520	ykd21061011act440101	1元区
1001000519	ykd21061010act440101	1元区
1001000518	ykd21061009act440101	1元区
1001000517	ykd21061007act440101	1元区
1001000516	ykd21061004act440101	1元区
1001000515	ykd21061003act440101	1元区
1001000514	ykd21061002act440101	1元区
1001000513	ykd21061001act440101	1元区
1001000512	ykd21051003act440101	1元区
1001000511	ykd21051002act440101	1元区
1001000510	ykd21051001act440101	1元区
1001000509	ykd20001001act440101	1元区
*/
-- 关联活动

-- 1001000420	ykd21061001act4401

SELECT * from pm_sku_dir ORDER BY link_id DESC  limit 3000;

SELECT * from pm_dir_info d ORDER BY dir_id DESC LIMIT 1000;
/*
1001000429	ykd21061013act4401	1元必抢
1001000428	ykd21061012act4401	1元必抢
1001000427	ykd21061011act4401	1元必抢
1001000426	ykd21061010act4401	1元必抢
1001000425	ykd21061009act4401	1元必抢
1001000424	ykd21061007act4401	1元必抢
1001000423	ykd21061004act4401	1元必抢
1001000422	ykd21061003act4401	1元必抢
1001000421	ykd21061002act4401	1元必抢
1001000420	ykd21061001act4401	1元必抢
1001000419	ykd21051003act4401	1元必抢
1001000418	ykd21051002act4401	1元必抢
1001000417	ykd21051001act4401	1元必抢
1001000416	ykd20001001act4401	1元必抢
*/
-- 5月主活动页  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
 --  ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`1008_bj_1元必抢（康佰馨）一稿` as a , pm_dir_info as c  , pm_prod_sku as b
where 
CONCAT('11YYY',a.康佰馨货号)   = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) 

 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id    BETWEEN 1001000509 and  1001000522 --  1001000417 and 1001000429
-- and c.dir_name = a.目录
;
--  zjt
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--   ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`1008_bj_1元必抢（正济堂）一稿` as a , pm_dir_info as c  , pm_prod_sku as b
where 
-- a.正济堂货号 
CONCAT('11YYY',a.正济堂货号)  = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634)
 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id    BETWEEN  1001000509 and  1001000522-- 1001000417 and 1001000429
;

--  ty
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--     ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`1008_ty_一元` as a , pm_dir_info as c  , pm_prod_sku as b
where 
-- a.货号 
CONCAT('11YYY',a.货号) = b.pharmacy_huohao and b.drugstore_id =200
 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id      BETWEEN  1001000509 and  1001000522--  1001000416 and 1001000429
;




SELECT * from pm_dir_info d ORDER BY dir_id DESC LIMIT 1000;
/*
1001000444	ykd21061013act4402	9.9超值
1001000443	ykd21061012act4402	9.9超值
1001000442	ykd21061011act4402	9.9超值
1001000441	ykd21061010act4402	9.9超值
1001000440	ykd21061009act4402	9.9超值
1001000439	ykd21061007act4402	9.9超值
1001000438	ykd21061004act4402	9.9超值
1001000437	ykd21061003act4402	9.9超值
1001000436	ykd21061002act4402	9.9超值
1001000435	ykd21061001act4402	9.9超值
1001000434	ykd21051003act4402	9.9超值
1001000433	ykd21051002act4402	9.9超值
1001000432	ykd21051001act4402	9.9超值
1001000431	ykd20001001act4402	9.9超值
*/
-- 5月主活动页  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--    ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`1008_bj_9.9超值（康佰馨）一稿` as a , pm_dir_info as c  , pm_prod_sku as b
where 
  CONCAT('11YJY',a.康佰馨货号) = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) 

 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id    BETWEEN 1001000569 and 1001000582 --  1001000539 and 1001000552-- 1001000431 and 1001000444
-- and c.dir_name = a.目录
;
--  zjt
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--   ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`1008_bj_9.9超值（正济堂）一稿` as a , pm_dir_info as c  , pm_prod_sku as b
where 
CONCAT('11YJY',a.正济堂货号)  = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634)
 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id    BETWEEN  1001000569 and 1001000582 -- 1001000539 and 1001000552--   1001000431 and 1001000444
;

--  ty
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--      ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`1008_ty_9.9超值` as a , pm_dir_info as c  , pm_prod_sku as b
where 
CONCAT('11YJY',a.货号) = b.pharmacy_huohao and b.drugstore_id =200
 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id      BETWEEN 1001000569 and 1001000582 --  1001000539 and 1001000552-- 1001000431 and 1001000444
;

SELECT * from pm_sku_dir ORDER BY link_id desc limit 3000;


SELECT * from sm_image_link WHERE link_name = '多买少算' ORDER BY link_id DESC;


-- 5z
SELECT * from pm_dir_info d WHERE dir_code like '%act44%' or dir_code is null ORDER BY dir_id DESC LIMIT 1000;

SELECT * from pm_dir_info d -- WHERE   dir_code is null 
ORDER BY dir_id DESC LIMIT 1000;

/*
1001000459	ykd21061013act4405	5折区
1001000458	ykd21061012act4405	5折区
1001000457	ykd21061011act4405	5折区
1001000456	ykd21061010act4405	5折区
1001000455	ykd21061009act4405	5折区
1001000454	ykd21061007act4405	5折区
1001000453	ykd21061004act4405	5折区
1001000452	ykd21061003act4405	5折区
1001000451	ykd21061002act4405	5折区
1001000450	ykd21061001act4405	5折区
1001000449	ykd21051003act4405	5折区
1001000448	ykd21051002act4405	5折区
1001000447	ykd21051001act4405	5折区
1001000446	ykd20001001act4405	5折区
*/
-- 5月主活动页  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--  ,a.序号 ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao,c.dir_name,a.折扣  ,a.*,b.*,c.*
-- b.*
 from 
as_test.1008_bj_主活动（康佰馨）二稿 as a , pm_dir_info as c  , pm_prod_sku as b
where 
  a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) 

 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id    BETWEEN   1001000446 and  1001000504-- 1001000459
 and c.dir_name = a.折扣
;
--  zjt
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--    ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.1008_bj_主活动（正济堂）二稿   as a , pm_dir_info as c  , pm_prod_sku as b
where 
a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id   in (1610,1611,1612)
 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id    BETWEEN      1001000446 and 1001000504-- 1001000444
and c.dir_name = a.折扣
;

--  ty
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--      ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`1008_ty_主活动表` as a , pm_dir_info as c  , pm_prod_sku as b
where 
a.货号 = b.pharmacy_huohao and b.drugstore_id =200
 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id      BETWEEN    1001000446 and 1001000504-- 1001000444
and c.dir_name = a.折扣
;


SELECT * from pm_sku_dir WHERE dir_code like '%act4402%' ORDER BY link_id DESC limit 5000;

SELECT * FROM pm_prod_sku  ORDER BY SKU_ID DESC;


SELECT * from pm_sku_dir ORDER BY link_id desc limit 3000;


SELECT * from sm_image_link WHERE link_name = '多买少算' ORDER BY link_id DESC;

-- SELECT * from sm_image_link    ORDER BY link_id DESC;


SELECT * from pm_dir_info d  WHERE dir_id = 2241445 -- < 2241456    
ORDER BY dir_id DESC LIMIT 10000;


SELECT * from pm_sku_dir sd -- LEFT JOIN pm_prod_sku s on sd.sku_id = s.sku_id 
WHERE sd.dir_id = 2241445
-- 5z
SELECT * from pm_dir_info d WHERE dir_code like '%act44%' or dir_code is null ORDER BY dir_id DESC LIMIT 1000;

SELECT * from pm_dir_info d -- WHERE   dir_code is null 
ORDER BY dir_id DESC LIMIT 1000;


SELECT a.`商品名称`,a.`康佰馨货号`,a.`正济堂货号`,a.`特价`,a.`市场价`
,b.*,c.*

 from as_test.`1008_bj_组合商品调价合并` a 
LEFT JOIN pm_prod_sku b on  (a.`康佰馨货号` = b.pharmacy_huohao  and b.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) )
or (a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id   in (1610,1611,1612,1634))

LEFT JOIN pm_sku_sale c on b.sku_id = c.sku_id
WHERE  b.drugstore_id=1620

 SELECT * from pm_sku_sale ss ;

-- 北京多买少算商品调整价格
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`) 
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT b.sku_id ,a.`市场价`*100,a.`特价`*100,'2017-11-01 00:00:00','2018-11-30 23:59:59',1,NOW(),NOW(),'11月组合商品调价'
-- ,a.`商品名称`,a.`康佰馨货号`,a.`正济堂货号`,a.`特价`,a.`市场价`,b.*-- ,c.*

 from as_test.`1008_bj_组合商品调价合并` a 
LEFT JOIN pm_prod_sku b on  (a.`康佰馨货号` = b.pharmacy_huohao  and b.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) )
or (a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id   in (1610,1611,1612,1634))

-- LEFT JOIN pm_sku_sale c on b.sku_id = c.sku_id
-- WHERE  b.drugstore_id=1620


SELECT * from as_test.1008_bj_所有活动商品;

SELECT * from as_test.`1008_ty_全场商品折扣力度`;

update as_test.1008_bj_所有活动商品 set 
促销折扣 = 
case when 折扣 = 0.5 then '5折'
when 折扣 = 0.8 then '8折'
when 折扣 = 0.9 then '9折'
when 折扣 = 0.95 then '95折'
else '' end ;


/*
1001000651	tyact4495z	95折
1001000650	tyact449z	9折
1001000649	tyact448z	8折
1001000648	tyact445z	5折
1001000647	bjact4495z	95折
1001000646	bjact449z	9折
1001000645	bjact448z	8折
1001000644	bjact445z	5折
*/

-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
-- SELECT s.sku_id  from  as_test.1008_bj_所有活动商品 a 
-- left JOIN  pm_prod_sku s on   a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) -- (a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) )
-- -- or (a.正济堂 = s.pharmacy_huohao and s.drugstore_id   in (1610,1611,1612,1634))
-- and a.促销折扣 in ('5折','8折','9折','95折') 
-- left JOIN pm_dir_info d on a.促销折扣 = d.dir_name and dir_id BETWEEN 1001000644 and 1001000647
-- 
WHERE d.dir_id is not null
;
-- 北京的折扣商品关联
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT d.dir_id,a.sku_id,d.dir_code,1,NOW() 
-- ,a.促销折扣,a.商品ID,a.康佰馨,a.正济堂,a.通用名
from (
SELECT s.sku_id  ,a.促销折扣-- ,a.商品ID,a.康佰馨,a.正济堂,a.通用名
  from  as_test.1008_bj_所有活动商品 a,  pm_prod_sku s
WHERE  ((a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) -- 24972
  or
  (a.正济堂 = s.pharmacy_huohao and s.drugstore_id   in (1610,1611,1612,1634))) -- 5477

and a.促销折扣 in ('5折','8折','9折','95折') 

) a LEFT JOIN  pm_dir_info d on a.促销折扣 = d.dir_name and dir_id BETWEEN 1001000644 and 1001000647
;

-- 天一的折扣商品关联
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT d.dir_id,a.sku_id,d.dir_code,a.序号,NOW() 
-- ,a.促销折扣,a.商品ID,a.康佰馨,a.正济堂,a.通用名
from (
SELECT s.sku_id  ,a.促销折扣,a.序号 -- ,a.*-- ,a.商品ID,a.康佰馨,a.正济堂,a.通用名
  from  as_test.`1008_ty_全场商品折扣力度`  a,  pm_prod_sku s
WHERE  a.货号 = s.pharmacy_huohao  and s.drugstore_id  =200
and a.促销折扣 in ('5折','8折','9折','95折') 
) a LEFT JOIN  pm_dir_info d on a.促销折扣 = d.dir_name and dir_id BETWEEN 1001000648 and 1001000651

ORDER BY a.序号
;


-- 北京的没有折扣商品关联
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  '1001001036',s.sku_id ,'bjact44sdmf' ,a.xh,NOW() 
-- ,a.促销折扣-- ,a.商品ID,a.康佰馨,a.正济堂,a.通用名
  from  as_test.1008_bj_所有活动商品 a,  pm_prod_sku s
WHERE   (a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) -- 24972
--   or
--   (a.正济堂 = s.pharmacy_huohao and s.drugstore_id   in (1610,1611,1612,1634))  -- 5477
ORDER BY a.xh 
;

-- 北京的没有折扣商品关联 
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  '1001001036',s.sku_id ,'bjact44sdmf' ,a.xh,NOW() 
-- ,a.促销折扣-- ,a.商品ID,a.康佰馨,a.正济堂,a.通用名
  from  as_test.1008_bj_所有活动商品 a,  pm_prod_sku s
WHERE  -- (a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) -- 24972
--   or
   (a.正济堂 = s.pharmacy_huohao and s.drugstore_id   in (1610,1611,1612,1634))  -- 5477
ORDER BY a.xh
;


-- 天一的没有折扣商品关联 
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  '1001001037',s.sku_id ,'tyact44sdmf' ,a.序号,NOW() 
-- ,a.促销折扣-- ,a.商品ID,a.康佰馨,a.正济堂,a.通用名
  from  as_test.`1008_ty_全场商品折扣力度` a,  pm_prod_sku s
WHERE a.货号 = s.pharmacy_huohao  and s.drugstore_id  =200
ORDER BY a.序号
;




SELECT *  from  as_test.`1008_ty_全场商品折扣力度` ORDER BY 序号
SELECT * from pm_prod_sku WHERE sku_id = 10118043;

SELECT * from 


SELECT  a.*
  from  as_test.1008_bj_所有活动商品 a
 

SELECT * from pm_dir_info d           where dir_id BETWEEN 1001000644 and 1001000647
SELECT * from pm_dir_info d WHERE dir_code like '%act44%' or dir_code is null ORDER BY dir_id DESC LIMIT 1000;


-- 配置限购，搞出来一票range
-- INSERT INTO `medstore`.`am_act_range` (  `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) 
-- VALUES ('1388', 'category', '1001000651', '1', '分类', '95折', '2018-10-23 16:23:38', '2018-10-23 16:21:28');
SELECT 'category',dir_id, '1', '分类', '限购',NOW(),now() from pm_dir_info
WHERE dir_id BETWEEN 1001000509 and 1001000522  -- 1元
or dir_id BETWEEN 1001000569 and 1001000582  -- 9.9元
;

-- 关联item和目录
-- INSERT INTO `medstore`.`am_item_range` ( `range_id`, `item_id`)
-- VALUES ('761', '729', '688');
SELECT  -- *,
 b.range_id,'1216' as item_id
from pm_dir_info a 
 LEFT JOIN am_act_range b on a.dir_id = b.range_value
--  LEFT JOIN am_act_item c on   a.dir_name = c.item_name
WHERE 
(a.dir_id BETWEEN 1001000509 and 1001000522
or a.dir_id BETWEEN 1001000569 and 1001000582) --  --  ty  bj秒杀目录 每店四个 
-- 1000036261 and 1000036275
-- 1000036276 and  1000036290 
-- and a.pharmacy_id = 200
-- and c.act_id = 183;



SELECT * from am_item_range;


SELECT * from am_stat_info WHERE sku_id in (25498816,11621301);


SELECT * from pm_prod_info WHERE prod_name like '%坤宁%';





SELECT * from pm_sku_dir WHERE dir_code like 'bjact44%' ORDER BY link_id desc LIMIT 1000;


SELECT * from pm_dir_info ORDER BY `dir_id` DESC ;
--   下面是秒杀了哦

SELECT * from pm_dir_info WHERE dir_code like '%act44ms%'

-- 新的秒杀目录 10点-12点  14点-16点 16点-18点 20点-22点
-- INSERT INTO `medstore`.`pm_dir_info` ( `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`)
-- VALUES ('1000046259', 'ykd20001001act4004', '新人免单', 'dir', '1', NULL, '100', '28', '2', 'http://imgstore.camore.cn/icon/act/ashacker/180615/新人网购首单00c0ff200_02.jpg', '1000046258', '2018-07-27 16:44:18', '2018-06-15 15:23:30', '#e1e1e1', '2018-6月夏凉', NULL, '200', NULL, NULL);
SELECT REPLACE(REPLACE(dir_code,'act40','act44'),'ms9','ms10')  `dir_code`
, CASE when dir_name='09:00:00-10:59:59' then '10:00:00-11:59:59'	
 when dir_name='14:00:00-15:59:59' then '14:00:00-15:59:59'
 when dir_name='16:00:00-17:59:59' then '16:00:00-17:59:59'
when dir_name='20:00:00-21:59:59' then '20:00:00-21:59:59'
else '' end as `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`
,NOW() `dir_update_time`
,NOW() `dir_create_time`
,CASE when dir_name='09:00:00-10:59:59' then '10点-12点'	
 when dir_name='14:00:00-15:59:59' then '14点-16点'
 when dir_name='16:00:00-17:59:59' then '16点-18点'
when dir_name='20:00:00-21:59:59' then '20点-22点'
else '' end as `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show` 
from pm_dir_info WHERE dir_id  BETWEEN 1000046260  and 1000046324; -- 1000046058 and 1000046124;



SELECT * from as_test.`1008_bj_品牌秒杀（康佰馨）一稿`;


-- 4.5   6月  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.`序号` as sku_order,NOW() as update_time 
--   ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao,c.dir_id,c.dir_code,c.dir_name  ,a.*,b.*,c.*
-- b.*
 from 
as_test.1008_bj_品牌秒杀（康佰馨）一稿 as a , pm_dir_info as c  , pm_prod_sku as b
where 
-- (
 (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
--   OR
--  ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634))
-- )

  AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  BETWEEN   1001000906  and 1001000969 -- 1000046058 and 1000046124 --  1000037143 and 1000037206 -- 北京店的秒杀目录 每店四个
and c.dir_remark = a.`时间段` 

-- and c.dir_id = 1001000921
;

SELECT * from pm_sku_dir WHERE  dir_code like '%44ms%' -- dir_id = 1001000921




-- 4.5   6月  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.`序号` as sku_order,NOW() as update_time 
 -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.1008_bj_品牌秒杀（正济堂）一稿 as a , pm_dir_info as c  , pm_prod_sku as b
where 
-- (
--  (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
--   OR
 a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612)
-- )

  AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  BETWEEN   1001000906  and 1001000969 -- 1000046058 and 1000046124 --  1000037143 and 1000037206 -- 北京店的秒杀目录 每店四个
and c.dir_remark = a.`时间段` 


and c.dir_id =1001000907;




-- 4.5   6月  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.`序号` as sku_order,NOW() as update_time 
 -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`1008_ty_秒杀` as a , pm_dir_info as c  , pm_prod_sku as b
where 
-- (
--  (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
--   OR
 a.货号 = b.pharmacy_huohao and b.drugstore_id =200
-- )

  AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  BETWEEN   1001000906  and 1001000969 -- 1000046058 and 1000046124 --  1000037143 and 1000037206 -- 北京店的秒杀目录 每店四个
and c.dir_remark = a.`时间` ;








-- 创建item
-- INSERT INTO `medstore`.`am_act_item` ( `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`, `item_num`, `item_title`) 
-- VALUES ('1063', 'single', '直降！限时秒杀', '175', '1', '90', 'quota', '20:00:00-21:59:59', '2018-06-08 16:39:39', '2018-04-26 17:51:09', '', '', '1635', 'false', '', '', '', NULL, NULL);
SELECT `item_attr`, `item_name`
,183 `act_id`, `item_status`, `item_priority`, `item_type`
,CASE when item_desc='09:00:00-10:59:59' then '10:00:00-11:59:59'	
 when item_desc='14:00:00-15:59:59' then '14:00:00-15:59:59'
 when item_desc='16:00:00-17:59:59' then '16:00:00-17:59:59'
when item_desc='20:00:00-21:59:59' then '20:00:00-21:59:59'
else '' end as `item_desc`
,NOW() `item_update_time`
,NOW() `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`, `item_num`, `item_title` 
from am_act_item a
 WHERE item_id BETWEEN 1077 and  1140;



-- 5.5.2 秒杀目录
-- INSERT INTO `medstore`.`am_act_range` ( `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`)
--  VALUES ('737', 'category', '1000036254', '1', '分类', '1元限购', '2018-04-02 15:18:46', '2018-04-02 15:08:12');

SELECT -- * ,
'category' , a.dir_id,'1' as range_status,'分类' as range_name,  a.dir_name as range_remark,NOW() as  range_update_time,NOW() as range_create_time
-- ,a.*
from pm_dir_info a 
WHERE dir_id BETWEEN   1001000906  and 1001000969;  -- 1000037147 and 1000037225 -- bj 和ty 所有秒杀目录


-- 5.6.2 秒杀 item 和 range 关联
-- INSERT INTO `medstore`.`am_item_range` ( `range_id`, `item_id`)
-- VALUES ('761', '729', '688');
SELECT   
 b.range_id,c.item_id
--  ,a.dir_code  ,c.*
from pm_dir_info a 
 LEFT JOIN am_act_range b on a.dir_id = b.range_value
 LEFT JOIN am_act_item c on a.pharmacy_id = c.pharmacy_id  and a.dir_name = c.item_desc
WHERE 
 a.dir_id BETWEEN 1001000906  and 1001000969   -- bj 和ty 所有秒杀目录
and c.act_id = 183;


 CREATE TEMPORARY TABLE tmp_mstime (     
stime VARCHAR(50) NOT NULL
) ;   
 

INSERT INTO tmp_mstime (stime)
VALUES  
('12-01'),
('12-02'),
('12-03'),
('12-04'),
('12-05')


('10-25'),
('10-26'),
('10-27'),
('10-28'),
('10-29'),
('10-30'),
('10-31'),
('11-01'),
('11-02'),
('11-03'),
('11-04'),
('11-05'),
('11-06'),
('11-07'),
('11-08'),
('11-09'),
('11-10'),
('11-11'),
('11-12'),
('11-13'),
('11-14'),
('11-15'),
('11-16'),
('11-17'),
('11-18'),
('11-19'),
('11-20'),
('11-21'),
('11-22'),
('11-23'),
('11-24'),
('11-25'),
('11-26'),
('11-27'),
('11-28'),
('11-29'),
('11-30')
 ;
 
SELECT * from tmp_mstime;
-- 6.1     每天的 秒杀目录 515 63
-- INSERT INTO `medstore`.`am_stages_sale` ( `pharmacy_id`, `sg_title`, `sg_status`, `sg_start_time`, `sg_end_time`, `quota_id`, `act_id`, `item_id`, `sg_create_time`, `sg_update_time`, `sg_json`, `sg_remark`) 
-- VALUES ('1', '1620', '09:00', '1', '2017-04-25 09:00:00', '2018-04-25 10:59:59', '236', '163', '691', '2018-04-13 17:15:26', '2018-04-13 17:15:28', NULL, NULL);
SELECT  
b.`pharmacy_id`
,sg_title  as `sg_title`
,1 as `sg_status`
,REPLACE(sg_start_time,'04-25',t.stime) as sg_start_time
,REPLACE(sg_end_time,'04-25',t.stime) as `sg_end_time`
,null -- c.`quota_id`
,183 as `act_id`
-- ,CASE WHEN b.`pharmacy_id`=200 then 179 else 200 end as `act_id`
, b.`item_id`
,NOW() as  `sg_create_time`
,NOW() as  `sg_update_time`, `sg_json`, `sg_remark` 
-- ,a.sg_title ,  SUBSTR(b.item_name,1,5)
--  ,b.*,a.*
-- SELECT *
FROM am_stages_sale as a ,am_act_item b -- ,am_quota_info c
,tmp_mstime t
WHERE 
-- b.item_id = c.item_id  -- c.item_id >= 279 and 
  a.sg_title =  SUBSTR(b.item_desc,1,5)
-- SUBSTR(a.sg_title, 2 ,6) LIKE '%'||b.item_name||'%'
-- a.sg_title like b.item_name
and sg_id BETWEEN 1 and 4
and b.item_id BETWEEN 1217 and  1280;-- 天一 和北京 的item




-- 新版秒杀需要配置的数据 新表
-- INSERT INTO `medstore`.`am_stages_sale_detail` (  `sg_id`, `act_id`, `item_id`, `quota_id`, `pharmacy_id`, `dir_id`, `sg_detail_create_time`, `sg_detail_update_time`, `sg_detail_remark`, `sg_detail_flag`, `sg_detail_type`)
--  VALUES ('23', '12', '163', '691', '236', '1620', '1000036249', '2018-08-13 10:20:33', '2018-08-13 10:20:33', '1', '0', '0');
SELECT  a.`sg_id`, a.`act_id`, a.`item_id`, a.`quota_id`, a.`pharmacy_id`
,c.range_value `dir_id`
,NOW() `sg_detail_create_time`
,NOW() `sg_detail_update_time`
,'11月 新秒杀' `sg_detail_remark`
,0 `sg_detail_flag`
, 0`sg_detail_type`
-- ,b.*,c.*
from am_stages_sale a LEFT JOIN am_item_range b on a.item_id = b.item_id
LEFT JOIN am_act_range c on b.range_id = c.range_id
WHERE act_id =183 
AND  a.`sg_id` BETWEEN 24570 and 26937
;


SELECT * from am_stages_sale_detail WHERE sg_id >=24570;

-- 2.3  秒杀商品的价格配置  beijing    这块比较麻烦，需要每个时段单独生效，配置5月15日-6月3日  ，每天四个时段的价格生效时间
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT  i.sku_id,a.市场价*100,a.秒杀价*100 as sale_price
 ,CASE WHEN a.时间段 = '10点-12点' then CONCAT('2018-',t.stime,' 10:00:00')
WHEN a.时间段 = '14点-16点' then CONCAT('2018-',t.stime,' 14:00:00')
WHEN a.时间段 = '16点-18点' then CONCAT('2018-',t.stime,' 16:00:00')
WHEN a.时间段 = '20点-22点' then CONCAT('2018-',t.stime,' 20:00:00')
ELSE '' end  as sale_start_time

,CASE WHEN a.时间段 = '10点-12点' then CONCAT('2018-',t.stime,' 11:59:59')
WHEN a.时间段 = '14点-16点' then CONCAT('2018-',t.stime,' 15:59:59')
WHEN a.时间段 = '16点-18点' then CONCAT('2018-',t.stime,' 17:59:59')
WHEN a.时间段 = '20点-22点' then CONCAT('2018-',t.stime,' 21:59:59')
ELSE null end as sale_end_time 
 ,'0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'11月 秒杀 bj '
--  ,i.*,a.*
from 
as_test.`1008_bj_品牌秒杀（康佰馨）一稿` as a , pm_prod_sku as i,tmp_mstime t
where  -- a.huohao = b.pharmacy_huohao and b.drugstore_id =1520;
a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id  in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) 
--   OR ( a.正济堂货号 = i.pharmacy_huohao and i.drugstore_id in (1610,1611,1612,1634)))
 ORDER BY a.序号; 



-- 2.3  秒杀商品的价格配置  beijing    这块比较麻烦，需要每个时段单独生效，配置5月15日-6月3日  ，每天四个时段的价格生效时间
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT  i.sku_id,a.市场价*100,a.秒杀价*100 as sale_price
 ,CASE WHEN a.时间段 = '10点-12点' then CONCAT('2018-',t.stime,' 10:00:00')
WHEN a.时间段 = '14点-16点' then CONCAT('2018-',t.stime,' 14:00:00')
WHEN a.时间段 = '16点-18点' then CONCAT('2018-',t.stime,' 16:00:00')
WHEN a.时间段 = '20点-22点' then CONCAT('2018-',t.stime,' 20:00:00')
ELSE '' end  as sale_start_time

,CASE WHEN a.时间段 = '10点-12点' then CONCAT('2018-',t.stime,' 11:59:59')
WHEN a.时间段 = '14点-16点' then CONCAT('2018-',t.stime,' 15:59:59')
WHEN a.时间段 = '16点-18点' then CONCAT('2018-',t.stime,' 17:59:59')
WHEN a.时间段 = '20点-22点' then CONCAT('2018-',t.stime,' 21:59:59')
ELSE null end as sale_end_time 
 ,'0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'11月 秒杀 bj '
--  ,i.*,a.*
from 
as_test.`1008_bj_品牌秒杀（正济堂）一稿` as a , pm_prod_sku as i,tmp_mstime t
where  -- a.huohao = b.pharmacy_huohao and b.drugstore_id =1520;
  a.正济堂货号 = i.pharmacy_huohao and i.drugstore_id in (1610,1611,1612,1634)
 ORDER BY a.序号; 



-- 2.3  秒杀商品的价格配置  beijing    这块比较麻烦，需要每个时段单独生效，配置5月15日-6月3日  ，每天四个时段的价格生效时间
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT  i.sku_id,a.市场价*100,a.秒杀价*100 as sale_price
 ,CASE WHEN a.时间段 = '10点-12点' then CONCAT('2018-',t.stime,' 10:00:00')
WHEN a.时间段 = '14点-16点' then CONCAT('2018-',t.stime,' 14:00:00')
WHEN a.时间段 = '16点-18点' then CONCAT('2018-',t.stime,' 16:00:00')
WHEN a.时间段 = '20点-22点' then CONCAT('2018-',t.stime,' 20:00:00')
ELSE '' end  as sale_start_time

,CASE WHEN a.时间段 = '10点-12点' then CONCAT('2018-',t.stime,' 11:59:59')
WHEN a.时间段 = '14点-16点' then CONCAT('2018-',t.stime,' 15:59:59')
WHEN a.时间段 = '16点-18点' then CONCAT('2018-',t.stime,' 17:59:59')
WHEN a.时间段 = '20点-22点' then CONCAT('2018-',t.stime,' 21:59:59')
ELSE null end as sale_end_time 
 ,'0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'11月 秒杀 bj '
--  ,i.*,a.*
from 
as_test.`1008_ty_秒杀` as a , pm_prod_sku as i,tmp_mstime t
where  -- a.huohao = b.pharmacy_huohao and b.drugstore_id =1520;
  a.货号 = i.pharmacy_huohao and i.drugstore_id =200  
 ORDER BY a.序号; 




-- 5.11 限购的商品配置库存
 -- INSERT INTO `medstore`.`am_stock_limit` (`item_id`, `quota_id`, `sku_id`, `sku_total`, `as_remark`,max_total) 
-- VALUES ('4477', '680', '232', '20132684', '100', '1610');

SELECT  -- * ,
DISTINCT
 b.item_id,b.quota_id,a.sku_id,999,'183',999
--   ,b.*,a.*-- ,d.*
from pm_sku_dir a LEFT JOIN am_stages_sale_detail b on a.dir_id = b.dir_id
   
--  LEFT JOIN am_quota_info d on b.item_id = d.item_id
WHERE  a.dir_id BETWEEN  1001000906 and 1001000969 -- bj 和ty 所有秒杀目录
-- and b.item_id = 1344

-- SELECT * from 

SELECT * from pm_sku_dir a WHERE  a.dir_id BETWEEN  1001000906 and 1001000969  

-- SELECT * from pm_dir_info WHERE  dir_id BETWEEN  1000037147 and 1000037225 

--  and i.drugstore_id = 1620/
376628	11691521	4050	3190	2018-10-26 14:00:00	2018-10-26 15:59:59	1	2018-10-25 15:09:28	2018-10-26 14:01:00	11月 秒杀 bj 

SELECT * from pm_sku_sale WHERE sale_end_time BETWEEN '2018-10-26 00:59:59' and '2018-10-26 23:59:59' ORDER BY sale_id DESC;

SELECT * from pm_dir_info WHERE dir_id = 1001000920;

SELECT * from pm_sku_dir limit 100 WHERE dir_id = 1001000920;

SELECT * from sm_config;

-- UPDATE am_stat_info set item_id = 1344-- item_type = 'quota' ,quota_id = 822
-- SELECT * from am_stat_info
 WHERE act_id = 183 and item_name ='限时秒杀'  and item_id  in (1217,1218,1219,1220)-- ORDER BY stat_id DESC;

SELECT * from am_stages_sale_detail WHERE act_id= ORDER BY sg_detail_id DESC;

-- UPDATE am_stages_sale set item_id = 1344 -- quota_id = 822
-- SELECT * from am_stages_sale
 WHERE act_id = 183 
-- and pharmacy_id=1620 and sg_title = '14:00'
ORDER BY sg_id DESC;


-- UPDATE am_stages_sale_detail set  item_id = 1344  -- quota_id = 822
-- SELECT * from am_stages_sale_detail 
WHERE act_id = 183 ORDER BY sg_id DESC;



SELECT * from am_stat_info order




--  四宫格


-- 第一个三宫格
-- INSERT INTO `medstore`.`sm_image_link` (`drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`) 
-- VALUES ('4614', '1620', '2', 'http://imgstore.camore.cn/icon/act/ashacker/180501/act34_5_22.jpg', 'http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1805?&showName=&pageSize=40&dirId=1000036678', 'web2', '女性大牌', '1000035214', '1', '2018-04-26 10:37:49', '2017-12-30 12:11:56', '45', '2017-12-20 00:00:00', '2018-05-23 00:00:00', 'essence', '0');
SELECT pharmacy_id
,1 as seq_num
,'http://imgstore.camore.cn/icon/act/ashacker/181001/促销2.jpg' as image_url
,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1810?dirId=',b.dir_id) as link_url
,'web2' link_type
,'1元必抢' link_name
,null as link_param
,1 link_status
,NOW() as link_update_time,NOW() as link_create_time
,'11月 1Y' as  link_remark
,'2017-11-01 00:00:00' as `link_start_time`
,'2018-11-30 23:59:59'  as `link_end_time`,'essence' as  `link_view` 
,1 as  `link_version`
-- ,b.*,a.*
from sm_image_link a , pm_dir_info b 
WHERE  
a.link_id = 4614 and
b.dir_id BETWEEN 1001000416 and 1001000429; -- 1000036674 and  1000036689 ;


-- 第二个三宫格
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`)
-- VALUES ('4615', '1620', '3', 'http://imgstore.camore.cn/icon/act/ashacker/180501/act34_5_33.jpg', 'http://deve.ykd365.com/medstore/actpage/stripGoods_module?dirId=2241445&showName=', 'web2', '多买少算', '1000035214', '1', '2018-04-27 13:44:39', '2017-12-30 12:11:56', '46', '2017-12-20 00:00:00', '2018-05-23 00:00:00', 'essence', '0');
SELECT pharmacy_id
,2 as seq_num
,'http://imgstore.camore.cn/icon/act/ashacker/181001/促销3.jpg' as image_url
,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1810?dirId=',b.dir_id) as link_url
,'web2' as  link_type
,'9.9超值' link_name
,b.dir_id as link_param
,1 link_status
,NOW() as link_update_time,NOW() as link_create_time
,'11月 9Y 9.9超值' as  link_remark
,'2017-11-01 00:00:00' as `link_start_time`
,'2018-11-30 23:59:59'  as `link_end_time`,'essence' as  `link_view`
 ,1 as `link_version`
-- ,b.*,a.*
from sm_image_link a , pm_dir_info b 
WHERE  
a.link_id = 4615 and
b.dir_id BETWEEN 1001000431 and 1001000444;
-- (b.dir_id BETWEEN 2241442 and  2241455 or b.dir_id = 2031356);


-- 第三个三宫格
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`)
-- VALUES ('4615', '1620', '3', 'http://imgstore.camore.cn/icon/act/ashacker/180501/act34_5_33.jpg', 'http://deve.ykd365.com/medstore/actpage/stripGoods_module?dirId=2241445&showName=', 'web2', '多买少算', '1000035214', '1', '2018-04-27 13:44:39', '2017-12-30 12:11:56', '46', '2017-12-20 00:00:00', '2018-05-23 00:00:00', 'essence', '0');
-- SELECT pharmacy_id
-- ,3 as seq_num
-- ,'http://imgstore.camore.cn/icon/act/ashacker/181001/促销4.jpg' as image_url
-- ,CONCAT('http://deve.ykd365.com/medstore/actpage/stripGoods_module?dirId=',b.dir_id) as link_url,link_type
-- ,'多买少算' link_name
-- ,null as link_param
-- ,0 link_status
-- ,NOW() as link_update_time,NOW() as link_create_time
-- ,'11月 dmss' as  link_remark
-- ,'2017-05-15 00:00:00' as `link_start_time`
-- ,'2018-06-03 23:59:59'  as `link_end_time`,'essence' as  `link_view`
--  ,1 as `link_version`
-- -- ,b.*,a.*
-- from sm_image_link a , pm_dir_info b 
-- WHERE  
-- a.link_id = 4615 and
-- (b.dir_id BETWEEN 2241442 and  2241455 or b.dir_id = 2031356);

-- SELECT * from pm_dir_info WHERE dir_id BETWEEN 2241442 and  2241455

-- 第四个三宫格 new
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`) 
-- VALUES ('4613', '1620', '1', 'http://imgstore.camore.cn/icon/act/ashacker/180501/act34_5_12.jpg', '', 'xsqg', '限时抢购', '1000035214', '1', '2018-04-26 10:49:14', '2017-12-30 12:11:56', '44', '2017-12-20 00:00:00', '2018-05-23 00:00:00', 'essence', '0');
-- SELECT pharmacy_id
-- ,4 as seq_num
-- ,'http://imgstore.camore.cn/icon/act/ashacker/180515/n_act35_g_04.jpg' as image_url
-- , link_url,link_type, link_name
-- ,null as link_param, link_status
-- ,NOW() as link_update_time,NOW() as link_create_time
-- ,null as  link_remark
-- ,'2017-05-15 00:00:00' as `link_start_time`
-- ,'2018-06-03 23:59:59'  as `link_end_time`,'essence' as  `link_view` 
-- ,1 as `link_version`
-- -- ,b.*,a.*
-- from sm_image_link a , pm_dir_info b 
-- WHERE  
-- a.link_id = 4613 and
-- b.dir_id BETWEEN 1000036674 and  1000036689 ;


SELECT * from sm_image_link ORDER BY link_id DESC;


-- 新的版本 44
-- 45
-- 46
-- 47
-- 限时抢购 4  47
-- 多买少算 3 46
-- 第二件*折 2 45
-- 女性大牌 1 44
-- INSERT INTO `medstore`.`lm_tem_item_ins` (`tem_ins_id`, `tem_item_id`, `relation_object`, `relation_id`, `ins_desc`)
SELECT b.tem_ins_id,CASE   
WHEN a.link_name = '9.9超值' then 45 
WHEN a.link_name = '1元必抢' then 44 
else null end tem_item_id
, 'image',a.link_id ,a.link_name
--   ,a.*,b.*
from

 sm_image_link a,lm_tem_instance b
 WHERE link_id BETWEEN 5729 and 5757
and b.tem_ins_id BETWEEN 73  and 87
and a.drugstore_id = b.pharmacy_id
ORDER BY link_id DESC;


 SELECT * FROM  lm_tem_item_ins ORDER BY  `tem_item_ins_id` DESC 

SELECT * from sm_image_link ORDER BY link_id desc;

-- INSERT INTO `medstore`.`sm_image_link` (`link_id`, `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`, `link_version`) 
-- VALUES ('5728', '1620', '1', 'http://ys1000.oss-cn-beijing.aliyuncs.com/icon/act/ashacker/180601/新版轮播.jpg', 'http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1811?&showName=%E5%BA%86%E5%AE%89&pageSize=40&dirId=1001000405', 'url', '父亲节活动', '1', '1', '2018-10-22 15:39:46', '2018-06-11 22:18:34', '6月活动 父亲节', '2018-06-12 00:00:00', '2019-06-20 23:59:59', '', '1');

-- 7.1。1
-- 轮播
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`,link_version) 
SELECT -- a.*,
a.pharmacy_id
,1 as seq_num
 ,'http://image.ykd365.cn/icon/act/ashacker/181001/11banner.jpg' as image_url
,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1811?dirId=',a.dir_id) as link_url
,'url' as link_type,'健康狂欢月' as link_name,'1' as link_param,'1' as link_status
,NOW() as link_update_time,NOW() as link_create_time
,'11月 主会场' as link_remark
,'2017-11-01 00:00:00' as `link_start_time`
,'2018-11-30 23:59:59'  as `link_end_time`,'' as  `link_view`
,1 as link_version
 from  pm_dir_info a 
WHERE a.dir_id BETWEEN  1001000401  and 1001000414 ; -- 200;

/*
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('912', '1', '1215', 'discount', 'rate', '95', '2018-10-24 15:51:01', '2018-10-23 16:28:07', '直降到底', '95折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('911', '1', '1214', 'discount', 'rate', '90', '2018-10-24 15:51:01', '2018-10-23 16:28:07', '直降到底', '9折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('910', '1', '1213', 'discount', 'rate', '80', '2018-10-24 15:51:01', '2018-10-23 16:28:07', '直降到底', '8折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('909', '1', '1212', 'discount', 'rate', '50', '2018-10-24 15:51:01', '2018-10-23 16:28:07', '直降到底', '5折');
*/


-- INSERT INTO `medstore`.`pm_sku_sale` (  `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
--  VALUES ('411147', '590098', '3580', '2890', '2018-11-30 20:00:00', '2018-11-30 21:59:59', '0', '2018-10-28 13:12:22', '2018-10-28 13:12:22', '11月 秒杀 bj ');
 
SELECT 
s.sku_id,s.sku_fee
,CASE when a.促销折扣 = '5折' then s.sku_price*0.5
when a.促销折扣 = '8折' then s.sku_price*0.8
when a.促销折扣 = '9折' then s.sku_price*0.9
when a.促销折扣 = '95折' then s.sku_price*0.95
else s.sku_price end sku_price

,'2017-11-01 00:00:00' as sale_start_time
,'2018-11-30 23:59:59'  as sale_end_time
,1 as sale_status
,NOW() sale_create_time
,NOW() sale_update_time
,CONCAT('11月折扣-',a.促销折扣,s.drugstore_id)  sale_remark
-- a.*,s.* 
from  as_test.1008_bj_所有活动商品 a 
left JOIN  pm_prod_sku s on   a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) -- (a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) )
-- or (a.正济堂 = s.pharmacy_huohao and s.drugstore_id   in (1610,1611,1612,1634))
and a.促销折扣 in ('5折','8折','9折','95折') 



-- INSERT INTO `medstore`.`pm_sku_sale` (  `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
--  VALUES ('411147', '590098', '3580', '2890', '2018-11-30 20:00:00', '2018-11-30 21:59:59', '0', '2018-10-28 13:12:22', '2018-10-28 13:12:22', '11月 秒杀 bj ');
SELECT 
s.sku_id,s.sku_fee
,CASE when a.促销折扣 = '5折' then s.sku_price*0.5
when a.促销折扣 = '8折' then s.sku_price*0.8
when a.促销折扣 = '9折' then s.sku_price*0.9
when a.促销折扣 = '95折' then s.sku_price*0.95
else s.sku_price end sku_price

,'2017-11-01 00:00:00' as sale_start_time
,'2018-11-30 23:59:59'  as sale_end_time
,1 as sale_status
,NOW() sale_create_time
,NOW() sale_update_time
,CONCAT('11月折扣-',a.促销折扣,s.drugstore_id)  sale_remark
-- a.*,s.* 
from  as_test.1008_bj_所有活动商品 a 
left JOIN  pm_prod_sku s on  --  a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) -- (a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) )
 a.正济堂 = s.pharmacy_huohao and s.drugstore_id   in (1610,1611,1612,1634) 
and a.促销折扣 in ('5折','8折','9折','95折') 




-- INSERT INTO `medstore`.`pm_sku_sale` (  `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
--  VALUES ('411147', '590098', '3580', '2890', '2018-11-30 20:00:00', '2018-11-30 21:59:59', '0', '2018-10-28 13:12:22', '2018-10-28 13:12:22', '11月 秒杀 bj ');
SELECT 
s.sku_id,s.sku_fee
,CASE when a.促销折扣 = '5折' then s.sku_price*0.5
when a.促销折扣 = '8折' then s.sku_price*0.8
when a.促销折扣 = '9折' then s.sku_price*0.9
when a.促销折扣 = '95折' then s.sku_price*0.95
else s.sku_price end sku_price

,'2017-11-01 00:00:00' as sale_start_time
,'2018-11-30 23:59:59'  as sale_end_time
,1 as sale_status
,NOW() sale_create_time
,NOW() sale_update_time
,CONCAT('11月折扣-',a.促销折扣,s.drugstore_id)  sale_remark
-- a.*,s.* 
from  as_test.`1008_ty_全场商品折扣力度` a 
left JOIN  pm_prod_sku s on  --  a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) -- (a.`康佰馨` = s.pharmacy_huohao  and s.drugstore_id   in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) )
 a.货号 = s.pharmacy_huohao and s.drugstore_id  =200 -- in (1610,1611,1612,1634) 
and a.促销折扣 in ('5折','8折','9折','95折') 











/*


SELECT sd.sku_id,s.sku_fee
,CASE when d.dir_name = ''

s.sku_price
,'2017-11-01 00:00:00' as sale_start_time
,'2018-11-30 23:59:59'  as sale_end_time
,1 as sale_status
,NOW() sale_create_time
,NOW() sale_update_time
,'11月折扣' sale_remark
from  pm_sku_dir sd ,pm_dir_info d,pm_prod_sku s
WHERE sd.dir_id = d.dir_id 
and sd.sku_id = s.sku_id
and d.dir_id BETWEEN 1001000446 and 1001000504;



update as_test.`1008_ty_全场商品折扣力度` set 
折扣  = 
case when 促销折扣 ='5折' then  0.5 
when 促销折扣 = '8折'  then 0.8
when 促销折扣 = '9折'  then  0.9
when 促销折扣 = '95折' then 0.95
else 1 end ;
*/

-- 配置不可用券
-- INSERT INTO `medstore`.`pm_sku_dir` (  `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
--  VALUES ('8698045', '1001000906', '25462477', '1610act44ms10', '1', '2018-10-25 12:06:27');
SELECT '1001001033',sku_id,'act44no',sku_order,NOW() from pm_sku_dir WHERE dir_code like '%act44ms%';

-- INSERT INTO `medstore`.`pm_sku_dir` (  `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT '1001001033',sku_id,'act44no',1,NOW() from pm_prod_sku WHERE sku_remark like '%11月%';

-- INSERT INTO `medstore`.`pm_sku_dir` (  `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT '1001001033',sku_id,'act44no',sku_order,NOW() 
from pm_sku_dir WHERE 
dir_id in ( SELECT dir_id from pm_dir_info WHERE dir_code like '%act2703%'   or dir_code like 'ykd20001001act2401%')

SELECT * from pm_dir_info WHERE dir_id = 2031356;

SELECT * from pm_dir_info WHERE  parent_dir_id  = 2241442;
 
SELECT * from pm_dir_info WHERE dir_name = '新-组合商品';

SELECT * from pm_dir_info WHERE dir_code like '%act2703%'   or dir_code like 'ykd20001001act2401%' 