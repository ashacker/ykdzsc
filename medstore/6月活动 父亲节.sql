
/**
1.创建活动商品 pm_prod_sku
2.创建活动价格 pm_sku_sale
3.创建活动目录 pm_dir_info
4.关联目录和商品（显示关系和活动规则关系） pm_sku_dir 
5.创建活动，活动条款，活动目录， 规则详情，限购，库存 
am_act_info     am_act_item    am_act_range  am_item_range 
am_item_details      am_details_rule    am_act_rule
am_quota_info   am_stock_limit
6.秒杀，三宫格 am_stages_sale   
 lm_template  lm_tem_item  lm_tem_item_ins  lm_tem_instance
7.轮播 弹出框 sm_image_link   sm_image_link_window
8.
*/


1000045802	ykd20001001act38	6月活动--200
1000045801	ykd21071001act38	6月活动--1635
1000045800	ykd21061014act38	6月活动--1634
1000045799	ykd21061013act38	6月活动--1633
1000045798	ykd21061012act38	6月活动--1632
1000045797	ykd21061011act38	6月活动--1631
1000045796	ykd21061010act38	6月活动--1630
1000045795	ykd21061009act38	6月活动--1629
1000045794	ykd21061007act38	6月活动--1627
1000045793	ykd21061004act38	6月活动--1623
1000045792	ykd21061003act38	6月活动--1622
1000045791	ykd21061002act38	6月活动--1621
1000045790	ykd21061001act38	6月活动--1620
1000045789	ykd21051003act38	6月活动--1612
1000045788	ykd21051002act38	6月活动--1611
1000045787	ykd21051001act38	6月活动--1610

1000045787 and 1000045801 -- 北京 38活动目录
1000045802 -- 天一 38活动目录

SELECT * from pm_dir_info 
WHERE dir_code like '%act38%'
ORDER BY dir_id DESC;


SELECT * from pm_dir_info 
-- WHERE dir_code like '%act34%'
ORDER BY dir_id DESC;
-- 
-- 1000037210   36
-- 
-- 1000036896   34

SELECT * from as_test.`61_bj_父亲节会场商品`;

SELECT * from as_test.`61_bj_秒杀商品`;
SELECT * from as_test.`61_bj_女性大牌页面【优化】`;

SELECT * from as_test.`61_ty_会场商品`;

SELECT * from as_test.`61_ty_秒杀页面`;
SELECT * from as_test.`61_ty_女性大牌页面（优化）`;

-- UPDATE as_test.`61_bj_父亲节会场商品` SET fee = fee*100,price = price*100 
-- UPDATE as_test.`61_ty_会场商品` SET fee = fee*100,price = price*100 

-- UPDATE as_test.`61_bj_女性大牌页面【优化】` SET fee = fee*100,price = price*100 
-- UPDATE as_test.`61_ty_女性大牌页面（优化）` SET fee = fee*100,price = price*100 
 

CREATE TEMPORARY TABLE tmp_mstime (     
stime VARCHAR(50) NOT NULL
) ;   
 

INSERT INTO tmp_mstime (stime)
VALUES  
('06-11'),
('06-12'),
('06-13'),
('06-14'),
('06-15'),
('06-16'),
('06-17'),
('06-18')
 ;
INSERT INTO tmp_mstime (stime)
VALUES  
('06-07'),
('06-08'),
('06-09'),
('06-10')
 ;
SELECT * from tmp_mstime;


-- UPDATE as_test.`61_bj_秒杀商品` SET fee = fee*100,price = price*100  
-- UPDATE as_test.`61_bj_秒杀商品` SET 时间段 = '09:00:00-10:59:59' WHERE  时间段 = '9：00-11：00';
UPDATE as_test.`61_bj_秒杀商品` SET 时间段 = '14:00:00-15:59:59' WHERE  时间段 = '14：00-16：00';
UPDATE as_test.`61_bj_秒杀商品` SET 时间段 = '16:00:00-17:59:59' WHERE  时间段 = '16：00-18：00';
UPDATE as_test.`61_bj_秒杀商品` SET 时间段 = '20:00:00-21:59:59' WHERE  时间段 = '20：00-22：00';
-- UPDATE as_test.`61_bj_秒杀商品` a  set a.`old康佰馨货号`= a.`康佰馨货号`,a.`old正济堂货号`= a.`正济堂货号`;
-- UPDATE as_test.`61_bj_秒杀商品` a  set a.`康佰馨货号`= CONCAT('6YMS',a.`old康佰馨货号`),a.`正济堂货号`= CONCAT('6YMS',a.`old正济堂货号`)


-- UPDATE as_test.`61_ty_秒杀页面` SET fee = fee*100,price = price*100  
-- UPDATE as_test.`61_ty_秒杀页面` SET 时间段 = '09:00:00-10:59:59' WHERE  时间段 = '9：00-11：00';
UPDATE as_test.`61_ty_秒杀页面` SET 时间段 = '14:00:00-15:59:59' WHERE  时间段 = '14：00-16：00';
UPDATE as_test.`61_ty_秒杀页面` SET 时间段 = '16:00:00-17:59:59' WHERE  时间段 = '16：00-18：00';
UPDATE as_test.`61_ty_秒杀页面` SET 时间段 = '20:00:00-21:59:59' WHERE  时间段 = '20：00-22：00';
-- UPDATE as_test.`61_ty_秒杀页面` a  set a.`old天一货号`= a.`天一货号` ;
-- UPDATE as_test.`61_ty_秒杀页面` a  set a.`天一货号`= CONCAT('6YMS',a.`old天一货号`) 


SELECT * from pm_prod_sku WHERE prod_name like '%矿物质老年%' and drugstore_id = 1620
11620870	902803	汤臣倍健多种维生素矿物质老年型	142818	1	13760	13760
25497057	6YMS902803	汤臣倍健多种维生素矿物质老年型	142818	1	14800	14800
;

UPDATE pm_prod_sku set source_id = 1  WHERE sku_remark like '%6月%'
SELECT a.* from pm_prod_sku a LEFT JOIN pm_prod_info b on a.prod_id = b.prod_id
 WHERE  a.sku_remark like '%6月%' and a.drugstore_id = 1620 -- sku_id = 11620870
-- and b.prod_id is NULL
ORDER BY 
-- UPDATE pm_prod_sku set sku_status = 1 
-- SELECT * from pm_prod_sku 
WHERE sku_remark like '%6月%'  ORDER BY sku_id DESC;

-- 1.1 用这个创建的生产 北京秒杀商品 v
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- SELECT   i.prod_id,'1' as sku_status,i.sku_price as sku_price,i.sku_price as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'5月 秒杀活动' as sku_remark,i.sku_logistics,i.prod_name,CONCAT('5YMX',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
SELECT      i.prod_id
,'1' as sku_status,a.fee as sku_price,a.fee as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'6月 秒杀活动 bj' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('6YMS',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 -- ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee
from
as_test.`61_bj_秒杀商品` a 
LEFT JOIN pm_prod_sku i on  ((a.old康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in  (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633) ) or (a.old正济堂货号 = i.pharmacy_huohao  and i.drugstore_id in  (1610,1611,1612,1634) )) 
WHERE `prod_id` IS not NULL 
ORDER BY i.drugstore_id,a.序号;




--  1.2 用这个创建的生产 天一秒杀商品 v
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- SELECT   i.prod_id,'1' as sku_status,i.sku_price as sku_price,i.sku_price as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'5月 秒杀活动' as sku_remark,i.sku_logistics,i.prod_name,CONCAT('5YMX',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
SELECT      i.prod_id
,'1' as sku_status,a.fee as sku_price,a.fee as sku_fee,i.drugstore_id as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'6月 秒杀活动 ty' as sku_remark,i.sku_logistics,i.prod_name
,CONCAT('6YMS',i.pharmacy_huohao)  as pharmacy_huohao,'1',i.sku_json,null as sku_attr,'' as sku_img,'' as sku_sum
 ,a.*,i.*,i.sku_price,i.sku_fee,a.price,a.fee,a.序号
from
as_test.`61_ty_秒杀页面` a 
LEFT JOIN pm_prod_sku i on  a.old天一货号 = i.pharmacy_huohao 
 and i.drugstore_id = 200
WHERE `prod_id` IS not NULL 
ORDER BY i.drugstore_id,a.序号;

update `medstore`.`pm_prod_sku` set `sku_status`=0,`source_id`=0 where `sku_remark` LIKE  '6月 秒杀活动%';
SELECT * FROM `pm_prod_sku` WHERE `sku_remark` LIKE  '6月 秒杀活动%'




-- 2.1  主活动  特价商品的价格配置 
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT b.sku_id,a.fee,a.price as sale_price
,'2017-06-11 00:00:00' as sale_start_time
,'2018-06-18 23:59:59' as sale_end_time ,
 '1' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time
,'6月 父亲节 特价商品 '
--  ,b.* 
from 
as_test.`61_bj_父亲节会场商品` as a , pm_prod_sku as b
where 

 ((a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
  OR ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634)))
-- and a.促销形式 = '特价' 
and a.促销形式 = '特价再满减' 
-- and a.促销时间 = '5月15日-6月3日'
-- and  b.drugstore_id = 1620
 ORDER BY  b.drugstore_id,a.xh
;





-- 2.2 主活动  特价商品的价格配置  ty
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT b.sku_id,a.fee,a.price as sale_price
,'2017-06-11 00:00:00' as sale_start_time
,'2019-06-18 23:59:59' as sale_end_time 
,'1' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time
,'6月 父亲节 特价商品 ty '
 -- ,b.*,a.*
from 
as_test.`61_ty_会场商品` as a , pm_prod_sku as b
where 
a.货号 = b.pharmacy_huohao and b.drugstore_id =200 
and a.促销形式 = '特价再满减'  
and a.促销时间 = '长期'
ORDER BY  a.xh
;



-- 2.3  秒杀商品的价格配置  beijing    这块比较麻烦，需要每个时段单独生效，配置5月15日-6月3日  ，每天四个时段的价格生效时间
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT  i.sku_id,a.fee,a.price as sale_price
 ,CASE WHEN a.时间段 = '09:00:00-10:59:59' then CONCAT('2018-',t.stime,' 09:00:00')
WHEN a.时间段 = '14:00:00-15:59:59' then CONCAT('2018-',t.stime,' 14:00:00')
WHEN a.时间段 = '16:00:00-17:59:59' then CONCAT('2018-',t.stime,' 16:00:00')
WHEN a.时间段 = '20:00:00-21:59:59' then CONCAT('2018-',t.stime,' 20:00:00')
ELSE '' end  as sale_start_time

,CASE WHEN a.时间段 = '09:00:00-10:59:59' then CONCAT('2018-',t.stime,' 10:59:59')
WHEN a.时间段 = '14:00:00-15:59:59' then CONCAT('2018-',t.stime,' 15:59:59')
WHEN a.时间段 = '16:00:00-17:59:59' then CONCAT('2018-',t.stime,' 17:59:59')
WHEN a.时间段 = '20:00:00-21:59:59' then CONCAT('2018-',t.stime,' 21:59:59')
ELSE null end as sale_end_time 
 ,'0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'6月 秒杀 bj'
--  ,i.*,a.*
from 
as_test.`61_bj_秒杀商品` as a , pm_prod_sku as i,tmp_mstime t
where  -- a.huohao = b.pharmacy_huohao and b.drugstore_id =1520;
((a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
  OR ( a.正济堂货号 = i.pharmacy_huohao and i.drugstore_id in (1610,1611,1612,1634)))
--  and i.drugstore_id = 1620/
ORDER BY a.序号; 

-- 1.6 秒杀商品的价格配置  tianyi    这块比较麻烦，需要每个时段单独生效，配置5月15日-6月3日  ，每天四个时段的价格生效时间
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT i.sku_id,a.fee,a.price as sale_price
,CASE WHEN a.时间段 = '09:00:00-10:59:59' then CONCAT('2018-',t.stime,' 09:00:00')
WHEN a.时间段 = '14:00:00-15:59:59' then CONCAT('2018-',t.stime,' 14:00:00')
WHEN a.时间段 = '16:00:00-17:59:59' then CONCAT('2018-',t.stime,' 16:00:00')
WHEN a.时间段 = '20:00:00-21:59:59' then CONCAT('2018-',t.stime,' 20:00:00')
ELSE '' end  as sale_start_time

,CASE WHEN a.时间段 = '09:00:00-10:59:59' then CONCAT('2018-',t.stime,' 10:59:59')
WHEN a.时间段 = '14:00:00-15:59:59' then CONCAT('2018-',t.stime,' 15:59:59')
WHEN a.时间段 = '16:00:00-17:59:59' then CONCAT('2018-',t.stime,' 17:59:59')
WHEN a.时间段 = '20:00:00-21:59:59' then CONCAT('2018-',t.stime,' 21:59:59')
ELSE null end as sale_end_time 
,'0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'6月 秒杀 ty'
--  SELECT * 
from 
as_test.`61_ty_秒杀页面` as a , pm_prod_sku as i,tmp_mstime t
where  a.天一货号 = i.pharmacy_huohao and i.drugstore_id =200 
ORDER BY a.序号; 


SELECT * from pm_prod_sku 
-- WHERE pharmacy_huohao = 19100698
ORDER BY 

SELECT * from pm_sku_sale ORDER BY sale_id DESC; 

SELECT * from pm_dir_info WHERE 

-- 3.1 创建活动目录 北京 zsc  dir_code dir_name img  dir_all_name
-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'38') AS dir_code ,
 CONCAT('6月活动--',pharmacy_id) AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum, 28 AS dir_num,'2' AS dir_level
,  'http://imgstore.camore.cn/icon/act/ashacker/180601/父爱如山-厚爱无言f7e36d_01.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'#f7e36d' dir_remark
,CONCAT(dir_all_name	,'--2018-6月活动') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 1000034627 AND 1000034641; --  or dir_id = 1000034601;

-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'38') AS dir_code ,
 CONCAT('6月活动--',pharmacy_id) AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum, 28 AS dir_num,'2' AS dir_level
,  'http://imgstore.camore.cn/icon/act/ashacker/180601/ty_父爱如山-厚爱无言f7e36d_01.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'#f7e36d' dir_remark
,CONCAT(dir_all_name	,'--2018-6月活动') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE    dir_id = 1000034601;




-- 3.3  不会照顾自己的老爸
-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'04') AS dir_code 
, '不会照顾自己的老爸' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 4 AS dir_num,'3' AS dir_level
, 'http://imgstore.camore.cn/icon/act/ashacker/180601/父爱如山-厚爱无言f7e36d_02.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'' as dir_remark
,CONCAT(dir_name	,'>>不会照顾自己的老爸') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1000045787 and 1000045801 or dir_id = 1000045802 ;-- 北京 天一 38活动目录


-- 3.4 粗心的老爸
-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'06') AS dir_code 
, '粗心的老爸' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 6 AS dir_num,'3' AS dir_level
, 'http://imgstore.camore.cn/icon/act/ashacker/180601/父爱如山-厚爱无言f7e36d_04.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'' as  dir_remark
,CONCAT(dir_name	,'>>粗心的老爸') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1000045787 and 1000045801 or dir_id = 1000045802 ;-- 北京 天一 38活动目录


-- 3.5 腼腆的老爸

-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'08') AS dir_code 
, '腼腆的老爸' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 8 AS dir_num,'3' AS dir_level
, 'http://imgstore.camore.cn/icon/act/ashacker/180601/父爱如山-厚爱无言f7e36d_05.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'' as  dir_remark
,CONCAT(dir_name	,'>>腼腆的老爸') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1000045787 and 1000045801 or dir_id = 1000045802 ;-- 北京 天一 38活动目录



-- 3.6 爱喝酒的老爸
-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'10') AS dir_code 
, '爱喝酒的老爸' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 10 AS dir_num,'3' AS dir_level
, 'http://imgstore.camore.cn/icon/act/ashacker/180601/父爱如山-厚爱无言f7e36d_06.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'' as  dir_remark
,CONCAT(dir_name	,'>>爱喝酒的老爸') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1000045787 and 1000045801 or dir_id = 1000045802 ;-- 北京 天一 38活动目录


-- 3.7 黑发年轻的老爸

-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'12') AS dir_code 
, '黑发年轻的老爸' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 12 AS dir_num,'3' AS dir_level
, 'http://imgstore.camore.cn/icon/act/ashacker/180601/父爱如山-厚爱无言f7e36d_07.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'' as  dir_remark
,CONCAT(dir_name	,'>>黑发年轻的老爸') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1000045787 and 1000045801 or dir_id = 1000045802 ;-- 北京 天一 38活动目录




-- 3.8 心脑健康的老爸

-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'14') AS dir_code 
, '心脑健康的老爸' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 14 AS dir_num,'3' AS dir_level
, 'http://imgstore.camore.cn/icon/act/ashacker/180601/父爱如山-厚爱无言f7e36d_08.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'' as  dir_remark
,CONCAT(dir_name	,'>>心脑健康的老爸') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1000045787 and 1000045801 or dir_id = 1000045802 ;-- 北京 天一 38活动目录



-- 3.9 腿脚灵活的老爸

-- INSERT INTO `medstore`.`pm_dir_info` (`dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
SELECT CONCAT(dir_code,'16') AS dir_code 
, '腿脚灵活的老爸' AS dir_name, 'dir' AS dir_type, '1' AS dir_status, NULL AS dir_revalue, '100' AS prod_sum,
 16 AS dir_num,'3' AS dir_level
, 'http://imgstore.camore.cn/icon/act/ashacker/180601/父爱如山-厚爱无言f7e36d_09.jpg', dir_id AS parent_dir_id, NOW() AS dir_update_time, NOW() AS dir_create_time
,'' as  dir_remark
,CONCAT(dir_name	,'>>腿脚灵活的老爸') AS dir_all_name, category_id, pharmacy_id, dir_gotocata, dir_main_show
 FROM pm_dir_info WHERE dir_id BETWEEN 
1000045787 and 1000045801 or dir_id = 1000045802 ;-- 北京 天一 38活动目录




-- 3.12    创建秒杀的目录  ，每个店  天一是单独的 1000036457
-- INSERT INTO `medstore`.`pm_dir_info` ( `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
-- VALUES ('1000036909', 'act34ms20', '20:00:00-21:59:59', 'dir', '1', '', '4', '2', '1', '', NULL, '2018-04-25 15:23:59', '2018-04-25 13:44:13', '', '', NULL, '162', '', NULL);
SELECT  CONCAT(b.pharmacy_id,a.dir_code) as `dir_code`, a.`dir_name`, a.`dir_type`, a.`dir_status`,  a.`dir_revalue`, a.`prod_sum`, a.`dir_num`, a.`dir_level`, a.`dir_img`, a.`parent_dir_id`, a.`dir_update_time`, a.`dir_create_time`
,  a.dir_name  `dir_remark`, a.`dir_all_name`, a.`category_id`, b.`pharmacy_id`, a.`dir_gotocata`, a.`dir_main_show`
-- ,a.*,b.*
 from pm_dir_info a LEFT JOIN pm_dir_info b on  a.dir_id BETWEEN 1000037143 and 1000037146 and  b.dir_id BETWEEN 1000036458 and 1000036472
WHERE -- a.dir_id =b.dir_id and 
  a.dir_id BETWEEN 1000037143 and 1000037146
 and b.dir_id BETWEEN 1000036458 and 1000036472;


--  3.13 创建秒杀的目录  天一  1000036457
-- INSERT INTO `medstore`.`pm_dir_info` ( `dir_code`, `dir_name`, `dir_type`, `dir_status`, `dir_revalue`, `prod_sum`, `dir_num`, `dir_level`, `dir_img`, `parent_dir_id`, `dir_update_time`, `dir_create_time`, `dir_remark`, `dir_all_name`, `category_id`, `pharmacy_id`, `dir_gotocata`, `dir_main_show`) 
-- VALUES ('1000036909', 'act34ms20', '20:00:00-21:59:59', 'dir', '1', '', '4', '2', '1', '', NULL, '2018-04-25 15:23:59', '2018-04-25 13:44:13', '', '', NULL, '162', '', NULL);
SELECT  CONCAT(b.pharmacy_id,a.dir_code) as `dir_code`, a.`dir_name`, a.`dir_type`, a.`dir_status`,  a.`dir_revalue`, a.`prod_sum`, a.`dir_num`, a.`dir_level`, a.`dir_img`, a.`parent_dir_id`, a.`dir_update_time`, a.`dir_create_time`
,  a.dir_name  `dir_remark`, a.`dir_all_name`, a.`category_id`, b.`pharmacy_id`, a.`dir_gotocata`, a.`dir_main_show`
-- ,a.*,b.*
 from pm_dir_info a LEFT JOIN pm_dir_info b on  a.dir_id BETWEEN 1000037143 and 1000037146 and  b.dir_id =1000036457
WHERE -- a.dir_id =b.dir_id and 
  a.dir_id BETWEEN 1000037143 and 1000037146
 and b.dir_id =1000036457;


SELECT * from pm_dir_info ORDER BY dir_id desc;


-- 4.1 6月 主活动页  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--  ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`61_bj_父亲节会场商品` as a , pm_dir_info as c  , pm_prod_sku as b
where 
(
 (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
  OR
 ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634))
)

 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  >=1000045787 
and c.dir_name = a.新楼层
ORDER BY b.drugstore_id,a.序号
;

-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--  ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`61_ty_会场商品` as a , pm_dir_info as c  , pm_prod_sku as b
where 
 a.货号 = b.pharmacy_huohao and b.drugstore_id =200

 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  >=1000045787 
and c.dir_name = a.目录
ORDER BY b.drugstore_id,a.序号
;



-- 4.3   6月  和商品的折扣  关联    北京
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
 --  ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`61_bj_父亲节会场商品` as a , pm_dir_info as c  , pm_prod_sku as b
where 
 (
 (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
   OR
  ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634))
 )
 and c.dir_id >=1000045787 -- beijing折扣目录
and c.dir_remark = a.促销形式
 and c.pharmacy_id = 162 
--   and b.drugstore_id = 1620
 ORDER BY b.drugstore_id,a.序号;

--  4.4   6月   和商品的折扣  关联    ty
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
 -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`61_ty_会场商品` as a , pm_dir_info as c  , pm_prod_sku as b
where 
 a.货号 = b.pharmacy_huohao and b.drugstore_id =200
 and c.dir_id >=1000045787 -- tianyi折扣目录
and c.dir_remark = a.促销形式
and c.pharmacy_id = 200 
 ORDER BY a.序号
;

-- SELECT * from pm_sku_dir ORDER BY link_id DESC;

 


-- 4.5   6月  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)

SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
--  ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`61_bj_秒杀商品` as a , pm_dir_info as c  , pm_prod_sku as b
where 
(
 (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
  OR
 ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634))
)

  AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  BETWEEN    1000046058 and 1000046124 --  1000037143 and 1000037206 -- 北京店的秒杀目录 每店四个
and c.dir_remark = a.`时间段` ;


 SELECT * from pm_sku_dir ORDER BY link_id DESC;-- WHERE dir_id  BETWEEN   1000037143 and 1000037206


-- 4.6   5月秒杀目录  和商品的关联 ty
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)

SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.序号 as sku_order,NOW() as update_time 
 -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`61_ty_秒杀页面` as a , pm_dir_info as c  , pm_prod_sku as b
where 
a.天一货号 = b.pharmacy_huohao and b.drugstore_id =200

  AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  BETWEEN    1000046058 and 1000046124 -- 北京店的秒杀目录 每店四个
and c.dir_name = a.`时间段` ;







SELECT * from am_act_info ORDER BY act_id desc;

-- 5.1 创建活动 act 168 169
-- INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`) VALUES ('174', '天一18年6月活动', 'date', '1', '打折促销', '2018-05-10 15:56:04', '2018-05-10 15:56:04', '2018-06-11 00:00:00', '2018-06-18 23:59:59', '1', '1', '', '', '200');
-- INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`) VALUES ('175', '北京18年6月活动', 'date', '1', '打折促销', '2018-05-10 15:55:18', '2018-05-10 15:56:04', '2018-06-11 00:00:00', '2018-06-18 23:59:59', '1', '1', '', '', '1623');


SELECT * from am_act_item ORDER BY item_id DESC;



-- 5.4.2 秒杀目录   tinayi 
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('1003', 'single', '直降！限时秒杀', '174', '1', '90', 'quota', '20:00:00-21:59:59', '2018-04-26 16:39:56', '2018-04-26 17:51:09', '', '', '200', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('1002', 'single', '直降！限时秒杀', '174', '1', '90', 'quota', '16:00:00-17:59:59', '2018-04-26 16:39:56', '2018-04-26 17:51:09', '', '', '200', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('1001', 'single', '直降！限时秒杀', '174', '1', '90', 'quota', '14:00:00-15:59:59', '2018-04-26 16:39:56', '2018-04-26 17:51:09', '', '', '200', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('1000', 'single', '超级秒杀1元购', '174', '1', '90', 'quota', '09:00:00-10:59:59', '2018-04-26 21:50:11', '2018-04-26 17:51:09', '', '', '200', 'false', '', '', '');



--  5.4.3 秒杀目录 根据天一 创建 北京的item
-- INSERT INTO `medstore`.`am_act_item` ( `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) 
-- VALUES ('698', 'single', '一元限购', '163', '1', '90', 'quota', '1元限购', '2018-04-02 15:16:58', '2018-03-29 17:51:09', NULL, NULL, '1631', NULL, NULL, NULL, NULL);
SELECT  `item_attr`,`item_name`,'173' as `act_id`, `item_status`, `item_priority`, `item_type`,`item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, b.`pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`
-- ,b.*
FROM am_act_item ai  -- LEFT JOIN pm_dir_info b on  b.dir_id BETWEEN   1000036458 and 1000036472 and ai.item_id BETWEEN 762 and  765 -- 天一和北京 的秒杀图
 ,pm_dir_info b
WHERE ai.item_id BETWEEN 1000 and  1003 -- 天一的 秒杀item 
and  b.dir_id BETWEEN 1000036458 and 1000036472; -- 仅仅提供一个 要点北京列表



SELECT * from am_act_range ORDER BY range_id DESC;

-- 5.5 关联目录
-- bj & ty
-- INSERT INTO `medstore`.`am_act_range` ( `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`)
-- VALUES ( 'category', '1000036373', '1', '分类', '特价', '2018-03-30 17:31:27', '2018-03-30 17:29:41');
-- SELECT 
-- 'category' , a.dir_id,'1' as range_status,'分类' as range_name,a.dir_name as range_remark,NOW() as  range_update_time,NOW() as range_create_time
-- -- ,a.*
-- from pm_dir_info a 
-- WHERE dir_id BETWEEN 1000037117 and 1000037130  or dir_id in (1000037142,1000037141);

-- 5.5.2 秒杀目录
-- INSERT INTO `medstore`.`am_act_range` ( `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`)
--  VALUES ('737', 'category', '1000036254', '1', '分类', '1元限购', '2018-04-02 15:18:46', '2018-04-02 15:08:12');

SELECT -- * ,
'category' , a.dir_id,'1' as range_status,'分类' as range_name,a.dir_name as range_remark,NOW() as  range_update_time,NOW() as range_create_time
-- ,a.*
from pm_dir_info a 
WHERE dir_id BETWEEN 1000046058 and   1000046124 -- 1000037147 and 1000037225 -- bj 和ty 所有秒杀目录



-- 5.6.2 秒杀 item 和 range 关联
-- INSERT INTO `medstore`.`am_item_range` ( `range_id`, `item_id`)
-- VALUES ('761', '729', '688');
SELECT   
 b.range_id,c.item_id
-- ,a.dir_code  ,c.*
from pm_dir_info a 
 LEFT JOIN am_act_range b on a.dir_id = b.range_value
 LEFT JOIN am_act_item c on a.pharmacy_id = c.pharmacy_id  and a.dir_remark = c.item_desc
WHERE 
 a.dir_id BETWEEN 1000046051 and   1000046124  -- bj 和ty 所有秒杀目录
and c.act_id in (175,174);


SELECT * from am_item_range



-- 5.7  关联折扣规则  第二件
-- INSERT INTO am_item_details ( `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) 
-- SELECT `details_level`, ai.item_id, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`
-- -- ,ai.*
-- -- , `item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`
--  FROM am_item_details id LEFT JOIN am_act_item ai on id.details_content = ai.item_name
-- WHERE details_id BETWEEN 235 and 239  
-- and ai.item_id BETWEEN 835 and 849;

-- 5.9  关联5.8  第二件
 
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('809', '850', '22');




-- 5.10 限购
-- INSERT INTO `medstore`.`am_quota_info` ( `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) 


-- INSERT INTO `medstore`.`am_quota_info` ( `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) 
-- VALUES ('247', '702', '1:1:1', 'main', 'force', '1', '0', '2018-04-03 17:25:10', '2018-04-02 15:25:29', '每人限购1个', '活动商品每人限购1个', '0');
SELECT `item_id`,'1:1:1' as `quota_rule`  ,'main' `quota_kind`  ,'force' `quota_type`,'1' `quota_status`,'0' `quota_total`,NOW() as `quota_update_time`,NOW() as  `quota_create_time`,'每人限购1个' as `quota_remark`,'活动商品每人限购1个' as  `quota_content`, '1' `qutoa_group`
 from  am_act_item a 
WHERE a.item_id BETWEEN 1000 and  1063-- 天一 和北京 的item
and a.item_remark = '09:00:00-10:59:59';
-- 862 and  925-- 天一 和北京 的item

-- INSERT INTO `medstore`.`am_quota_info` ( `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) 
-- VALUES ('247', '702', '1:1:1', 'main', 'force', '1', '0', '2018-04-03 17:25:10', '2018-04-02 15:25:29', '每人限购1个', '活动商品每人限购1个', '0');
SELECT `item_id`,'10:10:10' as `quota_rule`  ,'main' `quota_kind`  ,'force' `quota_type`,'1' `quota_status`,'0' `quota_total`,NOW() as `quota_update_time`,NOW() as  `quota_create_time`,'每人限购10个' as `quota_remark`,'活动商品每人限购10个' as  `quota_content`, '1' `qutoa_group`
 from  am_act_item a 
WHERE a.item_id BETWEEN 1000 and  1063 -- 天一 和北京 的item
and a.item_remark != '09:00:00-10:59:59';




-- 5.11 限购的商品配置库存
 -- INSERT INTO `medstore`.`am_stock_limit` (`item_id`, `quota_id`, `sku_id`, `sku_total`, `as_remark`) 
-- VALUES ('4477', '680', '232', '20132684', '100', '1610');

SELECT  -- * ,
 d.item_id,d.quota_id,a.sku_id,100,'174'
  ,c.*,d.*,b.*
from pm_sku_dir a LEFT JOIN am_act_range b on a.dir_id = b.range_value
 LEFT JOIN am_item_range c on b.range_id  = c.range_id 
 LEFT JOIN am_quota_info d on c.item_id = d.item_id
WHERE  dir_id BETWEEN 1000046051 and   1000046124   -- bj 和ty 所有秒杀目录
and c.item_id >=1000


-- 6.1     每天的 秒杀目录 515 63
-- INSERT INTO `medstore`.`am_stages_sale` ( `pharmacy_id`, `sg_title`, `sg_status`, `sg_start_time`, `sg_end_time`, `quota_id`, `act_id`, `item_id`, `sg_create_time`, `sg_update_time`, `sg_json`, `sg_remark`) 
-- VALUES ('1', '1620', '09:00', '1', '2017-04-25 09:00:00', '2018-04-25 10:59:59', '236', '163', '691', '2018-04-13 17:15:26', '2018-04-13 17:15:28', NULL, NULL);
SELECT -- b.item_name, 
-- t.stime,
b.`pharmacy_id`
-- CASE WHEN sg_title = '9:00:' then  '09:00' else sg_title end as `sg_title`,
,sg_title  as `sg_title`
,1 as `sg_status`
-- , `sg_start_time`
-- ,DATE_add(sg_start_time, INTERVAL 3 DAY) as sg_start_time
-- ,DATE_add(sg_end_time, INTERVAL 3 DAY) as `sg_end_time`
,REPLACE(sg_start_time,'04-25',t.stime) as sg_start_time
,REPLACE(sg_end_time,'04-25',t.stime) as `sg_end_time`
, c.`quota_id`
,CASE WHEN b.`pharmacy_id`=200 then 174 else 175 end as `act_id`, b.`item_id`
,NOW() as  `sg_create_time`
,NOW() as  `sg_update_time`, `sg_json`, `sg_remark` 
-- ,a.sg_title ,  SUBSTR(b.item_name,1,5)
--  ,b.*,a.*
FROM am_stages_sale as a ,am_act_item b ,am_quota_info c,tmp_mstime t
WHERE 
b.item_id = c.item_id  -- c.item_id >= 279 and 
 and a.sg_title =  SUBSTR(b.item_desc,1,5)
-- SUBSTR(a.sg_title, 2 ,6) LIKE '%'||b.item_name||'%'
-- a.sg_title like b.item_name
and sg_id BETWEEN 1 and 4
and b.item_id BETWEEN 1000 and  1063;-- 天一 和北京 的item






-- -- 7.1.0  直接将上半月的活动延期 ，新版app需要单独配置四宫格
-- UPDATE sm_image_link set link_end_time = '2018-06-03 23:59:59',link_update_time = NOW(),link_status =1
-- WHERE link_remark = '5月活动  女性健康' -- link_view = 'essence' and link_name in ('限时抢购','女性大牌','多买少算') and link_version =0 -- and link_end_time = '2018-05-14 23:59:59'
-- 
SELECT * from sm_image_link 
WHERE link_remark = '6月活动'
ORDER BY link_id desc;
-- 5月活动  儿童节活动
-- 7.1。1
-- 轮播
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`) 
SELECT -- a.*,
a.pharmacy_id
,1 as seq_num
 ,'http://imgstore.camore.cn/icon/act/ashacker/180601/旧版轮播.jpg' as image_url
,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1807?dirId=',a.dir_id) as link_url
,'url' as link_type,'父亲节活动' as link_name,'1' as link_param,'1' as link_status
,NOW() as link_update_time,NOW() as link_create_time
,'6月活动 父亲节' as link_remark
,'2017-06-11 00:00:00' as `link_start_time`
,'2018-06-18 23:59:59'  as `link_end_time`,'' as  `link_view`
 from  pm_dir_info a 
WHERE a.dir_id BETWEEN  

1000045787 and 1000045801
-- 1000036982 and 1000036996 -- or a.dir_id = 1000037132 ; -- 200;


-- 7.1.2 ty
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`) 
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`) 
SELECT -- a.*,
a.pharmacy_id
,1 as seq_num
 ,'http://imgstore.camore.cn/icon/act/ashacker/180601/ty_旧版轮播.jpg' as image_url
,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1807?dirId=',a.dir_id) as link_url
,'url' as link_type,'父亲节活动' as link_name,'1' as link_param,'1' as link_status
,NOW() as link_update_time,NOW() as link_create_time
,'6月活动 父亲节' as link_remark
,'2017-06-11 00:00:00' as `link_start_time`
,'2018-06-18 23:59:59'  as `link_end_time`,'' as  `link_view`
 from  pm_dir_info a 
WHERE a.dir_id = 1000045802
 ; -- 200;



-- 7.1.3
-- 轮播 新版 app
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`,link_version) 
SELECT  
a.pharmacy_id
,1 as seq_num
 ,'http://imgstore.camore.cn/icon/act/ashacker/180601/新版轮播.jpg' as image_url
,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1807?dirId=',a.dir_id) as link_url
,'url' as link_type,'父亲节活动' as link_name,'1' as link_param,'1' as link_status
,NOW() as link_update_time,NOW() as link_create_time
,'6月活动 父亲节' as link_remark
,'2017-06-11 00:00:00' as `link_start_time`
,'2018-06-18 23:59:59'  as `link_end_time`,'' as  `link_view`
,1 as link_version
 from  pm_dir_info a 
WHERE a.dir_id BETWEEN  
1000045787 and 1000045801

-- 7.1.4
-- 轮播 新版 app
-- INSERT INTO `medstore`.`sm_image_link` ( `drugstore_id`, `seq_num`, `image_url`, `link_url`, `link_type`, `link_name`, `link_param`, `link_status`, `link_update_time`, `link_create_time`, `link_remark`, `link_start_time`, `link_end_time`, `link_view`,link_version) 
SELECT -- a.*,
a.pharmacy_id
,1 as seq_num
 ,'http://imgstore.camore.cn/icon/act/ashacker/180601/ty_新版轮播.jpg' as image_url
,CONCAT('http://deve.ykd365.com/medstore/actUserpage/OneYuan_module_1807?dirId=',a.dir_id) as link_url
,'url' as link_type,'父亲节活动' as link_name,'1' as link_param,'1' as link_status
,NOW() as link_update_time,NOW() as link_create_time
,'6月活动 父亲节' as link_remark
,'2017-06-11 00:00:00' as `link_start_time`
,'2018-06-18 23:59:59'  as `link_end_time`,'' as  `link_view`
,1 as link_version
 from  pm_dir_info a 
WHERE a.dir_id = 1000045802; -- 200;

SELECT * from sm_config;

-- UPDATE am_stat_info set item_priority = 70 
-- SELECT * from am_stat_info
 WHERE act_id in (174,175)
and item_name not in ('直降！限时秒杀','超级秒杀1元购')
 ORDER BY stat_id DESC;

SELECT * from pm_dir_info ORDER BY dir_id DESC;

SELECT * from am_act_item ORDER BY item_id desc;

SELECT * from am_act_info ORDER BY act_id desc;

SELECT * from sm_image_link
-- WHERE drugstore_id = 1620
 ORDER BY link_id DESC;

SELECT * from am_stat_info ORDER BY stat_id DESC;


