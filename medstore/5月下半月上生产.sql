update `medstore`.`pm_prod_sku` set `sku_status`=1 -- where `sku_id`=25496682;
-- SELECT * from pm_prod_sku
WHERE `sku_remark` LIKE '55月%'
 ORDER BY sku_id DESC;



CREATE TEMPORARY TABLE tmp_mstime (     
stime VARCHAR(50) NOT NULL
) ;   


INSERT INTO tmp_mstime (stime)
VALUES ('05-15'),
('05-16'),
('05-17'),
('05-18'),
('05-19'),
('05-20'),
('05-21'),
('05-22'),
('05-23'),
('05-24'),
('05-25'),
('05-26'),
('05-27'),
('05-28'),
('05-29'),
('05-30'),
('05-31'),
('05-31'),
('06-01'),
('06-02'),
('06-03');

SELECT * from tmp_mstime;


-- 1.3 儿童节  特价商品的价格配置 
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT b.sku_id,a.fee,a.price as sale_price
,'2018-05-16 00:00:00' as sale_start_time
,'2018-06-03 23:59:59' as sale_end_time ,
 '0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time
,'55月 儿童节 特价商品 '
 -- ,b.* 
from 
as_test.`515_beijing儿童节明细` as a , pm_prod_sku as b
where 

 ((a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
  OR ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634)))
and a.促销形式 = '特价'  
and a.促销时间 = '5月15日-6月3日'
-- and  b.drugstore_id = 1620
 ORDER BY  b.drugstore_id,a.xh
;




-- 1.4 儿童节  特价商品的价格配置  ty
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT b.sku_id,a.fee,a.price as sale_price
,'2018-05-16 00:00:00' as sale_start_time
,'2018-06-03 23:59:59' as sale_end_time 
,'0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time
,'55月 儿童节 特价商品 ty '
--  ,b.*
from 
as_test.`515_tianyi儿童节明细` as a , pm_prod_sku as b
where 
a.货号 = b.pharmacy_huohao and b.drugstore_id =200 
and a.促销形式 = '特价'  
and a.促销时间 = '5月15日-6月3日'
ORDER BY  a.xh
;




-- DELETE from  tmp_mstime;
CREATE TEMPORARY TABLE tmp_mstime (     
stime VARCHAR(50) NOT NULL
) ;  
INSERT INTO tmp_mstime (stime)
VALUES -- ('05-15'),
('05-16'),
('05-17'),
('05-18'),
('05-19'),
('05-20'),
('05-21'),
('05-22'),
('05-23'),
('05-24'),
('05-25'),
('05-26'),
('05-27'),
('05-28'),
('05-29'),
('05-30'),
('05-31'),
('06-01'),
('06-02'),
('06-03');

SELECT * from tmp_mstime;


-- 1.5 秒杀商品的价格配置  beijing    这块比较麻烦，需要每个时段单独生效，配置5月15日-6月3日  ，每天四个时段的价格生效时间
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT  COUNT(*)  FROM (
SELECT  i.sku_id,a.fee,a.price as sale_price
 ,CASE WHEN a.时间段 = '09:00:00-10:59:59' then CONCAT('2018-',t.stime,' 09:00:00')
WHEN a.时间段 = '14:00:00-15:59:59' then CONCAT('2018-',t.stime,' 14:00:00')
WHEN a.时间段 = '16:00:00-17:59:59' then CONCAT('2018-',t.stime,' 16:00:00')
WHEN a.时间段 = '20:00:00-21:59:59' then CONCAT('2018-',t.stime,' 20:00:00')
ELSE '' end  as sale_start_time

,CASE WHEN a.时间段 = '09:00:00-10:59:59' then CONCAT('2018-',t.stime,' 10:59:59')
WHEN a.时间段 = '14:00:00-15:59:59' then CONCAT('2018-',t.stime,' 15:59:59')
WHEN a.时间段 = '16:00:00-17:59:59' then CONCAT('2018-',t.stime,' 17:59:59')
WHEN a.时间段 = '20:00:00-21:59:59' then CONCAT('2018-',t.stime,' 21:59:59')
ELSE null end as sale_end_time 
 ,'0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'55月 秒杀 bj'
--  ,i.*,a.*
from 
as_test.`515_beijing儿童节秒杀` as a , pm_prod_sku as i,tmp_mstime t
where  -- a.huohao = b.pharmacy_huohao and b.drugstore_id =1520;
((a.康佰馨货号 = i.pharmacy_huohao and i.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
  OR ( a.正济堂货号 = i.pharmacy_huohao and i.drugstore_id in (1610,1611,1612,1634)))
--  and i.drugstore_id = 1620/
    ) zzz
ORDER BY a.xh; 



-- 1.6 秒杀商品的价格配置  tianyi    这块比较麻烦，需要每个时段单独生效，配置5月15日-6月3日  ，每天四个时段的价格生效时间
-- INSERT INTO `medstore`.`pm_sku_sale` ( `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
-- VALUES ('6008', '547304', '1900', '1350', '2016-04-29 00:00:00', '2016-05-08 23:59:59', '0', '2016-04-22 17:20:44', '2016-05-09 00:01:00', '限购1430');
SELECT i.sku_id,a.fee,a.price as sale_price
,CASE WHEN a.时间段 = '09:00:00-10:59:59' then CONCAT('2018-',t.stime,' 09:00:00')
WHEN a.时间段 = '14:00:00-15:59:59' then CONCAT('2018-',t.stime,' 14:00:00')
WHEN a.时间段 = '16:00:00-17:59:59' then CONCAT('2018-',t.stime,' 16:00:00')
WHEN a.时间段 = '20:00:00-21:59:59' then CONCAT('2018-',t.stime,' 20:00:00')
ELSE '' end  as sale_start_time

,CASE WHEN a.时间段 = '09:00:00-10:59:59' then CONCAT('2018-',t.stime,' 10:59:59')
WHEN a.时间段 = '14:00:00-15:59:59' then CONCAT('2018-',t.stime,' 15:59:59')
WHEN a.时间段 = '16:00:00-17:59:59' then CONCAT('2018-',t.stime,' 17:59:59')
WHEN a.时间段 = '20:00:00-21:59:59' then CONCAT('2018-',t.stime,' 21:59:59')
ELSE null end as sale_end_time 
,'0' as sale_status,NOW() as sale_create_time,NOW() as  sale_update_time,'55月 秒杀 ty'
--  SELECT * 
from 
as_test.`515_tianyi儿童节秒杀` as a , pm_prod_sku as i,tmp_mstime t
where  a.天一货号 = i.pharmacy_huohao and i.drugstore_id =200 
ORDER BY a.xh;

SELECT * from pm_dir_info 
WHERE dir_code like '%act35%'
ORDER BY dir_id DESC; 



SELECT * from pm_dir_info ORDER BY dir_id desc;


-- 4.1 5月下半月 儿童节主活动页  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time 
 -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`515_beijing儿童节明细` as a , pm_dir_info as c  , pm_prod_sku as b
where 
(
 (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
  OR
 ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634))
)

 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  >=1000036982 
and c.dir_name = a.楼层
ORDER BY b.drugstore_id,a.xh
;



-- 4.2 5月下半月 儿童节主活动页  和商品的关联 ty
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time 
 -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`515_tianyi儿童节明细` as a , pm_dir_info as c  , pm_prod_sku as b
where 
a.货号 = b.pharmacy_huohao and b.drugstore_id =200 
 AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  >=1000036982 -- BETWEEN   1000036096 and 1000036200
and c.dir_name = a.目录
;

SELECT * FROM `pm_dir_info` ORDER BY `dir_id` DESC ;
747
-- DELETE FROM pm_sku_dir WHERE `dir_code` LIKE '%act36%'
SELECT  *  FROM `pm_sku_dir` -- WHERE `dir_code` LIKE '%act36%' 
ORDER BY `link_id` DESC LIMIT 1537
pm_sku_dir

-- 4.3 5月下半月 儿童节 和商品的折扣  关联    北京 xxxx
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time 
 --  ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`515_beijing儿童节明细` as a , pm_dir_info as c  , pm_prod_sku as b
where 
 (
 (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
   OR
  ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634))
 )
 and c.dir_id >=1000036982 -- beijing折扣目录
and c.dir_name = a.促销形式

 and c.pharmacy_id = 162 
and c.`dir_code` LIKE  '%act35%'
--  and b.drugstore_id = 1620
 ORDER BY b.drugstore_id,a.xh;



--  4.4   5月下半月 儿童节  和商品的折扣  关联    ty
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time 
 --  ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`515_tianyi儿童节明细` as a , pm_dir_info as c  , pm_prod_sku as b
where 
 a.货号 = b.pharmacy_huohao and b.drugstore_id =200
 and c.dir_id >=1000036982 -- tianyi折扣目录
and c.dir_name = a.促销形式
and c.pharmacy_id = 200 

and c.`dir_code` LIKE  '%act35%'
 ORDER BY a.xh
;




-- 4.5   5月秒杀目录  和商品的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)

SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time 
--  ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`515_beijing儿童节秒杀` as a , pm_dir_info as c  , pm_prod_sku as b
where 
(
 (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
  OR
 ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634))
)

  AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  BETWEEN   1000037143 and 1000037206 -- 北京店的秒杀目录 每店四个
and c.dir_name = a.`时间段` ;



-- 4.6   5月秒杀目录  和商品的关联 ty
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)

SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time 
 -- ,b.prod_id,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`515_tianyi儿童节秒杀` as a , pm_dir_info as c  , pm_prod_sku as b
where 
a.天一货号 = b.pharmacy_huohao and b.drugstore_id =200

  AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id  BETWEEN   1000037222 and 1000037225 -- 北京店的秒杀目录 每店四个
and c.dir_name = a.`时间段` ;




-- 4.7 5月下半月 儿童节主活动 和 第二件*折的关联 bj
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time 
 -- ,c.dir_name ,a.促销形式,b.prod_id,a.xh,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`515_beijing儿童节明细` as a , pm_dir_info as c  , pm_prod_sku as b
where 
 (
 (a.康佰馨货号 = b.pharmacy_huohao and b.drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)) 
   OR
  ( a.正济堂货号 = b.pharmacy_huohao and b.drugstore_id in (1610,1611,1612,1634))
 )
  AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id >=1000037233 -- beijing折扣目录
and a.促销形式 like '第二件%'
--  and b.drugstore_id = 1620
and c.`dir_code` LIKE  '%act35%'
 ORDER BY b.drugstore_id,a.xh;


-- 4.8 5月下半月 儿童节主活动 和 第二件*折的关联 ty
-- INSERT INTO `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`)
SELECT  c.dir_id as dir_id,b.sku_id as sku_id,c.dir_code as dir_code,a.xh as sku_order,NOW() as update_time 
 -- ,c.dir_name ,a.促销形式,b.prod_id,a.xh,b.prod_name,b.drugstore_id,b.pharmacy_huohao  ,a.*,b.*,c.*
-- b.*
 from 
as_test.`515_tianyi儿童节明细` as a , pm_dir_info as c  , pm_prod_sku as b
where 
 a.货号 = b.pharmacy_huohao and b.drugstore_id =200
  AND  b.drugstore_id = c.pharmacy_id
 and c.dir_id >=1000037233 -- beijing折扣目录
and a.促销形式 like '第二件%'
 and b.drugstore_id = 200
 ORDER BY b.drugstore_id,a.xh;



INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`) VALUES ('171', '天一18年5月下半月活动女性健康延期', 'date', '1', '打折促销', '2018-05-10 15:58:05', '2018-05-10 15:56:04', '2017-05-15 00:00:00', '2018-06-03 23:59:59', '1', '1', '', '', '200');
INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`) VALUES ('170', '北京18年5月下半月活动女性健康延期', 'date', '1', '打折促销', '2018-05-11 09:55:24', '2018-05-10 15:56:04', '2017-05-15 00:00:00', '2018-06-03 23:59:59', '1', '1', '', '', '1623');
INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`) VALUES ('169', '天一18年5月下半月活动', 'date', '1', '打折促销', '2018-05-10 15:58:05', '2018-05-10 15:56:04', '2017-05-15 00:00:00', '2018-06-03 23:59:59', '1', '1', '', '', '200');
INSERT INTO `medstore`.`am_act_info` (`act_id`, `act_name`, `act_type`, `act_status`, `act_content`, `act_update_time`, `act_create_time`, `act_start_time`, `act_end_time`, `act_level`, `act_remark`, `act_img`, `act_url`, `pharmacy_id`) VALUES ('168', '北京18年5月下半月活动', 'date', '1', '打折促销', '2018-05-10 15:58:14', '2018-05-10 15:56:04', '2017-05-15 00:00:00', '2018-06-03 23:59:59', '1', '1', '', '', '1623');


INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('925', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1635', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('924', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1635', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('923', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1635', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('922', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1635', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('921', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1634', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('920', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1634', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('919', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1634', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('918', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1634', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('917', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1633', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('916', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1633', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('915', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1633', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('914', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1633', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('913', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1632', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('912', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1632', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('911', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1632', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('910', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1632', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('909', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1631', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('908', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1631', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('907', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1631', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('906', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1631', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('905', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1630', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('904', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1630', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('903', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1630', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('902', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:12', '2018-04-26 17:51:09', '', '', '1630', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('901', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1629', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('900', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1629', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('899', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1629', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('898', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1629', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('897', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1627', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('896', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1627', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('895', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1627', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('894', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1627', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('893', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1623', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('892', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1623', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('891', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1623', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('890', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1623', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('889', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1622', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('888', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1622', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('887', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1622', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('886', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1622', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('885', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1621', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('884', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1621', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('883', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1621', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('882', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1621', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('881', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1620', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('880', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1620', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('879', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1620', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('878', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1620', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('877', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1612', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('876', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1612', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('875', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1612', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('874', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1612', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('873', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1611', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('872', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1611', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('871', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1611', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('870', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1611', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('869', 'single', '20:00:00-21:59:59', '168', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1610', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('868', 'single', '16:00:00-17:59:59', '168', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1610', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('867', 'single', '14:00:00-15:59:59', '168', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1610', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('866', 'single', '09:00:00-10:59:59', '168', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '1610', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('865', 'single', '20:00:00-21:59:59', '169', '1', '90', 'quota', '20:00:00-21:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '200', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('864', 'single', '16:00:00-17:59:59', '169', '1', '90', 'quota', '16:00:00-17:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '200', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('863', 'single', '14:00:00-15:59:59', '169', '1', '90', 'quota', '14:00:00-15:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '200', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('862', 'single', '09:00:00-10:59:59', '169', '1', '90', 'quota', '09:00:00-10:59:59', '2018-05-15 10:46:13', '2018-04-26 17:51:09', '', '', '200', 'false', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('861', 'single', '第二件9折', '171', '1', '90', 'discount', '本品享同品第二件9折', '2018-05-11 14:36:39', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/9z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('860', 'single', '第二件8折', '171', '1', '90', 'discount', '本品享同品第二件8折', '2018-05-11 14:36:35', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/8z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('859', 'single', '第二件7折', '171', '1', '90', 'discount', '本品享同品第二件7折', '2018-05-11 14:36:33', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/7z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('858', 'single', '第二件6折', '171', '1', '90', 'discount', '本品享同品第二件6折', '2018-05-11 14:36:30', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/6z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('857', 'single', '第二件5折', '171', '1', '90', 'discount', '本品享同品第二件5折', '2018-05-11 14:36:27', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/5z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('856', 'single', '特价', '171', '1', '90', 'nothing', '本品享受特价优惠', '2018-05-11 14:36:21', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/tj.jpg\"}', NULL, '162', NULL, '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('855', 'single', '第二件9折', '170', '1', '90', 'discount', '本品享同品第二件9折', '2018-05-11 14:36:18', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/9z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('854', 'single', '第二件8折', '170', '1', '90', 'discount', '本品享同品第二件8折', '2018-05-11 14:36:14', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/8z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('853', 'single', '第二件7折', '170', '1', '90', 'discount', '本品享同品第二件7折', '2018-05-11 14:36:11', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/7z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('852', 'single', '第二件6折', '170', '1', '90', 'discount', '本品享同品第二件6折', '2018-05-11 14:36:09', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/6z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('851', 'single', '第二件5折', '170', '1', '90', 'discount', '本品享同品第二件5折', '2018-05-11 14:36:05', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/5z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('850', 'single', '特价', '170', '1', '90', 'nothing', '本品享受特价优惠', '2018-05-11 14:35:57', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180501/tj.jpg\"}', '', '162', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('849', 'single', '第二件*折', '169', '1', '90', 'nothing', '本品享受特价优惠', '2018-05-15 16:08:53', '2018-04-04 17:46:31', '第二件*折', '', '200', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('848', 'single', '第二件*折', '168', '1', '90', 'nothing', '本品享受特价优惠', '2018-05-15 16:08:49', '2018-04-04 17:46:31', '第二件*折', '', '162', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('847', 'single', '特价', '169', '1', '90', 'nothing', '本品享受特价优惠', '2018-05-15 16:08:38', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/tj.jpg\"}', NULL, '200', NULL, '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('846', 'single', '第二件9折', '169', '1', '90', 'discount', '本品享同品第二件9折', '2018-05-15 16:08:38', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/9z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('845', 'single', '第二件8折', '169', '1', '90', 'discount', '本品享同品第二件8折', '2018-05-15 16:08:38', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/8z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('844', 'single', '第二件7折', '169', '1', '90', 'discount', '本品享同品第二件7折', '2018-05-15 16:08:38', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/7z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('843', 'single', '第二件6折', '169', '1', '90', 'discount', '本品享同品第二件6折', '2018-05-15 16:08:38', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/6z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('842', 'single', '第二件5折', '169', '1', '90', 'discount', '本品享同品第二件5折', '2018-05-15 16:08:38', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/5z.jpg\"}', NULL, '200', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('841', 'single', '多盒', '168', '1', '90', 'nothing', '本品享受特价优惠', '2018-04-08 10:38:49', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/dh.jpg\"}', '', '162', '', '', '', '');
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('840', 'single', '特价', '168', '1', '90', 'nothing', '本品享受特价优惠', '2018-04-08 10:38:49', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/tj.jpg\"}', NULL, '162', NULL, '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('839', 'single', '第二件9折', '168', '1', '90', 'discount', '本品享同品第二件9折', '2018-04-04 17:46:31', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/9z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('838', 'single', '第二件8折', '168', '1', '90', 'discount', '本品享同品第二件8折', '2018-04-04 17:46:31', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/8z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('837', 'single', '第二件7折', '168', '1', '90', 'discount', '本品享同品第二件7折', '2018-04-04 17:46:31', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/7z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('836', 'single', '第二件6折', '168', '1', '90', 'discount', '本品享同品第二件6折', '2018-04-04 17:46:31', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/6z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);
INSERT INTO `medstore`.`am_act_item` (`item_id`, `item_attr`, `item_name`, `act_id`, `item_status`, `item_priority`, `item_type`, `item_desc`, `item_update_time`, `item_create_time`, `item_remark`, `item_img`, `pharmacy_id`, `item_flag`, `sales_goto_type`, `sales_goto_title`, `sales_goto_url`) VALUES ('835', 'single', '第二件5折', '168', '1', '90', 'discount', '本品享同品第二件5折', '2018-04-04 17:46:31', '2018-04-04 17:46:31', '{\"itemImageR\":\"http://imgstore.camore.cn/icon/act/ashacker/180515/5z.jpg\"}', NULL, '162', 'false', '', NULL, NULL);



INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('986', 'category', '1000037225', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('985', 'category', '1000037224', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('984', 'category', '1000037223', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('983', 'category', '1000037222', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('982', 'category', '1000037221', '1', '分类', '第二件5折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('981', 'category', '1000037220', '1', '分类', '第二件6折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('980', 'category', '1000037219', '1', '分类', '第二件7折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('979', 'category', '1000037218', '1', '分类', '第二件8折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('978', 'category', '1000037217', '1', '分类', '第二件9折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('977', 'category', '1000037216', '1', '分类', '特价', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('976', 'category', '1000037215', '1', '分类', '第二件5折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('975', 'category', '1000037214', '1', '分类', '第二件6折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('974', 'category', '1000037213', '1', '分类', '第二件7折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('973', 'category', '1000037212', '1', '分类', '第二件8折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('972', 'category', '1000037211', '1', '分类', '第二件9折', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('971', 'category', '1000037210', '1', '分类', '特价', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('970', 'category', '1000037206', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('969', 'category', '1000037205', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('968', 'category', '1000037204', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('967', 'category', '1000037203', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('966', 'category', '1000037202', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('965', 'category', '1000037201', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('964', 'category', '1000037200', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('963', 'category', '1000037199', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('962', 'category', '1000037198', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('961', 'category', '1000037197', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('960', 'category', '1000037196', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('959', 'category', '1000037195', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('958', 'category', '1000037194', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('957', 'category', '1000037193', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('956', 'category', '1000037192', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('955', 'category', '1000037191', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('954', 'category', '1000037190', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('953', 'category', '1000037189', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('952', 'category', '1000037188', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('951', 'category', '1000037187', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('950', 'category', '1000037186', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('949', 'category', '1000037185', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('948', 'category', '1000037184', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('947', 'category', '1000037183', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('946', 'category', '1000037182', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('945', 'category', '1000037181', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('944', 'category', '1000037180', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('943', 'category', '1000037179', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('942', 'category', '1000037178', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('941', 'category', '1000037177', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('940', 'category', '1000037176', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('939', 'category', '1000037175', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('938', 'category', '1000037174', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('937', 'category', '1000037173', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('936', 'category', '1000037172', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('935', 'category', '1000037171', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('934', 'category', '1000037170', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('933', 'category', '1000037169', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('932', 'category', '1000037168', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('931', 'category', '1000037167', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('930', 'category', '1000037166', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('929', 'category', '1000037165', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('928', 'category', '1000037164', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('927', 'category', '1000037163', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('926', 'category', '1000037162', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('925', 'category', '1000037161', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('924', 'category', '1000037160', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('923', 'category', '1000037159', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('922', 'category', '1000037158', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('921', 'category', '1000037157', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('920', 'category', '1000037156', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('919', 'category', '1000037155', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('918', 'category', '1000037154', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('917', 'category', '1000037153', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('916', 'category', '1000037152', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('915', 'category', '1000037151', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('914', 'category', '1000037150', '1', '分类', '20:00:00-21:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('913', 'category', '1000037149', '1', '分类', '16:00:00-17:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('912', 'category', '1000037148', '1', '分类', '14:00:00-15:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('911', 'category', '1000037147', '1', '分类', '09:00:00-10:59:59', '2018-05-11 15:37:41', '2018-05-11 15:37:41');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('907', 'category', '1000037221', '1', '分类', '第二件5折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('906', 'category', '1000037220', '1', '分类', '第二件6折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('905', 'category', '1000037219', '1', '分类', '第二件7折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('904', 'category', '1000037218', '1', '分类', '第二件8折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('903', 'category', '1000037217', '1', '分类', '第二件9折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('902', 'category', '1000037216', '1', '分类', '特价', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('901', 'category', '1000037215', '1', '分类', '第二件5折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('900', 'category', '1000037214', '1', '分类', '第二件6折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('899', 'category', '1000037213', '1', '分类', '第二件7折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('898', 'category', '1000037212', '1', '分类', '第二件8折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('897', 'category', '1000037211', '1', '分类', '第二件9折', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('896', 'category', '1000037210', '1', '分类', '特价', '2018-05-11 11:38:26', '2018-05-11 11:38:26');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('895', 'category', '1000037142', '1', '分类', '第二件*折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('894', 'category', '1000037141', '1', '分类', '第二件*折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('893', 'category', '1000037130', '1', '分类', '特价', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('892', 'category', '1000037129', '1', '分类', '第二件9折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('891', 'category', '1000037128', '1', '分类', '第二件8折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('890', 'category', '1000037127', '1', '分类', '第二件7折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('889', 'category', '1000037126', '1', '分类', '第二件6折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('888', 'category', '1000037125', '1', '分类', '第二件5折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('887', 'category', '1000037123', '1', '分类', '多盒', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('886', 'category', '1000037122', '1', '分类', '特价', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('885', 'category', '1000037121', '1', '分类', '第二件9折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('884', 'category', '1000037120', '1', '分类', '第二件8折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('883', 'category', '1000037119', '1', '分类', '第二件7折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('882', 'category', '1000037118', '1', '分类', '第二件6折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');
INSERT INTO `medstore`.`am_act_range` (`range_id`, `range_type`, `range_value`, `range_status`, `range_name`, `range_remark`, `range_update_time`, `range_create_time`) VALUES ('881', 'category', '1000037117', '1', '分类', '第二件5折', '2018-05-10 16:21:09', '2018-05-10 16:21:09');

INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1247', '986', '865');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1246', '985', '864');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1245', '984', '863');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1244', '983', '862');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1243', '970', '925');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1242', '969', '924');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1241', '968', '923');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1240', '967', '922');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1239', '966', '921');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1238', '965', '920');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1237', '964', '919');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1236', '963', '918');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1235', '962', '917');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1234', '961', '916');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1233', '960', '915');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1232', '959', '914');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1231', '958', '913');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1230', '957', '912');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1229', '956', '911');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1228', '955', '910');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1227', '954', '909');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1226', '953', '908');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1225', '952', '907');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1224', '951', '906');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1223', '950', '905');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1222', '949', '904');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1221', '948', '903');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1220', '947', '902');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1219', '946', '901');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1218', '945', '900');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1217', '944', '899');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1216', '943', '898');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1215', '942', '897');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1214', '941', '896');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1213', '940', '895');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1212', '939', '894');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1211', '938', '893');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1210', '937', '892');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1209', '936', '891');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1208', '935', '890');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1207', '934', '889');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1206', '933', '888');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1205', '932', '887');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1204', '931', '886');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1203', '930', '885');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1202', '929', '884');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1201', '928', '883');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1200', '927', '882');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1199', '926', '881');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1198', '925', '880');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1197', '924', '879');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1196', '923', '878');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1195', '922', '877');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1194', '921', '876');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1193', '920', '875');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1192', '919', '874');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1191', '918', '873');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1190', '917', '872');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1189', '916', '871');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1188', '915', '870');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1187', '914', '869');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1186', '913', '868');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1185', '912', '867');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1184', '911', '866');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1182', '907', '857');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1181', '906', '858');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1180', '905', '859');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1179', '904', '860');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1178', '903', '861');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1177', '902', '856');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1175', '901', '851');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1174', '900', '852');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1173', '899', '853');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1172', '898', '854');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1171', '897', '855');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1170', '896', '850');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1169', '895', '849');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1168', '894', '848');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1167', '893', '847');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1166', '892', '846');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1165', '891', '845');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1164', '890', '844');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1163', '889', '843');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1162', '888', '842');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1161', '887', '841');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1160', '886', '840');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1159', '885', '839');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1158', '884', '838');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1157', '883', '837');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1156', '882', '836');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1155', '881', '835');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1154', '880', '833');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1153', '880', '832');
INSERT INTO `medstore`.`am_item_range` (`link_id`, `range_id`, `item_id`) VALUES ('1152', '879', '831');



INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('287', '1', '861', 'discount', 'rate', '95', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件9折', '第二件9折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('286', '1', '860', 'discount', 'rate', '90', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件8折', '第二件8折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('285', '1', '859', 'discount', 'rate', '85', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件7折', '第二件7折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('284', '1', '858', 'discount', 'rate', '80', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件6折', '第二件6折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('283', '1', '857', 'discount', 'rate', '75', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件5折', '第二件5折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('282', '1', '855', 'discount', 'rate', '95', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件9折', '第二件9折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('281', '1', '854', 'discount', 'rate', '90', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件8折', '第二件8折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('280', '1', '853', 'discount', 'rate', '85', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件7折', '第二件7折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('279', '1', '852', 'discount', 'rate', '80', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件6折', '第二件6折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('278', '1', '851', 'discount', 'rate', '75', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件5折', '第二件5折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('272', '1', '846', 'discount', 'rate', '95', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件9折', '第二件9折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('271', '1', '845', 'discount', 'rate', '90', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件8折', '第二件8折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('270', '1', '844', 'discount', 'rate', '85', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件7折', '第二件7折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('269', '1', '843', 'discount', 'rate', '80', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件6折', '第二件6折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('268', '1', '842', 'discount', 'rate', '75', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件5折', '第二件5折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('267', '1', '839', 'discount', 'rate', '95', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件9折', '第二件9折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('266', '1', '838', 'discount', 'rate', '90', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件8折', '第二件8折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('265', '1', '837', 'discount', 'rate', '85', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件7折', '第二件7折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('264', '1', '836', 'discount', 'rate', '80', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件6折', '第二件6折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('263', '1', '835', 'discount', 'rate', '75', '2018-04-04 17:40:41', '2018-04-04 16:26:18', '第二件5折', '第二件5折');
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('262', '1', '833', 'gift', 'sku_id', '5:20129488', '2018-05-04 10:57:09', '2018-05-03 16:04:48', '已满5件，赠体温计一支', NULL);
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('261', '1', '832', 'gift', 'self', '3:1', '2018-05-04 10:56:55', '2018-05-03 16:03:46', '已满3件赠1', NULL);
INSERT INTO `medstore`.`am_item_details` (`details_id`, `details_level`, `item_id`, `details_type`, `details_val_type`, `details_value`, `details_update_time`, `details_create_time`, `details_remark`, `details_content`) VALUES ('260', '1', '831', 'gift', 'sku_id', '20129488', '2018-05-04 09:55:16', '2018-05-03 16:02:46', '已满100元送体温计一支', NULL);


INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('242', '272', '22');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('243', '271', '22');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('244', '270', '22');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('245', '269', '22');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('246', '268', '22');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('247', '267', '22');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('248', '266', '22');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('249', '265', '22');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('250', '264', '22');
INSERT INTO `medstore`.`am_details_rule` (`link_id`, `details_id`, `rule_id`) VALUES ('251', '263', '22');



INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('454', '925', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('453', '924', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('452', '923', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('451', '921', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('450', '920', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('449', '919', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('448', '917', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('447', '916', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('446', '915', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('445', '913', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('444', '912', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('443', '911', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('442', '909', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('441', '908', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('440', '907', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('439', '905', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('438', '904', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('437', '903', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('436', '901', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('435', '900', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('434', '899', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('433', '897', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('432', '896', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('431', '895', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('430', '893', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('429', '892', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('428', '891', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('427', '889', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('426', '888', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('425', '887', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('424', '885', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('423', '884', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('422', '883', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('421', '881', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('420', '880', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('419', '879', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('418', '877', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('417', '876', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('416', '875', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('415', '873', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('414', '872', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('413', '871', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('412', '869', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('411', '868', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('410', '867', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('409', '865', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('408', '864', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('407', '863', '10:10:10', 'main', 'force', '1', '0', '2018-05-11 15:51:07', '2018-05-11 15:51:07', '每人限购10个', '活动商品每人限购10个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('391', '922', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('390', '918', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('389', '914', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('388', '910', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('387', '906', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('386', '902', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('385', '898', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('384', '894', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('383', '890', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('382', '886', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('381', '882', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('380', '878', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('379', '874', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('378', '870', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('377', '866', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');
INSERT INTO `medstore`.`am_quota_info` (`quota_id`, `item_id`, `quota_rule`, `quota_kind`, `quota_type`, `quota_status`, `quota_total`, `quota_update_time`, `quota_create_time`, `quota_remark`, `quota_content`, `qutoa_group`) VALUES ('376', '862', '1:1:1', 'main', 'force', '1', '0', '2018-05-11 15:51:05', '2018-05-11 15:51:05', '每人限购1个', '活动商品每人限购1个', '1');



-- 5.11 限购的商品配置库存
 -- INSERT INTO `medstore`.`am_stock_limit` (`item_id`, `quota_id`, `sku_id`, `sku_total`, `as_remark`) 
-- VALUES ('4477', '680', '232', '20132684', '100', '1610');

SELECT  -- * ,
 d.item_id,d.quota_id,a.sku_id,100,'168'
 -- ,c.*,d.* INSERT INTO `medstore`.`am_stock_limit` (`item_id`, `quota_id`, `sku_id`, `sku_total`, `as_remark`) 
-- VALUES ('4477', '680', '232', '20132684', '100', '1610');

SELECT  -- * ,
 d.item_id,d.quota_id,a.sku_id,100,'168'
 -- ,c.*,d.*
from pm_sku_dir a LEFT JOIN am_act_range b on a.dir_id = b.range_value
 LEFT JOIN am_item_range c on b.range_id  = c.range_id 
 LEFT JOIN am_quota_info d on c.item_id = d.item_id
WHERE  dir_id BETWEEN  1000037147 and 1000037225 
from pm_sku_dir a LEFT JOIN am_act_range b on a.dir_id = b.range_value
 LEFT JOIN am_item_range c on b.range_id  = c.range_id 
 LEFT JOIN am_quota_info d on c.item_id = d.item_id
WHERE  dir_id BETWEEN  1000037147 and 1000037225 -- bj 和ty 所有秒杀目录



-- 6.1     每天的 秒杀目录 515 63
-- INSERT INTO `medstore`.`am_stages_sale` ( `pharmacy_id`, `sg_title`, `sg_status`, `sg_start_time`, `sg_end_time`, `quota_id`, `act_id`, `item_id`, `sg_create_time`, `sg_update_time`, `sg_json`, `sg_remark`) 
-- VALUES ('1', '1620', '09:00', '1', '2017-04-25 09:00:00', '2018-04-25 10:59:59', '236', '163', '691', '2018-04-13 17:15:26', '2018-04-13 17:15:28', NULL, NULL);
SELECT -- b.item_name, 
-- t.stime,
b.`pharmacy_id`
-- CASE WHEN sg_title = '9:00:' then  '09:00' else sg_title end as `sg_title`,
,sg_title  as `sg_title`
,1 as `sg_status`
-- , `sg_start_time`
-- ,DATE_add(sg_start_time, INTERVAL 3 DAY) as sg_start_time
-- ,DATE_add(sg_end_time, INTERVAL 3 DAY) as `sg_end_time`
,REPLACE(sg_start_time,'04-25',t.stime) as sg_start_time
,REPLACE(sg_end_time,'04-25',t.stime) as `sg_end_time`
, c.`quota_id`
,CASE WHEN b.`pharmacy_id`=200 then 169 else 168 end as `act_id`, b.`item_id`
,NOW() as  `sg_create_time`
,NOW() as  `sg_update_time`, `sg_json`, `sg_remark` 
-- ,a.sg_title ,  SUBSTR(b.item_name,1,5)
--  ,b.*,a.*
FROM am_stages_sale as a ,am_act_item b ,am_quota_info c,tmp_mstime t
WHERE 
b.item_id = c.item_id  -- c.item_id >= 279 and 
 and a.sg_title =  SUBSTR(b.item_name,1,5)
-- SUBSTR(a.sg_title, 2 ,6) LIKE '%'||b.item_name||'%'
-- a.sg_title like b.item_name
and sg_id BETWEEN 1 and 4
and b.item_id BETWEEN 862 and  925 ;-- 天一 和北京 的item





UPDATE sm_image_link set link_end_time = '2018-06-03 23:59:59',link_update_time = NOW(),link_status =1
WHERE link_remark = '5月活动  女性健康' -- link_view = 'essence' and link_name in ('限时抢购','女性大牌','多买少算') and link_version =0 -- and link_end_time = '2018-05-14 23:59:59'
