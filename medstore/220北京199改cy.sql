
-- 这是昨天说的北京一元9.9商品修改，康佰馨和正济堂的我给你分开了，上完告诉我，，排序等你上完重新排《北京一元、9.9商品新增与修改.xls》
-- *********************************************
-- 新增1元商品
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sort`, `sku_rank`, `sku_type`)
--  VALUES ('25463893', '102757', '0', '990', '990', '1620', '1', '2018-08-23 11:12:57', '2017-12-11 12:24:57', '北京门店，康佰馨10家，正济堂3家门店上线商品明细', NULL, '康顾多 碘伏棉棒', '600111', '1', '{\"prod_spec\":\"8cm*24支\"}', '11100', NULL, '0', '0', NULL, '1', '0', '0', '0', 'normal');
SELECT `prod_id`,1 `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`
,NOW() `sku_update_time`
,NOW() `sku_create_time`
,'bj-0220-新增1元' `sku_remark`, `sku_logistics`, `prod_name`
,CONCAT('1901YYY',pharmacy_huohao) `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sort`, `sku_rank`, `sku_type` from pm_prod_sku WHERE pharmacy_huohao in (400051
,600111
,400031
,400041
)
and drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
;

-- 新增99商品
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sort`, `sku_rank`, `sku_type`)
--  VALUES ('25463893', '102757', '0', '990', '990', '1620', '1', '2018-08-23 11:12:57', '2017-12-11 12:24:57', '北京门店，康佰馨10家，正济堂3家门店上线商品明细', NULL, '康顾多 碘伏棉棒', '600111', '1', '{\"prod_spec\":\"8cm*24支\"}', '11100', NULL, '0', '0', NULL, '1', '0', '0', '0', 'normal');
SELECT `prod_id`,1 `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`
,NOW() `sku_update_time`
,NOW() `sku_create_time`
,'bj-0220-新增9.9元' `sku_remark`, `sku_logistics`, `prod_name`
,CONCAT('1901YYY',pharmacy_huohao) `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`, `sku_activate`, `sales_info`, `sku_sum_flag`, `sku_hot_order`, `sku_sort`, `sku_rank`, `sku_type` 
from pm_prod_sku WHERE pharmacy_huohao in (
400060
,400039

)
and drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
;

SELECT * from pm_prod_sku ORDER BY sku_id DESC;


--  pm_sku_sale build
-- 1元和99元商品的价格配置
-- INSERT INTO `medstore`.`pm_sku_sale` (  `sku_id`, `sale_fee`, `sale_price`, `sale_start_time`, `sale_end_time`, `sale_status`, `sale_create_time`, `sale_update_time`, `sale_remark`)
--  VALUES ('364577', '25506122', '950', '100', '2018-11-06 20:00:00', '2018-11-06 21:59:59', '0', '2018-09-15 23:40:26', '2018-09-15 23:40:26', '9月 秒杀 ty');
SELECT `sku_id`
,sku_fee `sale_fee`
,100 `sale_price`

,'2018-02-20 00:00:00' `sale_start_time`
,'2019-02-28 23:59:59'  `sale_end_time`
,1 `sale_status`
,NOW() `sale_create_time`
,NOW() `sale_update_time`
,sku_remark `sale_remark` 
from pm_prod_sku
WHERE sku_remark like 'bj-0220-新增1元'
UNION all
SELECT `sku_id`
,sku_fee `sale_fee`
,990 `sale_price`

,'2018-02-20 00:00:00' `sale_start_time`
,'2019-02-28 23:59:59'  `sale_end_time`
,1 `sale_status`
,NOW() `sale_create_time`
,NOW() `sale_update_time`
,sku_remark `sale_remark` 
from pm_prod_sku
WHERE sku_remark like 'bj-0220-新增9.9元';

-- 9.9改为1元 kbx
-- update pm_prod_sku s ,pm_sku_sale b  set sale_price = '100'
-- SELECT *  from pm_prod_sku s ,pm_sku_sale b
 WHERE
s.sku_id = b.sku_id
AND pharmacy_huohao in (
'1901YJY400052'
,'1901YJY400044'
,'1901YJY400043'
)
and drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
;

-- 9.9改为1元 zjt
-- update pm_prod_sku s ,pm_sku_sale b  set sale_price = '100'
-- SELECT *  from pm_prod_sku s ,pm_sku_sale b
 WHERE
s.sku_id = b.sku_id
AND pharmacy_huohao in (
'1901YJY41705107'
)
and drugstore_id in (1610,1611,1612) 
;


-- ----------------------
SELECT * from pm_prod_sku s  
WHERE sku_remark like 'bj-0220-新增1元'

-- 增加新1元的目录
-- INSERT INTO `medstore`.`pm_sku_dir` (  `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('8872286', '1001001766', '25515243', 'ykd21061001act460202', '1', '2019-01-07 14:59:25');
  SELECT `dir_id`, `sku_id`, `dir_code`,100 `sku_order`,NOW() `update_time`
from pm_prod_sku s ,pm_dir_info d
WHERE s.drugstore_id = d.pharmacy_id
and sku_remark like 'bj-0220-新增1元'
and d.dir_id BETWEEN 1001001762 and 1001001775 
;

-- 增加新99元的目录
-- INSERT INTO `medstore`.`pm_sku_dir` (  `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) 
-- VALUES ('8872286', '1001001766', '25515243', 'ykd21061001act460202', '1', '2019-01-07 14:59:25');
  SELECT `dir_id`, `sku_id`, `dir_code`,100 `sku_order`,NOW() `update_time`
from pm_prod_sku s ,pm_dir_info d
WHERE s.drugstore_id = d.pharmacy_id
and sku_remark like 'bj-0220-新增9.9元'
and d.dir_id BETWEEN 1001001792 and 1001001805
;



-- 9.9改为1元 kbx mulu
-- update  pm_prod_sku s ,pm_sku_dir b ,pm_dir_info d  set b.dir_id = d.dir_id ,b.dir_code = d.dir_code,b.update_time = NOW()
-- SELECT *  from pm_prod_sku s ,pm_sku_dir b ,pm_dir_info d
 WHERE
s.sku_id = b.sku_id
AND pharmacy_huohao in (
'1901YJY400052'
,'1901YJY400044'
,'1901YJY400043'
)
and drugstore_id in (1620,1621,1622,1623,1627,1629,1630,1631,1632,1633)
and b.dir_id BETWEEN 1001001792 and 1001001805
and s.drugstore_id = d.pharmacy_id
and d.dir_id BETWEEN 1001001762 and 1001001775 
;

-- 9.9改为1元 zjt
-- update  pm_prod_sku s ,pm_sku_dir b ,pm_dir_info d  set b.dir_id = d.dir_id ,b.dir_code = d.dir_code,b.update_time = NOW()
-- SELECT *  from pm_prod_sku s ,pm_sku_dir b ,pm_dir_info d
 WHERE
s.sku_id = b.sku_id
AND pharmacy_huohao in (
'1901YJY41705107'
)
and drugstore_id in (1610,1611,1612) 
and b.dir_id BETWEEN 1001001792 and 1001001805
and s.drugstore_id = d.pharmacy_id
and d.dir_id BETWEEN 1001001762 and 1001001775 
;

SELECT * from am_stat_info GROUP BY item_id;

SELECT * from am_quota_info ORDER BY quota_id DESC;

SELECT * from pm_sku_dir ORDER BY link_id DESC ;

SELECT * from sm_image_link WHERE drugstore_id = 1620 ORDER BY link_id DESC;
SELECT * FROM pm_dir_info WHERE dir_id = 1001001736;
SELECT * FROM pm_dir_info WHERE dir_code like '%act46%';

SELECT * from pm_sku_dir WHERE dir_id = 1001001766 dir_code like '%act4602%'
1001001762 and 1001001775 -- 1yuan mmulu
1001001792 and 1001001805 -- 99mulu
1001001775

SELECT * from pm_sku_dir sd,pm_prod_sku s WHERE sd.dir_id = 1001001766 and sd.sku_id = s.sku_id
-- **************************************8 -------------
