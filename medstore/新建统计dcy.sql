-- 新建统计

SELECT * from tj_flhz; -- 分类汇总

SELECT * from tj_bzspx; -- 标准商品线

SELECT * from om_prod_ins i  ,om_order_info o  WHERE ins_create_time BETWEEN '2018-06-01 00:00:00' and '2018-06-30 23:59:59'
and prod_id = 100100
and i.order_id = o.order_id 
and o.pharmacy_id = 1620
 limit 1000;


-- 六、统计报表名称：商品SKU销售组合统计

-- SELECT -- COUNT(class2),
-- a.*,i.*,o.* from tj_flhz a , om_prod_ins i ,om_order_info o 
-- WHERE i.order_id = o.order_id 
-- and a.prod_id = i.prod_id 
-- and i.ins_create_time BETWEEN '2018-06-01 00:00:00' and '2018-06-30 23:59:59'
-- and o.order_create_time BETWEEN '2018-06-01 00:00:00' and '2018-06-30 23:59:59'
-- and o.pharmacy_id = 1620
-- GROUP BY class2
-- ;
SELECT  门店名称
, 分类
, 销售sku
,销售sku/i2.scp SKU占比
, 零售金额
 ,零售金额/i2.sstp 金额占比
-- ,i2.sstp
-- ,i2.scp
 from (
SELECT dr.drugstore_name 门店名称
,i.class2 分类
,i.scp 销售sku
 -- ,i.scp/SUM(i.scp) SKU占比
,i.sstp 零售金额
-- ,i.sstp/SUM(i.sstp) 金额占比
FROM
	(
		SELECT
			SUM(cp) scp,
			COUNT(class2) cc,
			SUM(stp) sstp,
			a.class2,
			i.* -- , a.*
		FROM
			(
				SELECT 
					count(i.prod_id) cp,
					sum(i.total_price) stp,
					o.pharmacy_id,
					i.prod_id
					-- i.*
				FROM 
					om_prod_ins i,
					om_order_info o
				WHERE 
					i.order_id = o.order_id  
				AND o.order_status = 44
				AND o.order_create_time BETWEEN '2018-06-01 00:00:00'
				AND '2018-06-30 23:59:59'
				AND o.pharmacy_id = 1620
				GROUP BY i.prod_id
			) i,
			tj_flhz a
		WHERE
			a.prod_id = i.prod_id
		GROUP BY
			class2
	) i
LEFT JOIN sm_drugstore_info dr ON i.pharmacy_id = dr.drugstore_id
) i ,(
		SELECT
			SUM(cp) scp,
			COUNT(class2) cc,
			SUM(stp) sstp,
			a.class2,
			i.* -- , a.*
		FROM
			(
				SELECT 
					count(i.prod_id) cp,
					sum(i.total_price) stp,
					o.pharmacy_id,
					i.prod_id
					-- i.*
				FROM 
					om_prod_ins i,
					om_order_info o
				WHERE 
					i.order_id = o.order_id  
				AND	o.order_status = 44
				AND o.order_create_time BETWEEN '2018-06-01 00:00:00'
				AND '2018-06-30 23:59:59'
				AND o.pharmacy_id = 1620
				GROUP BY i.prod_id
			) i,
			tj_flhz a
		WHERE
			a.prod_id = i.prod_id
	
	) i2 
;



 

-- 2、导出明细详见附表：商品销售明细
SELECT o.`order_id` 订单号,
       s.`drugstore_name` 门店名称,
       `order_create_time` 下单时间,
       osf.`flow_create_time` 签收时间,
       if(o.pay_type= 'online' && o.`order_type`!= 'service', '在线支付', '货到付款') as 支付方式,
       u.`phone_num` 手机号,
       u.user_device 设备号,
       sku.`pharmacy_huohao` 门店货号,
       p.`prod_name` 商品名称,
       p.`prod_gen_name` 通用名,
       p.`prod_spec` 规格,
       p.`prod_firm` 生产厂家,
       `ins_amount` 数量,
       `ins_price` / 100 零售价,
       `total_price` / 100 零售金额,
       `order_pay_fee` / 100 实际交易金额,
       c.`coupon_value` / 100 优惠卷金额,
       ci.`coupon_desc` 优惠卷信息,
       addr.addr_info 收货地址,
       addr.accept_name 收货人,
       addr.logist_fee 快递费,
CASE WHEN  o.order_type = 'normal' THEN 'APP'
WHEN o.order_type = 'service' THEN '客服'
WHEN o.order_type = 'wechat' THEN '微信'
ELSE '' END 订单类型,
       f_user_is_new(u.`user_id`, `order_create_time`) 新老用户
  FROM `om_order_info` o
  LEFT JOIN `sm_drugstore_info` s ON s.`drugstore_id`= o.`pharmacy_id`
  LEFT JOIN `um_user_info` u ON u.`user_id`= o.`user_id`
  LEFT JOIN `om_prod_ins` i ON(o.`order_id`= i.`order_id`)
  LEFT JOIN `pm_prod_sku` sku ON sku.`sku_id`= i.`sku_id`
  LEFT JOIN `pm_prod_info` p ON p.`prod_id`= i.`prod_id`
  LEFT JOIN `om_order_coupon` c ON c.`order_id`= o.`order_id`
  LEFT JOIN `am_coupon_info` ci ON ci.`coupon_id`= c.`coupon_id`
  LEFT JOIN `om_order_addr` addr ON o.`order_id`= addr.order_id
  LEFT JOIN(
SELECT *
  from om_status_flow osf
WHERE osf.`prev_status`= 40
   AND osf.`current_status`= 44
   AND osf.`flow_create_time`BETWEEN  '2018-11-13 00:00:00' and '2018-11-13 23:59:59'
  ) osf ON o.`order_id`= osf.`order_id`
WHERE osf.`flow_id` IS NOT NULL
   AND o.`pharmacy_id` =200 --  IN(1610, 1611, 1612, 1620, 1621, 1622, 1623, 1627, 1629, 1630, 1631, 1632, 1633, 1634, 1635)
and o.`order_status` = 44
-- and o.`order_id` = 10689842

;

-- 五、统计报表名称：商品缺货统计
-- 1、页面模板
-- 2、导出明细详见附表：商品缺货明细（最多限制导出31天数据）
-- 3、商品标注中类明细，标准商品线明细见附表

SELECT * from pm_prod_sku s 
WHERE s.drugstore_id = 1627;



SELECT
			SUM(cp) scp,
			COUNT(class2) cc,
			SUM(stp) sstp,
			a.class2,
			i.* -- , a.*
SELECT * 
		FROM
			(
				SELECT * from pm_prod_sku s 
				WHERE s.drugstore_id = 1627
			) i,
			tj_flhz a
		WHERE
			a.prod_id = i.prod_id

