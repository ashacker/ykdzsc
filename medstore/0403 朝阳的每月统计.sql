/*
1、北京销售明细（4.2）  
2、北京销售明细（异常订单） 药店拒单、用户拒签、用户取消（原因）3.1-4.2
3、北京配送明细（3.1-3.31）
4、北京门店经营数据统计3月份（见附表） --
5、北京90天未交易用户数据（订单号、门店名称、下单时间、手机号、交易金额）
6、天一APP订单与代客订单统计表（见附表）
天一3月数据1、自提商品销售品种汇总：天一货号   品名   产地    销售数量     销售金额 -- 无
2、代客下单销售品种汇总：天一货号   品名   产地    销售数量     销售金额 
3、订单数据：手机号   订单数量    订单金额    优惠后订单金额     优惠券金额

4、现金券明细带销售金额。
*/

SELECT o.`order_id` 订单号,
       s.`drugstore_name` 门店名称,
       `order_create_time` 下单时间,
       osf.`flow_create_time` 签收时间,
       if(o.pay_type= 'online' && o.`order_type`!= 'service', '在线支付', '货到付款') as 支付方式,
       u.`phone_num` 手机号,
       u.user_device 设备号,
       sku.`pharmacy_huohao` 门店货号,
       p.`prod_name` 商品名称,
       p.`prod_gen_name` 通用名,
       p.`prod_spec` 规格,
       p.`prod_firm` 生产厂家,
       `ins_amount` 数量,
       `ins_price` / 100 零售价,
       `total_price` / 100 零售金额,
       `order_pay_fee` / 100 实际交易金额,
       c.`coupon_value` / 100 优惠卷金额,
       ci.`coupon_desc` 优惠卷信息,
       addr.addr_info 收货地址,
       addr.accept_name 收货人,
       addr.logist_fee 快递费,
       f_user_is_new(u.`user_id`, `order_create_time`) 新老用户
  FROM `om_order_info` o
  LEFT JOIN `sm_drugstore_info` s ON s.`drugstore_id`= o.`pharmacy_id`
  LEFT JOIN `um_user_info` u ON u.`user_id`= o.`user_id`
  LEFT JOIN `om_prod_ins` i ON(o.`order_id`= i.`order_id`)
  LEFT JOIN `pm_prod_sku` sku ON sku.`sku_id`= i.`sku_id`
  LEFT JOIN `pm_prod_info` p ON p.`prod_id`= i.`prod_id`
  LEFT JOIN `om_order_coupon` c ON c.`order_id`= o.`order_id`
  LEFT JOIN `am_coupon_info` ci ON ci.`coupon_id`= c.`coupon_id`
  LEFT JOIN `om_order_addr` addr ON o.`order_id`= addr.order_id
  LEFT JOIN(
SELECT *
  from om_status_flow osf
WHERE osf.`prev_status`= 40
   AND osf.`current_status`= 44
   AND osf.`flow_create_time`> '2018-04-02'
   AND osf.`flow_create_time`< '2018-04-03') osf ON o.`order_id`= osf.`order_id`
WHERE osf.`flow_id` IS NOT NULL
   AND o.`pharmacy_id` IN(1610, 1611, 1612, 1620, 1621, 1622, 1623, 1627, 1629, 1630, 1631, 1632, 1633, 1634, 1635)
and o.`order_status` = 44
;




SELECT o.`order_id` 订单号,
       s.`drugstore_name` 门店名称,
       `order_create_time` 下单时间,
       osf.`flow_create_time` 签收时间,
       CASE o.order_status WHEN 44 THEN '签收' WHEN 64 THEN '用户取消' WHEN 65 THEN '用户拒签' WHEN 66 THEN '药店拒单'  WHEN 68 THEN '退货' ELSE '其他' END AS 订单状态,
       IF(o.pay_type= 'online' && o.`order_type`!= 'service', '在线支付', '货到付款') AS 支付方式,
       osf.flow_message 备注信息,
       u.`phone_num` 手机号,
       u.user_device 设备号,
       sku.`pharmacy_huohao` 门店货号,
       p.`prod_name` 商品名称,
       p.`prod_gen_name` 通用名,
       p.`prod_spec` 规格,
       p.`prod_firm` 生产厂家,
       `ins_amount` 数量,
       `ins_price` / 100 零售价,
       `total_price` / 100 零售金额,
       `order_pay_fee` / 100 实际交易金额,
       addr.addr_info 收货地址,
       addr.accept_name 收货人,
       addr.logist_fee 快递费
  FROM `om_order_info` o
  LEFT JOIN `sm_drugstore_info` s ON s.`drugstore_id`= o.`pharmacy_id`
  LEFT JOIN `um_user_info` u ON u.`user_id`= o.`user_id`
  LEFT JOIN `om_prod_ins` i ON(o.`order_id`= i.`order_id`)
  LEFT JOIN `pm_prod_sku` sku ON sku.`sku_id`= i.`sku_id`
  LEFT JOIN `pm_prod_info` p ON p.`prod_id`= i.`prod_id`
  LEFT JOIN `om_order_addr` addr ON o.`order_id`= addr.order_id
  LEFT JOIN(
SELECT *
  FROM om_status_flow osf
 WHERE osf.`current_status`in (44,64,65,66,68)
   AND osf.`flow_create_time`> '2018-03-01'
   AND osf.`flow_create_time`< '2018-04-04') osf ON o.`order_id`= osf.`order_id`
 WHERE osf.`flow_id` IS NOT NULL
   AND o.`pharmacy_id` IN(1610, 1611, 1612, 1620, 1621, 1622, 1623, 1627, 1629, 1630, 1631, 1632, 1633, 1634, 1635)
;




select
    t6.drugstore_name as 药店名,
    t2.logist_comp AS logist_comp,
    t1.employee_phone as 快递员手机,
    t1.employee_name as 快递员,
    t2.order_id as 订单编号,
    t3.order_type as 订单类型,
    t3.order_fee AS 订单金额,
    t3.order_pay_fee as 实际支付金额,
    t3.order_create_time as 订单创建时间,
    t2.logist_start_time as 开始配送时间,
    t2.logist_end_time as 签收或拒收,
    TIMESTAMPDIFF(SECOND, t3.order_create_time, t2.logist_end_time) as 耗时,
--   t2.logist_arrival_time as 预约配送时间,
-- 要求送达时间在 2017-12-13 8:00:00-09:00之间
substring(t3.`order_remark`,locate('要求送达时间在',t3.`order_remark`)+7,locate('之间',t3.`order_remark`)-8) as 预约配送时间,
t3.`order_remark`  as 预约配送时间2,
    CASE WHEN t3.order_status= 16 then '新订单' WHEN t3.order_status= 32 then '待处理' WHEN t3.order_status= 40 then '配送中' WHEN t3.order_status= 44 then '签收' WHEN t3.order_status= 64 then '用户取消' WHEN t3.order_status= 65 then '用户拒签' WHEN t3.order_status= 66 then '药店拒单' WHEN t3.order_status= 68 then '退货' ELSE t3.order_status END as 订单状态,
    t2.addr_city as 收货城市,
    t2.addr_dist as 收货区域,
    t2.addr_info as 详细地址,
    t2.accept_name as 收货人,
    t7.`reply_synthesize` as 评价星级,
CASE WHEN t7.`reply_manner`= 0 then '快递态度不好' WHEN t7.`reply_manner`= 1 then '商品价格偏高' WHEN t7.`reply_manner`= 2 then '未能及时到货' WHEN t7.`reply_manner`= 3 then '对商品质量有疑问' WHEN t7.`reply_manner`= 4 then '商品外包装有破损' WHEN t7.`reply_manner`= 5 then '退换商品遇到阻碍' ELSE t7.`reply_manner` END as 主要问题,
    t7.`reply_content` as 评价详情
from om_order_info t3
    left JOIN om_order_addr t2 on t3.order_id= t2.order_id
    left JOIN sm_drugstore_info t6 on t3.pharmacy_id= t6.drugstore_id
    left JOIN om_order_reply t7 on t3.order_id= t7.order_id
    LEFT JOIN sm_admin_info t4 on t4.admin_id= t2.logist_comp
    LEFT JOIN sm_employee_info t1 on t1.employee_id= t4.employee_id
where t3.order_create_time BETWEEN '2018-03-01 00:00:00' and '2018-03-31 23:59:59'
    and t3.`pharmacy_id` in (1610,
            1611,
            1612,
            1634,
            1620,
            1621,
            1622,
            1623,
            1627,
            1629,
            1630,
            1631,
            1632,
            1633,
            1635)
    and t2.logist_comp is not null
ORDER BY t3.order_create_time












