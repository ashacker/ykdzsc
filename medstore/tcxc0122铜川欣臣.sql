SELECT * FROM as_test.`tcxc0122_科室找药`;
SELECT * FROM as_test.`tcxc0122_分类搜药`;
SELECT * from as_test.`tcxc0122_症状找药`;
SELECT * from as_test.`tcxc0122_医疗器械分类`;
SELECT * from as_test.`tcxc0122_汇总数据`;
SELECT * from as_test.`tcxc0122_汇总2`;

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`分类编码` as category_id from as_test.`tcxc0122_科室找药`
UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码` as category_id from as_test.`tcxc0122_分类搜药`
UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_症状找药`
UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码` from as_test.`tcxc0122_医疗器械分类`;

/*
INSERT INTO ``.`` (`中级`, `三级`, `四级`, `分类编码`, `自己药店货号`, `商品类别`, `商品名/品牌名称`, `通用名`, `规格`, `厂商`) VALUES ('内科', '呼吸道疾病', '感冒发烧', 'bz010101', '0004063', 'OTC', '广西禅方', '板蓝根颗粒 ', '5g*10袋', '广西禅方药业股份有限公司');
INSERT INTO ``.`` (`大类`, `中类`, `三级`, `四级`, `目录编码`, `自己药店货号`, `商品类别`, `商品名/品牌名称`, `通用名`, `规格`, `厂商`) VALUES ('中西药品', '镇痛用药', '', '骨质疏松', 'zxyp060105', '0004094', 'RX', '', '附桂骨痛片 ', '0.33g*72', '安国市天下康制药有限公司');
INSERT INTO ``.`` (`大类`, `小类`, `目录编码`, `自己药店货号`, `商品类别`, `商品名/品牌名称`, `通用名`, `规格`, `厂商`) VALUES ('咳嗽感冒发烧', '咳嗽感冒发烧', 'zz0101', '0004063', 'OTC', '广西禅方', '板蓝根颗粒 ', '5g*10袋', '广西禅方药业股份有限公司');
INSERT INTO ``.`` (`药快到商品id`, `大类`, `中类`, `三级`, `四级`, `目录编码`, `自己药店货号`, `商品名/品牌名称`, `通用名`, `规格`, `厂商`) VALUES ('', '医疗器械', '创伤护理产品', '', '伤口消毒', 'ylqx1102', '0003521', '', '癣毒净净肤液', '30g', '安徽省德济堂药业有限公司');
INSERT INTO ``.`` (`自己药店货号`, `商品类别`, `商品名/品牌名称`, `通用名`, `规格`, `厂商`, `单位`, `商品条码`, `零售价`, `批准文号`, `进口药品注册证号`, `图片1`, `图片2`, `图片3`, `图片4`, `图片5`, `商品名1`, `通用名称`, `英文名`, `化学名称`, `分子式`, `分子量`, `成份`, `性状`, `注射剂辅料`, `功能主治/适应症`, `作用类别`, `用法用量`, `不良反应`, `注意事项`, `禁忌`, `包装`, `药物过量`, `药代动力学`, `老年用药`, `儿童用药`, `孕妇及哺乳期妇女用药`, `药理毒理`, `药物相互作用`, `批准文号3`, `企业名称`, `临床试验`, `储存条件`, `备注`, `说明书修订日期`) VALUES ('0004094', 'RX', '', '附桂骨痛片 ', '0.33g*72', '安国市天下康制药有限公司', '盒', '6920611400669', '33.3', '国药准字Z13021827', '', 'AAA-001', 'AAA-002', 'AAA-004', 'AAA-006', '', '附桂骨痛片', '附桂骨痛片', '', '', '', '', '附子(制)、川乌(制)、肉桂、党参、 当归、 白芍(炒)、淫羊藿、乳香(制)。', ' 本品为糖衣片，除去糖衣后显棕褐色至褐色；气微香，味微苦。', '', '温阳散寒，益气活血，消肿止痛。用于阳虚寒湿型颈椎及膝关节增生性关节炎。症见：局部骨节疼痛，屈伸不利、麻木或肿胀，遇热则减，畏寒肢冷等。', '', '口服，一次6片，一日3次，饭后服。疗程3个月。如需继续治疗，必须停药一个月后遵医嘱服用。', '尚不明确。', ' 1.服药后少数可见胃脘不舒，停药后可自行消除。 2.服药期间注意血压变化。 3.高血压、严重消化道疾病慎用。 4.孕妇及有出血倾向者、阴虚内热者禁用。', '孕妇及有出血倾向者、阴虚内热者禁用。', '', '', '', '', '', '', '', '如与其他药物同时使用可能会发生药物相互作用，详情请咨询医师或药师。', '', '安国市天下康制药有限公司', '', '密封。', '', '42632');
INSERT INTO ``.`` (`自己药店货号`, `商品名/品牌名称`, `通用名`, `规格`, `厂商`, `单位`, `零售价`, `级别`, `图片1`, `图片2`, `图片3`, `图片4`, `图片5`, `图片6`, `图片7`, `图片8`) VALUES ('0003521', '', '癣毒净净肤液', '30g', '安徽省德济堂药业有限公司', '盒', '20', '', 'CCC-001', 'CCC-002', 'CCC-003', '', '', '', '', '');
*/
/*
附件为铜川欣臣药房增加的独有商品，药店ID：1600、1601，先建商品ID，再建两家店的SKUID，，上架状态
尽量在25号前完成！
*/
323264
25476205
SELECT * from pm_prod_info ORDER BY prod_id desc LIMIT 300; -- 323485

SELECT * from pm_prod_sku ORDER BY sku_id desc limit 300; -- 25475559

SELECT * from yjj_drug_detail ORDER BY drug_id desc limit 300; -- 262677  263065  263150
说明书啊 yjj_id 是啥啊
-- INSERT INTO `yjj_drug_detail` ( `yjj_id`, `common_name`, `product_name`, `product_name_en`, `product_name_py`, `product_name_chem`, `structural_formula`, `molecular_weight`
, `component`, `trait`, `assist_matreial`, `indication`, `role_category`, `dosage`, `adverse_reactions`
, `precautions`, `contraindications`, `format`, `package`, `over_dosage`, `pharmacokinetics`, `use_in_elderly`
, `use_in_children`, `user_in_preglact`, `mechanism_action`, `drug_interactions`, `number`, `company_name`, `clinical_trial`, `other`, `publish_date`, `prod_id`, `last_modify_time`, `condition`)
-- VALUES ('1', '8a887ea92a44e1bd012a5454b5e9000a', '阿普唑仑片', '阿普唑仑片', 'Alprazolam Tablets', 'Apuzuolun Pian', NULL, 'C17H13ClN4', '308.77', '1－甲基－6－苯基－8－氯－1－H－［1,2,4－三唑［4,3-a ］［1,4］］－苯并二氮杂卓。', '本品为白色片。', NULL, '主要用于焦虑、紧张、激动，也可用于催眠或焦虑的辅助用药，也可作为抗惊恐药，并能缓解急性酒精戒断症状。详见说明书。', NULL, '成人常用量：抗焦虑，开始一次0.4mg，一日3次，用量按需递增。详见说明书。', '1、常见的不良反应，嗜睡、头昏、乏力等，大剂量偶见共济失调、震颤、尿潴留、黄疸。 2、罕见的有皮疹、光敏、白细胞减少。 3、个别病人发生兴奋，多语，睡眠障碍，甚至幻觉。停药后，上述症状很快消失。 4、有成瘾性，长期应用后，停药可能发生撤药症状，表现为激动或忧郁。 5、少数病人有口干、精神不集中、多汗、心悸、便秘或腹泻、视物模糊、低血压。', '1、对苯二氮卓类药物过敏者，可能对本药过敏。2、肝肾功能损害者能延长本药清除半衰期。3、癫痫患者突然停药可导致发作。4、严重的精神抑郁可使病情加重，甚至产生自杀倾向，应采取预防措施。 5、避免长期大量使用而成瘾，如长期使用需停药时不宜骤停，应逐渐减量。 6、出现呼吸抑制或低血压常提示超量。 7、对本类药耐受量小的患者初用量宜小，逐渐增加剂量。 8、高空作业、驾驶员、精细工作、危险工作慎用。', '慎用： 1、中枢神经系统处于抑制状态的急性酒精中毒。 2、肝肾功能损害。 3、重症肌无力。 4、急性或易于发生的闭角型青光眼发作。 5、严重慢性阻塞性肺部病变。 6、驾驶员、高空作业者、危险精细作业者。', NULL, '塑料瓶包装，100片/瓶；铝塑包装，20片/板，40片/小盒。', '出现持续的精神错乱、严重嗜睡、抖动、语言不清、蹒跚、心跳异常减慢、呼吸短促或困难、严重乏力。超量或中毒宜及早对症处理，包括催吐或洗胃以及呼吸循环方面的支持疗法，中毒出现兴奋异常时，不能用巴比妥类药。苯二氮卓受体拮抗剂氟马西尼(flumazenil)可用于该类药物过量中毒的解救和诊断。', '口服吸收快而完全，血浆蛋白结合率约为80%。口服后1～2小时血药浓度达峰值。2～3天血药浓度达稳态。T1/2一般为12～15小时，老年人为19小时。经肝脏代谢，代谢产物a －羟基阿普唑仑，该产物也有一定的药理活性。经肾排泄。体内蓄积量极少，停药后清除快。', '本药对老年人较敏感，开始用小剂量，一次0.2mg，一日3次，逐渐增加至最大耐受量。', '18岁以下儿童，用量尚未有具体规定。', '1、在妊娠三个月内，本药有增加胎儿致畸的危险。 2、孕妇长期服用可引起依赖，使新生儿呈现撤药症状，妊娠后期用药影响新生儿中枢神经活动，分娩前及分娩时用药可导致新生儿肌张力较弱，孕妇应尽量避免使用。 3、本药可以分泌入乳汁，哺乳期妇女应慎用。', '本品为苯二氮卓类催眠镇静药和抗焦虑药。该药作用于中枢神经系统的苯二氮卓受体（BZR），加强中枢抑制性神经递质g－氨基丁酸（GABA）与GABAA受体的结合，促进氯通道开放，使细胞超极化，增强GABA能神经元所介导的突触抑制，使神经元的兴奋性降低。BZR分为I型和II型，据认为I 型受体兴奋可以解释BZ类药物的抗焦虑作用，而II 型受体与该类药物的镇静和骨骼肌松弛等作用有关。可引起中枢神经系统不同部位的抑制，随着用量的加大，临床表现可自轻度的镇静到催眠甚至昏迷。可通过胎盘，可分泌入乳汁。有成瘾性，少数患者可引起过敏。', '1、与中枢抑制药合用可增加呼吸抑制作用。 2、与易成瘾和其他可能成瘾药合用时，成瘾的危险性增加。 3、与酒及全麻药、可乐定、镇痛药、吩噻嗪类 、单胺氧化酶A型抑制药和三环类抗抑郁药合用时，可彼此增效，应调整用量。 4、与抗高血压药和利尿降压药合用，可使降压作用增强。 5、与西咪替丁、普奈洛尔合用本药清除减慢，血浆半衰期延长。 6、与扑米酮合用由于减慢后者代谢，需调整扑米酮的用量。 7、与左旋多巴合用时，可降低后者的疗效。 8、与利福平合用，增加本品的消除，血药浓度降低。 9、异烟肼抑制本品的消除，致血药浓度增高。 10、与地高辛合用，可增加地高辛血药浓度而致中毒。', '国药准字H37021277', '齐鲁制药有限公司', NULL, NULL, NULL, NULL, '2015-03-14 14:50:03', NULL);
SELECT  replace(uuid(),'-','') yjj_id,  `商品名1`, `通用名称`, `英文名`,null as '拼音', `化学名称`, `分子式`, `分子量`
, `成份`, `性状`, `注射剂辅料`, `功能主治/适应症`, `作用类别`, `用法用量`, `不良反应`
, `注意事项`, `禁忌`,规格, `包装`, `药物过量`, `药代动力学`, `老年用药`, `儿童用药`
, `孕妇及哺乳期妇女用药`, `药理毒理`, `药物相互作用`, `批准文号3`, `企业名称`
, `临床试验`, `备注`, `说明书修订日期`,null prod_id,null as  `last_modify_time`,`自己药店货号` as  `condition` 
 -- `储存条件`,`自己药店货号`
from  as_test.`tcxc0122_汇总数据` hz1
-- WHERE not EXISTS(SELECT * from pm_prod_sku as s where hz1.`自己药店货号` = s.pharmacy_huohao and s.drugstore_id in (1600,1601))
;


SELECT


-- SELECT * from yjj_drug_detail LIMIT 100

-- INSERT INTO `medstore`.`pm_prod_info` (`prod_id`, `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`, `prod_synonyms`)
-- VALUES ('100000', '神威 复方三维亚油酸胶丸Ⅰ', '神威', '1', '6938007000652', '国药准字H13023694', '550', '5', '神威药业集团有限公司', NULL, '2018-01-05 15:26:02', '2017-09-11 13:28:26', '2', '100粒', '瓶', '复方三维亚油酸胶丸Ⅰ', '1', '15303', '用于动脉粥样硬化的辅助治疗和预防。', '1', NULL, '【雾霾/防霾】');
-- 这是缺少的prod信息  ，还需要确认一下说明书
 -- `自己药店货号`, `商品类别`, `商品名/品牌名称`, `通用名`, `规格`, `厂商`, `单位`, `商品条码`, `零售价`, `批准文号`, `进口药品注册证号`, `图片1`, `图片2`, `图片3`, `图片4`, `图片5`, `商品名1`, `通用名称`, `英文名`, `化学名称`, `分子式`, `分子量`, `成份`, `性状`, `注射剂辅料`, `功能主治/适应症`, `作用类别`, `用法用量`, `不良反应`, `注意事项`, `禁忌`, `包装`, `药物过量`, `药代动力学`, `老年用药`, `儿童用药`, `孕妇及哺乳期妇女用药`, `药理毒理`, `药物相互作用`, `批准文号3`, `企业名称`, `临床试验`, `储存条件`, `备注`, `说明书修订日期`
SELECT  CONCAT(`商品名/品牌名称`,' ',`通用名`) as  prod_name ,`商品名/品牌名称` as `prod_brand` ,1 as  `prod_status`,`商品条码` as `prod_code` ,`批准文号` as  `prod_sn`
,ROUND(`零售价`*100) `prod_price`,0 as  `prod_level`,`厂商` as `prod_firm`,null as  `prod_logist`,NOW() as  `prod_update_time`,NOW() as  `prod_create_time`, 1 as `prod_type`,`规格` as  `prod_spec`,`单位` as `prod_unit`
 ,`通用名` as `prod_gen_name`, CONCAT('0122 tcxc yp ',`自己药店货号`) as `prod_remark`, 'xxxxx' as `drug_id`,`功能主治/适应症` as `prod_indication`,1 as  `source_id`,null as  `prod_order`
  ,`自己药店货号` as  `prod_synonyms` 
from  as_test.`tcxc0122_汇总数据` hz1
WHERE not EXISTS(SELECT * from pm_prod_sku as s where hz1.`自己药店货号` = s.pharmacy_huohao and s.drugstore_id in (1600,1601))

-- GROUP BY hz1.`自己药店货号`
-- HAVING COUNT(hz1.`自己药店货号`)>1


-- `自己药店货号`, `商品名/品牌名称`, `通用名`, `规格`, `厂商`, `单位`, `零售价`, `级别`, `图片1`, `图片2`, `图片3`, `图片4`, `图片5`, `图片6`, `图片7`, `图片8`) VALUES ('0003521', '', '癣毒净净肤液', '30g', '安徽省德济堂药业有限公司', '盒', '20', '', 'CCC-001', 'CCC-002', 'CCC-003', '', '', '', '', '');
-- INSERT INTO `medstore`.`pm_prod_info` (`prod_id`, `prod_name`, `prod_brand`, `prod_status`, `prod_code`, `prod_sn`, `prod_price`, `prod_level`, `prod_firm`, `prod_logist`, `prod_update_time`, `prod_create_time`, `prod_type`, `prod_spec`, `prod_unit`, `prod_gen_name`, `prod_remark`, `drug_id`, `prod_indication`, `source_id`, `prod_order`, `prod_synonyms`)
-- 器材 
SELECT  CONCAT(`商品名/品牌名称`,' ',`通用名`) as  prod_name ,`商品名/品牌名称` as `prod_brand` 
,1 as  `prod_status`,null as `prod_code` ,null as  `prod_sn`
,ROUND(`零售价`*100) `prod_price`,0 as  `prod_level`,`厂商` as `prod_firm`,null as  `prod_logist`,NOW() as  `prod_update_time`,NOW() as  `prod_create_time`, 1 as `prod_type`,`规格` as  `prod_spec`,`单位` as `prod_unit`
 ,`通用名` as `prod_gen_name`,CONCAT('0122 tcxc qc ',`自己药店货号`) as `prod_remark`, null as `drug_id`,null as `prod_indication`,1 as  `source_id`,null as  `prod_order`
  ,`自己药店货号` as  `prod_synonyms` 
from  as_test.`tcxc0122_汇总2` hz1
WHERE not EXISTS(SELECT * from pm_prod_sku as s where hz1.`自己药店货号` = s.pharmacy_huohao and s.drugstore_id in (1600,1601))




-- 新建sku 1600 QC
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
-- SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
SELECT  i.prod_id,'1' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee
,1600 as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'0122 tcxc 1600 qc' as sku_remark,i.prod_logist,i.`prod_name` ,a.`自己药店货号`  as pharmacy_huohao,'1' as source_id,concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
-- ,a.*
from
as_test.`tcxc0122_汇总2` a LEFT JOIN pm_prod_info i on  a.`自己药店货号`=  i.prod_synonyms  ;

SELECT  * from pm_prod_info ORDER BY  `prod_id`  DESC  LIMIT  172;
SELECT  * from pm_prod_sku ORDER BY  `sku_id`  DESC  LIMIT  272;
-- 新建sku 1601 qc
-- INSERT INTO `medstore`.`pm_prod_sku` ( `prod_id`, `sku_status`, `sku_price`, `sku_fee`, `drugstore_id`, `brand_id`, `sku_update_time`, `sku_create_time`, `sku_remark`, `sku_logistics`, `prod_name`, `pharmacy_huohao`, `source_id`, `sku_json`, `sku_attr`, `sku_img`, `sku_sum`)
-- VALUES ('100000', '100000', '1', '800', '800', '200', '1', '2017-11-02 23:27:48', '2015-03-13 16:20:36', '', NULL, '神威 复方三维亚油酸胶丸I', '0000111', '1', '{\"prod_spec\":\"100粒\"}', NULL, '', '3');
-- SELECT  i.prod_id,'0' as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee,c.ydid as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time,'1220 1元商品' as sku_remark,i.prod_logist,i.prod_name,CONCAT('1YYY',a.`huohao`)  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
SELECT  i.prod_id,1 as sku_status,i.prod_price as sku_price,i.prod_price as sku_fee
,1601 as drugstore_id,'1' as brand_id ,now() as sku_update_time,now() as   sku_create_time
,'0122 tcxc 1601 qc' as sku_remark,i.prod_logist,i.`prod_name` ,a.`自己药店货号`  as pharmacy_huohao,'1',concat('{\"prod_spec\":\"',i.prod_spec,'\"}'),null as sku_attr,'' as sku_img,'' as sku_sum
-- ,a.*
from
as_test.`tcxc0122_汇总2` a LEFT JOIN pm_prod_info i on  a.`自己药店货号`=  i.prod_synonyms  ;



SELECT * from  `medstore`.`pm_image_info`
WHERE img_remark = '0122 tcxc tp'
ORDER BY img_id DESC; 438871

-- 新增图片 都是新的
-- insert into `medstore`.`pm_image_info` ( `img_name`, `img_url`, `img_type`, `img_status`, `img_update_time`, `img_create_time`, `img_remark`, `img_other1`, `img_other2`)
-- values ( '0626ABC-029.jpg', 'http://imgstore.camore.cn/prod_img_ty/tc/0626ABC-029.jpg', 'jpg', '1', '2017-06-26 16:19:39', '2017-06-26 16:19:39', '322010', '1', '20170626’);

-- 查出所有的图片，上面的insert这个数据
SELECT i.prod_id AS img_name,CONCAT('http://imgstore.camore.cn/icon/act/ashacker/180122/',b.图片1,'.jpg'),'jpg',1,now(),NOW(),'0122 tcxc tp','',''
FROM (
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总数据`
UNION ALL
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总2`
) b LEFT JOIN pm_prod_info i on  b.`自己药店货号`=  i.prod_synonyms  

WHERE 图片1 is NOT null  and 图片1 !='' -- ORDER BY  图片1

UNION all

SELECT i.prod_id AS img_name,CONCAT('http://imgstore.camore.cn/icon/act/ashacker/180122/',b.图片2,'.jpg'),'jpg',1,now(),NOW(),'0122 tcxc tp','',''
FROM (
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总数据`
UNION ALL
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总2`
) b LEFT JOIN pm_prod_info i on  b.`自己药店货号`=  i.prod_synonyms  

WHERE 图片2 is NOT null and 图片2 !=''  -- ORDER BY  图片1

UNION all

SELECT i.prod_id AS img_name,CONCAT('http://imgstore.camore.cn/icon/act/ashacker/180122/',b.图片3,'.jpg'),'jpg',1,now(),NOW(),'0122 tcxc tp','',''
FROM (
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总数据`
UNION ALL
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总2`
) b LEFT JOIN pm_prod_info i on  b.`自己药店货号`=  i.prod_synonyms  

WHERE 图片3 is NOT null and 图片3 !=''  -- ORDER BY  图片1

UNION all

SELECT i.prod_id AS img_name,CONCAT('http://imgstore.camore.cn/icon/act/ashacker/180122/',b.图片4,'.jpg'),'jpg',1,now(),NOW(),'0122 tcxc tp','',''
FROM (
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总数据`
UNION ALL
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总2`
) b LEFT JOIN pm_prod_info i on  b.`自己药店货号`=  i.prod_synonyms  

WHERE 图片4 is NOT null and 图片4 !=''  -- ORDER BY  图片1

UNION all
SELECT i.prod_id AS img_name,CONCAT('http://imgstore.camore.cn/icon/act/ashacker/180122/',b.图片5,'.jpg'),'jpg',1,now(),NOW(),'0122 tcxc tp','',''
FROM (
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总数据`
UNION ALL
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,图片1,图片2,图片3,图片4,图片5 from as_test.`tcxc0122_汇总2`
) b LEFT JOIN pm_prod_info i on  b.`自己药店货号`=  i.prod_synonyms  

WHERE 图片5 is NOT null and 图片5 !='' -- ORDER BY  图片1




-- 根据上面的配置图片
-- INSERT INTO `medstore`.`pm_prod_img` ( `prod_id`, `attr_id`, `attr_value_id`, `img_id`, `img_kind`, `img_name`) 
-- VALUES ('9330', '100000', NULL, NULL, '9330', 'prodImage', 'http://imgstore.camore.cn/prod_img_ty/08/DSC_5408.jpg');
SELECT a.img_name as prod_id,
	NULL AS attr_id,
NULL AS attr_value_id,
a.img_id ,
'prodImage' as img_kind,
a.img_url as img_name
  from pm_image_info a 
WHERE img_remark = '0122 tcxc tp';


-- 分类去吧

增加pm_sku_dir 关联 1600

-- insert into `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) -- 1600 新增 1051 条
-- values ( '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');

SELECT   c.`dir_id`,a.sku_id,c.`dir_code`,'1000',now() -- ,a.prod_name,a.pharmacy_huohao ,b.*,c.`dir_name` ,c.`dir_code` ,c.`dir_id`,c.pharmacy_id, c.* -- , c.* -- COUNT( c.`dir_id`) --  1052 -- c.`dir_id`,a.sku_id,c.`dir_code`,'1000',now(),   a.prod_name,a.pharmacy_huohao ,b.*,c.`dir_name` ,c.`dir_code` ,c.`dir_id`,c.pharmacy_id   -- b.prod_id,'1',b.零售价*100,b.零售价*100,'1601',1,now(),now(),'20171121新增商品','',通用名,自己药店货号,'1',CONCAT('{\"prod_spec\":\"',规格,'\"}'), null, '', null -- b.*  -- c.`category_id` ,b.prod_id,c.`category_code` ,'1000' -- b.*,a.`link_id`,c.*  
from
(

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`分类编码` as category_id from as_test.`tcxc0122_科室找药` -- 69

UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码` as category_id from as_test.`tcxc0122_分类搜药`  -- 86
UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_症状找药` -- 31 
UNION ALL
SELECT `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_医疗器械分类` -- 41

) b LEFT JOIN pm_prod_sku a on  b.`自己药店货号`= a.pharmacy_huohao and a.drugstore_id = 1600 LEFT JOIN  pm_dir_info c on concat('ykd21041001',b.category_id) = c.`dir_code`
and c.`pharmacy_id`  = 1600;


SELECT * from pm_dir_info WHERE  dir_code LIKE 'ykd21041001%' and 
`pharmacy_id`  = 1600

SELECT * from pm_dir_info WHERE  dir_code LIKE 'ykd21041002%' and 
`pharmacy_id`  = 1601


增加pm_sku_dir 关联 1601

-- insert into `medstore`.`pm_sku_dir` ( `dir_id`, `sku_id`, `dir_code`, `sku_order`, `update_time`) -- 1600 新增 1051 条
-- values ( '200011', '100227', 'zxyp010104', '1', '2016-11-24 17:01:55');

SELECT   c.`dir_id`,a.sku_id,c.`dir_code`,'1000',now() -- ,a.prod_name,a.pharmacy_huohao ,b.*,c.`dir_name` ,c.`dir_code` ,c.`dir_id`,c.pharmacy_id, c.* -- , c.* -- COUNT( c.`dir_id`) --  1052 -- c.`dir_id`,a.sku_id,c.`dir_code`,'1000',now(),   a.prod_name,a.pharmacy_huohao ,b.*,c.`dir_name` ,c.`dir_code` ,c.`dir_id`,c.pharmacy_id   -- b.prod_id,'1',b.零售价*100,b.零售价*100,'1601',1,now(),now(),'20171121新增商品','',通用名,自己药店货号,'1',CONCAT('{\"prod_spec\":\"',规格,'\"}'), null, '', null -- b.*  -- c.`category_id` ,b.prod_id,c.`category_code` ,'1000' -- b.*,a.`link_id`,c.*  
from
(

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`分类编码` as category_id from as_test.`tcxc0122_科室找药` -- 69

UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码` as category_id from as_test.`tcxc0122_分类搜药`  -- 86
UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_症状找药` -- 31 
UNION ALL
SELECT `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_医疗器械分类` -- 41

) b LEFT JOIN pm_prod_sku a on  b.`自己药店货号`= a.pharmacy_huohao and a.drugstore_id = 1601 LEFT JOIN  pm_dir_info c on concat('ykd21041002',b.category_id) = c.`dir_code`
and c.`pharmacy_id`  = 1601;
-- ,`pm_dir_info` c,`pm_prod_sku`  a -- ,`pm_sku_dir` d
-- WHERE  
--  concat('ykd21041001',b.category_id) = c.`dir_code`
-- concat('ykd21041002',b.category_id) = c.`dir_code`

-- b.category_id = c.`dir_code`
-- and c.`pharmacy_id`  = 1601
-- AND c.`pharmacy_id`  = a.drugstore_id
-- and a.prod_id= b.prod_id

-- and d.`dir_id`  =  c.`dir_id` -- ,a.sku_id,c.`dir_code`
-- and d.`sku_id`  = a.sku_id
-- and not  EXISTS (
--     SELECT e.* from `pm_sku_dir` e
--     WHERE e.`dir_id`  =  c.`dir_id` -- ,a.sku_id,c.`dir_code`
--         and e.`sku_id`  = a.sku_id
-- )

ORDER BY b.category_id



SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`分类编码` as category_id from as_test.`tcxc0122_科室找药`
UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码` as category_id from as_test.`tcxc0122_分类搜药`
UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_症状找药`


SELECT COUNT(update_time),`update_time`  FROM `pm_sku_dir` 
WHERE `update_time` >'2018-01-24 00:00:00'
and `sku_order`  = 1000
GROUP BY `update_time` 
HAVING COUNT(`update_time`) >1

SELECT   b.category_id, c.`dir_id`,a.sku_id,c.`dir_code`,'1000',now() , a.*,b.*,c.* --  ,a.prod_name,a.pharmacy_huohao ,b.*,c.`dir_name` ,c.`dir_code` ,c.`dir_id`,c.pharmacy_id, c.* -- , c.* -- COUNT( c.`dir_id`) --  1052 -- c.`dir_id`,a.sku_id,c.`dir_code`,'1000',now(),   a.prod_name,a.pharmacy_huohao ,b.*,c.`dir_name` ,c.`dir_code` ,c.`dir_id`,c.pharmacy_id   -- b.prod_id,'1',b.零售价*100,b.零售价*100,'1601',1,now(),now(),'20171121新增商品','',通用名,自己药店货号,'1',CONCAT('{\"prod_spec\":\"',规格,'\"}'), null, '', null -- b.*  -- c.`category_id` ,b.prod_id,c.`category_code` ,'1000' -- b.*,a.`link_id`,c.*  
from
(

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`分类编码` as category_id from as_test.`tcxc0122_科室找药` -- 69

UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码` as category_id from as_test.`tcxc0122_分类搜药`  -- 86
UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_症状找药` -- 31 
UNION ALL
SELECT `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_医疗器械分类` -- 41

) b LEFT JOIN pm_prod_sku a on  b.`自己药店货号`= a.pharmacy_huohao and a.drugstore_id = 1600 LEFT JOIN  pm_dir_info c on concat('ykd21041001',b.category_id) = c.`dir_code`
and c.`pharmacy_id`  = 1600
-- WHERE  c.`dir_code` is  null

-- UPDATE 
-- SELECT * from

-- UPDATE as_test.`tcxc0122_分类搜药` set `目录编码` = 'zxypother'
WHERE `目录编码` like '%zxypother%'

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码` as category_id 
from as_test.`tcxc0122_分类搜药` WHERE 目录编码 like '%zxypother%'
/*
SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`分类编码` as category_id from as_test.`tcxc0122_科室找药` -- 69

UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码` as category_id from as_test.`tcxc0122_分类搜药`  -- 86
UNION ALL

SELECT  `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_症状找药` -- 31 
UNION ALL
SELECT `商品名/品牌名称`,通用名,`自己药店货号`,`目录编码`  as category_id from as_test.`tcxc0122_医疗器械分类` -- 41
*/
) aa
 set category_id = 'zxypother'
WHERE category_id like '%zxypother%'
-- ****************************************************************


SELECT * from pm_prod_sku LIMIT 100

SELECT * FROM as_test.`tcxc0122_科室找药` a ,as_test.`tcxc0122_分类搜药` b 
WHERE a.`自己药店货号` = b.`自己药店货号`;
SELECT * FROM pm_prod_info LIMIT 100
SELECT * 
FROM as_test.`tcxc0122_科室找药` as a 
LEFT JOIN pm_prod_sku as b 
on a.`自己药店货号` = b.pharmacy_huohao
WHERE b.drugstore_id in (1600,1601);

SELECT * from pm_prod_info LIMIT 100


SELECT CONCAT(b.图片1,'.jpg') imgname,CONCAT('http://ys1000.oss-cn-beijing.aliyuncs.com/icon/act/ashacker/171109/all/',b.图片1,'.jpg'),'jpg',1,now(),NOW(),'20171121新增商品','',''
FROM (
SELECT 通用名,prod_id,category_id,图片1,图片2,图片3,图片4,图片5 from as_test.tcxc0122_科室找药  -- 254
  -- 187
) b WHERE 图片1 is NOT null  -- ORDER BY  图片1
